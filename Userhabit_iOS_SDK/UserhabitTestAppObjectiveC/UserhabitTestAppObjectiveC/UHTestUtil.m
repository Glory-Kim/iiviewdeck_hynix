//
//  UHTestUtil.m
//  UserhabitTestAppObjectiveC
//
//  Created by r on 2017. 4. 3..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import "UHTestUtil.h"

@implementation UHTestUtil

+ (UIColor *) randomColor{
    float red = arc4random() %255+1;
    float green = arc4random() %255+1;
    float blue = arc4random() %255+1;
    float alpha = arc4random() %255+1;
    
    red = red / 255.0;
    green = green / 255.0;
    blue = blue / 255.0;
    alpha = alpha / 255.0;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

@end
