//
//  FirstViewController.m
//  UserhabitTestAppObjectiveC
//
//  Created by Jinuk Baek on 7/27/16.
//  Copyright © 2016 Andbut Co. All rights reserved.
//

#import "FirstViewController.h"
#import <AdSupport/ASIdentifierManager.h>
@import UserHabit;

#import "UHTestUtil.h"

@interface FirstViewController ()

@end

@implementation FirstViewController


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    NSLog(@"FirstViewController screen start");
    
//    self.tabBarController.selectedIndex = 3;
//    [UserHabit se˜tContentKey:@"key" value:@"value"];
#if UseUSERHABIT
//    [[UserHabit sharedInstance] setScreen:self withName:@"asdf"];
#endif
//    debug::
//    [self.tabBarController setSelectedIndex:4];
//    [self loadUserHabit];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    /*
     예: 가나다라123abc
     예: ここに来ます。
     예: 엄청 긴 문자열(1024문자 이상)
     이 경우 100자 이상은 자를 것
     예: كانت أولى أفلامه الروائية تتبع
     예: !@#$%^&*(":'[]/\,. (특수문자)
     */
    NSString *string = @"11";
    for (int i = 0; i <= 50; i++) {
        string = [string stringByAppendingString:@"i"];
        
    }
    
//    for (int i = 0; i<=1000; i ++) {
//
//        [UserHabit setContentKey:@"글자 긴 문자열을 테스트 해봅시다 야호호호 호호호 [[UserHabit sharedInstance] setScreen:self withName:string]" value:[NSString stringWithFormat:@"%d_닭튀김 또는 프라이드 치킨은 주로 밀가루로 튀긴 닭고기 조각 요리이다. 대한민국에서는 단순히 치킨이라고 부르기도 한다. 일반적으로 닭고기에 튀김옷을 입혀 고온의 기름에 튀긴 것을 뜻하며, 미국 남부에서 지금과 같은 형태의 닭튀김으로 정착하여, 정식 명칭은 남부 프라이드 치킨이다. 또한, 닭을 통째로 구워서 먹는 것도 있다.", i]];
//    }
#if UseUSERHABIT
//    [[UserHabit sharedInstance] setScreen:self withName:string];
#if !UseUSERHABIT_AUTO_TRACKING
//    [[UserHabit sharedInstance]setScreen:self withName:@"first view controller"];
#endif
#endif
}
- (IBAction)buttonAction:(UIButton *)sender {
    sender.backgroundColor = [UHTestUtil randomColor];
}

- (void)orientationChanged:(NSNotification *)notification{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    //do stuff
    NSLog(@"orientationChanged: %ld", orientation);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self // put here the view controller which has to be notified
                                             selector:@selector(orientationChanged:)
                                                 name:@"UIDeviceOrientationDidChangeNotification"
                                               object:nil];
    
    
    self.view.backgroundColor = [UHTestUtil randomColor];
    
    NSLog(@"accessibilityIdentifier %@", [self.view accessibilityIdentifier]);
#if UseUSERHABIT
//    [UserHabit takeScreenShot:self];
#endif
    
    /*
     * 뷰 생성 과부하 테스트
     
    for (int i = 0 ; i <= 200; i ++) {
        int x = arc4random_uniform(self.view.frame.size.width);
        int y = arc4random_uniform(self.view.frame.size.height);
        int width = arc4random_uniform(x);
        int height = arc4random_uniform(y);
        
        
        
        UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TestSecretViewController"];
        
        viewController.view.frame = CGRectMake(x, y, width,height);
        [self.view addSubview:viewController.view];

    }
    */
    
    
    NSLog(@"%@", [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString]);
    NSString *idfaString = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    
    NSLog(@"%@", idfaString);
    
    sideMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SideMenuViewController"];
    if (sideMenuViewController) {
        sideMenuViewController.view.frame = CGRectMake(self.view.frame.size.width, 0, 200, self.view.frame.size.height);
        sideMenuViewController.view.backgroundColor = [UIColor orangeColor];
        [self.view addSubview:sideMenuViewController.view];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - ibaction

- (IBAction)sideMenuButtonAction:(id)sender {
//    sideMenuViewController
//    UIButton *button = (UIButton *)sender;
    [UIView animateWithDuration:0.2f animations:^{
//        if (button.selected) {
//            sideMenuViewController.view.center = CGPointMake(sideMenuViewController.view.center.x + self.view.frame.size.width / 2, sideMenuViewController.view.center.y);
//        } else {
        self->sideMenuViewController.view.center = CGPointMake(self->sideMenuViewController.view.center.x - self.view.frame.size.width / 2, self->sideMenuViewController.view.center.y);
//        }
//        sideMenuViewController.view.center = CGPointMake(0, 0);
//        button.selected = !button.selected;
    } completion:^(BOOL finished) {
        [self->sideMenuViewController dataLoad];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(loadUserHabit) name:@"loadFirstViewController" object:nil];
    }];
}

-(void)loadUserHabit{
    [UserHabit setScreen:self withName:@"FirstViewController"];
}

- (IBAction)swiftSchemeButtonAction:(id)sender {
    NSLog(@"open other app");
    
    NSURL *url = [NSURL URLWithString:@"uhswift://"];
//    NSURL *url = [NSURL URLWithString:@"brich://order.complete?no=123"];
    
    [UIApplication.sharedApplication openURL:url];

//    [[UserHabit sharedInstance] sessionCloseWithUploadData:YES completeHandler:^{
//        NSLog(@"force exit");
//        exit(0);
//    }];
}
- (IBAction)takeScreenShotButttonAction:(UIButton *)sender {
//    sender.accessibilityIdentifier = @"야호호호 호호야 ";
    NSLog(@"%@", sender.accessibilityIdentifier);
    
//    [[UserHabit sharedInstance]takeScreenShot:self];

}

- (IBAction)textButtonAction:(id)sender {
    static int number = 6;
    
    UIButton *button = (UIButton *)sender;
//    button.accessibilityIdentifier
    NSString *screenTitle = [NSString stringWithFormat:@"button %d", number + 1];
//    [UserHabit setScreen:self withName:screenTitle];
    [button setTitle:screenTitle forState:UIControlStateNormal];
    number++;
    NSLog(@"야호");
    [UserHabit takeScreenShot:self];
}


- (IBAction)occurCrash1:(id)sender {
    //int res = 5/0;

    NSArray *array = [[NSArray alloc] init];
    
    NSLog(@"res == >> %@", [array objectAtIndex:100]);

    
//    @try {
//        NSArray *array = [[NSArray alloc] init];
//        
//        NSLog(@"res == >> %@", [array objectAtIndex:100]);
//    } @catch (NSException *exception) {
//        NSLog(@"%@", exception.name);
//        NSLog(@"%@", exception.reason);
//        NSLog(@"%@", exception.userInfo);
//        NSLog(@"%@", exception.callStackReturnAddresses);
//        NSLog(@"%@", exception.callStackSymbols);
//        
//    }
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    
    
    [UserHabit secretMode:YES];
    NSLog(@"touch!!!!");
    static int i =0;
    i++;
#if UseUSERHABIT
    [UserHabit setContentKey:@"key" value:[NSString stringWithFormat:@"%d", i]];
#endif
    
    /*
     cpu사용율 100% 이상 사용되는 코드
     */
//    [NSTimer scheduledTimerWithTimeInterval:0.00001 repeats:YES block:^(NSTimer * _Nonnull timer) {
//        int q;
//        q = 0.001+0.000002;
//        q = q/0.087831f;
//        [self.view setNeedsLayout];
//    }];
    
}
@end
