//
//  SideMenuViewController.m
//  ObjC
//
//  Created by lotco on 2018. 1. 22..
//  Copyright © 2018년 Andbut Co. All rights reserved.
//

#import "SideMenuViewController.h"
@import UserHabit;


@interface SideMenuViewController ()

@end

@implementation SideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dataLoad{
    [UserHabit setScreen:self withName:@"side menu"];
}

#pragma mark - ibaction
- (IBAction)sideMenuButtonAction:(id)sender {
    [UIView animateWithDuration:0.2f animations:^{
        self.view.center = CGPointMake(self.view.center.x + self.view.superview.frame.size.width / 2, self.view.center.y);
    } completion:^(BOOL finished) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"loadFirstViewController" object:nil];
    }];
}


@end
