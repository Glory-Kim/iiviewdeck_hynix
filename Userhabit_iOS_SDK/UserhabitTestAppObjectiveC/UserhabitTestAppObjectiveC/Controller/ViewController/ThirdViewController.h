//
//  ThirdViewController.h
//  UserhabitTestAppObjectiveC
//
//  Created by Jinuk Baek on 10/12/2016.
//  Copyright © 2016 Andbut Co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThirdViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>
@end
