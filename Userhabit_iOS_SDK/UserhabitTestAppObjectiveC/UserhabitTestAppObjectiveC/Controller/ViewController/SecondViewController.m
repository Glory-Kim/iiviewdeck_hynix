//
//  SecondViewController.m
//  UserhabitTestAppObjectiveC
//
//  Created by Jinuk Baek on 7/27/16.
//  Copyright © 2016 Andbut Co. All rights reserved.
//

#import "SecondViewController.h"
#import "UHTestUtil.h"
@import UserHabit;

@implementation SecondViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
#if UseUSERHABIT
    //    [[UserHabit sharedInstance] setScreen:self withName:string];
#if !UseUSERHABIT_AUTO_TRACKING
    [[UserHabit sharedInstance]setScreen:self withName:@"second view controller"];
//    [[UserHabit sharedInstance] setUserInfo:@"second userinfo" userType:0 gender:UHUserInfoGenderUncategorized age:99];
//    NSString *string = [NSString new];
//    int end = 100000;
//    for (int i= 0 ; i <= end; i ++) {
////        string = [string stringByAppendingString:[NSString stringWithFormat:@"%3d", i]];
//        string = [string stringByAppendingString:@"i"];
//        if(i==end){
//            string = [string stringByAppendingString:[NSString stringWithFormat:@"%d",i]];
//        }
//            
//    }
    [[UserHabit sharedInstance] setUserKey:UHUserInformationName value:@"user name user name user name user name user name user name user name user name user name user name "];
//    [[UserHabit sharedInstance] setContent:@"long text" with:string];
//    NSLog(@"%@", string);
#endif
#endif
}
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"SecondViewController screen start");
    self.view.backgroundColor = [UHTestUtil randomColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)occurCrash2:(id)sender {
    if (NSGetUncaughtExceptionHandler()) {
        NSLog(@"UncaughtExceptionHandler was setupped");
    }
    
    @throw NSRangeException;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    NSLog(@"touch");
}
@end
