//
//  ThirdViewController.m
//  UserhabitTestAppObjectiveC
//
//  Created by Jinuk Baek on 10/12/2016.
//  Copyright © 2016 Andbut Co. All rights reserved.
//

#import <UserHabit/UserHabit.h>
#import "ThirdViewController.h"
#import "UHTestUtil.h"
#define RLog_ViewCycle NSLog( @"!!!!!!!!! %s line ( %d )", __FUNCTION__ , __LINE__)

@interface ThirdViewController ()

@property (weak, nonatomic) IBOutlet UITableView *mainTapView;
@property (strong, nonatomic) NSMutableArray *arrayList;

@end

@implementation ThirdViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
#if UseUSERHABIT
#if !UseUSERHABIT_AUTO_TRACKING
    [[UserHabit sharedInstance]setScreen:self withName:@"third view controller"];
#endif
    [UserHabit addScrollView:@"third scroll view" scrollView:_mainTapView rootViewController:self];
//    [UserHabit addScrollView:@"dd third scroll view" scrollView:_mainTapView rootViewController:self];
#endif

}
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"ThirdViewController screen start");
    // Do any additional setup after loading the view.
    
    self.arrayList = [NSMutableArray arrayWithCapacity:1000];
    
    for (NSUInteger i = 0; i<380; i++) {
//        [self.arrayList addObject:[NSString stringWithFormat:@"%d >>> %d", (int)i, arc4random()%500]];
        [self.arrayList addObject:[NSString stringWithFormat:@"%d", (int)i]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickAddBtn:(id)sender {
    for (NSUInteger i = 0; i<200; i++) {
        [self.arrayList addObject:[NSString stringWithFormat:@"%d >>> %d", (int)i, arc4random()%500]];
    }
    [self.mainTapView reloadData];
}


- (IBAction)clickClearBtn:(id)sender {
    [self.arrayList removeAllObjects];
    [self.mainTapView reloadData];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.arrayList) {
        return self.arrayList.count;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.textLabel.text = self.arrayList[indexPath.row];
    cell.backgroundColor = [UHTestUtil randomColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    NSString *nowText = self.arrayList[indexPath.row];
#if UseUSERHABIT
    [UserHabit setContentKey:@"click" value:nowText];
#endif
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Title"
//                                                   message:[NSString stringWithFormat:@"You tapped %@", nowText]
//                                                  delegate:self
//                                         cancelButtonTitle:nil
//                                         otherButtonTitles:@"YES", nil];

    // alert창을 띄우는 method는 show이다.
//    [alert show];
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    int h = rand() % 50 + 40;
//    return h;
//}
/*
#pragma mark - scroll view delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    RLog_ViewCycle;
}
- (void)scrollViewDidZoom:(UIScrollView *)scrollView{
    RLog_ViewCycle;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    RLog_ViewCycle;
}
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    RLog_ViewCycle;
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    RLog_ViewCycle;
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    RLog_ViewCycle;
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    RLog_ViewCycle;
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    RLog_ViewCycle;
}

//- (nullable UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView;     // return a view that will be scaled. if delegate returns nil, nothing happens
- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(nullable UIView *)view {
    RLog_ViewCycle;
}
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(nullable UIView *)view atScale:(CGFloat)scale{
    RLog_ViewCycle;
}

//- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView{
//    RLog_ViewCycle;
//}
- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView{
    RLog_ViewCycle;
}
#pragma mark - table view delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    RLog_ViewCycle;
}
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section NS_AVAILABLE_IOS(6_0){
    RLog_ViewCycle;
}
- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section NS_AVAILABLE_IOS(6_0){
    RLog_ViewCycle;
}
- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath{
    RLog_ViewCycle;
}
- (void)tableView:(UITableView *)tableView didEndDisplayingHeaderView:(UIView *)view forSection:(NSInteger)section {
    RLog_ViewCycle;
}
- (void)tableView:(UITableView *)tableView didEndDisplayingFooterView:(UIView *)view forSection:(NSInteger)section{
    RLog_ViewCycle;
}
 */

@end
