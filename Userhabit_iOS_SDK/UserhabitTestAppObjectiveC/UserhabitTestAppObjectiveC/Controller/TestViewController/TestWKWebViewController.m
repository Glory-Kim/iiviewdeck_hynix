//
//  TestWKWebViewController.m
//  ObjC
//
//  Created by lotco on 2018. 5. 17..
//  Copyright © 2018년 Andbut Co. All rights reserved.
//
@import UserHabit;

#import "TestWKWebViewController.h"
#define RLog( s, ... ) NSLog( @"%s(%d) --> %@", __FUNCTION__ , __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
@interface TestWKWebViewController ()

@end

@implementation TestWKWebViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    RLog(@"");
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    RLog(@"");
    
    
    
//    [[wkWebView.configuration userContentController] addScriptMessageHandler:self name:@"UserHabitMessageHandler"];
    
    
    
    
    
    
    
//    [UserHabit addWebView:wkWebView];
//    [UserHabit addWebView:wkWebView];
//    [UserHabit addWebView:wkWebView];
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self layoutSetting];
    RLog(@"");
}

-(void)layoutSetting{
    
    
    
//    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc]init];
//
//
//
//    WKUserContentController *jsctrl = [[WKUserContentController alloc]init];
//    [jsctrl addScriptMessageHandler:self name:@"callbackHandler"];
//    [config setUserContentController:jsctrl];

//    WKUserScript *cookie_script = [[WKUserScript alloc]initWithSource:@"alert('load!')"
//                                                        injectionTime:WKUserScriptInjectionTimeAtDocumentStart
//                                                     forMainFrameOnly:NO];
//    [jsctrl addUserScript:cookie_script];
    
    
//    [wkWebView.configuration setUserContentController:jsctrl];
    
    
    RLog(@"뇽");
    wkWebView = [WKWebView new];
//    wkWebView = [[WKWebView alloc] initWithFrame:webAreaView.bounds configuration:config];
    
    [[wkWebView.configuration userContentController] addScriptMessageHandler:self name:@"callbackHandler"];
    
    RLog(@"육");
    
    
//    wkWebView = [WKWebView new];
    wkWebView.frame = webAreaView.bounds;
    [webAreaView addSubview:wkWebView];
    wkWebView.UIDelegate = self;
    wkWebView.navigationDelegate = self;
    
    

//    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://localhost:3000"]];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://userhabit.io"]];
//    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://10.50.101.254:3000"]];
//    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://192.168.1.58:3000"]];
//    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://127.0.0.1:3000"]];
//    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://google.com"]];
//    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://poc.vpay.co.kr/poc/jsp/view/mobile/main/main.jsp"]];
//    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://www.w3schools.com/html/tryit.asp?filename=tryhtml5_canvas_empty"]];

    [wkWebView loadRequest:request];
    [[wkWebView.configuration userContentController] addScriptMessageHandler:self name:@"UserHabitMessageHandler"];

    //todo::추가할때 웹뷰 그대로 삭제해서 에러남
//    [UserHabit addWebView:@"뇽뇽뇽"];
//    [UserHabit addWebView:self.view];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (nullable WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures{
    RLog(@"%@", webView);
    return webView;
}
- (void)webViewDidClose:(WKWebView *)webView {
    RLog(@"%@", webView);
}

- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler{

    
    
    
    RLog(@"%@", message);
    
    RLog(@"%@", webView);
    
    completionHandler();
}

- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL result))completionHandler{
    RLog(@"%@", webView);
}

- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(nullable NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * _Nullable result))completionHandler{
    RLog(@"%@", webView);
}

- (BOOL)webView:(WKWebView *)webView shouldPreviewElement:(WKPreviewElementInfo *)elementInfo{
    RLog(@"%@", webView);
    return YES;
    
}

- (nullable UIViewController *)webView:(WKWebView *)webView previewingViewControllerForElement:(WKPreviewElementInfo *)elementInfo defaultActions:(NSArray<id <WKPreviewActionItem>> *)previewActions{
    return [webView.UIDelegate webView:webView previewingViewControllerForElement:elementInfo defaultActions:previewActions];

}
- (void)webView:(WKWebView *)webView commitPreviewingViewController:(UIViewController *)previewingViewController{
    RLog(@"%@", webView);
}

- (void)webView:(WKWebView *)webView runOpenPanelWithParameters:(WKOpenPanelParameters *)parameters initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSArray<NSURL *> * _Nullable URLs))completionHandler{
    RLog(@"%@", webView);
}


//------navigation
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    
    decisionHandler(WKNavigationActionPolicyAllow);
    RLog(@"%@ / %@", webView, navigationAction.request.URL);
}
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler{
    decisionHandler(WKNavigationResponsePolicyAllow);
    RLog(@"%@", webView);
}
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(null_unspecified WKNavigation *)navigation{
    RLog(@"%@", webView);
}
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(null_unspecified WKNavigation *)navigation{
    RLog(@"%@", webView);
}
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    RLog(@"%@", webView);
}
- (void)webView:(WKWebView *)webView didCommitNavigation:(null_unspecified WKNavigation *)navigation{
    RLog(@"%@", webView);
}
- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation{
    RLog(@"%@", webView);

    
    [UserHabit addWebView:wkWebView];
//    [UserHabit addScrollView:@"wkwebview" scrollView:wkWebView.scrollView rootViewController:self];

}
- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    RLog(@"%@", webView);
}

//- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * _Nullable credential))completionHandler{
//    completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, );
////    RLog(@"%@", webView);
//}

- (void)webViewWebContentProcessDidTerminate:(WKWebView *)webView API_AVAILABLE(macosx(10.11), ios(9.0)){
    RLog(@"%@", webView);
}

#pragma mark - message
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message{
    RLog(@"%@ / %@", userContentController, message);
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    RLog(@"%@", [wkWebView.configuration userContentController].userScripts);
//    RLog(@"%@", [wkWebView.configuration userContentController].)
//    [UserHabit addWebView:wkWebView];
    
//    WKUserScript *webViewPlatformSetting = [[WKUserScript alloc]initWithSource:@"userhabitSetPlatform(22);" injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
//    [[wkWebView.configuration userContentController] addUserScript:webViewPlatformSetting];
//
//    webViewPlatformSetting = [[WKUserScript alloc]initWithSource:@"alert('왜 안되니');" injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
//    [[wkWebView.configuration userContentController] addUserScript:webViewPlatformSetting];
//
//    [wkWebView evaluateJavaScript:@"userhabitSetPlatform(22); userhabitSetWebViewName('엉엉싫다');" completionHandler:^(id _Nullable data, NSError * _Nullable error) {
//        RLog(@"com");
//    }];
    
    
//    [wkWebView evaluateJavaScript:@"alert('asdasdasd2222')" completionHandler:^(id _Nullable data, NSError * _Nullable error) {
//        RLog(@"com");
//    }];
    
    
    
    
    
}
@end
