//
//  TestSecretViewController.h
//  UserhabitTestAppObjectiveC
//
//  Created by r on 2017. 4. 12..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestSecretViewController : UIViewController{
    
    __weak IBOutlet UIView *touchAreaView;
    __weak IBOutlet UIView *secretAreaView;
    __weak IBOutlet UIView *numberAreaView;
    __weak IBOutlet UILabel *numberLabel;
}

@end
