//
//  TestMultipleWebViewController.h
//  ObjC
//
//  Created by lotco on 2018. 6. 22..
//  Copyright © 2018년 Andbut Co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface TestMultipleWebViewController : UIViewController<WKUIDelegate, WKNavigationDelegate>{
    WKWebView *wkWebView2;
    WKWebView *wkWebView3;
    
    __weak IBOutlet UIView *webViewArea1;
    __weak IBOutlet UIView *webViewArea2;
}

@end
