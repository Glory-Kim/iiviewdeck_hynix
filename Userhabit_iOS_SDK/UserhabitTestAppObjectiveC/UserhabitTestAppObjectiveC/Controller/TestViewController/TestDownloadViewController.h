//
//  TestDownloadViewController.h
//  UserhabitTestAppObjectiveC
//
//  Created by r on 2017. 4. 18..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestDownloadViewController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource>{
    
    __weak IBOutlet UIView *InformationAreaView;
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UIProgressView *downloadProgressView;
    __weak IBOutlet UIView *collectionViewAreaView;
    
    __weak IBOutlet UICollectionView *imageCollectionView;
    NSArray *downloadUrlArray;
    NSMutableArray *imageUrlArray;
}

@end
