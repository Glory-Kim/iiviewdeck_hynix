//
//  TestMultipleWebViewController.m
//  ObjC
//
//  Created by lotco on 2018. 6. 22..
//  Copyright © 2018년 Andbut Co. All rights reserved.
//

#import "TestMultipleWebViewController.h"

@import UserHabit;

@implementation TestMultipleWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"zxczxcxz  %@", NSStringFromCGRect(webViewArea1.frame));
    wkWebView2 = [WKWebView new];
    
    [webViewArea1 addSubview:wkWebView2];
    wkWebView2.UIDelegate = self;
    wkWebView2.navigationDelegate = self;
    
    wkWebView3 = [WKWebView new];
    
    [webViewArea2 addSubview:wkWebView3];
    wkWebView3.UIDelegate = self;
    wkWebView3.navigationDelegate = self;
    
    //                 @"https://poc.vpay.co.kr/poc/jsp/view/mobile/login/pocLogin.jsp",
    //                 @"https://poc-test.vpay.co.kr/poc/jsp/view/mobile/login/pocLoginNew.jsp",
    //                 @"https://poc.vpay.co.kr/poc/jsp/view/mobile/menu/pwdChgGate.jsp",
    //                 @"https://poc.vpay.co.kr/poc/jsp/view/mobile/main/allMenu.jsp",
    //                 @"https://poc.vpay.co.kr/poc/jsp/view/mobile/main/main.jsp"

    
    
//    NSURLRequest *request1 = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://poc.vpay.co.kr/poc/jsp/view/mobile/login/pocLogin.jsp"]];
//    NSURLRequest *request1 = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://userhabit.io"]];
    NSURLRequest *request2 = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://poc-test.vpay.co.kr/poc/jsp/view/mobile/login/pocLoginNew.jsp"]];
//    NSURLRequest *request2 = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://userhabit.io"]];
    NSURLRequest *request3 = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://poc.vpay.co.kr/poc/jsp/view/mobile/menu/pwdChgGate.jsp"]];
//    NSURLRequest *request3 = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://userhabit.io"]];
//    NSURLRequest *request2 = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://google.com"]];
//    NSURLRequest *request3 = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://developer.mozilla.org/ko/docs/Web/HTML/Canvas/Tutorial/Drawing_shapes"]];
//    NSURLRequest *request4 = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://poc.vpay.co.kr/poc/jsp/view/mobile/main/allMenu.jsp"]];
    
    [wkWebView2 loadRequest:request2];
    [wkWebView3 loadRequest:request3];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [UserHabit setScreen:self withName:@"치킨이 고프다"];
    
    [UserHabit addWebView:wkWebView2];
    [UserHabit addWebView:wkWebView3];
    
    [UserHabit addScrollView:@"s2" scrollView:wkWebView2.scrollView rootViewController:self];
    [UserHabit addScrollView:@"s3" scrollView:wkWebView3.scrollView rootViewController:self];
}

-(void)viewDidLayoutSubviews{
    wkWebView2.frame = CGRectMake(0, webViewArea1.frame.size.height /2, webViewArea1.frame.size.width, webViewArea1.frame.size.height /2);
    wkWebView3.frame = CGRectMake(0, 0, webViewArea2.frame.size.width, webViewArea2.frame.size.height /2);
}
@end
