//
//  TestButtonViewController.h
//  ObjC
//
//  Created by lotco on 2017. 9. 4..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestButtonViewController : UIViewController{
    
    __weak IBOutlet UIView *firstButtonArea;
    __weak IBOutlet UIButton *codeButton;
}

@end
