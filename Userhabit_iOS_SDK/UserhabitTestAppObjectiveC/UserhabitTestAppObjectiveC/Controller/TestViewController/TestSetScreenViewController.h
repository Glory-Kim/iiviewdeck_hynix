//
//  TestSetScreenViewController.h
//  UserhabitTestAppObjectiveC
//
//  Created by r on 2017. 6. 19..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@interface TestSetScreenViewController : UIViewController<iCarouselDelegate, iCarouselDataSource>

@end
