//
//  TestScrollSampleViewController.m
//  UserhabitTestAppObjectiveC
//
//  Created by r on 2017. 6. 20..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import "TestScrollSampleViewController.h"
#import "UHTestUtil.h"
@import UserHabit;

@implementation TestScrollSampleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
#if UseUSERHABIT
#if UseUSERHABIT_AUTO_TRACKING
//    [[UserHabit sharedInstance] takeScreenShot:self];
#else
    [[UserHabit sharedInstance]setScreen:self withName:@"test collection view controller"];
#endif
#endif

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - tableview datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 100;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell * cell;
    
    static NSString *identifier = @"Cell";
    
    cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    [cell.textLabel setText:[NSString stringWithFormat:@"%ld",indexPath.row ]];
    return cell;
}
#pragma mark - collection view datasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 5;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 100;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * const reuseIdentifier = @"Cell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    cell.backgroundColor = [UHTestUtil randomColor];
    // Configure the cell
    
    return cell;
}

@end
