//
//  TestDownloadViewController.m
//  UserhabitTestAppObjectiveC
//
//  Created by r on 2017. 4. 18..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import "TestDownloadViewController.h"
#import "UHTestUtil.h"
#import "TestNetworkController.h"
@import UserHabit;

@implementation TestDownloadViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
#if UseUSERHABIT
#if !UseUSERHABIT_AUTO_TRACKING
    [[UserHabit sharedInstance]setScreen:self withName:@"test download view controller"];
#endif
#endif

}
- (void)viewDidLoad {
    [super viewDidLoad];
    collectionViewAreaView.backgroundColor = [UHTestUtil randomColor];
    InformationAreaView.backgroundColor = [UHTestUtil randomColor];
    self.automaticallyAdjustsScrollViewInsets = NO;

    downloadUrlArray = @[@"http://download.thinkbroadband.com/1GB.zip",
                         @"http://download.thinkbroadband.com/512MB.zip",
                         @"http://download.thinkbroadband.com/200MB.zip",
                         @"http://download.thinkbroadband.com/100MB.zip",
                         @"http://download.thinkbroadband.com/50MB.zip",
                         @"http://download.thinkbroadband.com/20MB.zip",
                         @"http://download.thinkbroadband.com/10MB.zip",
                         @"http://download.thinkbroadband.com/5MB.zip"];
    
    if ([TESTNetworkManager.manager.downloadTasks count] >= 1){
        NSURLSessionDownloadTask *downloadTask = TESTNetworkManager.manager.downloadTasks[0];
        NSProgress *downloadProgress = [TESTNetworkManager.manager downloadProgressForTask:downloadTask];
        downloadProgressView.observedProgress = downloadProgress;
    }
    
    imageUrlArray = [NSMutableArray new];
    /*
     *다음 이미지 검색
     *https://developers.daum.net/services/apis/search/image
     *pageno 검색쿼리 페이지 넘버
     */
    
    AFHTTPSessionManager *httpManager = [AFHTTPSessionManager manager];
    [httpManager GET:@"https://apis.daum.net/search/image?apikey=64759f41cfed502e81a802a6910c953d&result=20&q=%EA%B3%A0%EC%96%91%EC%9D%B4&output=json"
          parameters:nil
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 for (id inObject in responseObject[@"channel"][@"item"]) {
//                     NSLog(@"%@", inObject[@"image"]);
                     [self->imageUrlArray addObject:inObject[@"image"]];
                 }
                 [self->imageCollectionView reloadData];
    } failure:nil];
    
}

- (IBAction)downloadButtonAction:(id)sender {
    if ([TESTNetworkManager.manager.downloadTasks count] == 0){
        int rand = arc4random() % [downloadUrlArray count];
        NSURL *URL = [NSURL URLWithString:downloadUrlArray[rand]];
        NSURLRequest *request = [NSURLRequest requestWithURL:URL];
        
        NSURLSessionDownloadTask *downloadTask;
        downloadTask= [TESTNetworkManager.manager downloadTaskWithRequest:request
                                                                 progress: ^(NSProgress *_Nonnull downloadProgress){
                                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                                         float total = (float)(downloadProgress.totalUnitCount / 1024 / 1024);
                                                                         float current = (float)((float)downloadProgress.completedUnitCount / 1024.0 / 1024.0);
                                                                         self->titleLabel.text = [NSString stringWithFormat:@"%.2f/%.2f",current, total ];
                                                                     });
                                                                     self->downloadProgressView.observedProgress = downloadProgress;
                                                                 }
                                                              destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
                                                                  NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
                                                                  return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
                                                              }
                                                        completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
                                                            NSLog(@"File downloaded to: %@", filePath);
                                                        }];
        [downloadTask resume];
    } else {
        titleLabel.text = @"진행 중인 다운로드가 있습니다";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [imageUrlArray count];
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
//    UIImageView *imageView = [cell viewWithTag:101];
//    [imageView setImageWithURL:[NSURL URLWithString:imageUrlArray[indexPath.row]] placeholderImage:[UIImage imageNamed:@"logo_40"]];
    cell.backgroundColor = [UHTestUtil randomColor];

    return cell;
}

@end
