//
//  TestButtonViewController.m
//  ObjC
//
//  Created by lotco on 2017. 9. 4..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import "TestButtonViewController.h"
#import "UHTestUtil.h"

@import UserHabit;

@implementation TestButtonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [codeButton addTarget:self action:@selector(changeRandomColor) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [UserHabit takeScreenShot:self];
}

- (void)changeRandomColor{
    firstButtonArea.backgroundColor = [UHTestUtil randomColor];
}
#pragma mark - ibaction

- (IBAction)xibActionButtonAction:(id)sender {
    [self changeRandomColor];
}

- (IBAction)sessionCloseButtonAction:(id)sender {
    NSLog(@"session start");
    [UserHabit sessionCloseWithUploadData:YES completeHandler:^{
        NSLog(@"session close");
        exit(0);
    }];
}

- (IBAction)nowSessionCloseButtonAction:(id)sender {
    NSLog(@"now session close");
    [UserHabit sessionCloseWithUploadData:NO completeHandler:^{
        NSLog(@"now session close");
        exit(0);
    }];
}

@end
