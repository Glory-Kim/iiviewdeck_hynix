//
//  TestActionSheetViewController.m
//  ObjC
//
//  Created by lotco on 2018. 9. 28..
//  Copyright © 2018년 Andbut Co. All rights reserved.
//

#import "TestActionSheetViewController.h"
@import UserHabit;

@implementation TestActionSheetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionButtonAction:(id)sender {
    NSLog(@"???");
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"My Alert"
                                                                   message:@"This is an alert."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [UserHabit setScreen:self withName:@"TestActionSheetViewController"];
                                                          }];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    [UserHabit setScreen:self withName:@"경고창"];
}

-(void)tapClicked:(UIGestureRecognizer *)gestureRecognizer {
    
    CGPoint fr = [gestureRecognizer locationInView:self.view];
    NSLog(@"제발!!! %@",NSStringFromCGPoint(fr));
}


- (IBAction)actionSheetButtonAction:(id)sender {
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"My Alert"
                                                                   message:@"This is an alert."
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"다리" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [UserHabit setScreen:self withName:@"TestActionSheetViewController"];
                                                          }];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
    [UserHabit setScreen:self withName:@"치킨 액션 야호"];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"clickedButtonAtIndex - %@ / %ld", actionSheet, buttonIndex);
    NSLog(@"%@", actionSheet.superview);
    NSLog(@"%@", actionSheet.gestureRecognizers);
    
//    NSLog(@"%@", [UIApplication sharedApplication].windows);
    
//    for (UIWindow *window in [UIApplication sharedApplication].windows) {
//        for (UIGestureRecognizer *gesture in window.gestureRecognizers) {
//            NSLog(@"%@ / %@", window, gesture);
//        }
//    }
    
}
- (void)actionSheetCancel:(UIActionSheet *)actionSheet{
    NSLog(@"actionSheetCancel: - %@", actionSheet);
}

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet{
    NSLog(@"willPresentActionSheet: - %@", actionSheet);
}

- (void)didPresentActionSheet:(UIActionSheet *)actionSheet{
    NSLog(@"didPresentActionSheet: - %@", actionSheet);
}

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex{
    NSLog(@"willDismissWithButtonIndex: - %@ / %ld", actionSheet, buttonIndex);
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    NSLog(@"didDismissWithButtonIndex: - %@ / %ld", actionSheet, buttonIndex);
}


#pragma mark - gesture delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    NSLog(@"gestureRecognizerShouldBegin");
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    NSLog(@"shouldRecognizeSimultaneouslyWithGestureRecognizer");
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    NSLog(@"shouldRequireFailureOfGestureRecognizer");
    return YES;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    NSLog(@"shouldBeRequiredToFailByGestureRecognizer");
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    NSLog(@"shouldReceiveTouch");
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceivePress:(UIPress *)press{
    NSLog(@"shouldReceivePress");
    return YES;
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    NSLog(@"나와라");
}
@end
