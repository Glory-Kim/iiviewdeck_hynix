//
//  TestSecretViewController.m
//  UserhabitTestAppObjectiveC
//
//  Created by r on 2017. 4. 12..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import "TestSecretViewController.h"
#import "UHTestUtil.h"
@import UserHabit;

@implementation TestSecretViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
#if UseUSERHABIT
#if !UseUSERHABIT_AUTO_TRACKING
    [UserHabit setScreen:self withName:@"test secret view controller"];
#endif
    [UserHabit addSecretView:secretAreaView];
#endif

}
- (void)viewDidLoad {
    [super viewDidLoad];
    touchAreaView.backgroundColor = [UHTestUtil randomColor];
    secretAreaView.backgroundColor = [UHTestUtil randomColor];
    numberAreaView.backgroundColor = [UHTestUtil randomColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)testNumberButtonAction:(UIButton *)sender {
    [numberLabel setText:sender.titleLabel.text];
#if UseUSERHABIT
    [UserHabit setContentKey:sender.titleLabel.text value:@"button number"];
#endif
}

@end
