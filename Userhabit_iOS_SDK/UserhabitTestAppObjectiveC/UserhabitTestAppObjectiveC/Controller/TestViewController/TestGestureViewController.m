//
//  TestGestureViewController.m
//  UserhabitTestAppObjectiveC
//
//  Created by r on 2017. 6. 21..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import "TestGestureViewController.h"
#import "AFTestObject.h"

@import UserHabit;

@implementation TestGestureViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
#if UseUSERHABIT
#if UseUSERHABIT_AUTO_TRACKING
    //    [[UserHabit sharedInstance] takeScreenShot:self];
#else
    [[UserHabit sharedInstance]setScreen:self withName:@"test gesture view controller"];
#endif
#endif

}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panAction:)];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(panAction:)];
    [self.view addGestureRecognizer:panGesture];
    [self.view addGestureRecognizer:tapGesture];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)panAction:(UIGestureRecognizer *)panGesture{
    checkView.center = [panGesture locationInView:self.view];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    AFTestObject *object = [AFTestObject sharedInstance];
    
    
    for (int i = 0; i <= 100 ; i++) {
        [object testApi:[NSString stringWithFormat:@"%d", i]];
        [AFTestObject testSetContents:[NSString stringWithFormat:@"%d", i]];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
