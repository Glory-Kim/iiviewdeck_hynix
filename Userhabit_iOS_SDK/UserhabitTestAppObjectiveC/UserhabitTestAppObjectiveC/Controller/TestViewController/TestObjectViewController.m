//
//  TestObjectViewController.m
//  UserhabitTestAppObjectiveC
//
//  Created by lotco on 2017. 8. 8..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import "TestObjectViewController.h"
@import UserHabit;

@implementation TestObjectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    testSegmentedControl.accessibilityIdentifier = @"소스 파일에서 지정한 이름";
    
    UIButton *testButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [testButton addTarget:self action:@selector(testButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)testButtonAction:(id)sender {
    testLabel.text = @"touched test button!";
}

- (IBAction)takeScreenshot:(id)sender {
    [UserHabit takeScreenShot:self];
}

@end
