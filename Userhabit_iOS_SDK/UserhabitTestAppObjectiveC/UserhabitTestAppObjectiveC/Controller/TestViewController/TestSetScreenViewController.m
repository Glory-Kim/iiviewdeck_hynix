//
//  TestSetScreenViewController.m
//  UserhabitTestAppObjectiveC
//
//  Created by r on 2017. 6. 19..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import "TestSetScreenViewController.h"
#import "UHTestUtil.h"
@import UserHabit;

@implementation TestSetScreenViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
#if UseUSERHABIT
#if !UseUSERHABIT_AUTO_TRACKING
    [[UserHabit sharedInstance]setScreen:self withName:@"test setScreen controller"];
#endif
#endif

}
- (void)viewDidLoad {
    [super viewDidLoad];
    iCarousel *carousel = [[iCarousel alloc]initWithFrame:CGRectMake(50, 50, 300, 400)];
    carousel.dataSource = self;
    carousel.delegate = self;
    carousel.backgroundColor = [UHTestUtil randomColor];
    carousel.type = iCarouselTypeCoverFlow2;
    carousel.pagingEnabled = YES;
    [self.view addSubview:carousel];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - icarouosel datasource
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return 5;
}
- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    UILabel *label;
    UIButton *button;
    //create new view if no view is available for recycling
    if (view == nil)
    {
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0f, 200.0f)];
        view.contentMode = UIViewContentModeCenter;
        
        label = [[UILabel alloc] initWithFrame:view.bounds];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [label.font fontWithSize:50];
        label.tag = 1;
        
        button = [UIButton buttonWithType:UIButtonTypeSystem];
        [button setBackgroundColor:[UHTestUtil randomColor]];
        button.frame = CGRectMake(10, 10, 100, 100);
        button.tag = 2;
        
        [view addSubview:label];
        [view addSubview:button];
    }
    else
    {
        label = (UILabel *)[view viewWithTag:1];
        button = [view viewWithTag:2];
    }
    
    label.text = [NSString stringWithFormat:@"%ld", index];
    [button setTitle:[NSString stringWithFormat:@"%ld", index] forState:UIControlStateNormal];
    
    
    view.backgroundColor = [UHTestUtil randomColor];
    
    return view;
}
#pragma mark - icarousel delegate
- (void)carouselDidEndScrollingAnimation:(iCarousel *)carousel{
#if UseUSERHABIT
    static BOOL isFirstLoad = YES;
    if (isFirstLoad) {
        isFirstLoad = NO;
    } else {
        [UserHabit setScreen:self withName:[NSString stringWithFormat:@"carousel %ld", carousel.currentItemIndex]];
    }
#endif
}
@end
