//
//  TestCustomCellTableViewController.m
//  UserhabitTestAppObjectiveC
//
//  Created by r on 2017. 6. 22..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import "TestCustomCellTableViewController.h"
#import "TestCustomTableViewCell.h"
#import "UHTestUtil.h"
@import UserHabit;

@implementation TestCustomCellTableViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
#if UseUSERHABIT
#if UseUSERHABIT_AUTO_TRACKING

#else
    [[UserHabit sharedInstance]setScreen:self withName:@"test custom cell view controller"];
#endif
#endif

}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
#if UseUSERHABIT
//    [UserHabit takeScreenShot:self];
#endif
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 100;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TestCustomTableViewCell * cell;
    
    static NSString *identifier = @"TestCustomTableViewCell";
    
    cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[TestCustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        [cell setButtonActionBlock:^(NSError *error){
            [UserHabit setScreen:self withName:[NSString stringWithFormat:@"custom cell - %ld",indexPath.row]];
        }];
        
    }else {
    }
    
    [cell setButtonActionBlock:^(NSError *error){
#if UseUSERHABIT
        [UserHabit setScreen:self withName:[NSString stringWithFormat:@"custom cell - %ld",indexPath.row]];
#endif
    }];
    cell.backgroundColor = [UHTestUtil randomColor];
    cell.mainTextLabel.text = [NSString stringWithFormat:@"%ld",indexPath.row];
    
//    [cell.textLabel setText:[NSString stringWithFormat:@"%ld",indexPath.row ]];
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
