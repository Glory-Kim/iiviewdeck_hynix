//
//  TestWKWebViewController.h
//  ObjC
//
//  Created by lotco on 2018. 5. 17..
//  Copyright © 2018년 Andbut Co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
@interface TestWKWebViewController : UIViewController<WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler>{
    WKWebView *wkWebView;
    __weak IBOutlet UIView *webAreaView;
}

@end
