//
//  TestTableViewController.m
//  
//
//  Created by r on 2017. 4. 11..
//
//

#import "TestTableViewController.h"
#import "UHTestUtil.h"
@import UserHabit;

@implementation TestTableViewController
- (void)awakeFromNib{
    [super awakeFromNib];
    dataArray = @[@"TestMapViewController",
                  @"TestSecretViewController",
                  @"TestActionSheetViewController",
                  @"TestWKWebViewController",
                  @"TestSetScreenViewController",
                  @"TestScrollSampleViewController",
                  @"TestCollectionViewController",
                  @"TestGestureViewController",
                  @"TestCustomCellTableViewController",
                  @"TestObjectViewController",
                  @"TestSlowViewController",
                  @"TestButtonViewController",
                  @"TestMultipleWebViewController",
                  @"crash cell"];
    
    
    
}



-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [UserHabit setScreen:self withName:@"수동 스크롤뷰 테스트"];
    
#if UseUSERHABIT
#if !UseUSERHABIT_AUTO_TRACKING
    [[UserHabit sharedInstance]setScreen:self withName:@"test table view controller"];
#endif
    [UserHabit addScrollView:@"test scroll table view" scrollView:(UIScrollView *)self.view rootViewController:self];
#endif
    
    [UserHabit secretMode:NO];

}


- (void)viewDidLoad {
    [super viewDidLoad];
        NSLog(@"TestTableViewController screen start");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [dataArray count] * 10;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    int h = rand() % 50 + 40;
//    return h;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell * cell;

    static NSString *identifier = @"Cell";
    
    cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
//    NSLog(@"%ld / %ld", indexPath.row, [dataArray count]);
    if (indexPath.row < [dataArray count]) {
        [cell.textLabel setText:dataArray[indexPath.row]];
    } else {
        [cell.textLabel setText:[NSString stringWithFormat:@"%ld", indexPath.row ]];
    }
    cell.backgroundColor = [UHTestUtil randomColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *textCell = [tableView cellForRowAtIndexPath:indexPath];
    [UserHabit setContentKey:@"UH-CONTENTS" value:[NSString stringWithFormat:@"{content-name:""%@""}", textCell.textLabel.text]];
     
    
    if (indexPath.row < [dataArray count]) {
        UIViewController *pushViewController = [self.storyboard instantiateViewControllerWithIdentifier:dataArray[indexPath.row]];
        [self.navigationController pushViewController:pushViewController animated:YES];
        
        UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessibilityIdentifier = cell.textLabel.text;
    } else {
        NSLog(@"cell touch");
        UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
//        cell.accessibilityIdentifier = @"야호야호 test cell!!!! ";
        cell.accessibilityIdentifier = cell.textLabel.text;
        
        
        
//        UIViewController *pushViewController = [self.storyboard instantiateViewControllerWithIdentifier:dataArray[indexPath.row / 2]];
//        [self.navigationController presentViewController:pushViewController animated:YES completion:nil];
    }
}

@end
