//
//  TestMapViewController.h
//  ObjC
//
//  Created by lotco on 2020/05/07.
//  Copyright © 2020 Andbut Co. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TestMapViewController : UIViewController{
    
    __weak IBOutlet UITextField *mapNameTextField;
    __weak IBOutlet UILabel *xLabel;
    __weak IBOutlet UISlider *xSlider;
    __weak IBOutlet UILabel *yLabel;
    __weak IBOutlet UISlider *ySlider;
    __weak IBOutlet UILabel *displayTextLabel;
    __weak IBOutlet UIButton *inputButton;
}

@end

NS_ASSUME_NONNULL_END
