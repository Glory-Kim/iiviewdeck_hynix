//
//  TestObjectViewController.h
//  UserhabitTestAppObjectiveC
//
//  Created by lotco on 2017. 8. 8..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestObjectViewController : UIViewController{
    
    __weak IBOutlet UISegmentedControl *testSegmentedControl;
    __weak IBOutlet UILabel *testLabel;
}

@end
