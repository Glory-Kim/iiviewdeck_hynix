//
//  TestMapViewController.m
//  ObjC
//
//  Created by lotco on 2020/05/07.
//  Copyright © 2020 Andbut Co. All rights reserved.
//

#import "TestMapViewController.h"
@import UserHabit;

@interface TestMapViewController ()

@end

@implementation TestMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (IBAction)ySliderValueChange:(UISlider *)sender {
    yLabel.text = [NSString stringWithFormat:@"%f", sender.value];
}
- (IBAction)xSliderValueChange:(UISlider *)sender {
    xLabel.text = [NSString stringWithFormat:@"%f", sender.value];
}
- (IBAction)inputButtonAction:(id)sender {
    
    NSString *text = [NSString stringWithFormat:@"{map-name:'%@',x:%d,y:%d}",mapNameTextField.text, [@(xSlider.value) intValue], [@(ySlider.value) intValue]];
    
    [UserHabit setContentKey:@"UH-MAP-POSITION" value:text];
    displayTextLabel.text = text;
//    [displayTextLabel autoresizingMask
    
}


@end
