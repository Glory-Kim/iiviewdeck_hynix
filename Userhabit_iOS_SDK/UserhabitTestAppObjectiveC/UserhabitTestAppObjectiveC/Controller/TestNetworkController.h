//
//  TestNetworkController.h
//  UserhabitTestAppObjectiveC
//
//  Created by r on 2017. 4. 18..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

#define TESTNetworkManager [TestNetworkController sharedInstance]
@interface TestNetworkController : NSObject

@property AFURLSessionManager *manager;

+(instancetype)sharedInstance;
@end
