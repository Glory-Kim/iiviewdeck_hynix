//
//  AFTestObject.h
//  ObjC
//
//  Created by lotco on 21/02/2019.
//  Copyright © 2019 Andbut Co. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AFNetworking.h"



@interface AFTestObject : NSObject

@property AFURLSessionManager *manager;
+(instancetype)sharedInstance;
-(void)testApi:(NSString *)string;
-(void)testSetContents:(NSString *)string;
+(void)testSetContents:(NSString *)string;
@end
