//
//  AFTestObject.m
//  ObjC
//
//  Created by lotco on 21/02/2019.
//  Copyright © 2019 Andbut Co. All rights reserved.
//

#import "AFTestObject.h"
@import UserHabit;

@implementation AFTestObject


+(instancetype)sharedInstance
{
    static AFTestObject *singletonObject;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singletonObject = [[AFTestObject alloc]init];
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        singletonObject.manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    });
    
    return singletonObject;
}



-(void)testApi:(NSString *)string{

    NSLog(@"start");
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [UserHabit setContentKey:@"start" value:string];
    [manager GET:@"https://jsonplaceholder.typicode.com/todos/1" parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@", responseObject);
        [self testSetContents:string];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
        [self testSetContents:string];
    }];
    
    
}

-(void)testSetContents:(NSString *)string{
    NSLog(@"---");
    [UserHabit setContentKey:@"---" value:string];
}

+(void)testSetContents:(NSString *)string{
    NSLog(@"+++");
    [UserHabit setContentKey:@"+++" value:string];
    
}
@end
