//
//  TestNetworkController.m
//  UserhabitTestAppObjectiveC
//
//  Created by r on 2017. 4. 18..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import "TestNetworkController.h"

@implementation TestNetworkController

+(instancetype)sharedInstance
{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
        [_sharedObject setting];
    });
    return _sharedObject;
}

-(void)setting{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    _manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
}
@end
