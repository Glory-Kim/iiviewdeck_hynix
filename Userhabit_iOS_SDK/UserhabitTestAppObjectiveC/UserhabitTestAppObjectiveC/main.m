//
//  main.m
//  UserhabitTestAppObjectiveC
//
//  Created by Jinuk Baek on 7/27/16.
//  Copyright © 2016 Andbut Co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
