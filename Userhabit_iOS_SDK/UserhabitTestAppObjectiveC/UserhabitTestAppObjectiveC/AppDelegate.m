//
//  AppDelegate.m
//  UserhabitTestAppObjectiveC
//
//  Created by Jinuk Baek on 7/27/16.
//  Copyright © 2016 Andbut Co. All rights reserved.
//

#import "AppDelegate.h"

@import UserHabit;

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken{
    NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
    //Format token as you need:
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    NSLog(@"deviceToken: %@", token);
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    NSLog(@"User Info : %@",notification.request.content.userInfo);
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}

//Called to let your app know which action was selected by the user for a given notification.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler{
    NSLog(@"User Info : %@",response.notification.request.content.userInfo);
    completionHandler();
}
- (void)registerForRemoteNotifications {
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){

        if(!error){
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            });
            
        }
    }];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self registerForRemoteNotifications];
    // Override point for customization after application launch.
#if UseUSERHABIT
    /*
     * 실제 서버 키 dev_b3068e50a8afca37a2909990f9b8c0f7efbe2168  https://dashboard.userhabit.io/apps/2050/developers
     * 서비스 서버 base64 dev_598a6b499f21da59abaafca203bcd58bbdfd5385 https://dashboard.userhabit.io/apps/2058/developers
     
     *          dev_1c8f4b71d1152dfcd69d96bebc9a84a5f50ae40d
     
     * 스테이징    dev_23c308d6fa4b36c8bf7379ef4a420d0dc119d5a2  http://staging.userhabit.io/apps/3
     *           dev_22eef4bfed674439cd5623f7ea026bbab0ab11e9 http://staging.userhabit.io/apps/39/developers
     *           dev_5a7c53aa11842951b05ad6a40c481b11fbbb7ed4   test scroll
     *           dev_6908f83cb92ff7dada33a79f0d5f9965b2ab311d   test scroll 2
     *          dev_a53b1b420f4d53c61c5400667710bcc71f5ddc77    test scroll 3
     *          dev_300b25c623b0eae5ad00c6de40d01db7fd3ca007    test scroll 4
     *          dev_f39a34eb5bdfaa2af3a0d68129a13109518d71f7    test scroll 5
     *          dev_c4231dd7dea013ae9a58161f9bb25881d4575e90    lasTest
     *          dev_6d95243a3351c16e0c0cefeccde2dad1d4e77de1        143
     */
    [UserHabit setUserKey:UHUserInformationName value:@"이름"];
    [UserHabit setUserKey:UHUserInformationGender value:@1000];
    [UserHabit setUserKey:UHUserInformationType value:@1000];
    [UserHabit setUserKey:UHUserInformationAge value:@1000];
    
    [UserHabit setUserhabitOption:@{UHOptionScreenshotTapticFeedback    : @YES,
                                    UHOptionScreenTime                  : @.5f,
                                    UHOptionTrackingException           : @NO
                                    }];
    
    [UserHabit setDebug:YES];
#if UseUSERHABIT_AUTO_TRACKING
    
    [UserHabit sessionStart:@"dev_b3068e50a8afca37a2909990f9b8c0f7efbe2168" withAutoTracking:YES];
    //dev_35b39e91e597813ce0aca6fedcf1f49fee4caad0  하이닉스
//    [UserHabit sessionStart:@"dev_35b39e91e597813ce0aca6fedcf1f49fee4caad0" withAutoTracking:YES];
    
//    6d95243a3351c16e0c0cefeccde2dad1d4e77de1
//    dev_1c936b64c246e7662e83aac92c0ae159247c7482  unity test api key
//    dev_b3068e50a8afca37a2909990f9b8c0f7efbe2168
#else
    [[UserHabit sessionStart:@"dev_23c308d6fa4b36c8bf7379ef4a420d0dc119d5a2" withAutoTracking:NO];
//    [[UserHabit setUserInfo:@"userinfo" userType:0 gender:UHUserInfoGenderUnuse age:99];
    [[UserHabit setUserKey:UHUserInformationName value:@"아아아 이름 이름 :! / @ 어어"];
    [[UserHabit setUserKey:UHUserInformationGender value:@(UHUserInfoGenderFemale)];
    
#endif
    [UserHabit setSessionDelayTime:2];
//     NSArray *excludeArray = @[@"SideMenuViewController",
//                               @"FirstViewController"];
     NSArray *excludeArray = @[@"HSIntroViewController",
                               @"HSTodayNewsViewController",
                               @"HSDiscoveryCheckPFilt…",
                               @"HSMainContainerViewContro…",
                               @"HSFirstFavoriteIssueSelectViewController"];
     
     [UserHabit excludeClasses:excludeArray];
#endif
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(nonnull NSURL *)url options:(nonnull NSDictionary<NSString *,id> *)options {
    NSLog(@"url = %@", url.absoluteString);
    NSLog(@"url = %@", url);
    NSLog(@"options = %@", options);
//    if ([[UserHabitchainDeeplink:url]) {
//        return YES;
//    }
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
