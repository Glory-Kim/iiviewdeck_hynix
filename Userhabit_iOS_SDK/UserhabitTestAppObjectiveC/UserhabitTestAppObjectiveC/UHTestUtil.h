//
//  UHTestUtil.h
//  UserhabitTestAppObjectiveC
//
//  Created by r on 2017. 4. 3..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UHTestUtil : NSObject
+ (UIColor *) randomColor;
@end
