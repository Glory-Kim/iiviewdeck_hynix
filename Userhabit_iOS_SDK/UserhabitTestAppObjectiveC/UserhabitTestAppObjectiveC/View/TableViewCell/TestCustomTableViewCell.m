//
//  TestCustomTableViewCell.m
//  UserhabitTestAppObjectiveC
//
//  Created by r on 2017. 6. 23..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import "TestCustomTableViewCell.h"
@import UserHabit;
@implementation TestCustomTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
- (IBAction)cellButtonAction:(id)sender {
    NSLog(@"touch! cell");
    mainButtonAction(nil);
}
-(void)setButtonActionBlock:(RActionBlock)action{
    mainButtonAction = action;
}

@end
