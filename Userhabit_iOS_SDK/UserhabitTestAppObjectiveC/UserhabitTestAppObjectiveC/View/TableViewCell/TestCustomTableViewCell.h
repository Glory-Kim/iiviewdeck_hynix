//
//  TestCustomTableViewCell.h
//  UserhabitTestAppObjectiveC
//
//  Created by r on 2017. 6. 23..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^RActionBlock)(NSError *error);
@interface TestCustomTableViewCell : UITableViewCell{
    RActionBlock mainButtonAction;
}
@property (weak, nonatomic) IBOutlet UILabel *mainTextLabel;
-(void)setButtonActionBlock:(RActionBlock)action;
@end
