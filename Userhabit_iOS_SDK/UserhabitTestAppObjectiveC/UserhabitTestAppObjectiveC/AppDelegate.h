//
//  AppDelegate.h
//  UserhabitTestAppObjectiveC
//
//  Created by Jinuk Baek on 7/27/16.
//  Copyright © 2016 Andbut Co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

