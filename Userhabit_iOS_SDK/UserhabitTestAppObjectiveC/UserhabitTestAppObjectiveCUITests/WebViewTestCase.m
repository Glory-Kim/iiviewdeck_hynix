//
//  WebViewTestCase.m
//  UserhabitTestAppObjectiveC
//
//  Created by r on 2017. 4. 14..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface WebViewTestCase : XCTestCase

@end

@implementation WebViewTestCase

- (void)setUp {
    [super setUp];
    
    self.continueAfterFailure = NO;
    [[[XCUIApplication alloc] init] launch];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testExample {
    XCUIApplication *app = [[XCUIApplication alloc] init];
    [app.tabBars.buttons[@"Item"] tap];

    XCUIElement *testwebviewcontrollerStaticText = app.tables.staticTexts[@"TestWebViewController"];
    [testwebviewcontrollerStaticText tap];
    XCUIElement *backButton = [[[app.navigationBars[@"TestWebView"] childrenMatchingType:XCUIElementTypeButton] matchingIdentifier:@"Back"] elementBoundByIndex:0];
    [backButton tap];

    
    XCTestExpectation *expectation = [[XCTestExpectation alloc]initWithDescription:@"testing...."];
    XCTWaiter *waiter = [[XCTWaiter alloc]initWithDelegate:self];
    
    int testCount = 30;
    __block int i = 0;
    [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
        i ++;
        if (i == testCount) {
            [timer invalidate];
            timer = nil;
            
            [XCUIDevice.sharedDevice pressButton:1];
            NSLog(@"test complete and wait 12 seconds to close session");
            sleep(5);
            NSLog(@"close sessino. test all complete");
            [expectation fulfill];
            
        } else {
            NSLog(@"=====================================      count %d     ========================", i);
            
            XCUIApplication *app = [[XCUIApplication alloc] init];
            XCUIElement *testwebviewcontrollerStaticText = app.tables.staticTexts[@"TestWebViewController"];
            [testwebviewcontrollerStaticText tap];
            
            XCUIElement *element = [[[[[[[[[app childrenMatchingType:XCUIElementTypeWindow] elementBoundByIndex:0] childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element;
            [element tap];
            [element swipeLeft];
            [element swipeUp];
            [element swipeDown];
            [element swipeUp];
            [element swipeDown];
            
            XCUIElement *backButton = [[[app.navigationBars[@"TestWebView"] childrenMatchingType:XCUIElementTypeButton] matchingIdentifier:@"Back"] elementBoundByIndex:0];
            [backButton tap];
        }
    }];
    
    [waiter waitForExpectations:@[expectation] timeout:60*60*10];
}

@end
