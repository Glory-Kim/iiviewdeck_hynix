//
//  ScrollViewTestCase.m
//  UserhabitTestAppObjectiveC
//
//  Created by lotco on 2017. 12. 4..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface ScrollViewTestCase : XCTestCase

@end

@implementation ScrollViewTestCase

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    [[[XCUIApplication alloc] init] launch];

    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // Use recording to get started writing UI tests.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
    [app.tabBars.buttons[@"Third"] tap];
    XCUIElementQuery *tablesQuery = app.tables;
    
    XCTestExpectation *exception = [[XCTestExpectation alloc]initWithDescription:@"testing...."];
    XCTWaiter *waiter = [[XCTWaiter alloc]initWithDelegate:self];
    
    int testCount = 5;
    __block int i = 5;
    [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
//        i ++;
        if (i == testCount) {
            [timer invalidate];
//            [self testScrollView];
            [exception fulfill];
        } else {
            @try {

                NSString *cell = [NSString stringWithFormat:@"%d >>>", i];
                [tablesQuery.staticTexts[cell] swipeUp];
                [tablesQuery.staticTexts[cell] tap];
                    
                i = i + 5;

            } @catch (NSException *exception) {
                NSLog(@"??");
            }
            NSLog(@"=============================== %d ===============================", i);
        }
        
    }];
    
    [waiter waitForExpectations:@[exception] timeout:60*60*10];
    
}


- (void)testScrollView {
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
    [app.tabBars.buttons[@"Item"] tap];
    XCUIElementQuery *tablesQuery = app.tables;
    
    XCTestExpectation *exception = [[XCTestExpectation alloc]initWithDescription:@"testing...."];
    XCTWaiter *waiter = [[XCTWaiter alloc]initWithDelegate:self];
    
    int testCount = 20;
    __block int i = 0;
    [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
        //        i ++;
        if (i == testCount) {
            [XCUIDevice.sharedDevice pressButton:1];
            NSLog(@"test 111complete and wait 12 seconds to close session");
            sleep(5);
            NSLog(@"close 111sessino. test all complete");
            [exception fulfill];
            [timer invalidate];
        } else {
            @try {
                NSLog(@"테스트?? %d", i);
                NSString *cell = [NSString stringWithFormat:@"%d", i];
                [tablesQuery.staticTexts[cell] swipeUp];
                [tablesQuery.staticTexts[cell] tap];
                
                i = i + 2;
                
            } @catch (NSException *exception) {
                NSLog(@"??");
            }
            NSLog(@"=============================== %d ===============================", i);
        }
        
    }];
    
    [waiter waitForExpectations:@[exception] timeout:60*60*10];

    
}

@end
