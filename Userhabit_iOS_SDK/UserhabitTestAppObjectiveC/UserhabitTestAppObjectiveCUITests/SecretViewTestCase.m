//
//  SecretViewTestCase.m
//  UserhabitTestAppObjectiveC
//
//  Created by r on 2017. 4. 12..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface SecretViewTestCase : XCTestCase

@end

@implementation SecretViewTestCase

- (void)setUp {
    [super setUp];
    self.continueAfterFailure = NO;
    [[[XCUIApplication alloc] init] launch];
}

- (void)tearDown {
    [super tearDown];
    NSLog(@"tear down");
}

- (void)testExample {
    XCUIApplication *app = [[XCUIApplication alloc] init];
    [app.tabBars.buttons[@"Item"] tap];
    
    XCTestExpectation *exception = [[XCTestExpectation alloc]initWithDescription:@"testing...."];
    XCTWaiter *waiter = [[XCTWaiter alloc]initWithDelegate:self];
    
    int testCount = 40;
    __block int i = 0;
    [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
        i ++;
        if (i == testCount) {
            [XCUIDevice.sharedDevice pressButton:1];
            NSLog(@"test complete and wait 12 seconds to close session");
            sleep(5);
            NSLog(@"close sessino. test all complete");
            [exception fulfill];
        } else {
            @try {
                [app.tables.staticTexts[@"TestSecretViewController"] tap];
                XCUIElement *element = [[[[[[[[[[[app childrenMatchingType:XCUIElementTypeWindow] elementBoundByIndex:0] childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther] elementBoundByIndex:0];
                [element doubleTap];
                [element tap];
                [element pressForDuration:4.0f];
                [app.sliders[@"50%"] pressForDuration:2.4];
                
                
                for (int i = 0; i < 3; i ++) {
                    NSString *number = [NSString stringWithFormat:@"%d", arc4random_uniform(9)];
                    [app.buttons[number] tap];
                    [app.buttons[number] doubleTap];
                    [app.buttons[number] swipeUp];
                }
                [element tap];
                [[[[app.navigationBars[@"TestSecretView"] childrenMatchingType:XCUIElementTypeButton] matchingIdentifier:@"Back"] elementBoundByIndex:0] tap];
            } @catch (NSException *exception) {
                NSLog(@"??");
            }
            NSLog(@"=============================== %d ===============================", i);
        }
        
    }];
    
    [waiter waitForExpectations:@[exception] timeout:60*60*10];
    
}

@end
