//
//  GestureTestCase.m
//  UserhabitTestAppObjectiveC
//
//  Created by r on 2017. 5. 22..
//  Copyright © 2017년 Andbut Co. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface GestureTestCase : XCTestCase

@end

@implementation GestureTestCase

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    [[[XCUIApplication alloc] init] launch];

    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    XCTestExpectation *expectation = [[XCTestExpectation alloc]initWithDescription:@"testing...."];
    XCTWaiter *waiter = [[XCTWaiter alloc]initWithDelegate:self];
    
    int testCount = 40;
    __block int i = 0;
    [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
        i ++;
        if (i == testCount) {
            [timer invalidate];
            timer = nil;
            
            [XCUIDevice.sharedDevice pressButton:1];
            NSLog(@"test complete and wait 12 seconds to close session");
            sleep(5);
            NSLog(@"close sessino. test all complete");
            [expectation fulfill];
            
        } else {
            XCUIApplication *app = [[XCUIApplication alloc] init];
            XCUIElement *button1Button = app.buttons[@"Button 1"];
            [button1Button tap];
            [button1Button doubleTap];
            [button1Button pressForDuration:3.0f];
            [button1Button swipeDown];
            [button1Button swipeRight];
            [button1Button swipeUp];
            [button1Button swipeLeft];
            
            XCUIElement *button2Button = app.buttons[@"Button 2"];
            [button2Button tap];
            [button2Button doubleTap];
            [button2Button pressForDuration:3.0f];
            
            [button1Button tap];
            [button2Button tap];
            [button2Button tap];
            [button1Button tap];
            [button1Button tap];
            [button2Button tap];
            [button1Button tap];
            [button2Button tap];
            
            XCUIElement *element = [[[[[[app childrenMatchingType:XCUIElementTypeWindow] elementBoundByIndex:0] childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element;
            [element tap];
            [element tap];
            [element doubleTap];
            [element pressForDuration:3.0f];
            [element swipeDown];
            [element swipeRight];
            [element swipeUp];
            [element swipeLeft];
sleep(15);
        }
    }];
    [waiter waitForExpectations:@[expectation] timeout:60*60*10];
}

@end
