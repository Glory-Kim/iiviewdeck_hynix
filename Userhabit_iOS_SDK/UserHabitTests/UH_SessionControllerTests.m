//
//  UH_SessionControllerTests.m
//  UserHabit
//
//  Created by Jinuk Baek on 04/02/2017.
//  Copyright © 2017 Andbut. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UH_SessionController.h"

@interface UH_SessionControllerTests : XCTestCase

@end

@implementation UH_SessionControllerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.

}


- (void)testRemoveUnusingData {

    NSString *userhabitPath = [UH_SessionController sharedInstance].userhabitPath;

    NSLog(@"%@", userhabitPath);
    
    // clear UserHabit Directory
    [NSFileManager.defaultManager removeItemAtPath:userhabitPath error:nil];
    [NSFileManager.defaultManager createDirectoryAtPath:userhabitPath
                            withIntermediateDirectories:YES
                                             attributes:nil
                                                  error:nil];

    // Valid Data - session dirs
    NSArray<NSString *> *sessionStrs = @[@"1", @"105", @"2094"];
    [sessionStrs enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        [self createDummyFile:userhabitPath fileName:obj];
    }];

    // Valid Data - using data
    NSArray<NSString *> *dataStrs = @[@"sessionInfo.data",
            @"session.dat",
            @"sessionCnt.dat",
            @"latestSuccessTime.dat",
            @"deviceId.data",
            @"userInfo.data"];
    [dataStrs enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        [self createDummyFile:userhabitPath fileName:obj];
    }];

    // Invalid Data
    NSArray<NSString *> *invalidDataStrs = @[@"abc.jpg",
            @"def.png",
            @"aburakatabura.dan"];
    [invalidDataStrs enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        [self createDummyFile:userhabitPath fileName:obj];
    }];


    // delete data
    [[UH_SessionController sharedInstance] removeUnusefulData];


    // check results
    [sessionStrs enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        XCTAssertTrue([self existFile:userhabitPath fileName:obj]);
    }];
    [dataStrs enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        XCTAssertTrue([self existFile:userhabitPath fileName:obj]);
    }];
    [invalidDataStrs enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        XCTAssertFalse([self existFile:userhabitPath fileName:obj]);
    }];


    // clear
    [NSFileManager.defaultManager removeItemAtPath:userhabitPath error:nil];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.

    [super tearDown];
}



- (void)createDummyFile:(NSString *)userhabitPath fileName:(NSString *)fileName {

    NSString *newFileName = [userhabitPath stringByAppendingPathComponent:fileName];

    [[NSFileManager defaultManager] createFileAtPath:newFileName
                                            contents:[@"Temp" dataUsingEncoding:NSUTF8StringEncoding]
                                          attributes:nil];
}

- (BOOL) existFile:(NSString *)userhabitPath fileName:(NSString *)fileName {

    NSString *comFileName = [userhabitPath stringByAppendingPathComponent:fileName];

    return [NSFileManager.defaultManager fileExistsAtPath:comFileName];
}


@end
