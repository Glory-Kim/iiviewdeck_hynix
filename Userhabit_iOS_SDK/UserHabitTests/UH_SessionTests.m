//
//  UH_SessionTests.m
//  UserHabit
//
//  Created by Jinuk Baek on 25/01/2017.
//  Copyright © 2017 Andbut. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UH_SessionController.h"

@interface UH_SessionTests : XCTestCase

@end

@implementation UH_SessionTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.

    [[UH_SessionController sharedInstance] sessionStart];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [[UH_SessionController sharedInstance] sessionClose];

    [super tearDown];

}




@end
