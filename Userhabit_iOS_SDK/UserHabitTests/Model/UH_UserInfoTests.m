//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UH_UserInfo.h"

@class builtUserInfo;

@interface UH_UserInfoTests : XCTestCase

@end

@implementation UH_UserInfoTests

- (UH_UserInfo *)createValidUserInfo {
    UH_UserInfo *userInfo = [UH_UserInfo new];

    NSString *customUserId = @"customUserId";
    int customUserType = 1;
    UHUserInfoGender gender = UHUserInfoGenderMale;
    int age = 31;

    userInfo.customUserId = customUserId;
    userInfo.customUserType = customUserType;
    userInfo.gender = gender;
    userInfo.age = age;

    return userInfo;
}

- (UH_UserInfo *)createValidUserInfoWithoutData {
    UH_UserInfo *userInfo = [UH_UserInfo new];

    return userInfo;
}

- (NSDictionary *)validBuiltUserInfo {
    NSDictionary *userInfo = @{
            @"i": @"customUserId",
            @"t": @1,
            @"g": @1,
            @"a": @31
    };

    return userInfo;
}

- (void)testBuild {
    UH_UserInfo *userInfo = [self createValidUserInfo];
    NSDictionary *builtUserInfo = [userInfo build];
    NSDictionary *validUserInfo = [self validBuiltUserInfo];

    XCTAssertTrue([builtUserInfo isEqual:validUserInfo]);
}

- (void)testBuildIfNoHadData {
    UH_UserInfo *userInfo = [self createValidUserInfoWithoutData];
    NSDictionary *builtUserInfo = [userInfo build];
    NSDictionary *validUserInfo = [NSDictionary dictionary];

    XCTAssertTrue([builtUserInfo isEqual:validUserInfo]);
}

@end