//
//  UH_TapInteractionTests.m
//  UserHabit
//
//  Created by Jinuk Baek on 09/02/2017.
//  Copyright © 2017 Andbut. All rights reserved.
//


#import <XCTest/XCTest.h>
#import "UH_TapInteraction.h"

@interface UH_TapInteractionTests : XCTestCase

@end

@implementation UH_TapInteractionTests

- (UH_TapInteraction *)createTapInteraction {
    int x = 100;
    int y = 200;

    UH_TapInteraction *tabInteraction = [[UH_TapInteraction alloc] initPosition:CGPointMake(x, y)];

    return tabInteraction;
}

- (UH_TapInteraction *)createTapInteractionWithObject {
    int x = 100;
    int y = 200;
    NSString *objectId = @"objectId";
    NSString *objectDescription = @"objectDescription";

    UH_TapInteraction *tabInteraction = [[UH_TapInteraction alloc] initPosition:CGPointMake(x, y)];
    tabInteraction.objectId = objectId;
    tabInteraction.objectDescription = objectDescription;

    return tabInteraction;
}

- (NSDictionary *)validTapInteraction {
    NSDictionary *validTapInteraction = @{
            @"p": @[@100, @200]
    };

    return validTapInteraction;
}

- (NSDictionary *)validTapInteractionWithObject {
    NSDictionary *validTapInteraction = @{
            @"p": @[@100, @200],
            @"o": @"objectId",
            @"d": @"objectDescription"
    };

    return validTapInteraction;
}

- (void)testBuild {
    UH_TapInteraction *tapInteraction = [self createTapInteraction];
    NSDictionary *builtTapInteraction = [tapInteraction build];
    NSDictionary *validTapInteraction = [self validTapInteraction];

    XCTAssertTrue([builtTapInteraction isEqual:validTapInteraction]);
}

- (void)testBuildWithObject {
    UH_TapInteraction *tapInteraction = [self createTapInteractionWithObject];
    NSDictionary *builtTapInteraction = [tapInteraction build];
    NSDictionary *validTapInteraction = [self validTapInteractionWithObject];

    XCTAssertTrue([builtTapInteraction isEqual:validTapInteraction]);
}

@end
