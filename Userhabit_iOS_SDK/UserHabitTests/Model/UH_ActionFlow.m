//
// Created by Jinuk Baek on 15/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UH_ActionFlow.h"
#import "UH_ScreenInfo.h"
#import "UH_TapInteraction.h"
#import "UH_SwipeInteraction.h"
#import "UH_CrashInfo.h"
#import "UH_ContentInfo.h"
#import "UH_InnerData.h"
#import "UH_SessionInfo.h"

@interface UH_ActionFlowTests : XCTestCase

@end

@implementation UH_ActionFlowTests

- (NSDictionary *)validActionFlow:(UHActionFlowType)type at:(int)uptime with:(UH_Model *)message {
    NSDictionary *actionFlow = @{
            @"t": @(type),
            @"u": @(uptime),
            @"m": [message build]
    };

    return actionFlow;
}


- (NSDictionary *)validActionFlow:(UHActionFlowType)type at:(int)uptime {
    NSDictionary *actionFlow = @{
            @"t": @(type),
            @"u": @(uptime)
    };

    return actionFlow;
}


- (void)testBuildWithData {
    UH_ActionFlow *actionFlow = [[UH_ActionFlow alloc] initAt:200 from:100 typeOf:UHActionFlowTypeAppStart];
    NSDictionary *builtActionFlow = [actionFlow build];
    NSDictionary *validActionFlow = [self validActionFlow:UHActionFlowTypeAppStart at:100];

    XCTAssertTrue([builtActionFlow isEqual:validActionFlow]);
}


- (void)testBuildWithDataMessage {
    UH_TapInteraction *tapInteraction = [[UH_TapInteraction alloc] initPosition:CGPointMake(1, 2)];

    UH_ActionFlow *actionFlow = [[UH_ActionFlow alloc] initAt:200 from:100 typeOf:UHActionFlowTypeDoubleTap with:tapInteraction];
    NSDictionary *builtActionFlow = [actionFlow build];
    NSDictionary *validActionFlow = [self validActionFlow:UHActionFlowTypeDoubleTap
                                                       at:100
                                                     with:tapInteraction];

    XCTAssertTrue([builtActionFlow isEqual:validActionFlow]);
}


@end
