//
//  UH_InnerDataTests.m
//  UserHabit
//
//  Created by Jinuk Baek on 09/02/2017.
//  Copyright © 2017 Andbut. All rights reserved.
//


#import <XCTest/XCTest.h>
#import "UH_InnerData.h"

@interface UH_InnerDataTests : XCTestCase

@end

@implementation UH_InnerDataTests

- (UH_InnerData *)createInnerData {
    NSString *type = @"type";
    NSString *value = @"value";

    UH_InnerData *innerData = [[UH_InnerData alloc] initValue:value typeOf:type];

    return innerData;
}

- (NSDictionary *)validInnerData {
    NSDictionary *validInnerData = @{
            @"t": @"type",
            @"v": @"value"
    };

    return validInnerData;
}

- (void)testBuild {
    UH_InnerData *innerData = [self createInnerData];
    NSDictionary *builtInnerData = [innerData build];
    NSDictionary *validInnerData = [self validInnerData];

    XCTAssertTrue([builtInnerData isEqual:validInnerData]);
}

@end