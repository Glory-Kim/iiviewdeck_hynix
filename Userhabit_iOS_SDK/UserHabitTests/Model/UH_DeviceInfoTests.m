//
//  UH_DeviceInfoTests.m
//  UserHabit
//
//  Created by Jinuk Baek on 04/02/2017.
//  Copyright © 2017 Andbut. All rights reserved.
//


#import <XCTest/XCTest.h>
#import "UH_DeviceInfo.h"

@interface UH_DeviceInfoTests : XCTestCase

@end

@implementation UH_DeviceInfoTests

- (UH_DeviceInfo *)createValidDeviceInfo {
    NSString *brandName = @"brandName";
    NSString *deviceBoard = @"deviceBoard";
    NSString *deviceId = @"deviceId";
    NSString *deviceModel = @"deviceModel";
    NSString *deviceName = @"deviceName";
    int deviceResolutionWidth = 1080;
    int deviceResolutionHeight = 1920;
    int displayDpi = 400;
    // The device platform is always 0

    UH_DeviceInfo *deviceInfo = [UH_DeviceInfo new];
    deviceInfo.brandName = brandName;
    deviceInfo.deviceBoard = deviceBoard;
    deviceInfo.deviceId = deviceId;
    deviceInfo.deviceModel = deviceModel;
    deviceInfo.deviceName = deviceName;
    [deviceInfo setDeviceResolutionSize:CGSizeMake(deviceResolutionWidth, deviceResolutionHeight)];
    deviceInfo.displayDpi = displayDpi;

    return deviceInfo;
}

- (NSDictionary *)validBuiltDeviceInfo {
    NSDictionary *deviceInfo = @{
            @"c": @"brandName",
            @"b": @"deviceBoard",
            @"i": @"deviceId",
            @"m": @"deviceModel",
            @"n": @"deviceName",
            @"s": @{
                    @"w": @1080,
                    @"h": @1920
            },
            @"d": @400,
            @"o": @2
    };

    return deviceInfo;
}

- (void)testBuild {
    UH_DeviceInfo *deviceInfo = [self createValidDeviceInfo];
    NSDictionary *builtDeviceInfo = [deviceInfo build];
    NSDictionary *validDeviceInfo = [self validBuiltDeviceInfo];

    XCTAssertTrue([builtDeviceInfo isEqual:validDeviceInfo]);
}

- (void)testSerializeDeserialize {
    UH_DeviceInfo *deviceInfo = [self createValidDeviceInfo];
    NSDictionary *builtDeviceInfo = [deviceInfo build];

    UH_DeviceInfo *deviceInfo1 = [[UH_DeviceInfo alloc] initDictionary:builtDeviceInfo];
    NSDictionary *builtDeviceInfo1 = [deviceInfo1 build];

    XCTAssertTrue([builtDeviceInfo isEqual:builtDeviceInfo1]);
}


@end
