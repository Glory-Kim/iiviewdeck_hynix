//
//  UH_SwipeInteractionTests.m
//  UserHabit
//
//  Created by Jinuk Baek on 09/02/2017.
//  Copyright © 2017 Andbut. All rights reserved.
//


#import <XCTest/XCTest.h>
#import "UH_SwipeInteraction.h"
#import "UserHabitConstant.h"

@interface UH_SwipeInteractionTests : XCTestCase

@end

@implementation UH_SwipeInteractionTests

- (UH_SwipeInteraction *)createSwipeInteraction {
    UHSwipeDirection direction = UHSwipeDirectionLeft;
    int x = 100;
    int y = 200;
    int x2 = 300;
    int y2 = 400;

    UH_SwipeInteraction *swipeInteraction = [[UH_SwipeInteraction alloc] initDirection:direction
                                                                            beginPoint:CGPointMake(x, y)
                                                                          withEndPoint:CGPointMake(x2, y2)];

    return swipeInteraction;
}

- (UH_SwipeInteraction *)createSwipeInteractionWithObject {
    UHSwipeDirection direction = UHSwipeDirectionLeft;
    int x = 100;
    int y = 200;
    int x2 = 300;
    int y2 = 400;
    NSString *objectId = @"objectId";
    NSString *objectDescription = @"objectDescription";


    UH_SwipeInteraction *swipeInteraction = [[UH_SwipeInteraction alloc] initDirection:direction
                                                                            beginPoint:CGPointMake(x, y)
                                                                          withEndPoint:CGPointMake(x2, y2)];

    swipeInteraction.objectId = objectId;
    swipeInteraction.objectDescription = objectDescription;

    return swipeInteraction;
}

- (NSDictionary *)validSwipeInteraction {
    NSDictionary *validSwipeInteraction = @{
            @"i": @3,
            @"p": @[@100, @200],
            @"t": @[@300, @400]
    };

    return validSwipeInteraction;
}

- (NSDictionary *)validSwipeInteractionWithObject {
    NSDictionary *validSwipeInteraction = @{
            @"i": @3,
            @"p": @[@100, @200],
            @"t": @[@300, @400],
            @"o": @"objectId",
            @"d": @"objectDescription"
    };

    return validSwipeInteraction;
}

/*
 * p가 시작좌표고 t가 끝나는 좌표로 문서에 나와있으나 반대로 보내야지만 정상적으로 스와이프 제스쳐가 보임
 */
- (void)testBuild {
    UH_SwipeInteraction *swipeInteraction = [self createSwipeInteraction];
    NSDictionary *builtSwipeInteraction = [swipeInteraction build];
    NSDictionary *validSwipeInteraction = [self validSwipeInteraction];
    
    XCTAssertTrue([builtSwipeInteraction isEqual:validSwipeInteraction]);
}

/*
 * p가 시작좌표고 t가 끝나는 좌표로 문서에 나와있으나 반대로 보내야지만 정상적으로 스와이프 제스쳐가 보임
 */
- (void)testBuildWithObject {
    UH_SwipeInteraction *swipeInteraction = [self createSwipeInteractionWithObject];
    NSDictionary *builtSwipeInteraction = [swipeInteraction build];
    NSDictionary *validSwipeInteraction = [self validSwipeInteractionWithObject];

    XCTAssertTrue([builtSwipeInteraction isEqual:validSwipeInteraction]);
}

@end
