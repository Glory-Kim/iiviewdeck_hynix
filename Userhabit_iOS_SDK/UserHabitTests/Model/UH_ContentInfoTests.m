//
//  UH_ContentInfoTests.m
//  UserHabit
//
//  Created by Jinuk Baek on 09/02/2017.
//  Copyright © 2017 Andbut. All rights reserved.
//


#import <XCTest/XCTest.h>
#import "UH_ContentInfo.h"

@interface UH_ContentInfoTests : XCTestCase

@end

@implementation UH_ContentInfoTests

- (UH_ContentInfo *)createContentInfo {
    NSString *action = @"action";
    NSString *contentName = @"contentName";

    UH_ContentInfo *contentInfo = [[UH_ContentInfo alloc] initAction:action conentNameOf:contentName];

    return contentInfo;
}

- (NSDictionary *)validContentInfo {
    NSDictionary *validContentInfo = @{
            @"a": @"action",
            @"o": @"contentName"
    };

    return validContentInfo;
}

- (void)testBuild {
    UH_ContentInfo *contentInfo = [self createContentInfo];
    NSDictionary *builtContentInfo = [contentInfo build];
    NSDictionary *validContentInfo = [self validContentInfo];

    XCTAssertTrue([builtContentInfo isEqual:validContentInfo]);
}

@end