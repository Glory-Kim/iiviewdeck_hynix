//
//  UH_ScreenInfoTests.m
//  UserHabit
//
//  Created by Jinuk Baek on 04/02/2017.
//  Copyright © 2017 Andbut. All rights reserved.
//


#import <XCTest/XCTest.h>
#import "UH_ScreenInfo.h"
#import "UH_ObjectInfo.h"

@interface UH_ScreenInfoTests : XCTestCase

@end

@implementation UH_ScreenInfoTests

- (UH_ObjectInfo *)createObjectInfo:(int)num {
    NSString *objectId = [NSString stringWithFormat:@"object%d", num];
    int left = 100 + num;
    int top = 200 + num;
    int width = 300 + num;
    int height = 400 + num;

    return [[UH_ObjectInfo alloc] initId:objectId typeOf:@"button" frame:CGRectMake(left, top, width, height)];
}

- (UH_ScreenInfo *)createValidScreenInfoDefaultScreen {
    NSString *screenName = @"screenName";
    NSString *fragmentName = @"fragmentName";
    UHScreenOrientation orientation = UHScreenOrientationPortrait;
    CGSize size = CGSizeMake(300, 400);

    UH_ScreenInfo *screenInfo = [[UH_ScreenInfo alloc] initInDefault:screenName
                                                            fragment:fragmentName
                                                         orientation:orientation
                                                                size:size];

    return screenInfo;
}

- (NSDictionary *)validBuiltScreenInfoDefaultScreen {
    NSDictionary *screenInfo = @{
            @"a": @"screenName",
            @"f": @"fragmentName",
            @"r": @(UHScreenOrientationPortrait),
            @"t": @(UHScreenTypeDefault),
            @"p": @{ @"w": @300, @"h": @400 }
    };

    return screenInfo;
}

- (void)testBuildDefaultScreen {
    UH_ScreenInfo *screenInfo = [self createValidScreenInfoDefaultScreen];
    NSDictionary *builtScreenInfo = [screenInfo build];
    NSDictionary *validScreenInfo = [self validBuiltScreenInfoDefaultScreen];

    XCTAssertTrue([builtScreenInfo isEqual:validScreenInfo]);
}

- (UH_ScreenInfo *)createValidScreenInfoDefaultScreenNoFragment {
    NSString *screenName = @"screenName";
    UHScreenOrientation orientation = UHScreenOrientationPortrait;
    CGSize size = CGSizeMake(300, 400);

    UH_ScreenInfo *screenInfo = [[UH_ScreenInfo alloc] initInDefault:screenName
                                                            fragment:nil
                                                         orientation:orientation
                                                                size:size];
    return screenInfo;
}

- (NSDictionary *)validBuiltScreenInfoDefaultScreenNoFragment {
    NSDictionary *screenInfo = @{
            @"a": @"screenName",
            @"r": @(UHScreenOrientationPortrait),
            @"t": @(UHScreenTypeDefault),
            @"p": @{ @"w": @300, @"h": @400 }
    };

    return screenInfo;
}

- (void)testBuildDefaultScreenNoFragment {
    UH_ScreenInfo *screenInfo = [self createValidScreenInfoDefaultScreenNoFragment];
    NSDictionary *builtScreenInfo = [screenInfo build];
    NSDictionary *validScreenInfo = [self validBuiltScreenInfoDefaultScreenNoFragment];

    XCTAssertTrue([builtScreenInfo isEqual:validScreenInfo]);
}

- (NSDictionary *)validBuiltScreenInfoDialog {
    NSDictionary *screenInfo = @{
            @"a": @"screenName",
            @"r": @(UHScreenOrientationPortrait),
            @"t": @(UHScreenTypeDialog),
            @"b": @{ @"w": @100, @"h": @200 },
            @"p": @{ @"w": @300, @"h": @400 }
    };

    return screenInfo;
}

- (void)testBuildDialog {
    UH_ScreenInfo *screenInfo = [self createValidScreenInfoDialog];
    NSDictionary *builtScreenInfo = [screenInfo build];
    NSDictionary *validScreenInfo = [self validBuiltScreenInfoDialog];

    XCTAssertTrue([builtScreenInfo isEqual:validScreenInfo]);
}

- (UH_ScreenInfo *)createValidScreenInfoDefaultScreenWithObjectInfos {
    NSString *screenName = @"screenName";
    NSString *fragmentName = @"fragmentName";
    UHScreenOrientation orientation = UHScreenOrientationPortrait;
    CGSize size = CGSizeMake(300, 400);

    UH_ScreenInfo *screenInfo = [[UH_ScreenInfo alloc] initInDefault:screenName
                                                            fragment:fragmentName
                                                         orientation:orientation
                                                                size:size];
    
    screenInfo.objectInfos = @[[self createObjectInfo:1], [self createObjectInfo:2]];

    return screenInfo;
}

- (NSDictionary *)validBuiltScreenInfoDefaultScreenWithObjectInfos {
    NSDictionary *screenInfo = @{
            @"a": @"screenName",
            @"f": @"fragmentName",
            @"r": @(UHScreenOrientationPortrait),
            @"t": @(UHScreenTypeDefault),
            @"p": @{ @"w": @300, @"h": @400 },
            @"o": @[
                    [[self createObjectInfo:1] build],
                    [[self createObjectInfo:2] build]
            ]
    };

    return screenInfo;
}

- (void)testBuildDefaultScreenWithObjectInfos {
    UH_ScreenInfo *screenInfo = [self createValidScreenInfoDefaultScreenWithObjectInfos];
    NSDictionary *builtScreenInfo = [screenInfo build];
    NSDictionary *validScreenInfo = [self validBuiltScreenInfoDefaultScreenWithObjectInfos];

    XCTAssertTrue([builtScreenInfo isEqual:validScreenInfo]);
}


/*
- (UH_ScreenInfo *)createValidScreenInfoDefaultScreenWithScrollInfos {
    NSString *screenName = @"screenName";
    NSString *fragmentName = @"fragmentName";
    UHScreenOrientation orientation = UHScreenOrientationPortrait;
    CGSize size = CGSizeMake(300, 400);
    
    UH_ScreenInfo *screenInfo = [[UH_ScreenInfo alloc] initInDefault:screenName
                                                            fragment:fragmentName
                                                         orientation:orientation
                                                                size:size];
    
    screenInfo.scrollInfos = @[[self createObjectInfo:1], [self createObjectInfo:2]];

    return screenInfo;
}

- (NSDictionary *)validBuiltScreenInfoDefaultScreenWithScrollInfos {
    NSDictionary *screenInfo = @{
            @"a": @"screenName",
            @"f": @"fragmentName",
            @"r": @(UHScreenOrientationPortrait),
            @"t": @(UHScreenTypeDefault),
            @"p": @{ @"w": @300, @"h": @400 },
            @"s": @[
                    [[self createObjectInfo:1] build],
                    [[self createObjectInfo:2] build]
            ]
    };

    return screenInfo;
}

- (void)testBuildDefaultScreenWithScrollInfos {
    UH_ScreenInfo *screenInfo = [self createValidScreenInfoDefaultScreenWithScrollInfos];
    NSDictionary *builtScreenInfo = [screenInfo build];
    NSDictionary *validScreenInfo = [self validBuiltScreenInfoDefaultScreenWithScrollInfos];

    XCTAssertTrue([builtScreenInfo isEqual:validScreenInfo]);
}
*/
@end
