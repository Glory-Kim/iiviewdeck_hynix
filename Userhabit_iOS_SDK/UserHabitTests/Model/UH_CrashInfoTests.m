//
//  UH_CrashInfoTests.m
//  UserHabit
//
//  Created by Jinuk Baek on 09/02/2017.
//  Copyright © 2017 Andbut. All rights reserved.
//


#import <XCTest/XCTest.h>
#import "UH_CrashInfo.h"
#import "UserHabitConstant.h"

@interface UH_CrashInfoTests : XCTestCase

@end

@implementation UH_CrashInfoTests

- (UH_CrashInfo *)createCrashInfo {

    float battery = 100.0f;
    BOOL charging = YES;
    NSString *crashMessage = @"crashMessage";
    NSString *exceptionType = @"exceptionType";
    long long freeDisk = 10000L;
    long long freeMemory = 10001L;
    BOOL rooting = NO;
    UHNetworkStatus networkStatus = UHNetworkStatusWifi;
    int processes = 10;
    NSString *stackTrace = @"stackTrace";
    long long totalDisk = 20000L;
    long long totalMemory = 20001L;


    UH_CrashInfo *crashInfo = [UH_CrashInfo new];
    crashInfo.battery = battery;
    crashInfo.charging = charging;
    crashInfo.crashMessage = crashMessage;
    crashInfo.exceptionType = exceptionType;
    crashInfo.freeDisk = freeDisk;
    crashInfo.freeMemory = freeMemory;
    crashInfo.jailBreaking = rooting;
    crashInfo.networkStatus = networkStatus;
    crashInfo.processes = processes;
    crashInfo.stackTrace = stackTrace;
    crashInfo.totalDisk = totalDisk;
    crashInfo.totalMemory = totalMemory;

    return crashInfo;
}

- (NSDictionary *)validCrashInfo {
    NSDictionary *validCrashInfo = @{
            @"b": @(100.0f),
            @"c": @YES,
            @"m": @"crashMessage",
            @"t": @"exceptionType",
            @"fd": @(10000L),
            @"fm": @(10001L),
            @"j": @NO,
            @"n": @(UHNetworkStatusWifi),
            @"p": @10,
            @"s": @"stackTrace",
            @"td": @20000L,
            @"tm": @20001L
    };

    return validCrashInfo;
}

- (void)testBuild {
    UH_CrashInfo *crashInfo = [self createCrashInfo];
    NSDictionary *builtCrashInfo = [crashInfo build];
    NSDictionary *validCrashInfo = [self validCrashInfo];

    XCTAssertTrue([builtCrashInfo isEqual:validCrashInfo]);
}


@end