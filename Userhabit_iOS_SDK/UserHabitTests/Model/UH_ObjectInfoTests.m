//
//  UH_ObjectInfoTests.m
//  UserHabit
//
//  Created by Jinuk Baek on 09/02/2017.
//  Copyright © 2017 Andbut. All rights reserved.
//


#import <XCTest/XCTest.h>
#import "UH_ObjectInfo.h"

@interface UH_ObjectInfoTests : XCTestCase

@end

@implementation UH_ObjectInfoTests

- (UH_ObjectInfo *)createObjectInfo {
    NSString *objectId = @"objectId";
    NSString *objectType = @"objectType";
    int left = 10;
    int top = 20;
    int width = 100;
    int height = 200;

    return [[UH_ObjectInfo alloc] initId:objectId
                                  typeOf:objectType
                                    frame:CGRectMake(left, top, width, height)];
}


- (NSDictionary *)validObjectInfo {
    NSDictionary *objectInfo = @{
            @"t": @"objectType",
            @"i": @"objectId",
            @"r": @{
                    @"l": @10,
                    @"t": @20,
                    @"w": @100,
                    @"h": @200
            }
    };

    return objectInfo;
}


- (void)testBuild {
    UH_ObjectInfo *objectInfo = [self createObjectInfo];
    NSDictionary *builtObjectInfo = [objectInfo build];
    NSDictionary *validObjectInfo = [self validObjectInfo];

    XCTAssertTrue([builtObjectInfo isEqual:validObjectInfo]);
}

@end
