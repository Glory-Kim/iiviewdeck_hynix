//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UH_SessionInfo.h"
#import "UserHabitConstant.h"

@interface UH_SessionInfoTests : XCTestCase

@end

@implementation UH_SessionInfoTests

- (UH_SessionInfo *)createValidSessionInfo {
    UH_SessionInfo *sessionInfo = [UH_SessionInfo new];

    sessionInfo.sessionId = @"sessionIdByClient";
    sessionInfo.sessionStartTime = 5000L;
    sessionInfo.osVersion = @"osVersion";
    sessionInfo.locale = @"locale";
    sessionInfo.timezoneOffset = @"timezoneOffset";
    sessionInfo.timezoneName = @"timezoneName";
    sessionInfo.uptimeStart = 10000L;
    sessionInfo.deviceTime = 20000L;
    sessionInfo.sessionCount = 1;
    sessionInfo.networkStatus = UHNetworkStatusWifi;
    sessionInfo.referrerTimestamp = 30000L;
    sessionInfo.latestSuccessTimestamp = 40000L;

    return sessionInfo;
}


- (UH_SessionInfo *)createValidSessionInfoWithoutReferrer {
    UH_SessionInfo *sessionInfo = [UH_SessionInfo new];

    sessionInfo.sessionId = @"sessionIdByClient";
    sessionInfo.sessionStartTime = 5000L;
    sessionInfo.osVersion = @"osVersion";
    sessionInfo.locale = @"locale";
    sessionInfo.timezoneOffset = @"timezoneOffset";
    sessionInfo.timezoneName = @"timezoneName";
    sessionInfo.uptimeStart = 10000L;
    sessionInfo.deviceTime = 20000L;
    sessionInfo.sessionCount = 1;
    sessionInfo.networkStatus = UHNetworkStatusWifi;
    sessionInfo.latestSuccessTimestamp = 40000L;

    return sessionInfo;
}

- (NSDictionary *)validBuiltSessionInfo {
    NSDictionary *sessionInfo = @{
            @"i": @"sessionIdByClient",
            @"t": @(5000L),
            @"v": @"osVersion",
            @"l": @"locale",
            @"o": @"timezoneOffset",
            @"z": @"timezoneName",
            @"u": @(10000L),
            @"d": @(20000L),
            @"c": @1,
            @"n": @(UHNetworkStatusWifi),
            @"rt": @(30000L),
            @"lt": @(40000L)
    };

    return sessionInfo;
}


- (NSDictionary *)validBuiltSessionInfoWithoutReferrer {
    NSDictionary *sessionInfo = @{
            @"i": @"sessionIdByClient",
            @"t": @(5000L),
            @"v": @"osVersion",
            @"l": @"locale",
            @"o": @"timezoneOffset",
            @"z": @"timezoneName",
            @"u": @(10000L),
            @"d": @(20000L),
            @"c": @1,
            @"n": @(UHNetworkStatusWifi),
            @"lt": @(40000L)
    };

    return sessionInfo;
}

- (void)testBuild {
    UH_SessionInfo *sessionInfo = [self createValidSessionInfo];
    NSDictionary *builtSessionInfo = [sessionInfo build];
    NSDictionary *validSessionInfo = [self validBuiltSessionInfo];

    XCTAssertTrue([builtSessionInfo isEqual:validSessionInfo]);
}

- (void)testBuildWithoutReferrer {
    UH_SessionInfo *sessionInfo = [self createValidSessionInfoWithoutReferrer];
    NSDictionary *builtSessionInfo = [sessionInfo build];
    NSDictionary *validSessionInfo = [self validBuiltSessionInfoWithoutReferrer];

    XCTAssertTrue([builtSessionInfo isEqual:validSessionInfo]);
}

- (void)testSerializeDeserialize {
    UH_SessionInfo *sessionInfo = [self createValidSessionInfo];
    NSDictionary *builtSessionInfo = [sessionInfo build];

    UH_SessionInfo *sessionInfo1 = [[UH_SessionInfo alloc] initDictionary:builtSessionInfo];
    NSDictionary *builtSessionInfo1 = [sessionInfo1 build];

    XCTAssertTrue([builtSessionInfo isEqual:builtSessionInfo1]);
}

- (void)testSerializeDeserializeWithoutReferrer {
    UH_SessionInfo *sessionInfo = [self createValidSessionInfoWithoutReferrer];
    NSDictionary *builtSessionInfo = [sessionInfo build];

    UH_SessionInfo *sessionInfo1 = [[UH_SessionInfo alloc] initDictionary:builtSessionInfo];
    NSDictionary *builtSessionInfo1 = [sessionInfo1 build];

    XCTAssertTrue([builtSessionInfo isEqual:builtSessionInfo1]);
}

@end
