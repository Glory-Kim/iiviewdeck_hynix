//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UH_AppInfo.h"

@interface UH_AppInfoTests : XCTestCase

@end

@implementation UH_AppInfoTests

- (UH_AppInfo *)createValidAppInfo {
    UH_AppInfo *appInfo = [UH_AppInfo new];

    NSString *apiKey = @"apiKey";
    int appVersionCode = 1;
    NSString *appVersionName = @"appVersionName";
    int sdkVersionCode = 2;
    NSString *sdkVersionName = @"sdkVersionName";

    appInfo.apiKey = apiKey;
    appInfo.appVersionCode = appVersionCode;
    appInfo.appVersionName = appVersionName;
    appInfo.sdkVersionCode = sdkVersionCode;
    appInfo.sdkVersionName = sdkVersionName;

    return appInfo;
}

- (NSDictionary *)validBuiltAppInfo {
    NSDictionary *appInfo = @{
            @"a": @"apiKey",
            @"c": @1,
            @"n": @"appVersionName",
            @"s": @2,
            @"v": @"sdkVersionName"
    };

    return appInfo;
}

- (void)testBuild {
    UH_AppInfo *appInfo = [self createValidAppInfo];
    NSDictionary *builtAppInfo = [appInfo build];
    NSDictionary *validAppInfo = [self validBuiltAppInfo];

    XCTAssertTrue([builtAppInfo isEqual:validAppInfo]);
}

- (void)testSerializeDeserialize {
    UH_AppInfo *appInfo = [self createValidAppInfo];
    NSDictionary *builtAppInfo = [appInfo build];

    UH_AppInfo *appInfo1 = [[UH_AppInfo alloc] initDictionary:builtAppInfo];
    NSDictionary *builtAppInfo1 = [appInfo1 build];

    XCTAssertTrue([builtAppInfo isEqual:builtAppInfo1]);
}

@end