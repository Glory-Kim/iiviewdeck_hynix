//
//  UH_ChangeDataPathTests.m
//  UserHabit
//
//  Created by r on 2017. 4. 19..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UH_SessionController.h"
#import "UH_Util.h"

@interface UH_ChangeDataPathTests : XCTestCase

@end

@implementation UH_ChangeDataPathTests

- (void)setUp {
    [super setUp];

    //document 에 파일을 만든다
    [[UH_SessionController sharedInstance] previousFilePath];
    [[UH_SessionController sharedInstance] loadSessionCount];
}

- (void)tearDown {
    [super tearDown];
    [[UH_SessionController sharedInstance] dataPathSetting];
}

- (void)testExample {
    [[UH_SessionController sharedInstance] checkPathVaildity];
}

@end
