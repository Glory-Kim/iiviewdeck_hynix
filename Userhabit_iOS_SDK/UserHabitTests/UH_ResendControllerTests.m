//
//  UH_SessionControllerTests.m
//  UserHabit
//
//  Created by Jinuk Baek on 04/02/2017.
//  Copyright © 2017 Andbut. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UH_SessionController.h"
#import "UH_ResendController.h"


@interface UH_ResendControllerTests : XCTestCase

@end

@implementation UH_ResendControllerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.

}


- (void)testRemoveOldSessionsSmallValue {

    NSString *userhabitPath = [UH_SessionController sharedInstance].userhabitPath;

    // clear UserHabit Directory
    [NSFileManager.defaultManager removeItemAtPath:userhabitPath error:nil];
    [NSFileManager.defaultManager createDirectoryAtPath:userhabitPath
                            withIntermediateDirectories:YES
                                             attributes:nil
                                                  error:nil];


    NSArray<NSString *> *sessionStrs = @[@"1", @"2", @"3", @"4", @"5"];
    [sessionStrs enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        [self createDummyFile:userhabitPath fileName:obj];
    }];


    // works
    [[UH_ResendController sharedInstance] removeOldData:6];

    // check results
    NSArray<NSString *> *sessionTrueStrs = @[@"1", @"2", @"3", @"4", @"5"];

    [sessionTrueStrs enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        XCTAssertTrue([self existFile:userhabitPath fileName:obj]);
    }];


    // clear
    [NSFileManager.defaultManager removeItemAtPath:userhabitPath error:nil];
}



- (void)testRemoveOldSessions {

    NSString *userhabitPath = [UH_SessionController sharedInstance].userhabitPath;

    // clear UserHabit Directory
    [NSFileManager.defaultManager removeItemAtPath:userhabitPath error:nil];
    [NSFileManager.defaultManager createDirectoryAtPath:userhabitPath
                            withIntermediateDirectories:YES
                                             attributes:nil
                                                  error:nil];


    NSArray<NSString *> *sessionStrs = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10"];
    [sessionStrs enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        [self createDummyFile:userhabitPath fileName:obj];
    }];


    // works
    [[UH_ResendController sharedInstance] removeOldData:11];


    // check results
    NSArray<NSString *> *sessionTrueStrs = @[@"5", @"6", @"7", @"8", @"9", @"10"];
    NSArray<NSString *> *sessionFalseStrs = @[@"1", @"2", @"3", @"4"];

    [sessionTrueStrs enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        XCTAssertTrue([self existFile:userhabitPath fileName:obj]);
    }];
    [sessionFalseStrs enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        XCTAssertFalse([self existFile:userhabitPath fileName:obj]);
    }];


    // clear
    [NSFileManager.defaultManager removeItemAtPath:userhabitPath error:nil];
}


- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.

    [super tearDown];
}



- (void)createDummyFile:(NSString *)userhabitPath fileName:(NSString *)fileName {

    NSString *newFileName = [userhabitPath stringByAppendingPathComponent:fileName];

    [[NSFileManager defaultManager] createFileAtPath:newFileName
                                            contents:[@"Temp" dataUsingEncoding:NSUTF8StringEncoding]
                                          attributes:nil];
}

- (BOOL) existFile:(NSString *)userhabitPath fileName:(NSString *)fileName {

    NSString *comFileName = [userhabitPath stringByAppendingPathComponent:fileName];

    return [NSFileManager.defaultManager fileExistsAtPath:comFileName];
}


@end
