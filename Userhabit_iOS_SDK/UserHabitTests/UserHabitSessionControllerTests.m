//
//  UserHabitSessionControllerTests.m
//  UserHabit
//
//  Created by Jinuk Baek on 23/01/2017.
//  Copyright © 2017 Andbut. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UH_SessionController.h"
#import "UH_UserInfo.h"

@class UH_UserInfo;

@interface UserHabitSessionControllerTests : XCTestCase

@end

@implementation UserHabitSessionControllerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    [[UH_SessionController sharedInstance] initialize];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.

    [super tearDown];
    
}

- (void)testSaveAndLoadUserInfo {
    __block NSDictionary *dict;
    NSString *userId = @"test1";
    UHUserInfoGender gender = UHUserInfoGenderMale;
    int userType = 1;
    int8_t age = 30;
    
    // Save userinfo
    UH_SessionManager.userInformation = [[UH_UserInfo alloc] init];
    UH_SessionManager.userInformation.customUserId = userId;
    UH_SessionManager.userInformation.customUserType = userType;
    UH_SessionManager.userInformation.gender = gender;
    UH_SessionManager.userInformation.age = age;
    NSLog(@"%@", [UH_SessionController sharedInstance].userhabitPath);
    [[UH_SessionController sharedInstance] saveUserInfo];
//    [[UH_SessionController sharedInstance].opQueue waitUntilAllOperationsAreFinished];
    
    // Load userinfo
    [NSTimer timerWithTimeInterval:1.f repeats:NO block:^(NSTimer * _Nonnull timer) {
        dict = [[UH_SessionController sharedInstance] loadUserInfo];
        XCTAssertEqualObjects(dict[@"i"], userId);
        XCTAssertEqualObjects(dict[@"g"], @(gender));
        XCTAssertEqualObjects(dict[@"a"], @(age));
        XCTAssertEqualObjects(dict[@"t"], @(userType));
    }];
}

- (void)testSaveAndLoadUserInfoNullCase {
    NSString *userId = nil;
    UHUserInfoGender gender = UHUserInfoGenderUnuse;
    int userType = -1;
    int8_t age = -1;
    
    // Save userinfo
    
    UH_SessionManager.userInformation = [[UH_UserInfo alloc] init];
    UH_SessionManager.userInformation.customUserId = userId;
    UH_SessionManager.userInformation.customUserType = userType;
    UH_SessionManager.userInformation.gender = gender;
    UH_SessionManager.userInformation.age = age;

    [[UH_SessionController sharedInstance] saveUserInfo];
//    [[UH_SessionController sharedInstance].opQueue waitUntilAllOperationsAreFinished];
   
    // Load userinfo
//    dispatch_async([UH_SessionController sharedInstance].operationQueue, ^{
        NSDictionary *dict = [[UH_SessionController sharedInstance] loadUserInfo];
        XCTAssertNil(dict[@"i"]);
        XCTAssertNil(dict[@"g"]);
        XCTAssertNil(dict[@"a"]);
        XCTAssertNil(dict[@"t"]);
//    });
}

@end
