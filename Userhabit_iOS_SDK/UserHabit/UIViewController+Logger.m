//
//  UIViewController+Logger.m
//  UserHabit
//
//  Created by DoHyoungKim on 2015. 12. 11..
//  Copyright © 2015년 Andbut. All rights reserved.
//

#import "UIViewController+Logger.h"
#import <objc/runtime.h>
#import "UH_SessionController.h"
#import "UH_Util.h"
#import "UH_ViewControllerManager.h"
#import "UH_ActionController.h"
#import "UH_ManualCaptureController.h"

@implementation UIViewController_Logger

//+(void)load {
//    static dispatch_once_t UH_OnceToken;
//    dispatch_once(&UH_OnceToken, ^{
//        RLog(@"Swizzle method");
//
//        //swizzled view did appear
//        Method didOriginal, didSwizzled;
//        // willAppear < -> viewDidAppear
//
//        // A -> swizzle 제외. 셋스크린 노가다
//        // B -> swizzle 메소드 시점 변경
//        // screenshot 수집 안될 가능성 있음 / 스크린샷 수집 공수
//        didOriginal = class_getInstanceMethod(self, @selector(viewWillAppear:));
//        // C -> viewdeck (라이브러리 수정 포인트 찾아서 해결하고 SDK 수정 (난이도 상 )
//
//        // D -> viewdeck 의존성을 건다 ( 문제점 viewdeck만 수동수집 )
//
//        didOriginal = class_getInstanceMethod(self, @selector(viewDidAppear:));
//        didSwizzled = class_getInstanceMethod(self, @selector(swizzled_viewDidAppear:));
//
//         method_exchangeImplementations(didOriginal, didSwizzled);
//
//        //swizzled view did disappear
//        Method didDisappearOriginal, didDisappearSwizzled;
//
//        didDisappearOriginal = class_getInstanceMethod(self, @selector(viewDidDisappear:));
//        didDisappearSwizzled = class_getInstanceMethod(self, @selector(swizzled_viewDidDisappear:));
//
////        method_exchangeImplementations(didDisappearOriginal, didDisappearSwizzled);
//
//        //swizzled view rotate
//        Method willRotateOrigin, willRotateSwizzled;
//        willRotateOrigin = class_getInstanceMethod(self, @selector(willAnimateRotationToInterfaceOrientation:duration:));
//        willRotateSwizzled = class_getInstanceMethod(self, @selector(swizzled_willAnimateRotationToInterfaceOrientation:duration:));
//
////        method_exchangeImplementations(willRotateOrigin, willRotateSwizzled);
//
//    });
//}
+(void)swizzle_lifeCycle{
    static dispatch_once_t UH_OnceToken;
        dispatch_once(&UH_OnceToken, ^{
            RLog(@"Swizzle method");
    
            //swizzled view did appear
            Method didOriginal, didSwizzled;
            // willAppear < -> viewDidAppear
    
            // A -> swizzle 제외. 셋스크린 노가다
            // B -> swizzle 메소드 시점 변경
            // screenshot 수집 안될 가능성 있음 / 스크린샷 수집 공수
            didOriginal = class_getInstanceMethod(self, @selector(viewWillAppear:));
            // C -> viewdeck (라이브러리 수정 포인트 찾아서 해결하고 SDK 수정 (난이도 상 )
    
            // D -> viewdeck 의존성을 건다 ( 문제점 viewdeck만 수동수집 )
    
            didOriginal = class_getInstanceMethod(self, @selector(viewDidAppear:));
            didSwizzled = class_getInstanceMethod(self, @selector(swizzled_viewDidAppear:));
    
             method_exchangeImplementations(didOriginal, didSwizzled);
    
            //swizzled view did disappear
            Method didDisappearOriginal, didDisappearSwizzled;
    
            didDisappearOriginal = class_getInstanceMethod(self, @selector(viewDidDisappear:));
            didDisappearSwizzled = class_getInstanceMethod(self, @selector(swizzled_viewDidDisappear:));
    
            method_exchangeImplementations(didDisappearOriginal, didDisappearSwizzled);
    
            //swizzled view rotate
            Method willRotateOrigin, willRotateSwizzled;
            willRotateOrigin = class_getInstanceMethod(self, @selector(willAnimateRotationToInterfaceOrientation:duration:));
            willRotateSwizzled = class_getInstanceMethod(self, @selector(swizzled_willAnimateRotationToInterfaceOrientation:duration:));
    
            method_exchangeImplementations(willRotateOrigin, willRotateSwizzled);
    
        });
}


#pragma mark - swizzle method
-(void)swizzled_viewDidDisappear:(BOOL)animated{
    UH_ActionController *actionController = [UH_ActionController new];
    [actionController processMaximumScrollView:self.view];
}

-(void)swizzled_viewDidAppear:(BOOL)animated {
    @try {
        //todo::코드 확인후 삭제
        //메뉴 수동 스크린샷 셋팅
//        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
//
//        if (UH_SessionManager.isDebug &&
//            UH_SessionManager.isDevelopMode) {
//            UH_ManualCaptureController *manualCaptureController = [UH_ManualCaptureController sharedInstance];
//            [manualCaptureController setting];
//        }
        UIViewController *checkViewController;
        
        if ([self isKindOfClass:[UINavigationController class]]){
            checkViewController = ((UINavigationController *)self).visibleViewController;
        } else if ([self isKindOfClass:[UITabBarController class]]){
            checkViewController = ((UITabBarController *)self).selectedViewController;
        } else {
            checkViewController = self;
        }
        
        if ([UH_Util isCatchViewController:[UH_Util removeSwiftTargetName:checkViewController]]) {
            UH_SessionManager.nowShowController = [UH_Util removeSwiftTargetName:checkViewController];
            [UH_VCManager addViewController:checkViewController
                             withScreenName:UH_SessionManager.nowShowController
                              withIsSubView:NO];
        }
        
    } @catch (NSException *exception) {
        [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                   typeOf:UHInnerDataTypeTryCatch
                                               actionFlow:nil];
    }
}

-(void)swizzled_willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    @try {
        if ([UH_Util isCatchViewController:[UH_Util removeSwiftTargetName:self]]) {
            switch(toInterfaceOrientation) {
                case UIInterfaceOrientationPortrait:
                    [UH_SessionController sharedInstance].nowOrientation = UHScreenOrientationPortrait;
                    break;

                case UIInterfaceOrientationLandscapeLeft:
                    [UH_SessionController sharedInstance].nowOrientation = UHScreenOrientationLandscapeLeft;
                    break;

                case UIInterfaceOrientationLandscapeRight:
                    [UH_SessionController sharedInstance].nowOrientation = UHScreenOrientationLandscapeRight;
                    break;

                case UIInterfaceOrientationPortraitUpsideDown:
                    [UH_SessionController sharedInstance].nowOrientation = UHScreenOrientationPortraitUpsideDown;
                    break;

                default:
                    [UH_SessionController sharedInstance].nowOrientation = UHScreenOrientationPortrait;
                    break;
            }
            
            [UH_VCManager addViewController:self
                             withScreenName:UH_SessionManager.nowShowController
                              withIsSubView:NO];
        }
    } @catch (NSException *exception) {
        [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                   typeOf:UHInnerDataTypeTryCatch
                                               actionFlow:nil];
    }
}


- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return self.preferredInterfaceOrientationForPresentation;
}

- (BOOL)shouldAutorotate {
    return YES;
}

@end
