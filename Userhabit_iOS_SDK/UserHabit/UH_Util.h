//
//  UH_Util.h
//  UserHabit
//
//  Created by DoHyoungKim on 2015. 12. 3..
//  Copyright © 2015년 Andbut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UserHabitConstant.h"

#define UHMAXStringLength 100

typedef NS_ENUM(NSUInteger, UHDebugType) {
    UHDebugTypeUserHabit,
    UHDebugTypeScreenChange,
    UHDebugTypeObjectTouch,
    UHDebugTypeCrash,
    UHDebugTypeWarning,
    UHDebugTypeNetwork,
    UHDebugTypeSession,
    UHDebugTypeScreenCaptureImage,
    UHDebugTypeScreenCapture,
};

@class UH_ScrollViewInfo;

@interface UH_Util : NSObject

+(instancetype _Nonnull )sharedInstance;
+ (void)logDebugType:(UHDebugType)type withMessage:(NSString * _Nullable)message;

+ (UHNetworkStatus)networkStatus;

-(NSString * _Nonnull)detectViewController:(UIView * _Nonnull)sendView;

+ (UIViewController * _Nonnull)detectViewControllerClass:(id _Nonnull)sendView;
/*
 * @param view view에서 scrollview를 찾아 리턴해준다 스크롤뷰가 아닐경우 nil로 리턴
 */
+ (UIPanGestureRecognizer * _Nullable)findPanGestureTo:(UIView * _Nonnull)view;

+ (NSString * _Nonnull)makeJsonString:(NSDictionary * _Nonnull)dic;
+ (BOOL)isCatchViewController:(NSString * _Nonnull)controllerName;
-(NSMutableDictionary<NSString*, NSString*>* _Nonnull)parseQueryString:(NSString * _Nonnull)query;
-(void)compressDataFrom:(NSString * _Nonnull)from to:(NSString* _Nonnull)to;



+(NSString * _Nullable)checkObjectName:(NSString * _Nonnull)string;
/*
 * 화면 이름, 키, 값이 설정치를 넘어가면 경고와 함께 편집 한 후 해당값을 출력해준다
 */
+(NSString * _Nullable)checkScreenName:(NSString * _Nonnull)string;

/*
 * 라이브러리 경로 반환
 */
+(NSString * _Nonnull)applicationLibraryDirectoryWith:(NSString * _Nullable)path;

/*!
 * @brief checkViwe가 터치 제외 속성인지 확인한다.
 * @discussion 
 * checkView혹은 checkView의 상위뷰가 터치 제외 설정 뷰인지 확인한다
 * checkView가 터지 제외 뷰가 아닐 경우 checkView.superView를 비교한다
 * checkView가 UIWindow일 경우 반복을 종료하고 NO를 리턴한다
 * 터치 제외 뷰는 [UH_VCManager secretViewSet] 과 비교한다
 *
 * @param UIView 검색하고 싶은 객체
 * @return BOOL 터치제외속성일 경우에만 YES로 리턴한다
 */
+(BOOL)checkSecretView:(UIView * _Nonnull)checkView;

/*
 * swift 클래스일경우 target.class로 생성되는데 target.를 삭제하고 class만 리턴한다
 */
+(NSString * _Nonnull)removeSwiftTargetName:(id _Nonnull )object;


+ (NSString * _Nonnull)imageFileName;

/*
 * UH_SessionManager.currentSession.scrollViewSet 과 view를 비교하여 해당 view가 스크롤뷰 추적으로 등록되었는지 안되었는지 확인한다.
 */
+(UH_ScrollViewInfo * _Nullable)findScrollView:(UIView * _Nonnull)view;


+ (BOOL) validationTime:(int)compareTime;

#pragma mark - network util
+ (BOOL)responseCheck:(NSDictionary * _Nonnull)object;

//todo::image 처리 과정 정리 
#pragma mark - image view
+ (UIImage * _Nonnull)imageWithView:(UIView * _Nonnull)view;
+ (UIImage * _Nonnull)addImage:(UIImage * _Nonnull)image1 toImage:(UIImage * _Nonnull)image2;
@end
