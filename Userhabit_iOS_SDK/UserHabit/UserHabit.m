//
//  Userhabit.m
//  Userhabit
//
//  Created by DoHyoungKim on 2015. 5. 16..
//  Copyright (c) 2015년 Andbut. All rights reserved.
//

#import "UserHabit.h"
#import "UH_SessionController.h"
#import "UH_Util.h"
#import "UH_ViewControllerManager.h"
#import "UH_Session.h"
#import "UH_ActionFlow.h"
#import "UH_ContentInfo.h"
#import "UH_UserInfo.h"
#import "UH_SessionInfo.h"
#import "UH_ScrollViewInfo.h"
#import "UHNetworking.h"
#import "UH_InnerData.h"
#import "UH_ObjectController.h"
#import "UH_ManualCaptureController.h"
#import "UH_FileController.h"
#import "UH_ImageController.h"
#import <WebKit/WebKit.h>

////// impoartant parameters ///////////

// update::SDK 버전이 올라가면 얘도 카운트가 올라가야 함
int UHVersionCode = 38;
//NSString *const UHSdkVersion = @"1.1.15";
//하이닉스 배포버전 1.1.16
NSString *const UHSdkVersion = @"1.1.16";


#if UHForDeveloper
//NSString *const UHGatewayHost = @"http://ec2-13-124-117-97.ap-northeast-2.compute.amazonaws.com:8887";  //아네호
//NSString *const UHGatewayHost = @"https://api2.userhabit.io";
//NSString *const UHGatewayHost = @"http://staging.userhabit.io:8000";      //워커
NSString *const UHReferrerHost = @"http://staging.userhabit.io:8000";
#else
//NSString *const UHGatewayHost = @"https://api2.userhabit.io";
NSString *const UHReferrerHost = @"https://click.userhabit.io";
#endif

#if USERHABIT_U2
    NSString *const UHGatewayHost = @"http://127.0.0.1:8000";
#else
//    NSString *const UHGatewayHost = @"https://api2.userhabit.io";
      NSString *const UHGatewayHost = @"https://emmp-gw.skhynix.com/_appanalytics";
#endif

////////////////////////////////////////

UHUserInformationKey const UHUserInformationGender = @"UHUserInformationKeyGender";
UHUserInformationKey const UHUserInformationType = @"UHUserInformationKeyType";
UHUserInformationKey const UHUserInformationName = @"UHUserInformationKeyName";
UHUserInformationKey const UHUserInformationAge = @"UHUserInformationKeyAge";

NSString *const UHOptionScreenshotTapticFeedback = @"UHOptionScreenshotTapticFeedback";
NSString *const UHOptionScreenTime = @"UHOptionScreenTime";
NSString *const UHOptionTrackingException = @"UHOptionTrackingException";
NSString *const UHOptionRootWindow = @"UHOptionRootWindow";

@interface UserHabit ()
@property (nonatomic, strong) NSArray *requestToBase64Array;
@end

@implementation UserHabit

+(UserHabit * _Nonnull)sharedInstance
{
    static UserHabit *singletonObject;

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singletonObject = [[UserHabit alloc] init];
        [singletonObject setting];
    });
    
    return singletonObject;
}

- (void)setting{
    _requestToBase64Array = @[@"15396ae71f6307ecb112f27c2d72f4903f82f87e",  //vp ISP/페이북
                              @"2ac434a317333cadb4a0f6f4f8850f9c4d25f524",  //staging base64 test app number 25
                              @"598a6b499f21da59abaafca203bcd58bbdfd5385",  //service base64 test app number 2058
//                              @"23c308d6fa4b36c8bf7379ef4a420d0dc119d5a2",  //staging test key
//                              @"22eef4bfed674439cd5623f7ea026bbab0ab11e9",  //staging test key
                              ];
}
-(void)SessionStart:(nonnull NSString *)key
{
    [UH_SessionController sharedInstance].isAutoCatch = YES;
    
    [self startSession:key];
}
+(void)sessionStart:(nonnull NSString *)key{
    [[self sharedInstance] SessionStart:key];
}

-(void)SessionStart:(nonnull NSString *)key withAutoTracking:(BOOL)YesOrNo
{
    [UH_SessionController sharedInstance].isAutoCatch = YesOrNo;
    
    [self startSession:key];
}

+(void)sessionStart:(nonnull NSString *)key withAutoTracking:(BOOL)YesOrNo
{
    [[self sharedInstance] SessionStart:key withAutoTracking:YesOrNo];
}


-(void)startSession:(nonnull NSString *)key{
    [[UHNetworkReachabilityManager sharedManager] startMonitoring];
    
    UH_SessionManager.apiKey = key;
        //_requestToBase64Array 안에 있는 api키를 검색하여
    UH_SessionManager.isBase64 = NO;
    [_requestToBase64Array enumerateObjectsUsingBlock:^(NSString * base64ApiKey, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([key rangeOfString:base64ApiKey].length >= 40) {
            UH_SessionManager.isBase64 = YES;
            RLog(@"base64mode!!");
            *stop = YES;
        }
    }];
    
    if ([key hasPrefix:@"dev_"]) {
        UH_SessionManager.isDevelopMode = YES;
        NSLog(@"%@", [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@",
                      @"Please, change your API key with Production Key before you release your app in production.\n\n",
                      @"██╗   ██╗███████╗███████╗██████╗ ██╗  ██╗ █████╗ ██████╗ ██╗████████╗\n",
                      @"██║   ██║██╔════╝██╔════╝██╔══██╗██║  ██║██╔══██╗██╔══██╗██║╚══██╔══╝\n",
                      @"██║   ██║███████╗█████╗  ██████╔╝███████║███████║██████╔╝██║   ██║   \n",
                      @"██║   ██║╚════██║██╔══╝  ██╔══██╗██╔══██║██╔══██║██╔══██╗██║   ██║   \n",
                      @"╚██████╔╝███████║███████╗██║  ██║██║  ██║██║  ██║██████╔╝██║   ██║   \n",
                      @" ╚═════╝ ╚══════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ ╚═╝   ╚═╝   \n",
                      @"██████╗ ███████╗██╗   ██╗███╗   ███╗ ██████╗ ██████╗ ███████╗        \n",
                      @"██╔══██╗██╔════╝██║   ██║████╗ ████║██╔═══██╗██╔══██╗██╔════╝        \n",
                      @"██║  ██║█████╗  ██║   ██║██╔████╔██║██║   ██║██║  ██║█████╗          \n",
                      @"██║  ██║██╔══╝  ╚██╗ ██╔╝██║╚██╔╝██║██║   ██║██║  ██║██╔══╝          \n",
                      @"██████╔╝███████╗ ╚████╔╝ ██║ ╚═╝ ██║╚██████╔╝██████╔╝███████╗        \n",
                      @"╚═════╝ ╚══════╝  ╚═══╝  ╚═╝     ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝        \n",
                      @"                                                                     \n"]);
        /*
        NSLog(@"UserHabit : Screenshot capture mode = %@", sessionController.isScreenshotCaptureMode ? @"YES" : @"NO");
        NSLog(@"UserHabit : Auto catch mode = %@", sessionController.isAutoCatch ? @"YES" : @"NO");
        NSLog(@"UserHabit : Using random id = %@", sessionController.isDeviceRandomId ? @"YES" : @"NO");
         */
    }else{
        UH_SessionManager.isDevelopMode = NO;
    }
    
    [UH_SessionManager initialize];
    [UH_SessionManager sessionStart];
//    NSLog(@"time::%@",UHNowTimeStamp);
}

-(void)sessionCloseWithUploadData:(BOOL)isUpload completeHandler:(void(^_Nullable)(void))completeHandler{
    RLog(@"session manual close");
    UH_UITapGestureRecognizer *gestureRecognizer = [UH_SessionManager.gestureController gesture];
    
    [gestureRecognizer startProgressGesture];
    
    
    UH_ActionFlow *actionFlow = [UH_SessionManager.currentSession genActionFlowWillResignActive:UHNowTimeStamp];
    [UH_SessionManager.currentSession insertActionFlow:actionFlow];
    
    [UH_SessionManager sessionCloseWithUploadData:isUpload completeHandler:completeHandler];
}

//todo::추후 메소드 삭제 

+(void)sessionCloseWithUploadData:(BOOL)isUpload completeHandler:(void(^_Nullable)(void))completeHandler{
    [[self sharedInstance] sessionCloseWithUploadData:isUpload completeHandler:completeHandler];
}

-(void)setScreen:(UIViewController * _Nonnull)viewController withName:(NSString * _Nonnull)subViewName{
    if (![viewController isKindOfClass:[UIViewController class]]) {
        [UH_Util logDebugType:UHDebugTypeCrash withMessage:[NSString stringWithFormat:@"Incompatible pointer types sending '%@ *' to parameter of type 'UIViewController *'", [viewController class]]];
        return;
    }
    
    if (!UH_SessionManager.gestureController) {
        UH_SessionManager.gestureController = [UH_GestureController new];
        [UH_SessionManager.gestureController gestureSetting];
    }
    
    @try {
        //subViewName의 유효성 검사
        
        NSString *checkString = [UH_Util checkScreenName:subViewName];
        if (checkString) {
            subViewName = checkString;
        }
        
        RLog(@"%@", subViewName);
        if ([[UH_SessionController sharedInstance].nowShowController isEqualToString:subViewName]) {
            return;
        }
        
        if (![subViewName isEqualToString:[UH_SessionController sharedInstance].nowShowController]) {
            if (subViewName.length > 100) {
                subViewName = [subViewName substringFromIndex:99];
            }
            [UH_SessionController sharedInstance].nowShowController = subViewName;
                [UH_VCManager addViewController:viewController
                                 withScreenName:subViewName
                                  withIsSubView:YES];
        }

    } @catch (NSException *exception) {
        [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                   typeOf:UHInnerDataTypeTryCatch
                                               actionFlow:nil];
        [UH_Util logDebugType:UHDebugTypeWarning withMessage:@"-setScreen:withName: error. Please, Check the sender object"];
    }
}

+(void)setScreen:(UIViewController * _Nonnull)viewController withName:(NSString * _Nonnull)subViewName{
    [[self sharedInstance]setScreen:viewController withName:subViewName];
}

/*
 *scrollView.delegate를 가져와서 이벤트를 가져오면 사용자가 delegate를 사용 할 경우
 */
//TODO:rootViewController 삭제하고 자동으로 할 수 있도록 하기
+(void)addScrollView:(NSString * _Nonnull)scrollViewName scrollView:(UIScrollView * _Nonnull)scrollView rootViewController:(UIViewController *)rootViewController{
    
    if (![scrollView isKindOfClass:[UIScrollView class]]) {
        [UH_Util logDebugType:UHDebugTypeWarning withMessage:@"-setScreen:withName: error. Please, Check the sender object"];
        return;
    }
    @try {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"scrollViewName", scrollViewName];
        UH_ScrollViewInfo *scrollInfo = [[UH_SessionManager.currentSession.scrollViewArray filteredArrayUsingPredicate:predicate] firstObject];

        if (!scrollInfo) {
            scrollInfo = [UH_ScrollViewInfo new];
            scrollInfo.mapping = (int)[UH_SessionManager.currentSession.scrollViewArray count];
            scrollInfo.scrollViewName = scrollViewName;
        }
        
        BOOL isAddScrollView = YES;         //스크롤뷰 중복 등록 체크
        
        if ([UH_SessionManager.currentSession.scrollViewArray firstObject]) {
            for (UH_ScrollViewInfo *inScrollInfo in UH_SessionManager.currentSession.scrollViewArray) {
                if (inScrollInfo.superViewController == rootViewController) {
                    isAddScrollView = NO;
                }
            }
        }
        
        if (isAddScrollView) {
            scrollInfo.scrollView = scrollView;
            scrollInfo.superViewController = rootViewController;
            if (![UH_SessionManager.currentSession.scrollViewArray containsObject:scrollInfo]) {
                [UH_SessionManager.currentSession.scrollViewArray addObject:scrollInfo];
            }
        } else {
            [UH_Util logDebugType:UHDebugTypeWarning withMessage:[NSString stringWithFormat:@"Only one scrollView(\"%@\") can be registered on one Screen", scrollViewName]];
        }

    } @catch (NSException *exception) {
        [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                   typeOf:UHInnerDataTypeTryCatch
                                               actionFlow:nil];
    }
}

+(void)addWebView:(WKWebView *)webView{

    if (!UH_VCManager.webViewController) {
        RLog(@"웹뷰 컨트롤러 생성");
        [UH_VCManager createWebViewController];
    }
    
    [UH_FileController checkHybridJSFile];
    [UH_VCManager.webViewController settingWKWebView:webView];
}

-(void)setSessionDelayTime:(float)delayInterval
{
    if (delayInterval <= 1.0) {
        NSLog(@"UserHabit : Invalid value for session timeout. Resetting with default value: 10s.");
    }else{
        [UH_SessionController sharedInstance].sessionDelayTime = @(delayInterval);
    }
}

+(void)setSessionDelayTime:(float)delayInterval{
    [[self sharedInstance] setSessionDelayTime:delayInterval];
}

-(void)takeScreenShot:(id _Nonnull)sender{
    RLog(@"%@", sender);
    @try {
        if ([UH_SessionController sharedInstance].isDevelopMode) {
            [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(takeScreenShotProcess:) userInfo:@{@"sender":sender} repeats:NO];
        }
    } @catch (NSException *exception) {
        [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                   typeOf:UHInnerDataTypeTryCatch
                                               actionFlow:nil];
    }
}

+(void)takeScreenShot:(id _Nonnull)sender{
    [[self sharedInstance] takeScreenShot:sender];
}

-(void)takeScreenShotProcess:(NSTimer *)timer{
    RLog_ViewCycle;
    
    id sender = timer.userInfo[@"sender"];
    __weak UIViewController *viewController;
    
    if ([sender isKindOfClass:[UIView class]]) {
        viewController = [UH_Util detectViewControllerClass:sender];
    } else if ([sender isKindOfClass:[UIViewController class]]) {
        viewController = sender;
    }
    
    RLog(@"%@", UH_VCManager.screenHistoryList);
    RLog(@"%@", viewController);
    RLog(@"%@", UH_SessionManager.nowShowController);
    
    
    UH_ScreenInfo *screenInformation = UH_VCManager.screenHistoryList[UH_SessionManager.nowShowController];
    if (screenInformation) {
        screenInformation.isCompleteUploadScreenshot = NO;
        screenInformation.isTakeScreenshot = YES;
        [UH_ImageManager renderTakeScreenShot:screenInformation isForce:YES];
    }
    
    //오브젝트 스크린샷 재 촬영
    UH_ObjectController *manager = [UH_ObjectController new];
    UH_ImageManager.forceCapture = YES;
    
    [manager readAllObject:viewController
         completionHandler:^(NSException *exception) {
             if (!exception) {
                 [[UH_SessionManager connectionController] chkSendObjectInfo:UH_SessionManager.nowShowController];
             }
     }];
}

-(void)excludeClasses:(NSArray<__kindof NSString *> * _Nonnull)excludeArray{
    if (!excludeArray ||
        ![excludeArray firstObject] ||
        ![excludeArray isKindOfClass:[NSArray class]]) {
        [UH_Util logDebugType:UHDebugTypeWarning withMessage:[NSString stringWithFormat:@"Failed. can't read excludeArray %@", excludeArray]];
        return;
    }
    [UH_ViewControllerManager sharedInstance].excludeArray = [NSArray arrayWithArray:excludeArray];
}

+(void)excludeClasses:(NSArray<__kindof NSString *> * _Nonnull)excludeArray{
    [[self sharedInstance]excludeClasses:excludeArray];
}

+(void)enableDeviceRandomID{

    [UH_SessionController sharedInstance].isDeviceRandomId = YES;
}

//todo:deprecated 해두었음 나중에 삭제하기
- (void)setScreenshotCaptureMode:(BOOL)mode
{
//    [UH_SessionController sharedInstance].isScreenshotCaptureMode = mode;
}

- (void)setContent:(NSString * _Nonnull)action with:(NSString * _Nonnull)contentName{
    UH_ContentInfo *contentInfo = [[UH_ContentInfo alloc] initAction:action
                                                        conentNameOf:contentName];
    UH_ActionFlow *actionFlow = [[UH_SessionManager currentSession] genActionFlowContentTracking:UHNowTimeStamp
                                                                                            with:contentInfo];

    [[UH_SessionManager currentSession] insertActionFlow:actionFlow];
}



+(void)setContentKey:(NSString * _Nonnull)key value:(NSString * _Nonnull)value{
    [[self sharedInstance]setContent:key with:value];
}

-(void)addSecretView:(id)viewObject{
    if ([viewObject isKindOfClass:[UIView class]]) {
        [UH_VCManager.secretViewSet addObject:viewObject];
    } else if ([viewObject isKindOfClass:[UIViewController class]]){
        UIViewController *viewController = viewObject;
        [UH_VCManager.secretViewSet addObject:viewController.view];
    } else {
        [UH_Util logDebugType:UHDebugTypeUserHabit withMessage:@"Only UIView or UIViewController is available for addSecretView"];
        return;
    }
}

+(void)addSecretView:(id)viewObject{
    [[self sharedInstance] addSecretView:viewObject];
}

+ (void)secretMode:(BOOL)value{
    UH_VCManager.secretMode = value;
}

-(void)setUserKey:(UHUserInformationKey _Nonnull )key value:(id _Nullable )value{
    
    if (!UH_SessionManager.userInformation) {
        UH_SessionManager.userInformation = [UH_UserInfo new];
    }
    RLog(@"%@ / %@", key, [value class]);
    if([key isEqualToString:UHUserInformationAge]){
        UH_SessionManager.userInformation.age = [value intValue];
    } else if ([key isEqualToString:UHUserInformationName]){
        UH_SessionManager.userInformation.customUserId = value;
    } else if ([key isEqualToString:UHUserInformationType]){
        UH_SessionManager.userInformation.customUserType = [value intValue];
    } else if ([key isEqualToString:UHUserInformationGender]){
        UH_SessionManager.userInformation.gender = [value intValue];
    }
    
    [[UH_SessionController sharedInstance] saveUserInfo];
}

+(void)setUserKey:(UHUserInformationKey _Nonnull )key value:(id _Nullable )value{
    [[self sharedInstance] setUserKey:key value:value];
}

+ (void)setDebug:(BOOL)debug{
    [UH_SessionController sharedInstance].isDebug = debug;
}

+ (void)setUserhabitOption:(NSDictionary *)options{

    if (!UH_SessionManager.options) {
        UH_SessionManager.options = [NSMutableDictionary new];
        //옵션 기본값
        UH_SessionManager.options[UHOptionTrackingException] = @YES;
    }

    if ([options[UHOptionScreenshotTapticFeedback] isEqualToNumber:@YES] ||
        [options[UHOptionScreenshotTapticFeedback] isEqualToNumber:@NO]) {
        UH_SessionManager.options[UHOptionScreenshotTapticFeedback] = options[UHOptionScreenshotTapticFeedback];
    } else if (options[UHOptionScreenTime]){
        if ([options[UHOptionScreenTime] isKindOfClass:[NSNumber class]]) {
            UH_SessionManager.options[UHOptionScreenTime] = options[UHOptionScreenTime];
        } else {
            [UH_Util logDebugType:UHDebugTypeWarning withMessage:@"UHOptionScreenTime is Only NSNumber"];
        }
    } else if ([options[UHOptionTrackingException] isEqualToNumber:@YES] ||
        [options[UHOptionTrackingException] isEqualToNumber:@NO]) {
        UH_SessionManager.options[UHOptionTrackingException] = options[UHOptionTrackingException];
    } else if (options[UHOptionRootWindow]){
        
        
        if ([options[UHOptionRootWindow] isKindOfClass:[UIWindow class]]) {
            UH_SessionManager.options[UHOptionRootWindow] = options[UHOptionRootWindow];
            [UH_SessionManager.gestureController windowSetting:options[UHOptionRootWindow]];
            UH_SessionManager.rootWindow = options[UHOptionRootWindow];
            //debug::기본값 비활성
//            [[UH_ManualCaptureController sharedInstance] showCaptureMenuView];
        } else {
            [UH_Util logDebugType:UHDebugTypeWarning withMessage:@"UHOptionRootWindow is Only UIWindow"];
        }
    }
    RLog(@"%@" ,UH_SessionManager.options);
}

- (void)version{
    [UserHabit version];
}
+(void)version{
    //update:: 버젼 올리기
    [UH_Util logDebugType:UHDebugTypeUserHabit withMessage:@"1.1.15(73)"];
}

@end
