//
//  UncaughtExceptionHandler.m
//  UncaughtExceptions
//
//  Created by Matt Gallagher on 2010/05/25.
//  Copyright 2010 Matt Gallagher. All rights reserved.
//
//  Permission is given to use this source code file, free of charge, in any
//  project, commercial or otherwise, entirely at your risk, with the condition
//  that any redistribution (in part or whole) of source code must retain
//  this copyright and permission notice. Attribution in compiled projects is
//  appreciated but not required.
//

#import "UH_UncaughtExceptionHandler.h"
#include <libkern/OSAtomic.h>
#include <execinfo.h>
#import "UserHabitConstant.h"
#import "UH_Util.h"
#import "UH_SessionController.h"
#import "UH_Session.h"
#import "UH_SystemServices.h"
#import "UH_CrashInfo.h"

NSString * const UncaughtExceptionHandlerSignalExceptionName = @"UncaughtExceptionHandlerSignalExceptionName";
NSString * const UncaughtExceptionHandlerSignalKey = @"UncaughtExceptionHandlerSignalKey";
NSString * const UncaughtExceptionHandlerAddressesKey = @"UncaughtExceptionHandlerAddressesKey";

volatile int32_t UncaughtExceptionCount = 0;
const int32_t UncaughtExceptionMaximum = 10;

const NSUInteger UncaughtExceptionHandlerSkipAddressCount = 4;
const NSUInteger UncaughtExceptionHandlerReportAddressCount = 20;


@interface UH_UncaughtExceptionHandler()
@property (nonatomic, strong) NSException *thisException;
@end

@implementation UH_UncaughtExceptionHandler

//static void dumpThreads(void) {
//    char name[256];
//    mach_msg_type_number_t count;
//    thread_act_array_t list;
//    task_threads(mach_task_self(), &list, &count);
//    for (int i = 0; i < count; ++i) {
//        pthread_t pt = pthread_from_mach_thread_np(list[i]);
//        if (pt) {
//            name[0] = '\0';
//            int rc = pthread_getname_np(pt, name, sizeof name);
//            NSLog(@"mach thread %u: getname returned %d: %s", list[i], rc, name);
//        } else {
//            NSLog(@"mach thread %u: no pthread found", list[i]);
//        }
//    }
//}

+ (NSArray *)backtrace
{
	 void* callstack[128];
	 int frames = backtrace(callstack, 128);
	 char **strs = backtrace_symbols(callstack, frames);
	 
	 NSMutableArray *backtrace = [NSMutableArray arrayWithCapacity:frames];
	 for (
	 	NSUInteger i = UncaughtExceptionHandlerSkipAddressCount;
	 	i < UncaughtExceptionHandlerSkipAddressCount +
			UncaughtExceptionHandlerReportAddressCount;
		i++)
	 {
	 	[backtrace addObject:@(strs[i])];
	 }
	 free(strs);
	 
	 return backtrace;
}

- (void)validateAndSaveCriticalApplicationData
{
	UH_Session *session = [UH_SessionController sharedInstance].currentSession;
	UH_CrashInfo *crashInfo = [[UH_CrashInfo alloc] init];
    long long uptime = UHNowTimeStamp;
    
    // batteryLevel info
	crashInfo.battery =[[UH_SystemServices sharedServices] batteryLevel];
	crashInfo.charging = [[UH_SystemServices sharedServices] charging];
	crashInfo.stackTrace = @"No Stacktrace Data";
    
    // exception info
	NSException *thisException = self.thisException;

    if (thisException.reason) {
		crashInfo.crashMessage = thisException.reason;
    } else {
//        crashInfo.crashMessage = @"Unknown reason";
    }
    
    if (thisException.name) {
        if ([thisException.name isEqualToString:UncaughtExceptionHandlerSignalExceptionName]) {
            
            int signal = [thisException.userInfo[UncaughtExceptionHandlerSignalKey] intValue];
            
            switch (signal) {
                case SIGABRT: crashInfo.exceptionType = @"SIGABRT"; break;
                case SIGILL: crashInfo.exceptionType = @"SIGILL"; break;
                case SIGSEGV: crashInfo.exceptionType = @"SIGSEGV"; break;
                case SIGFPE: crashInfo.exceptionType = @"SIGFPE"; break;
                case SIGBUS: crashInfo.exceptionType = @"SIGBUS"; break;
                case SIGTRAP: crashInfo.exceptionType = @"SIGTRAP"; break;
                default: crashInfo.exceptionType = [NSString stringWithFormat:@"%d", signal];
            }

			crashInfo.stackTrace = [thisException.userInfo[UncaughtExceptionHandlerAddressesKey] componentsJoinedByString:@"\n"];
            
        } else {
			crashInfo.exceptionType = thisException.name;
			crashInfo.stackTrace = [thisException.userInfo[UncaughtExceptionHandlerAddressesKey] componentsJoinedByString:@"\n"];
        }
    }
    
    if (thisException.userInfo) {
        if (thisException.userInfo[UncaughtExceptionHandlerAddressesKey]) {
        }
    }

	// Jailbreak?
//	crashInfo.jailBreaking = [[UH_SystemServices sharedServices] jailbroken] != NOTJAIL;
    crashInfo.jailBreaking = NO;

	//Network Stauts
	crashInfo.networkStatus = [UH_Util networkStatus];
    
    //Process Count
	crashInfo.processes = (int)[[UH_SystemServices sharedServices] numberProcessors];
    
    //Total Disk
	crashInfo.totalDisk = [[UH_SystemServices sharedServices] longDiskSpace];
    
    //Disk Usage
	crashInfo.freeDisk = [[UH_SystemServices sharedServices] longFreeDiskSpace];
    
    //Total Memory
	crashInfo.totalMemory = (long long int) ([[UH_SystemServices sharedServices] totalMemory] * 1024LL * 1024LL);
    
    //Memory Usage
	crashInfo.freeMemory = (long long int) ([[UH_SystemServices sharedServices] usedMemoryinRaw] * 1024LL * 1024LL);

    [session insertActionFlow:[session genActionFlowCrash:uptime with:crashInfo]];
}

- (void)handleException:(NSException *)exception
{
    self.thisException = exception;
	[self validateAndSaveCriticalApplicationData];
//    [[UH_SessionController sharedInstance].opQueue waitUntilAllOperationsAreFinished];
    
	NSSetUncaughtExceptionHandler(NULL);
	signal(SIGABRT, SIG_DFL);
	signal(SIGILL, SIG_DFL);
	signal(SIGSEGV, SIG_DFL);
	signal(SIGFPE, SIG_DFL);
	signal(SIGBUS, SIG_DFL);
	signal(SIGTRAP, SIG_DFL);
	
	if ([exception.name isEqualToString:UncaughtExceptionHandlerSignalExceptionName]) {
		raise([exception.userInfo[UncaughtExceptionHandlerSignalKey] intValue]);
    } else {
        [exception raise];
    }
}

@end

static void HandleException(NSException *exception)
{
	int32_t exceptionCount = OSAtomicIncrement32(&UncaughtExceptionCount);
    
	if (exceptionCount > UncaughtExceptionMaximum)
	{
		return;
	}
	
	NSArray *callStack = exception.callStackSymbols;
	NSMutableDictionary *userInfo =
		[NSMutableDictionary dictionaryWithDictionary:exception.userInfo];
	userInfo[UncaughtExceptionHandlerAddressesKey] = callStack;
    
    
    [[UH_UncaughtExceptionHandler new] performSelectorOnMainThread:@selector(handleException:)
                                                        withObject:[NSException exceptionWithName:exception.name
                                                                                           reason:exception.reason
                                                                                         userInfo:userInfo]
                                                     waitUntilDone:YES];
}

static void SignalHandler(int signal)
{
	int32_t exceptionCount = OSAtomicIncrement32(&UncaughtExceptionCount);
	if (exceptionCount > UncaughtExceptionMaximum)
	{
		return;
	}
	
	NSArray *callStack = [UH_UncaughtExceptionHandler backtrace];
    NSDictionary *userInfo = @{UncaughtExceptionHandlerSignalKey: @(signal),
                               UncaughtExceptionHandlerAddressesKey: callStack};

	
	[[[UH_UncaughtExceptionHandler alloc] init]
		performSelectorOnMainThread:@selector(handleException:)
		withObject:
				[NSException
						exceptionWithName:UncaughtExceptionHandlerSignalExceptionName
								   reason:
										   [NSString stringWithFormat:
												   NSLocalizedString(@"Signal %d was raised.", nil),
												   signal]
								 userInfo: userInfo]
		waitUntilDone:YES];
}

void InstallUncaughtExceptionHandler()
{
    @try {
        NSSetUncaughtExceptionHandler(&HandleException);
        signal(SIGABRT, SignalHandler);
        signal(SIGILL, SignalHandler);
        signal(SIGSEGV, SignalHandler);
        signal(SIGFPE, SignalHandler);
        signal(SIGBUS, SignalHandler);
        signal(SIGTRAP, SignalHandler);
    } @catch (NSException *exception) {
        RLog(@"%@", exception);
    }
}

