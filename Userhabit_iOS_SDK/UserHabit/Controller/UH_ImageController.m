//
//  UH_ImageController.m
//  UserHabit
//
//  Created by lotco on 13/02/2019.
//  Copyright © 2019 Andbut. All rights reserved.
//

#import "UH_ImageController.h"

//controller
#import "UH_SessionController.h"
#import "UH_ViewControllerManager.h"
#import "UH_ActionController.h"

//model
#import "UH_ScreenInfo.h"
#import "UH_WKWebViewObject.h"
#import "UH_ObjectInfo.h"


@implementation UH_ImageController

+(instancetype)sharedInstance
{
    static UH_ImageController *singletonObject;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singletonObject = [[UH_ImageController alloc]init];
        singletonObject.screenShotSet = [NSMutableSet new];
        singletonObject.objectShotSet = [NSMutableDictionary new];
//        singletonObject.screenHistoryList = [NSMutableDictionary new];
    });
    
    return singletonObject;
}


- (void)renderScreenShot:(UH_ScreenInfo *)screenInfo{
    RLog_ViewCycle;
    if (![UH_SessionManager.currentSession chkTransportStatus:UHTransportMethodScreenshotInfo]) {
        return ;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(randerScreenShotProcess:) userInfo:@{@"screenInformation":screenInfo} repeats:NO];
        }
        @catch (NSException *exception) {
            [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                       typeOf:UHInnerDataTypeTryCatch
                                                   actionFlow:nil];
        }
    });
}


- (void)randerScreenShotProcess:(NSTimer *)timer{
    
    @try {
        UH_ScreenInfo *screenInfo = timer.userInfo[@"screenInformation"];
        UIViewController *rootController = UH_SessionManager.rootWindow.rootViewController;
        UIGraphicsBeginImageContextWithOptions(rootController.view.bounds.size, false, 0.0);
        UIViewController *presentController = nil;
        
        if (rootController.presentedViewController) {
            presentController = rootController.presentedViewController;
        } else if (rootController.navigationController
                   && rootController.navigationController.presentationController
                   && rootController.navigationController.presentationController.presentedViewController) {
            presentController = rootController.navigationController.presentationController.presentedViewController;
            
        } else if (rootController.tabBarController
                   && rootController.tabBarController.presentingViewController
                   && rootController.tabBarController.presentingViewController.presentedViewController) {
            
            presentController = rootController.tabBarController.presentationController.presentedViewController;
            
        } else {
            presentController = rootController;
        }
        
        //randerViewController.view.bounds를 사용하면 디바이스 해상도에 작은 화면이 강제로 늘어나서 적용됨, presentController.view.frame로 바꿀 경우 프레임 위치가 맞는 상태로 실제 크기로 적용됨
        [presentController.view drawViewHierarchyInRect:presentController.view.frame
                                     afterScreenUpdates:NO];
        //    [presentController.view drawViewHierarchyInRect:randerViewController.view.bounds
        //                                 afterScreenUpdates:NO];
        UIGraphicsBeginImageContextWithOptions(presentController.view.bounds.size, presentController.view.opaque, 0.0);
        [rootController.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();

        NSData *imageData = UIImageJPEGRepresentation(image, 0.1f);
        
        screenInfo.fileName = [UH_Util imageFileName];
        screenInfo.path = UH_SessionManager.userhabitScreenImagePath;
        if ([imageData writeToFile:[NSString stringWithFormat:@"%@/%@.jpg", screenInfo.path, screenInfo.fileName] atomically:YES]) {
            //todo 이미지를 파일로 저장하지 않고 바로 전송하기
            [UH_SessionManager.connectionController chkSendScreenInfo];
            [UH_Util logDebugType:UHDebugTypeScreenCapture withMessage:[NSString stringWithFormat:@"%@",screenInfo.screenName]];
        }
    } @catch (NSException *exception) {
        
        [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                   typeOf:UHInnerDataTypeTryCatch
                                               actionFlow:nil];
    }
}

-(void)renderTakeScreenShot:(UH_ScreenInfo *)screenInformation isForce:(BOOL)isForce{
    RLog(@"%@", [screenInformation build]);
    
    if (![[UH_SessionController sharedInstance].currentSession chkTransportStatus:UHTransportMethodScreenshotInfo]) {
        return ;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            if ([self chkScreenShot:[UH_SessionController sharedInstance].nowShowController with:YES] ||
                isForce) {
                UIViewController *rootcontroller = UH_SessionManager.rootWindow.rootViewController;
                UIGraphicsBeginImageContextWithOptions(rootcontroller.view.bounds.size, rootcontroller.view.opaque, 0.0f);
                [rootcontroller.view drawViewHierarchyInRect:rootcontroller.view.bounds afterScreenUpdates:NO];
                
                UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                NSData *imageData = [[NSData alloc]initWithData:UIImageJPEGRepresentation(img, 0.1f)];
                
                screenInformation.path = [UH_SessionController sharedInstance].userhabitScreenImagePath;
                screenInformation.fileName = [UH_Util imageFileName];
                [imageData writeToFile:[NSString stringWithFormat:@"%@/%@.jpg", screenInformation.path, screenInformation.fileName] atomically:YES];
                [UH_SessionManager.connectionController chkSendScreenInfo];
                RLog_ViewCycle;
            }
        }
        @catch (NSException *exception) {
            RLog(@"%@", exception);
            [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                       typeOf:UHInnerDataTypeTryCatch
                                                   actionFlow:nil];
        }
    });
}

-(BOOL)chkScreenShot:(NSString *)chkName with:(BOOL)isScreen{
    
//        RLog(@"%@ / %d / %d", chkName, isScreen, _screenShotSet.count);
    
    @try {
        NSArray<UH_ScreenInfo *> *screenshotArray = [_screenShotSet allObjects];
        BOOL returnValue = YES;
        if ([UH_SessionController sharedInstance].isDevelopMode) {
            if (isScreen) {
                for (NSUInteger i = 0; i < screenshotArray.count; i++) {
                    UH_ScreenInfo *screenInformation = screenshotArray[i];
                    if (([chkName isEqualToString:screenInformation.fragment] ||
                         [chkName isEqualToString:screenInformation.screenName]) &&
                        UH_SessionManager.nowOrientation == (UHScreenOrientation)screenInformation.orientation) {
                        returnValue = NO;
                        break;
                    }
                }
            }else{
                if (_objectShotSet[UH_SessionManager.nowShowController]) {
                    BOOL isCompare = YES;
                    for (NSUInteger i = 0; i < [_objectShotSet[UH_SessionManager.nowShowController] count]; i++) {
                        if ([_objectShotSet[UH_SessionManager.nowShowController][i] isEqualToString:chkName]) {
                            isCompare = NO;
                            break;
                        }
                    }
                    
                    returnValue = isCompare;
                }
            }
        }else{
            returnValue = NO;
        }
        //        RLog(@"%d", returnValue);
        return returnValue;
    } @catch (NSException *exception) {
        [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                   typeOf:UHInnerDataTypeTryCatch
                                               actionFlow:nil];
        
        return NO;
    }
}


#pragma mark - scroll capture
//scroll capture
- (void)scrollCaptureViewProcess:(RCompleteHandlerBlock)completeHandler{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"superViewController",UH_VCManager.visibleViewController];
    
    UH_ScrollViewInfo *scrollInfo = [[UH_SessionManager.currentSession.scrollViewArray filteredArrayUsingPredicate:predicate] firstObject];
    
    if (!scrollInfo.images) {
        scrollInfo.images = [NSMutableArray new];
    }
    
    if (scrollInfo) {
        UH_SessionManager.rootWindow.userInteractionEnabled = NO;
        
        UIScrollView *captureScrollView = scrollInfo.scrollView;
        static int h;
        h = 0;
        [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
            RLog(@"timer h %d" , h);
            float offsetCorrectionHeight;
            
            if (UH_SessionManager.rootWindow.frame.size.height == captureScrollView.frame.size.height) {
                offsetCorrectionHeight = captureScrollView.frame.size.height - scrollInfo.superViewController.topLayoutGuide.length - scrollInfo.superViewController.bottomLayoutGuide.length;
            } else {
                offsetCorrectionHeight = captureScrollView.frame.size.height;
            }
            
            
            
            [captureScrollView setContentOffset:CGPointMake(captureScrollView.contentOffset.x, captureScrollView.contentOffset.y + offsetCorrectionHeight) animated:YES];
            
            UIImage *image = [self imageWithView:captureScrollView withCorrectionHeightValue:offsetCorrectionHeight];
            
            NSData *imageData = UIImageJPEGRepresentation(image, 0.1f);
            NSString *screenImagePath = UH_SessionManager.userhabitScreenImagePath;
            NSString *path = [NSString stringWithFormat:@"%@/%d.jpg", screenImagePath, h];
            [imageData writeToFile:path atomically:YES];
            
            // Save image.
            [UIImagePNGRepresentation(image) writeToFile:path atomically:YES];
            
            UH_ScrollImageInformation *info = [[UH_ScrollImageInformation alloc]initWithIndex:h path:path size:image.size];
            [scrollInfo.images addObject:info];
            h++;
            
            if (captureScrollView.contentOffset.y > captureScrollView.contentSize.height - offsetCorrectionHeight ||
                h >= 10) {
                [timer invalidate];
               
                completeHandler(scrollInfo,nil);
                
                UH_ActionController *actionController = [UH_ActionController new];
                [actionController processMaximumScrollView:captureScrollView];
                UH_SessionManager.rootWindow.userInteractionEnabled = YES;
            } else {
                
            }
            
            if (h == 10) {
                [UH_Util logDebugType:UHDebugTypeWarning withMessage:@"Up to 10 ScrollViewImage are possible."];
            }
        }];
    } else {
        RLog(@" Can not read screenshot");
    }
}

- (UIImage *)imageWithView:(UIScrollView *)view withCorrectionHeightValue:(int)correctionHeight {
    static int offsetCorrectionValue = -1;
    int viewContentsOffsetY;
    
    if (view.contentOffset.y < 0) {
        if (offsetCorrectionValue == -1) {
            offsetCorrectionValue = view.contentOffset.y;
        }
        viewContentsOffsetY = view.contentOffset.y + offsetCorrectionValue;
        
    } else {
        viewContentsOffsetY = view.contentOffset.y;
    }
    
    CGSize captureSize;
    if (view.contentSize.height - view.contentOffset.y < view.bounds.size.height) {
        captureSize = CGSizeMake(view.bounds.size.width, view.contentSize.height - view.contentOffset.y + offsetCorrectionValue);
        offsetCorrectionValue = offsetCorrectionValue ;
    } else {
        captureSize = CGSizeMake(view.bounds.size.width, correctionHeight);
    }
    
    UIGraphicsBeginImageContextWithOptions(captureSize, YES, [UIScreen mainScreen].scale);
    CGPoint offset = view.contentOffset;
    CGContextTranslateCTM(UIGraphicsGetCurrentContext(), -offset.x, -offset.y + offsetCorrectionValue);
    
    if (captureSize.width <= 0 ||
        captureSize.height <= 0) {
        return nil;
    }
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}



//todo::스크롤뷰 하나로 합치기
-(void)webviewCaptureViewProcess:(UH_WebViewObject *)webObject{
    UH_SessionManager.rootWindow.userInteractionEnabled = NO;
    UIScrollView *scrollView;
    
    UH_WKWebViewObject *inWebViewObject = (UH_WKWebViewObject *)webObject;
    RLog(@"wk webview");
    scrollView = inWebViewObject.webView.scrollView;
    
    webObject.imageCaptureCount = 0;
    NSMutableArray *imageArray = [NSMutableArray new];
    
    [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
        //        RLog(@"name %@ timer h %d" , webObject.name ,webObject.imageCaptureCount);
        float offsetCorrectionHeight;
        offsetCorrectionHeight = scrollView.frame.size.height;
        
        [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, scrollView.contentOffset.y + offsetCorrectionHeight) animated:YES];
        
        UIImage *image = [self imageWithView:scrollView withCorrectionHeightValue:offsetCorrectionHeight];
        
        if (image) {
            [imageArray addObject:image];
            webObject.imageCaptureCount++;
            
        } else {
            [timer invalidate];
        }
        
        if (scrollView.contentOffset.y > scrollView.contentSize.height - offsetCorrectionHeight ||
            webObject.imageCaptureCount >= 100) {
            [timer invalidate];
            UIImage *marageImage = [UIImage new];
            
            int q = 0;
            for (UIImage *inImage in imageArray) {
                if (inImage == [imageArray firstObject]) {
                    marageImage = inImage;
                } else {
                    marageImage = [UH_Util addImage:marageImage toImage:inImage];
                    q++;
                }
            }
            
            NSData *imageData = UIImageJPEGRepresentation(marageImage, 0.1f);
            NSString *screenImagePath = UH_SessionManager.userhabitScreenImagePath;
            //todo::이미지 파일 이름 변경
            NSString *path = [NSString stringWithFormat:@"%@/%@.jpg", screenImagePath, webObject.name];
            [imageData writeToFile:path atomically:YES];
            
            // Save image.
            [UIImagePNGRepresentation(marageImage) writeToFile:path atomically:YES];
            
            
            [UH_VCManager.webViewController cropObjectToWebviewImage:marageImage withWebObject:webObject];
            
            UH_SessionManager.rootWindow.userInteractionEnabled = YES;
        }
        
    }];
}



#pragma mark - object

-(void)takeObjectScreenShot:(UH_ObjectInfo *)objectInformation completeHandler:(RActionBlock)completeHandler{

    RLog_ViewCycle;
    
    @try {
        if ([UH_ImageManager chkScreenShot:objectInformation.fileName with:NO] ||
            _forceCapture) {
            
            if (UH_SessionManager.currentSession &&
                [UH_SessionManager.currentSession chkTransportStatus:UHTransportMethodScreenshotInfo]) {
                
                //객체가 버튼일 경우
                if ([objectInformation.objectView isKindOfClass:[UIButton class]]) {
                    UIButton *btn = (UIButton *)objectInformation.objectView;
                    NSString *objectImagePath = [NSString stringWithFormat:@"%@/%@.jpg", [UH_SessionController sharedInstance].userhabitObjectImagePath, objectInformation.fileName];
                    RLog(@"%@", objectImagePath);
                    
                    if([btn imageForState:UIControlStateNormal] ||
                       [btn backgroundImageForState:UIControlStateNormal]){
                        NSData *imageData;
                        
                        if([btn imageForState:UIControlStateNormal]){
                            imageData = [NSData dataWithData:UIImageJPEGRepresentation([btn imageForState:UIControlStateNormal], 0.1f)];
                        } else {
                            imageData = [NSData dataWithData:UIImageJPEGRepresentation([btn backgroundImageForState:UIControlStateNormal], 0.1f)];
                        }
                        if([imageData writeToFile:objectImagePath atomically:YES]){
                            completeHandler(nil);
                        }
                    }else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self viewAsImage:objectInformation completeHandler:^(NSError *error) {
                                completeHandler(error);
                            }];
                        });
                    }
                } else {
                    if ([objectInformation.objectView isKindOfClass:[UITextField class]] ||
                        [objectInformation.objectView isKindOfClass:[UITextView class]]){
                        CGRect convertRect = [objectInformation.objectView convertRect:objectInformation.objectView.bounds toView:[UIApplication sharedApplication].keyWindow];
                        CGRect objectRect = CGRectMake(convertRect.origin.x - 5, convertRect.origin.y - 5, objectInformation.objectView.frame.size.width + 5, objectInformation.objectView.frame.size.height + 5);
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self cropImage:objectRect withObjectName:objectInformation.fileName];
                        });
                    }else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self viewAsImage:objectInformation completeHandler:^(NSError *error) {
                                completeHandler(nil);
                            }];
                        });
                    }
                }
            }
        }
    } @catch (NSException *exception) {
        [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                   typeOf:UHInnerDataTypeTryCatch
                                               actionFlow:nil];
    }
}



-(void)viewAsImage:(UH_ObjectInfo *)objectInformation completeHandler:(RActionBlock)completeHandler{
    RLog(@"%@", objectInformation.fileName);
    UIGraphicsBeginImageContextWithOptions(objectInformation.objectView.bounds.size, objectInformation.objectView.opaque, 0.0f);
    [objectInformation.objectView drawViewHierarchyInRect:objectInformation.objectView.bounds afterScreenUpdates:NO];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSString *objectImagePath = [UH_SessionController sharedInstance].userhabitObjectImagePath;
    NSData *imageData = [NSData dataWithData:UIImageJPEGRepresentation(img, 0.1f)];
    //todo::이미지 파일 이름 변경
    
    if([imageData writeToFile:[NSString stringWithFormat:@"%@/%@.jpg", objectImagePath, objectInformation.fileName] atomically:YES]){
        completeHandler(nil);
    }
}



-(void)cropImage:(CGRect)objectFrame withObjectName:(NSString *)objectName
{
    RLog(@"%@", objectName);
    UIImage *controllerImage;
    NSString *screenImagePath = [UH_SessionController sharedInstance].userhabitScreenImagePath;
    NSString *objectImagePath = [UH_SessionController sharedInstance].userhabitObjectImagePath;
    
    if ([[NSFileManager defaultManager]fileExistsAtPath:[NSString stringWithFormat:@"%@/%@.jpg", screenImagePath, [UH_SessionController sharedInstance].nowShowController]]) {
        controllerImage = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@.jpg", screenImagePath, [UH_SessionController sharedInstance].nowShowController]];
    }else{
        controllerImage = [UH_ImageManager captureControllerImage];
    }
    
    CGRect cropFrame = CGRectMake(objectFrame.origin.x * controllerImage.scale,
                                  objectFrame.origin.y * controllerImage.scale,
                                  objectFrame.size.width * controllerImage.scale,
                                  objectFrame.size.height * controllerImage.scale);
    CGImageRef imageRef = CGImageCreateWithImageInRect(controllerImage.CGImage, cropFrame);
    UIImage *result = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    NSData *imageData = [NSData dataWithData:UIImageJPEGRepresentation(result, 0.1f)];
    //todo::이미지 파일 이름 변경
    [imageData writeToFile:[NSString stringWithFormat:@"%@/%@.jpg", objectImagePath, objectName] atomically:YES];
    
}

-(UIImage *)captureControllerImage
{
//    UIGraphicsBeginImageContextWithOptions([UIScreen mainScreen].bounds.size, showController.view.opaque, 0.0);/
    UIGraphicsBeginImageContextWithOptions([UIScreen mainScreen].bounds.size, NO, 0.0);
    [[UIApplication sharedApplication].keyWindow.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}



@end
