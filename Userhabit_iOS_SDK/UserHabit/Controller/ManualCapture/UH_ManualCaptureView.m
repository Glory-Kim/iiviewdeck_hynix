//
//  UH_ManualCaptureView.m
//  UserHabit
//
//  Created by lotco on 2017. 9. 19..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import "UH_ManualCaptureView.h"
#import "UH_ViewControllerManager.h"
#import "UH_SessionController.h"

@implementation UH_ManualCaptureView
-(void)setting{
    RLog_ViewCycle;
    self.frame = CGRectMake(0, 100, 60, 420);
//    self.backgroundColor = [UIColor brownColor];
    if (!buttonAreaView) {
        buttonAreaView = [UIView new];
        buttonAreaView.frame = CGRectMake(0, 0, 60, 340);
        buttonAreaView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.9f];
        buttonAreaView.layer.cornerRadius = 8;
        buttonAreaView.layer.borderColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f].CGColor;
        buttonAreaView.layer.borderWidth = 0.5f;
        [self addSubview:buttonAreaView];
    }
    
    
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panGesture:)];
    [self addGestureRecognizer:panGesture];
    
    informationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [informationButton setTitle:@"i" forState:UIControlStateNormal];
    [informationButton addTarget:self action:@selector(informationButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [informationButton setFrame:CGRectMake(5, UHCaptureButtonVerticalPadding, 50, 50)];
    [informationButton setBackgroundColor:[UIColor colorWithRed:0.68 green:0.68 blue:0.68 alpha:1]];
    informationButton.layer.cornerRadius = 25;
    [buttonAreaView addSubview:informationButton];
    
    informationLabel = [UILabel new];
    informationLabel.frame = CGRectMake(0, informationButton.frame.origin.y + informationButton.frame.size.height + UHCaptureLabelVerticalPadding, 0, 0);
    [informationLabel setText:@"정보 Off"];
    [informationLabel setFont:[UIFont systemFontOfSize:11]];
    [informationLabel sizeToFit];
    informationLabel.center = CGPointMake(informationButton.center.x, informationLabel.center.y);
    [buttonAreaView addSubview:informationLabel];
    
    UIButton *normalCaptureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [normalCaptureButton setTitle:@"+" forState:UIControlStateNormal];
    [normalCaptureButton addTarget:self action:@selector(normalCaptureButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [normalCaptureButton setFrame:CGRectMake(5, informationLabel.frame.origin.y + informationLabel.frame.size.height + UHCaptureButtonVerticalPadding, 50, 50)];
    [normalCaptureButton setBackgroundColor:[UIColor colorWithRed:0.314 green:0.627 blue:0.961 alpha:1.0]];
    normalCaptureButton.layer.cornerRadius = 25;
    [buttonAreaView addSubview:normalCaptureButton];

    UILabel *normalCaptureLabel = [UILabel new];
    normalCaptureLabel.frame = CGRectMake(0, normalCaptureButton.frame.origin.y + normalCaptureButton.frame.size.height + UHCaptureLabelVerticalPadding, 0, 0);
    [normalCaptureLabel setText:@"일반화면 수집"];
    [normalCaptureLabel setFont:[UIFont systemFontOfSize:10]];
    [normalCaptureLabel sizeToFit];
    normalCaptureLabel.center = CGPointMake(normalCaptureButton.center.x, normalCaptureLabel.center.y);
    [buttonAreaView addSubview:normalCaptureLabel];
    
    
    scrollCaptureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [scrollCaptureButton setTitle:@"+" forState:UIControlStateNormal];
    [scrollCaptureButton addTarget:self action:@selector(scrollCaptureButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [scrollCaptureButton setFrame:CGRectMake(5, normalCaptureLabel.frame.origin.y + normalCaptureLabel.frame.size.height + UHCaptureButtonVerticalPadding, 50, 50)];
    [scrollCaptureButton setBackgroundColor:[UIColor colorWithRed:0.68 green:0.68 blue:0.68 alpha:1]];
    scrollCaptureButton.layer.cornerRadius = 25;
    [buttonAreaView addSubview:scrollCaptureButton];
    
    UILabel *scrollCaptureLabel = [UILabel new];
    scrollCaptureLabel.frame = CGRectMake(0, scrollCaptureButton.frame.origin.y + scrollCaptureButton.frame.size.height + UHCaptureLabelVerticalPadding, 0, 0);
    [scrollCaptureLabel setText:@"스크롤\n화면수집"];
    scrollCaptureLabel.numberOfLines = 0;
    scrollCaptureLabel.textAlignment = NSTextAlignmentCenter;
    [scrollCaptureLabel setFont:[UIFont systemFontOfSize:10]];
    [scrollCaptureLabel sizeToFit];
    scrollCaptureLabel.center = CGPointMake(scrollCaptureButton.center.x, scrollCaptureLabel.center.y);
    [buttonAreaView addSubview:scrollCaptureLabel];

    webviewCaptureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [webviewCaptureButton setTitle:@"+" forState:UIControlStateNormal];
    [webviewCaptureButton addTarget:self action:@selector(webviewCaptureButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [webviewCaptureButton setFrame:CGRectMake(5,scrollCaptureLabel.frame.origin.y + scrollCaptureLabel.frame.size.height + UHCaptureButtonVerticalPadding, 50, 50)];
    [webviewCaptureButton setBackgroundColor:[UIColor colorWithRed:0.68 green:0.68 blue:0.68 alpha:1]];
    webviewCaptureButton.layer.cornerRadius = 25;
    [buttonAreaView addSubview:webviewCaptureButton];
    
    UILabel *webviewCaptureLabel = [UILabel new];
    webviewCaptureLabel.frame = CGRectMake(0, webviewCaptureButton.frame.origin.y + webviewCaptureButton.frame.size.height + UHCaptureLabelVerticalPadding, 0, 0);
    [webviewCaptureLabel setText:@"웹뷰\n화면수집"];
    webviewCaptureLabel.numberOfLines = 0;
    webviewCaptureLabel.textAlignment = NSTextAlignmentCenter;
    [webviewCaptureLabel setFont:[UIFont systemFontOfSize:10]];
    [webviewCaptureLabel sizeToFit];
    webviewCaptureLabel.center = CGPointMake(webviewCaptureButton.center.x, webviewCaptureLabel.center.y);
    [buttonAreaView addSubview:webviewCaptureLabel];
    
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeButton setTitle:@"X" forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(closeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [closeButton setFrame:CGRectMake(5, buttonAreaView.frame.size.height + 20, 55, 55)];
    [closeButton setBackgroundColor:[UIColor colorWithRed:0.18 green:0.18 blue:0.18 alpha:1]];
    closeButton.layer.cornerRadius = 55/2;
    [self addSubview:closeButton];
    
    [self reloadInformation];
}
-(void)closeButtonAction:(id)sender{
    if ([_delegate respondsToSelector:@selector(manualCaptureViewDelegate:closeButtonAction:)]) {
        [_delegate manualCaptureViewDelegate:self closeButtonAction:sender];
    }
}


-(void)informationButtonAction:(id)sender{
    RLog(@"정보!");
    
    if (informationButton.selected) {
        [informationLabel setText:@"정보 Off"];
        [informationButton setBackgroundColor:[UIColor colorWithRed:0.68 green:0.68 blue:0.68 alpha:1]];
    } else {
        [informationLabel setText:@"정보 On"];
        [informationButton setBackgroundColor:[UIColor colorWithRed:0.314 green:0.627 blue:0.961 alpha:1.0]];
    }
    informationButton.selected = !informationButton.selected;

    if ([_delegate respondsToSelector:@selector(manualCaptureViewDelegate:informationButtonAction:)]) {
        [_delegate manualCaptureViewDelegate:self informationButtonAction:sender];
    }
}

-(void)normalCaptureButtonAction:(id)sender{
    RLog(@"캡쳐");
    if ([_delegate respondsToSelector:@selector(manualCaptureViewDelegate:normalCaptureButtonAction:)]) {
        [_delegate manualCaptureViewDelegate:self normalCaptureButtonAction:sender];
    }
}

-(void)scrollCaptureButtonAction:(id)sender{
    RLog(@"스크롤 캡쳐");
    if ([_delegate respondsToSelector:@selector(manualCaptureViewDelegate:scrollCaptureButtonAction:)]) {
        [_delegate manualCaptureViewDelegate:self scrollCaptureButtonAction:sender];
    }
}

-(void)webviewCaptureButtonAction:(id)sender{
    RLog(@"웹뷰 캡쳐");
    if ([_delegate respondsToSelector:@selector(manualCaptureViewDelegate:webviewCaptureButtonAction:)]) {
        [_delegate manualCaptureViewDelegate:self webviewCaptureButtonAction:sender];
    }
}


-(void)panGesture:(UIPanGestureRecognizer *)gesture{
    if (gesture.state == UIGestureRecognizerStateEnded) {
        
        CGRect rect = self.frame;
        if (self.frame.origin.x < 0) {
            rect = CGRectMake(0, rect.origin.y, rect.size.width, rect.size.height);
        }
        if(self.frame.origin.y < 0) {
            rect = CGRectMake(rect.origin.x, 0, rect.size.width, rect.size.height);
        }
        if(self.frame.origin.x + self.frame.size.width > UH_SessionManager.rootWindow.frame.size.width) {
            rect = CGRectMake(UH_SessionManager.rootWindow.frame.size.width - self.frame.size.width, rect.origin.y, rect.size.width, rect.size.height);
        }
        if(self.frame.origin.y + self.frame.size.height > UH_SessionManager.rootWindow.frame.size.height) {
            rect = CGRectMake(rect.origin.x, UH_SessionManager.rootWindow.frame.size.height - self.frame.size.height, rect.size.width, rect.size.height);
        }
        
        if (self.frame.origin.x < UH_SessionManager.rootWindow.frame.size.width / 2) {
            rect = CGRectMake(0, rect.origin.y, rect.size.width, rect.size.height);
        } else {
            rect = CGRectMake(UH_SessionManager.rootWindow.frame.size.width - self.frame.size.width, rect.origin.y, rect.size.width, rect.size.height);
        }
        
        [UIView animateWithDuration:0.3f animations:^{
            self.frame = rect;
        }];
    } else {
        self.center = [gesture locationInView:self.superview];
    }
}

-(void)reloadInformation{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"superViewController",UH_VCManager.visibleViewController];
    UH_ScrollViewInfo *scrollInfo = [[UH_SessionManager.currentSession.scrollViewArray filteredArrayUsingPredicate:predicate] firstObject];
    
    if (scrollInfo) {
        scrollCaptureButton.backgroundColor = [UIColor colorWithRed:0.314 green:0.627 blue:0.961 alpha:1.0];
    } else {
        scrollCaptureButton.backgroundColor = [UIColor colorWithRed:0.68 green:0.68 blue:0.68 alpha:1];
    }
    
    if ([[UH_VCManager.webViewController visibleWebObjectSet] anyObject]) {
        webviewCaptureButton.backgroundColor = [UIColor colorWithRed:0.314 green:0.627 blue:0.961 alpha:1.0];
    } else {
        webviewCaptureButton.backgroundColor = [UIColor colorWithRed:0.68 green:0.68 blue:0.68 alpha:1];
    }
}
@end
