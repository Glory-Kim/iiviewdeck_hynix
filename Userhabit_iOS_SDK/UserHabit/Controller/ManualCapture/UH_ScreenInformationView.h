//
//  UH_ScreenInformationView.h
//  UserHabit
//
//  Created by lotco on 2017. 9. 26..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UH_ScreenInformationView : UIView{
    UILabel *screenNameContentsLabel;
    UILabel *collectedLabel;
    UILabel *scrollCheckLabel;
}
-(void)setting;
-(void)reloadInformation;
@end
