//
//  UHManualCaptureController.h
//  UserHabit
//
//  Created by lotco on 2017. 9. 14..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UH_ManualCaptureView.h"
#import "UH_ScreenInformationView.h"
#import "UH_ViewControllerManager.h"

@interface UH_ManualCaptureController : NSObject<UH_ManualCaptureViewDelegate>{
    id orientationObserver;
    UH_ManualCaptureView *manualCaptureView;
    UH_ScreenInformationView *screenInformationView;    
}

+ (instancetype)sharedInstance;
- (void)orientationChanged:(NSNotification *)notification;
- (void)changeScreen;

//Modal 형식으로 2번 호출되면 수동 스크린샷 뷰가 안보여서 해당 부분 확인 후 최상단으로 올리는 코스
- (void)checkManualView;

//debug::
- (void)showCaptureMenuView;
@end

