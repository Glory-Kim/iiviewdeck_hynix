//
//  UH_ScreenInformationView.m
//  UserHabit
//
//  Created by lotco on 2017. 9. 26..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import "UH_ScreenInformationView.h"
#import "UH_ViewControllerManager.h"
#import "UH_SessionController.h"
#import "UH_ImageController.h"
#import "UH_ActionFlow.h"

@implementation UH_ScreenInformationView

-(void)setting{
    
    int correctionValue = 0;
    
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {
            case 1136:
            case 1334:
            case 1920:
            case 2208:
                RLog(@"not notch");
                break;
                     
            case 2436:
            case 2688:
            case 1792:
                RLog(@"notch");
                correctionValue = 30;
                break;

            default:
                printf("Unknown");
                break;
        }
    }
    
    self.frame = CGRectMake(0, 0, UH_SessionManager.rootWindow.frame.size.width, 80 + correctionValue);
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    UIView *subAreaView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 40 + correctionValue)];
    subAreaView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    [self addSubview:subAreaView];
    
    int contentLabelCenterY =  (subAreaView.frame.size.height + correctionValue) / 2;
    
    UILabel *screenNameLabel = [UILabel new];
    screenNameLabel.text = @"화면이름";
    [screenNameLabel sizeToFit];
    screenNameLabel.textColor = [UIColor whiteColor];
    screenNameLabel.frame = CGRectMake(15, 0, screenNameLabel.frame.size.width, screenNameLabel.frame.size.height);
    screenNameLabel.center = CGPointMake(screenNameLabel.center.x, contentLabelCenterY);

    [subAreaView addSubview:screenNameLabel];
    
    int screenNameContentsLabelX = screenNameLabel.frame.size.width + screenNameLabel.frame.origin.x + 8;
    screenNameContentsLabel = [UILabel new];
    screenNameContentsLabel.text = NSStringFromClass([UH_VCManager.visibleViewController class]);
    [screenNameContentsLabel sizeToFit];
    screenNameContentsLabel.textColor = [UIColor colorWithRed:0.314 green:0.627 blue:0.961 alpha:1.0];
    screenNameContentsLabel.frame = CGRectMake(screenNameContentsLabelX, 0, subAreaView.frame.size.width - screenNameContentsLabelX - 15, screenNameContentsLabel.frame.size.height);
    screenNameContentsLabel.center = CGPointMake(screenNameContentsLabel.center.x, contentLabelCenterY);
    [subAreaView addSubview:screenNameContentsLabel];
    
    /*
     //TODO:서버에 즐겨찾기 화면과 실시간 동기화 기능 추후 추가 예정
    //☆ ★
    UILabel *collectContentsLabel = [UILabel new];
    collectContentsLabel.textColor = [UIColor colorWithRed:0.314 green:0.627 blue:0.961 alpha:1.0];
    collectContentsLabel.text = @"★";
    [collectContentsLabel sizeToFit];
    collectContentsLabel.frame = CGRectMake(subAreaView.frame.size.width - collectContentsLabel.frame.size.width - 8, 0, collectContentsLabel.frame.size.width, collectContentsLabel.frame.size.height);
    collectContentsLabel.center = CGPointMake(collectContentsLabel.center.x, subAreaView.center.y);
    [subAreaView addSubview:collectContentsLabel];
    
    UILabel *collectLabel = [UILabel new];
    collectLabel.textColor = [UIColor whiteColor];
    collectLabel.text = @"수집할 화면";
    [collectLabel sizeToFit];
    collectLabel.frame = CGRectMake((collectContentsLabel.frame.origin.x) - collectLabel.frame.size.width - 8, 0, collectLabel.frame.size.width, collectLabel.frame.size.height);
    collectLabel.center = CGPointMake(collectLabel.center.x, subAreaView.center.y);
    [subAreaView addSubview:collectLabel];
    */
    UILabel *nameLabel = [UILabel new];
    nameLabel.textColor = [UIColor whiteColor];
    nameLabel.text = @"일반 화면";
    [nameLabel sizeToFit];
    nameLabel.frame = CGRectMake(screenNameLabel.frame.origin.x, 0, nameLabel.frame.size.width, nameLabel.frame.size.height);
    nameLabel.center = CGPointMake(nameLabel.center.x, self.frame.size.height / 4 * 3);
    [self addSubview:nameLabel];
    
    collectedLabel = [UILabel new];
    collectedLabel.textColor = [UIColor whiteColor];
    [self checkCollectScreen];
    collectedLabel.backgroundColor = [UIColor colorWithRed:0.314 green:0.627 blue:0.961 alpha:1.0];
    [collectedLabel sizeToFit];
    collectedLabel.frame = CGRectMake(nameLabel.frame.origin.x + nameLabel.frame.size.width + 20, 0, collectedLabel.frame.size.width + 5, collectedLabel.frame.size.height + 5);
    collectedLabel.center = CGPointMake(collectedLabel.center.x, nameLabel.center.y);
    collectedLabel.clipsToBounds = YES;
    collectedLabel.layer.cornerRadius = 10;
    [self addSubview:collectedLabel];
    
    
    scrollCheckLabel = [UILabel new];
    scrollCheckLabel.textColor = [UIColor whiteColor];
    scrollCheckLabel.text = @" 해당없음";
    scrollCheckLabel.backgroundColor = [UIColor grayColor];
    [scrollCheckLabel sizeToFit];
    scrollCheckLabel.frame = CGRectMake(self.frame.size.width - scrollCheckLabel.frame.size.width - 30, 0, scrollCheckLabel.frame.size.width + 5, scrollCheckLabel.frame.size.height + 5);
    scrollCheckLabel.center = CGPointMake(scrollCheckLabel.center.x, collectedLabel.center.y);
    scrollCheckLabel.clipsToBounds = YES;
    scrollCheckLabel.layer.cornerRadius = 10;
    [self addSubview:scrollCheckLabel];
    
    
    UILabel *scrollDisplayLabel = [UILabel new];
    scrollDisplayLabel.textColor = [UIColor whiteColor];
    scrollDisplayLabel.text = @"스크롤 화면";
    [scrollDisplayLabel sizeToFit];
    scrollDisplayLabel.frame = CGRectMake(scrollCheckLabel.frame.origin.x - scrollDisplayLabel.frame.size.width - 20, 0, scrollDisplayLabel.frame.size.width, scrollDisplayLabel.frame.size.height);
    scrollDisplayLabel.center = CGPointMake(scrollDisplayLabel.center.x, nameLabel.center.y);
    [self addSubview:scrollDisplayLabel];
    
    [self reloadInformation];
}
-(void)reloadInformation{
    UH_ScreenInfo *latestScreen = (UH_ScreenInfo *)UH_SessionManager.latestScreenAction.message;
        
    screenNameContentsLabel.text = latestScreen.screenName;
    [self checkCollectScreen];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"superViewController",UH_VCManager.visibleViewController];
    UH_ScrollViewInfo *scrollInfo = [[UH_SessionManager.currentSession.scrollViewArray filteredArrayUsingPredicate:predicate] firstObject];
    
    if (scrollInfo) {
        scrollCheckLabel.text = @" 해당있음";
        scrollCheckLabel.backgroundColor = [UIColor colorWithRed:0.314 green:0.627 blue:0.961 alpha:1.0];
    } else {
        scrollCheckLabel.text = @" 해당없음";
        scrollCheckLabel.backgroundColor = [UIColor grayColor];
    }
}

-(void)checkCollectScreen{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"screenName",NSStringFromClass([UH_VCManager.visibleViewController class])];

    //todo::이미지 수집 컨트롤러 안으로 이동
    
        UH_ScreenInfo *screenInfo = [[UH_ImageManager.screenShotSet filteredSetUsingPredicate:predicate] anyObject];
    if (screenInfo) {
        collectedLabel.text = @" 수집됨";
        collectedLabel.backgroundColor = [UIColor colorWithRed:0.314 green:0.627 blue:0.961 alpha:1.0];
    } else {
        collectedLabel.text = @" 미수집";
        collectedLabel.backgroundColor = [UIColor grayColor];
    }
}

@end
