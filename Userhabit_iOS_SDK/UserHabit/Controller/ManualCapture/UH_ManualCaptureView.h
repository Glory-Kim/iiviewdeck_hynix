//
//  UH_ManualCaptureView.h
//  UserHabit
//
//  Created by lotco on 2017. 9. 19..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import <UIKit/UIKit.h>

#define UHCaptureButtonVerticalPadding 10
#define UHCaptureLabelVerticalPadding 3

@protocol UH_ManualCaptureViewDelegate;

@interface UH_ManualCaptureView : UIView{
    UIButton *informationButton;
    UILabel *informationLabel;
    
    UIButton *scrollCaptureButton;
    UIButton *webviewCaptureButton;

    
    UIView *buttonAreaView;
}
@property (weak) id delegate;
-(void)setting;
-(void)reloadInformation;
@end


@protocol UH_ManualCaptureViewDelegate <NSObject>
- (void)manualCaptureViewDelegate:(UH_ManualCaptureView *)view informationButtonAction:(UIButton *)button;
- (void)manualCaptureViewDelegate:(UH_ManualCaptureView *)view normalCaptureButtonAction:(UIButton *)button;
- (void)manualCaptureViewDelegate:(UH_ManualCaptureView *)view scrollCaptureButtonAction:(UIButton *)button;
- (void)manualCaptureViewDelegate:(UH_ManualCaptureView *)view webviewCaptureButtonAction:(UIButton *)button;
- (void)manualCaptureViewDelegate:(UH_ManualCaptureView *)view closeButtonAction:(UIButton *)button;
@end
