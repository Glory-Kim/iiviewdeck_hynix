 //
//  UHManualCaptureController.m
//  UserHabit
//
//  Created by lotco on 2017. 9. 14..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import "UH_ManualCaptureController.h"

#import "UH_SessionController.h"
#import "UH_ConnectionController.h"
#import "UH_ActionController.h"
#import "UH_ImageController.h"

#import "UserHabit.h"
#import "UHNetworking.h"
#import "UH_WKWebViewObject.h"

@implementation UH_ManualCaptureController

+(instancetype)sharedInstance
{
    static UH_ManualCaptureController *singletonObject;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singletonObject = [UH_ManualCaptureController new];
    });
    
    //debug:: 수동 캡쳐 메뉴를 앱 실행시 강제로 활성화 시킨다
    //          기본값-주석처리
//    [singletonObject showCaptureMenuView];
    
    return singletonObject;
}

//todo::확인 후 삭제
-(void)setting{
//    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    
//    if (!orientationObserver) {
//        orientationObserver =
//        [[NSNotificationCenter defaultCenter] addObserverForName:UIDeviceOrientationDidChangeNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
//            [self orientationChanged:note];
//        }];
//    }
    
    
    
    
}


- (void)orientationChanged:(NSNotification *)notification{
    static BOOL isFaceDown = NO;
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    
    
    //1 똑바로
    //2 뒤집
    //3 오른쪽 홈버튼
    //4 왼쪽 홈버튼
    if (orientation == UIDeviceOrientationFaceDown) {
        isFaceDown = YES;
    } else if(isFaceDown &&
              orientation == UIDeviceOrientationFaceUp){
        RLog(@"출현");
        [self showCaptureMenuView];
    }
}


-(void)showCaptureMenuView{
    RLog(@"%@", manualCaptureView);
    if (!manualCaptureView) {
        manualCaptureView = [UH_ManualCaptureView new];
        manualCaptureView.delegate = self;
        [manualCaptureView setting];
        [UH_SessionManager.rootWindow addSubview:manualCaptureView];
        
        [UH_SessionManager.currentSession insertInnerData:@"수동 스크린샷 메뉴 실행"
                                                   typeOf:UHInnerDataTypeLog
                                               actionFlow:nil];
    }
}

#pragma mark UH_ManualCaptureViewDelegate
- (void)manualCaptureViewDelegate:(UH_ManualCaptureView *)view closeButtonAction:(UIButton *)button{
    if (manualCaptureView) {
        [manualCaptureView removeFromSuperview];
        manualCaptureView = nil;
    }
    
    if(screenInformationView){
        [screenInformationView removeFromSuperview];
        screenInformationView = nil;
    }
    RLog_ViewCycle;
}
- (void)manualCaptureViewDelegate:(UH_ManualCaptureView *)view informationButtonAction:(UIButton *)button{
    RLog_ViewCycle;
    [UH_SessionManager.currentSession insertInnerData:@"화면 정보 캡쳐 버튼"
                                               typeOf:UHInnerDataTypeLog
                                           actionFlow:nil];
    
    if (!screenInformationView) {
        screenInformationView = [UH_ScreenInformationView new];
        [screenInformationView setting];
        [screenInformationView setUserInteractionEnabled:NO];
        [UH_SessionManager.rootWindow addSubview:screenInformationView];
    } else {
        [screenInformationView removeFromSuperview];
        screenInformationView = nil;
    }
}

- (void)manualCaptureViewDelegate:(UH_ManualCaptureView *)view normalCaptureButtonAction:(UIButton *)button{
    RLog_ViewCycle;
    [UH_SessionManager.currentSession insertInnerData:@"일반 화면 캡쳐 버튼"
                                               typeOf:UHInnerDataTypeLog
                                           actionFlow:nil];
    
    if ([UHNetworkReachabilityManager sharedManager].reachable) {
        [UserHabit takeScreenShot:UH_VCManager.visibleViewController];
    } else {
        [UH_Util logDebugType:UHDebugTypeWarning withMessage:@"This function must be used the network"];
    }
}

- (void)manualCaptureViewDelegate:(UH_ManualCaptureView *)view scrollCaptureButtonAction:(UIButton *)button{
    RLog_ViewCycle;
    [UH_SessionManager.currentSession insertInnerData:@"스크롤 캡쳐 버튼"
                                               typeOf:UHInnerDataTypeLog
                                           actionFlow:nil];
    
    if ([UHNetworkReachabilityManager sharedManager].reachable) {
        [UH_ImageManager scrollCaptureViewProcess:^(id result, NSError *error) {
            RLog_ViewCycle;
            [self sendScrollImageProcess:result];
        }];
        
    } else {
        [UH_Util logDebugType:UHDebugTypeWarning withMessage:@"This function must be used the network"];
    }
}

- (void)manualCaptureViewDelegate:(UH_ManualCaptureView *)view webviewCaptureButtonAction:(UIButton *)button{
    RLog_ViewCycle;
    [UH_SessionManager.currentSession insertInnerData:@"웹뷰 캡쳐 버튼"
                                               typeOf:UHInnerDataTypeLog
                                           actionFlow:nil];
    
    if ([UHNetworkReachabilityManager sharedManager].reachable) {
        [[UH_VCManager.webViewController visibleWebObjectSet] enumerateObjectsUsingBlock:^(UH_WebViewObject *  _Nonnull obj, BOOL * _Nonnull stop) {
            [UH_ImageManager webviewCaptureViewProcess:obj];
        }];
        [UH_VCManager.webViewController captureWebviewObjectImage];
    } else {
        [UH_Util logDebugType:UHDebugTypeWarning withMessage:@"This function must be used the network"];
    }
}


-(void)changeScreen{
       
    if (manualCaptureView) {
        [manualCaptureView reloadInformation];
        
    }
    
    if (screenInformationView) {
        [screenInformationView reloadInformation];
    }
    
}

#pragma mark - scrollview capture
- (UIImage *)imageWithView:(UIScrollView *)view withCorrectionHeightValue:(int)correctionHeight {
    static int offsetCorrectionValue = -1;
    int viewContentsOffsetY;
    
    if (view.contentOffset.y < 0) {
        if (offsetCorrectionValue == -1) {
            offsetCorrectionValue = view.contentOffset.y;
        }
        viewContentsOffsetY = view.contentOffset.y + offsetCorrectionValue;
        
    } else {
        viewContentsOffsetY = view.contentOffset.y;
    }
    
    CGSize captureSize;
    if (view.contentSize.height - view.contentOffset.y < view.bounds.size.height) {
        captureSize = CGSizeMake(view.bounds.size.width, view.contentSize.height - view.contentOffset.y + offsetCorrectionValue);
        offsetCorrectionValue = offsetCorrectionValue ;
    } else {
        captureSize = CGSizeMake(view.bounds.size.width, correctionHeight);
    }
    
    UIGraphicsBeginImageContextWithOptions(captureSize, YES, [UIScreen mainScreen].scale);
    CGPoint offset = view.contentOffset;
    CGContextTranslateCTM(UIGraphicsGetCurrentContext(), -offset.x, -offset.y + offsetCorrectionValue);
    
    if (captureSize.width <= 0 ||
        captureSize.height <= 0) {
        return nil;
    }
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
     
    return img;
}


-(void)sendScrollImageProcess:(UH_ScrollViewInfo *)scrollInfo{
    RLog(@"%@", UH_VCManager.screenHistoryList);
    [UH_VCManager.screenHistoryList enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, UH_ScreenInfo * _Nonnull obj, BOOL * _Nonnull stop) {
        RLog(@"%@ / %@", key , obj.rootViewController);
        RLog(@"%@", scrollInfo.superViewController);
        
        if (scrollInfo.superViewController == obj.rootViewController) {
//            RLog(@"동일 뷰");
            obj.scrollViewInformation = scrollInfo;
            
            [scrollInfo.images enumerateObjectsUsingBlock:^(UH_ScrollImageInformation * _Nonnull imageObj, NSUInteger idx, BOOL * _Nonnull stop) {
                RLog(@"전송 시도 %d / %@", imageObj.index, imageObj.filePath);
                
                UH_ConnectionController *connectionController = [UH_ConnectionController new];
                [connectionController sendScrollScreenInfo:imageObj withScreenInfo:obj];
            }];
            *stop = YES;
        }
    }];
}

- (void)checkManualView{    
    if (manualCaptureView) {       
        if ([UH_SessionManager.rootWindow.subviews lastObject] != manualCaptureView) {
            [UH_SessionManager.rootWindow bringSubviewToFront:manualCaptureView];
        }
        [self changeScreen];
    }
}

@end
