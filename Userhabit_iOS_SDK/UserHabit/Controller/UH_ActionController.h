//
//  UH_ActionController.h
//  UserHabit
//
//  액션을 생성하고 저장하는 과정을 전체적으로 컨트롤하는 클래스
//  Create action and save control class
//
//  Created by lotco on 2017. 7. 25..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface UH_ActionController : NSObject

/*
 * 화면이 바뀔때 (viewDidDisappear)와 홈버튼을 눌러 앱이 종료될때 해당 프로세스를 실행한다.
 * 스크롤뷰의 최대 좌표를 actionFlow.db에 저장한다
 */
- (void)processMaximumScrollView:(UIView *)view;
@end
