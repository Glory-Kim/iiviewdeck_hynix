//
//  UH_SessionController.h
//  UserHabit
//
//  Created by DoHyoungKim on 2015. 12. 11..
//  Copyright © 2015년 Andbut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UH_GestureController.h"
#import <UserHabit/UserHabit.h>
#import "UserHabitConstant.h"
#import "UH_Session.h"
#import "UH_ConnectionController.h"

#define UH_SessionManager [UH_SessionController sharedInstance]

@class UH_UserInfo;
@class UH_ConnectionController;

@interface UH_SessionController : NSObject

@property (nonatomic, strong) NSString *apiKey;
@property (atomic, strong) NSString *nowShowController;
@property (weak) UIWindow *rootWindow;
@property (nonatomic, strong) NSNumber *sessionDelayTime;
@property (nonatomic, assign) UHScreenOrientation nowOrientation;
@property (nonatomic, assign) BOOL onSession;
@property (nonatomic, assign) BOOL isDevelopMode;
@property (nonatomic, assign) BOOL isBase64;
@property (nonatomic, assign) BOOL isAutoCatch;         //자동 추적 모드
@property (nonatomic, assign) BOOL isDeviceRandomId;
@property (nonatomic, assign) BOOL isScreenshotCaptureMode;
@property (nonatomic, assign) BOOL isDebug;
@property (nonatomic, assign) BOOL isLoadingComplete;   //앱 로딩 체크 해당 값이 체크되기전에 액션값이 입력되지않는다
@property (nonatomic, strong) NSMutableDictionary *options;
@property (atomic, weak) UH_ActionFlow *latestAction;
@property (atomic, weak) UH_ActionFlow *latestScreenAction;     //가장 최근 본 화면을 기록한다

@property (atomic, strong) dispatch_queue_t operationQueue;

@property (nonatomic, strong, readonly) NSString *userhabitPath;
@property (nonatomic, strong, readonly) NSString *userhabitScreenImagePath;
@property (nonatomic, strong, readonly) NSString *userhabitObjectImagePath;

@property (nonatomic, strong, readonly) UH_Session *currentSession;

@property (nonatomic, strong) UH_ConnectionController *connectionController;
@property (nonatomic, strong) NSString *venderIdentifier;

@property (nonatomic, strong) UH_GestureController *gestureController;

@property (nonatomic, strong) UH_UserInfo * userInformation;
+(instancetype)sharedInstance;

//TODO: 파일 경로 다른 곳으로 이동시키기
-(NSString *)getSessionCntFilePath;
-(NSString *)getReferrerFileNamePath;
-(NSString *)getLatestSuccessTimeFilePath;
-(NSString *)getDeviceIdFilePath;
-(NSString *)getUserInfoFilePath;

-(void)removeUnusefulData;

-(void)initialize;
-(void)sessionStart;
-(void)sessionCloseWithUploadData:(BOOL)isUpload completeHandler:(void(^)(void))completeHandler;
-(void)sessionClose;                                        //notification으로 인자값없이 실행하기 위한 메소드 

-(long long)loadLatestSessionTime;
-(void)saveLatestSessionTime:(long long)latestSuccessTime;
-(void)saveUserInfo;
-(NSDictionary *)loadUserInfo;

#pragma mark - only use test case method
-(int)loadSessionCount;
-(void)checkPathVaildity;
-(void)previousFilePath;
-(void)dataPathSetting;
@end
