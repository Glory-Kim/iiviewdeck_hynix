//
//  UH_ActionController.m
//  UserHabit
//
//  Created by lotco on 2017. 7. 25..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import "UH_ActionController.h"

#import "UH_SessionController.h"
#import "UH_SessionInfo.h"
#import "UH_ActionFlow.h"
#import "UH_ScrollViewOffsetInfo.h"

@implementation UH_ActionController
- (void)processMaximumScrollView:(UIView *)view{
    UH_ScrollViewInfo *scrollViewInfo = [UH_Util findScrollView:view];
    if (scrollViewInfo) {
        
        UH_ScrollViewOffsetInfo *scrollViewOffset = [UH_ScrollViewOffsetInfo new];
        scrollViewOffset.contentsOffset = scrollViewInfo.maximumScrollViewPosition;
        
        if (scrollViewOffset.contentsOffset.y == 0) {
            scrollViewOffset.contentsOffset = CGPointMake(scrollViewOffset.contentsOffset.x, scrollViewInfo.scrollView.bounds.size.height);
        }
        
        scrollViewOffset.scrollMappingNumber = scrollViewInfo.mapping;
        
        UH_ActionFlow *scrollEndActionFlow = [[UH_ActionFlow alloc] initAt:UHNowTimeStamp
                                                                      from:UH_SessionManager.currentSession.sessionInfo.uptimeStart + 5
                                                                    typeOf:UHActionFlowTypeMaxScroll
                                                                      with:scrollViewOffset];
        
        [UH_SessionManager.currentSession insertActionFlow:scrollEndActionFlow];
        RLog(@"스크롤뷰 최대 좌표 저장 %@", [scrollEndActionFlow build]);
    } else {
        RLog(@"스크롤뷰 아님 %@", self);
    }
}
@end
