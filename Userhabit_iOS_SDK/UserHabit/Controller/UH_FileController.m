//
//  UH_FileController.m
//  UserHabit
//
//  Created by lotco on 2017. 12. 28..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import "UH_FileController.h"
#import "UH_ConnectionController.h"
#import "UH_Util.h"

NSString *const UHS3URL = @"https://s3-ap-northeast-1.amazonaws.com/userhabit-production/webview_js";
NSString *const UHUserhabitJSFilename = @"/userhabit_4.min.js";
//NSString *const UHUserhabitJSFilename = @"/userhabit.js";

@implementation UH_FileController

+(void)checkHybridJSFile{
    NSURL *libraryUrl = [[NSFileManager defaultManager] URLForDirectory:NSLibraryDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
    libraryUrl = [libraryUrl URLByAppendingPathComponent:@"/UserHabit/"];
    libraryUrl = [libraryUrl URLByAppendingPathComponent:UHUserhabitJSFilename];
    RLog(@"%@", [NSString stringWithFormat:@"%@%@", UHS3URL, UHUserhabitJSFilename]);
    
    //debug::js파일 강제 다운로드
//    if (1) {
    if (![[NSFileManager defaultManager] fileExistsAtPath:[libraryUrl path]]) {
        UH_ConnectionController *fileDownloader = [UH_ConnectionController new];
        [fileDownloader downloadFileURL:[NSString stringWithFormat:@"%@%@", UHS3URL, UHUserhabitJSFilename] completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
            RLog(@"File downloaded to: %@", filePath);
        }];
    }
}

+ (void)checkSessionFile:(NSString *)filePath completeHandler:(RActionBlock)completeHandler{
    RLog(@"%@", filePath);
    
    NSString *content = [NSString stringWithContentsOfFile:filePath
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
    NSArray *array = [content componentsSeparatedByString:@"\n"];
    NSDictionary *beforeActionFlow;
    NSDictionary *firstActionFlow;
    NSError *error;
    
    for (NSString *inString in array) {
        if (inString.length > 0) {
            NSData *data = [inString dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *actionFlow = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            if (beforeActionFlow) {
                int beforeActionTime = [beforeActionFlow[@"u"] intValue];
                int currentActionTime = [actionFlow[@"u"] intValue];
                
                RLog(@"마지막 액션과 시간 차이 %d", currentActionTime - beforeActionTime);
                RLog(@"첫번째 액션과 시간 차이 %d", currentActionTime - [firstActionFlow[@"u"] intValue]);
                if (currentActionTime - beforeActionTime > UHLastActionCompareValue) {
//                if (currentActionTime - beforeActionTime > 1000) {
                    error = [NSError errorWithDomain:@"야호" code:4000 userInfo:nil];
                    RLog(@"시간 차이 많이 남");
                    break;
                }
            } else {
                firstActionFlow = actionFlow;
            }
            
            beforeActionFlow = actionFlow;
            RLog(@"%@", actionFlow);
        }
    }
    RLog(@"끝!");
    completeHandler(error);
}

@end
