//
//  UH_FileController.h
//  UserHabit
//
//  Created by lotco on 2017. 12. 28..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UH_FileController : NSObject
extern NSString *const UHUserhabitJSFilename;
/*
 * canvas자바 스크립 파일을 확인하고 없을경우 다운로드 받는다
 */
+ (void)checkHybridJSFile;

/*
 * 세션 파일을 읽은 후 이상 감지 한다
 */
+ (void)checkSessionFile:(NSString *)filePath completeHandler:(RActionBlock)completeHandler;

@end
