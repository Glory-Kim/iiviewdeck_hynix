//
//  UH_ConnectionController.h
//  UserHabit
//
//  Created by DoHyoungKim on 2015. 12. 4..
//  Copyright © 2015년 Andbut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UHURLSessionManager.h"
#import "UH_ScrollViewInfo.h"
#import "UH_ScreenInfo.h"
@class UH_Session;
@class UH_ObjectInfo;

typedef NS_ENUM(NSUInteger, UHConnectionStatus) {
    UHConnectionStatusStart,
    UHConnectionStatusEnd,
    UHConnectionStatusCrash,
    UHConnectionStatusFileSend,
    UHConnectionStatusScreenshot
};

@interface UH_ConnectionController : NSObject<NSURLConnectionDelegate>

@property (nonatomic, assign) int reCount;
@property (nonatomic, assign) UHConnectionStatus status;
@property (atomic, strong) UHURLSessionManager * _Nonnull manager;

-(void)sessionStartConnection:(UH_Session * _Nonnull)session;
-(void)sessionEndConnection:(UH_Session * _Nonnull)session completeHandler:(void(^ _Nullable)(void))completeHandler;
-(void)objectMessageSend:(UH_ObjectInfo * _Nonnull)objectInfo screenName:(NSString * _Nonnull)screenName;
-(void)getScreenInfo;
-(BOOL)sendScreenInfo:(UH_ScreenInfo * _Nonnull)screenName;
-(void)chkSendScreenInfo;
-(void)chkSendObjectInfo:(NSString * _Nonnull)screenName;
-(void)sendScrollScreenInfo:(UH_ScrollImageInformation * _Nonnull)scrollViewImageInformation withScreenInfo:(UH_ScreenInfo * _Nonnull)screenInformation;
-(NSString *_Nullable)getToken; // sha256 토큰값 추가 - 하이닉스전용
#pragma mark - download file
-(void)downloadFileURL:(NSString * _Nonnull)url completionHandler:(void (^ _Nonnull)(NSURLResponse * _Nonnull response, NSURL * _Nonnull filePath, NSError * _Nullable error))completionHandler;
@end
