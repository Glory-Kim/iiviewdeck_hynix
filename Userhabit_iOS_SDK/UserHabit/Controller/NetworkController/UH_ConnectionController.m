//
//  UH_ConnectionController.m
//  UserHabit
//
//  Created by DoHyoungKim on 2015. 12. 4..
//  Copyright © 2015년 Andbut. All rights reserved.
//

#import "UH_ConnectionController.h"
#import "UserHabitConstant.h"
#import "UH_SessionController.h"
#import "UH_ViewControllerManager.h"
#import "UH_Util.h"
#import "UH_Session.h"
#import "UH_AppInfo.h"
#import "UH_DeviceInfo.h"
#import "UH_SessionInfo.h"
#import "UH_ScreenInfo.h"
#import "UH_ObjectInfo.h"
#import "UH_ResendController.h"
#import "UH_MutableURLRequest.h"
#import "UH_ImageController.h"
#import <CommonCrypto/CommonDigest.h>

@implementation UH_ConnectionController

- (instancetype)init {
    self = [super init];

    if (self) {
        _manager = [[UHURLSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        [[_manager operationQueue] setMaxConcurrentOperationCount:UHHttpMaxCount];
    }
    
    return self;
}

- (void)sessionStartConnectionReal:(UH_Session *)session{
    RLog_ViewCycle;
    _status = UHConnectionStatusStart;

    long long l = UHNowTimeStamp;
    
    NSDictionary *sendDic = [session sessionStartInformationDictionary];
    NSData *sendData = [NSJSONSerialization dataWithJSONObject:sendDic options:0 error:nil];

    NSString *urlStr = [NSString stringWithFormat:@"%@/v2/a/%@/s/%@/%@",
                                                  UHGatewayHost,
                                                  UH_SessionManager.apiKey,
                                                  UH_SessionManager.venderIdentifier,
                                                  session.sessionInfo.sessionId];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    
    request.HTTPBody = sendData;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    request.timeoutInterval = requestTimeoutInterval;
    // 하이닉스 전용 sha 토큰값추가
    request.HTTPMethod = UHHttpMethodTypePOST;
//    NSString *shatoken = [self getToken];
//    [request setValue:@"token" forHTTPHeaderField:shatoken];
    
    NSMutableDictionary * header = [NSMutableDictionary dictionaryWithDictionary:request.allHTTPHeaderFields];
    RLog(@" header =>  : %@ ", header );
//    header[@"token"] = shatoken;  블록 처리해도 통신됨
    [request setAllHTTPHeaderFields:header];
    
//TODO: data request말고 그냥 일반 json 텍스트로 보내기
    [[_manager dataTaskWithRequest:request
                    uploadProgress:nil
                  downloadProgress:nil
                 completionHandler:^(NSURLResponse *response, id data, NSError *error) {
                     
                     NSInteger statusCode = ((NSHTTPURLResponse *)response).statusCode;
                     if (error) {
                         session.sessionStatus = UHSessionStatusTypeResendAbnormal;
                     }
                     
                     if (statusCode == 404) {
                         [self printChangeApiKey];
                         session.sessionStatus = UHSessionStatusTypeResendAbnormal;
                         UH_SessionManager.onSession = NO;
                     } else if (statusCode == 503 || statusCode == 504) {
                         if (self->_reCount < 10) {
                             [self performSelector:@selector(sessionStartConnectionReal:) withObject:session afterDelay:5.0f];
                             self->_reCount++;
                         }

                         return ;
//                     정상 세션 처리
                     }else if (statusCode < 300 && statusCode >= 200){
                         NSDictionary *receivedDic = [NSDictionary dictionaryWithDictionary:data];
                         
                         RLog(@"%@", receivedDic);
                         

                         long long t1 = UHNowTimeStamp;

                         long long sTime = [receivedDic[@"syncTime"] longLongValue];
                         long long sessionStartTime = sTime + ((t1 - l) / 2);

                         [UH_Util logDebugType:UHDebugTypeNetwork withMessage:@"Session open succeeded"];
                         
                         UH_SessionManager.onSession = YES;
                         [session setSessionTransportData:receivedDic];
                         //세션이 일정 조건에 만족하면 무시한다
                         if (session.transportConst.type != UHTransportConstTypeNone) {
                             long long vValue = session.transportConst.value;
                             
                             //세션 일정 횟수동안 무시하기
                             if (session.transportConst.type == UHTransportConstTypeTimeProhibition) {
                                 if (session.sessionInfo.sessionCount <= vValue) {
                                     UH_SessionManager.onSession = NO;
                                 }
                             //세션 일정 시간 동안 무시하기
                             }else if (session.transportConst.type == UHTransportConstTypeCountProhibition){
                                 if ([[NSUserDefaults standardUserDefaults] objectForKey:@"UH_ConstTime"] == nil) {
                                     [[NSUserDefaults standardUserDefaults] setObject:@(session.sessionInfo.sessionStartTime + vValue) forKey:@"UH_ConstTime"];
                                     UH_SessionManager.onSession = NO;
                                 }else{
                                     if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"UH_ConstVersion"]longLongValue] >= session.sessionInfo.sessionStartTime) {
                                         UH_SessionManager.onSession = NO;
                                     }else{
                                         [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"UH_ConstVersion"];
                                     }
                                 }
                             //세션 일정 버젼동안 무시하기
                             }else if (session.transportConst.type == UHTransportConstTypeVersionProhibition){
                                 if ([[NSUserDefaults standardUserDefaults]objectForKey:@"UH_ConstVersion"] == nil) {
                                     [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@", [NSBundle mainBundle].infoDictionary[@"CFBundleVersion"]] forKey:@"UH_ConstVersion"];
                                     UH_SessionManager.onSession = NO;
                                 }else{
                                     NSString *versionStr = [NSString stringWithFormat:@"%@", [NSBundle mainBundle].infoDictionary[@"CFBundleVersion"]];
                                     if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"UH_ConstVersion"]isEqualToString:versionStr]) {
                                         UH_SessionManager.onSession = NO;
                                     }else{
                                         [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"UH_ConstVersion"];
                                     }
                                 }
                             }

                             [[NSUserDefaults standardUserDefaults]synchronize];
                         }

                         // save latest session time for daily postback
                         // it will be sent in the next time
                         [UH_SessionManager saveLatestSessionTime:sessionStartTime];

                         session.sessionInfo.sessionStartTime = sessionStartTime;

                         if (UH_SessionManager.onSession) {
                             if ([session chkTransportStatus:UHTransportMethodScreenshotInfo]) {
                                 [self getScreenInfo];
                             }
                         }

                         session.sessionStatus = UHSessionStatusTypeNormal;
                     
//                     비정상 세션
                     } else {
                         session.sessionStatus = UHSessionStatusTypeResendAbnormal;
                     }

                     [session saveSessionInfo];
                 }]
     resume];
}


-(void)sessionStartConnection:(UH_Session *)session{
    _reCount = 0;
    [self sessionStartConnectionReal:session];
}
/**
 U2 JOB
 기존 zip 으로 압축해서 전송
 u2 -> (json)다이렉트로 전송
 */
-(void)sessionEndConnection:(UH_Session *)session completeHandler:(void(^)(void))completeHandler{
    RLog_ViewCycle;
    @try {
        self.status = UHConnectionStatusEnd;

        NSString *sessionDirPath = [session getSessionDirPath];
        NSString *sessionDataFilePath = [session getSessionDataFilePath];        
        
        NSDictionary *fileInfo;
        if (!(fileInfo = [[NSFileManager defaultManager] attributesOfItemAtPath:sessionDataFilePath error:nil])) {
            [[NSFileManager defaultManager]removeItemAtPath:sessionDirPath error:nil];
        }
        NSDictionary *sessionEndInformationDictionary = [session sessionEndInformationDictionary];
        
        
        RLog(@"%@", sessionEndInformationDictionary);
        
        
        NSString *urlStr = [NSString stringWithFormat:@"%@/v2/a/%@/s/%@/%@/c",
                            UHGatewayHost,
                            UH_SessionManager.apiKey,
                            UH_SessionManager.venderIdentifier,
                            session.sessionInfo.sessionId];
        NSMutableURLRequest *request;
        RLog(@"%@", urlStr);
        
        if (UH_SessionManager.isBase64) {
            NSData *data = [[NSFileManager defaultManager] contentsAtPath:sessionDataFilePath];
            request = [UH_MutableURLRequest sessionEndRequestBase64Url:urlStr fileData:data object:sessionEndInformationDictionary];

        } else {
            
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:sessionEndInformationDictionary options:0 error:nil];
            NSInputStream *resultData = [NSInputStream inputStreamWithFileAtPath:sessionDataFilePath];
            
            
            RLog(@"%@/ %@ / %@ / %@", urlStr, resultData, fileInfo, jsonData);
            RLog(@"%@/ %@ / %@ / %@", [urlStr class], [resultData class], [fileInfo class], [jsonData class]);
            
            request = [UH_MutableURLRequest sessionEndRequestMultipartFormUrl:urlStr fileStream:resultData fileInfo:fileInfo jsonData:jsonData];
        }
        request.HTTPMethod = UHHttpMethodTypePOST;
        request.timeoutInterval = requestTimeoutInterval;
        
        // sha256 토큰값추가 - 하이닉스 전용
        NSString *shatoken = [self getToken];
        NSMutableDictionary * header = [NSMutableDictionary dictionaryWithDictionary:request.allHTTPHeaderFields];
//        header[@"token"] = shatoken; 됨

        [request setAllHTTPHeaderFields:header];
        [request setValue:shatoken forHTTPHeaderField:@"token"]; // 되
        RLog(@"=> token  : %@ ",shatoken);
        RLog(@"=> header  : %@ ",header);
         // sha256 토큰값추가 - 하이닉스 전용
        
        //todo::앱 강제종료할때 세션 보내는 방법 있나 연구해보기
        RLog_ViewCycle;
        [[_manager uploadTaskWithStreamedRequest:request
                                        progress:nil
                               completionHandler:^(NSURLResponse * _Nonnull response, id _Nullable responseObject, NSError *_Nullable error) {
                                   RLog_ViewCycle;
                                   RLog(@"%@", responseObject);
                                   
                                   if (error) {
                                       RLog(@"SessionEndConnection Error = %@", error);
                                   } else {
                                       if ([UH_Util responseCheck:responseObject]) {
//                                           debug:: 세션 종료후 데이터 삭제, 데이터 파일을 확인할때 사용한다
                                                       //기본값 --활성
                                           [session deleteSessionData];
                                           [UH_Util logDebugType:UHDebugTypeNetwork
                                                     withMessage:[NSString stringWithFormat:@"Session data sent (%d)", session.sessionInfo.sessionCount]];
                                       }
                                   }
                                   
                                   if (completeHandler) {
                                       RLog(@"Run manual close session close complete handler.");
                                       completeHandler();
                                   }
                                   
                                   // send a next old session when resending is completed
                                   if (session.reloaded) {
                                       [[UH_ResendController sharedInstance] runOldSessionWork];
                                   }
                               }]
         resume];

    } @catch (NSException *exception) {
        [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                   typeOf:UHInnerDataTypeTryCatch
                                               actionFlow:nil];
    }
}

-(void)getScreenInfo{
    RLog_ViewCycle;
    UH_SessionController *uhSessionController = [UH_SessionController sharedInstance];

    if (uhSessionController.onSession) {
        self.status = UHConnectionStatusScreenshot;
        
        NSString *urlStr = [NSString stringWithFormat:@"%@/v2/a/%@/g",
                            UHGatewayHost,
                            uhSessionController.apiKey];
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[UH_SessionManager.currentSession sessionInformationDictionary] options:0 error:nil];

        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
        request.HTTPMethod = UHHttpMethodTypePOST;
        request.HTTPBody = jsonData;
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        request.timeoutInterval = requestTimeoutInterval;

        [[self.manager dataTaskWithRequest:request
                            uploadProgress:nil
                          downloadProgress:nil
                        completionHandler:^(NSURLResponse *response, id data, NSError *error) {
                            if (!error) {
                                RLog(@"load screen shot data %@", data);
                                
                                
                                NSDictionary *receivedDic = [NSDictionary dictionaryWithDictionary:data];
                                RLog(@"%@", data);
                                
                                [receivedDic[@"screenList"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                    UH_ScreenInfo *screenInformation = [[UH_ScreenInfo alloc]initWithDictionary:obj];
                                    [UH_ImageManager.screenShotSet addObject:screenInformation];
                                }];
                                UH_ImageManager.objectShotSet = [NSMutableDictionary dictionary];

                                NSArray *screenObjectList = [NSArray arrayWithArray:receivedDic[@"screenObjectList"]];

                                //TODO:모델 작업
                                for (NSDictionary *screenObject in screenObjectList) {
                                    NSString *key = screenObject[@"a"];
                                    NSArray *objectArray = screenObject[@"o"];
                                    NSMutableArray<NSString *> *objectsArray = [NSMutableArray arrayWithCapacity:objectArray.count];

                                    for (NSDictionary *screen in objectArray) {
                                        NSDictionary *dic = [NSDictionary dictionaryWithDictionary:screen];
                                        [objectsArray addObject:dic[@"i"]];

                                    }

                                    UH_ImageManager.objectShotSet[key] = objectsArray;
                                }
                            }
                        }
        ] resume];
    }
}

/*
 * 저장되어있는 스크린샷 정보 확인
 */
-(void)chkSendScreenInfo{
    if (UH_SessionManager.onSession) {
        @try {
            if (UH_SessionManager.currentSession &&
                [UH_SessionManager.currentSession chkTransportStatus:UHTransportMethodScreenshotInfo]) {
                NSArray<NSString *> *imagePaths = [[NSFileManager defaultManager]contentsOfDirectoryAtPath:UH_SessionManager.userhabitScreenImagePath error:nil];
                [imagePaths enumerateObjectsUsingBlock:^(NSString * _Nonnull imagePath, NSUInteger idx, BOOL * _Nonnull stop) {
                    if (![imagePath isEqualToString:UHScreenshotIgnoreFileName]) {
                        NSString *imageFileName = imagePath.precomposedStringWithCanonicalMapping;
                        NSRange replaceRange = NSMakeRange(imageFileName.length - 4, 4); // remove ".jpg"
                        NSString *fileName = [imageFileName stringByReplacingCharactersInRange:replaceRange withString:@""];
                        
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"fileName", fileName];
                        NSArray *compareArray = [[UH_VCManager.screenHistoryList allValues] filteredArrayUsingPredicate:predicate];
                        
                        if([compareArray count] == 1){
                            UH_ScreenInfo *screenInformation = [compareArray firstObject];
                            if (!screenInformation.isCompleteUploadScreenshot &&
                                !screenInformation.isUploadLoading) {
                                [self sendScreenInfo:screenInformation];
                            }
                        } else if ([[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/%@", UH_SessionManager.userhabitScreenImagePath, imagePath] error:nil]) {
                            
                            RLog(@"파일 삭제");
                            
                        }
                    }
                }];
            }
        } @catch (NSException *exception) {
            [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                       typeOf:UHInnerDataTypeTryCatch
                                                   actionFlow:nil];
        }
    }
}
// sha256 토큰값추가 - 하이닉스 전용
-(NSString *)getToken{
    UH_Session *session = UH_SessionManager.currentSession;
    
    RLog(@"%@", UH_SessionManager.venderIdentifier);
    NSString *hashD1 = [UH_SessionManager.venderIdentifier substringToIndex:20];
    NSString *hashD2 = [UH_SessionManager.venderIdentifier substringFromIndex:20];
    
    NSString *hashS1 = [session.sessionInfo.sessionId substringToIndex:16];
    NSString *hashS2 = [session.sessionInfo.sessionId substringFromIndex:16];
    RLog(@"%@", hashD1);
    RLog(@"%@", hashD2);
    
    NSString * input = [NSString stringWithFormat:@"%@%@%@%@",
        hashS2,hashD2,hashD1,hashS1];
    
    const char* str = [input UTF8String];
       unsigned char result[CC_SHA256_DIGEST_LENGTH];
       CC_SHA256(str, (CC_LONG)strlen(str), result);
       
       NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH*2];
       for(int i = 0; i<CC_SHA256_DIGEST_LENGTH; i++)
       {
           [ret appendFormat:@"%02x",result[i]];
       }
  
    return ret;
}


-(void)chkSendObjectInfo:(NSString *)screenName{
    RLog(@"%@", screenName);
    UH_ScreenInfo *screenInfo = [UH_ViewControllerManager sharedInstance].screenHistoryList[screenName];
//    NSArray<UH_ObjectInfo *> *objectArray = [NSArray arrayWithArray:screenInfo.objectInfos];

    if (screenInfo.objectSet) {
        [screenInfo.objectSet enumerateObjectsUsingBlock:^(UH_ObjectInfo * _Nonnull objectItem, BOOL * _Nonnull stop) {
            if ([UHNetworkReachabilityManager sharedManager].reachable) {
                //                RLog(@"데이터 보내기 %@", [objectItem build]);
                [self objectMessageSend:objectItem screenName:screenName];
            }
        }];
    }
}

- (BOOL)sendScreenInfo:(UH_ScreenInfo *)screenInformation{
    @try {
        if (!UH_SessionManager.onSession) {
//TODO:리턴값 처음에 없었음 no가 맞는지 확인하기
            return NO;
        }
        screenInformation.isUploadLoading = YES;
        _status = UHConnectionStatusFileSend;
        NSString *imgFileName = [NSString stringWithFormat:@"%@/%@.jpg", screenInformation.path, screenInformation.fileName];
        UIImage *img = [UIImage imageWithContentsOfFile:imgFileName];
        RLog(@"%@", imgFileName);
        if (img == nil) {
            return NO;
        }
        NSData *imgData = UIImageJPEGRepresentation(img, 1.0f);

        NSDictionary *informationDictionary = [UH_SessionManager.currentSession sessionInformationAddObject:@{UHProtocolKeyScreenInformation : [screenInformation build]}];
        RLog(@"%@", informationDictionary);
        if (imgData != nil && imgData.length != 0) {
            NSString *urlStr = [NSString stringWithFormat:@"%@/v2/a/%@/v",
                                UHGatewayHost,
                                UH_SessionManager.apiKey];
            NSMutableURLRequest *request;
            if(UH_SessionManager.isBase64){
                request = [UH_MutableURLRequest requestBase64Url:urlStr withObject:informationDictionary withData:imgData];
            } else {
                request = [UH_MutableURLRequest requestMultipartFormUrl:urlStr object:informationDictionary data:imgData fileName:imgFileName];
            }
            
            
            NSURLSessionUploadTask *uploadTask;
            uploadTask = [_manager uploadTaskWithStreamedRequest:request
                                                        progress:nil
                                               completionHandler:^(NSURLResponse * _Nonnull response, id _Nullable responseObject, NSError *_Nullable error){
                                                   screenInformation.isUploadLoading = NO;
                                                   
                                                   
                                                   //스크린샷 피드백
                                                   if ([UH_SessionManager.options[UHOptionScreenshotTapticFeedback] boolValue]) {
                                                       UIImpactFeedbackGenerator *feedbackGenerator = [[UIImpactFeedbackGenerator alloc] initWithStyle:(UIImpactFeedbackStyleHeavy)];
                                                       [feedbackGenerator impactOccurred];
                                                       
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           UILabel __block *label = [UILabel new];
                                                           
                                                           label.text = [NSString stringWithFormat:@"%@",screenInformation.screenName];
                                                           label.backgroundColor = [UIColor darkGrayColor];
                                                           label.textColor = [UIColor colorWithWhite:0.8f alpha:0.5f];
                                                           label.layer.borderColor = [UIColor colorWithWhite:0.8f alpha:0.5f].CGColor;
                                                           label.layer.borderWidth = 0.5f;
                                                           [label sizeToFit];
                                                           label.center = UH_SessionManager.rootWindow.center;
                                                           [UH_SessionManager.rootWindow addSubview:label];
                                                           
                                                           [UIView animateWithDuration:1.5f animations:^{
                                                               label.alpha = 0.f;
                                                           } completion:^(BOOL finished) {
                                                               [label removeFromSuperview];
                                                               label = nil;
                                                           }];
                                                       });
                                                   }
                                                   
                                                   
                                                   if (error) {
                                                       if (error.code == NSURLErrorTimedOut) {
                                                           RLog(@"UserHaibit : screen Upload request timeout");
                                                       }
                                                       screenInformation.uploadTryCount++;
                                                       if (screenInformation.uploadTryCount >3) {
                                                           [[NSFileManager defaultManager] removeItemAtPath:imgFileName error:nil];
                                                           RLog(@"재전송 시도 실패 삭제");
                                                       } else {
                                                           RLog(@"%d", screenInformation.uploadTryCount);
                                                       }
                                                   } else {
                                                       
                                                       if ([UH_Util responseCheck:responseObject]) {
                                                           [UH_Util logDebugType:UHDebugTypeScreenCaptureImage withMessage:@"Succeeded to send"];
                                                           RLog(@"upload screen image %@ / %@",screenInformation.fileName, screenInformation.screenName );
//                                                           screenInfo.orientation = UH_SessionManager.nowOrientation;
                                                           [UH_ImageManager.screenShotSet addObject:screenInformation];
                                                           screenInformation.isCompleteUploadScreenshot = YES;
                                                           
//                                                       debug::파일 삭제
                                                           //기본값 활성
                                                           [[NSFileManager defaultManager] removeItemAtPath:imgFileName error:nil];
                                                       }
                                                   }
                                                  
                                                   
                                                   [self chkSendScreenInfo];
                                               }];
            [uploadTask resume];
        }else{
            [[NSFileManager defaultManager]removeItemAtPath:imgFileName error:nil];
        }
    } @catch (NSException *exception) {
        [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                   typeOf:UHInnerDataTypeTryCatch
                                               actionFlow:nil];
    }
    
    return YES;
}

-(void)objectMessageSend:(UH_ObjectInfo * _Nonnull)objectInfo screenName:(NSString * _Nonnull)screenName{
    RLog(@"%@ / %@", [objectInfo build], screenName);
    if (!UH_SessionManager.onSession ||
        !UH_SessionManager.nowShowController ||
        !screenName ||
        !objectInfo) {
        
        return ;
    }
    @try {
        self.status = UHConnectionStatusFileSend;

        NSString *urlStr = [NSString stringWithFormat:@"%@/v2/a/%@/o", UHGatewayHost, UH_SessionManager.apiKey];
        NSString *objectImagePath = [NSString stringWithFormat:@"%@/%@.jpg", objectInfo.path, objectInfo.fileName];
        UIImage *objectImage = [UIImage imageWithContentsOfFile:objectImagePath];

        NSData *imgData = UIImageJPEGRepresentation(objectImage, 1.0f);
        
        if (imgData != nil && imgData.length != 0) {
            
            NSDictionary *paramDic = [UH_SessionManager.currentSession sessionInformationAddObject:@{
                                                                                                     UHProtocolKeyScreenInformation : [[UH_ViewControllerManager sharedInstance].screenHistoryList[screenName] build],
                                                                                                     UHProtocolKeyObjectInformation : [objectInfo build],
                                                                                                     @"i" : [UH_SessionManager.currentSession.sessionInfo build],
                                                                                                     UHProtocolKeyCurrentTimeStamp : @(UHNowTimeStamp)
                                                                                                     }];
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:paramDic options:0 error:nil];
            NSMutableURLRequest *request;
            if (UH_SessionManager.isBase64) {
                request = [UH_MutableURLRequest objectRequestBase64Url:urlStr jsonData:jsonData imageData:imgData objectInformation:objectInfo test:paramDic];
            } else {
                request = [UH_MutableURLRequest objectMultipartformUrl:urlStr jsonData:jsonData imageData:imgData objectInformation:objectInfo];
            }
            
            request.timeoutInterval = requestTimeoutInterval;

            NSURLSessionUploadTask *uploadTask;
            uploadTask = [_manager uploadTaskWithStreamedRequest:request
                                                        progress:nil
                                               completionHandler:^(NSURLResponse *_Nonnull response, id _Nullable responseObject, NSError *_Nullable error){
                                                   
                                                   if (error) {
                                                       RLog(@"%@", error);
                                                       if (error.code == NSURLErrorTimedOut) {
                                                           NSLog(@"UserHabit : object Message Send upload request time out");
                                                       }
                                                   }else{
                                                       RLog(@"object upload server response =>%@ // objectid =>%@ // file name=>%@", responseObject, objectInfo.objectId ,objectInfo.fileName);
                                                       //todo::define으로 만들것
                                                       if ([responseObject[@"status"] isEqualToString:@"OK"]) {
                                                           [[NSFileManager defaultManager] removeItemAtPath:objectImagePath
                                                                                                      error:nil];
                                                           [UH_Util logDebugType:UHDebugTypeNetwork withMessage:@"Object Upload Success"];
                                                       }
                                                       
                                  
                                                       if (UH_ImageManager.objectShotSet[UH_SessionManager.nowShowController] == nil) {
                                                           UH_ImageManager.objectShotSet[UH_SessionManager.nowShowController] = [NSMutableArray array];
                                                       }
                                                       [UH_ImageManager.objectShotSet[UH_SessionManager.nowShowController] addObject:objectInfo.objectId];
                                                   }
                                               }];
            [uploadTask resume];
        }
    } @catch (NSException *exception) {
        
        RLog(@"%@", exception.reason);
        [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                   typeOf:UHInnerDataTypeTryCatch
                                               actionFlow:nil];
    }
}

-(void)printChangeApiKey{
    NSLog(@"%@", [NSString stringWithFormat:@"%@%@%@%@%@%@%@",
                  @"Please check if your API key is valid and try again with the correct API key.\n\n",
                  @"██╗   ██╗███████╗███████╗██████╗ ██╗  ██╗ █████╗ ██████╗ ██╗████████╗\n",
                  @"██║   ██║██╔════╝██╔════╝██╔══██╗██║  ██║██╔══██╗██╔══██╗██║╚══██╔══╝\n",
                  @"██║   ██║███████╗█████╗  ██████╔╝███████║███████║██████╔╝██║   ██║   \n",
                  @"██║   ██║╚════██║██╔══╝  ██╔══██╗██╔══██║██╔══██║██╔══██╗██║   ██║   \n",
                  @"╚██████╔╝███████║███████╗██║  ██║██║  ██║██║  ██║██████╔╝██║   ██║   \n",
                  @" ╚═════╝ ╚══════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ ╚═╝   ╚═╝   \n"]);
}

-(void)sendScrollScreenInfo:(UH_ScrollImageInformation *)scrollViewImageInformation withScreenInfo:(UH_ScreenInfo *)screenInformation{

    NSDictionary *data = [UH_SessionManager.currentSession
                          sessionInformationAddObject:@{
                                                        UHProtocolKeyScreenInformation : [screenInformation build],
                                                        @"tcnt":@([screenInformation.scrollViewInformation.images count]),
                                                        @"ssidx":@(scrollViewImageInformation.index + 1),
                                                        @"width":@(scrollViewImageInformation.size.width),
                                                        @"height":@(scrollViewImageInformation.size.height),
                                                        @"svk":screenInformation.scrollViewInformation.scrollViewName
                                                        }];
    
    self.status = UHConnectionStatusFileSend;
    
    NSString *urlStr = [NSString stringWithFormat:@"%@/v2/a/%@/ss", UHGatewayHost, UH_SessionManager.apiKey];
    UIImage *objectImage = [UIImage imageWithContentsOfFile:scrollViewImageInformation.filePath];
    NSData *imgData = UIImageJPEGRepresentation(objectImage, 1.0f);
    if (imgData&& imgData.length != 0) {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:0 error:nil];
        NSMutableURLRequest *request;
 
//        if (UH_SessionManager.isBase64) {
//            request = [UH_MutableURLRequest objectRequestBase64Url:urlStr jsonData:jsonData imageData:imgData objectInformation:objectInfo];
//        } else {
            request = [UH_MutableURLRequest objectMultipartformUrl:urlStr jsonData:jsonData imageData:imgData fileName:[NSString stringWithFormat:@"%d", scrollViewImageInformation.index + 1]];
//        }
        
        request.timeoutInterval = requestTimeoutInterval;
        NSURLSessionUploadTask *uploadTask;
        uploadTask = [_manager uploadTaskWithStreamedRequest:request
                                                    progress:nil
                                           completionHandler:^(NSURLResponse *_Nonnull response, id _Nullable responseObject, NSError *_Nullable error){
                                               if (error) {
                                                   if (error.code == NSURLErrorTimedOut) {
                                                       NSLog(@"UserHabit : object Message Send upload request time out");
                                                   }
                                               }else{
                                                   RLog(@"%@", responseObject);
//                                                   [[NSFileManager defaultManager] removeItemAtPath:scrollViewImageInformation.filePath error:nil];
                                                   [UH_Util logDebugType:UHDebugTypeScreenCaptureImage withMessage:@"Succeeded to send"];
                                               }

                                           }];
            [uploadTask resume];
        }
//    } @catch (NSException *exception) {
//        [UH_SessionManager.currentSession insertInnerData:exception.reason
//                                                   typeOf:UHInnerDataTypeTryCatch
//                                               actionFlow:nil];
//    }

}


-(void)downloadFileURL:(NSString *)url completionHandler:(void (^)(NSURLResponse *response, NSURL *filePath, NSError *error))completionHandler{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    UHURLSessionManager *manager = [[UHURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *libraryUrl = [[NSFileManager defaultManager] URLForDirectory:NSLibraryDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        libraryUrl = [libraryUrl URLByAppendingPathComponent:@"/UserHabit/"];
        return [libraryUrl URLByAppendingPathComponent:[response suggestedFilename]];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        completionHandler(response, filePath, error);
    }];
    [downloadTask resume];
}
@end
