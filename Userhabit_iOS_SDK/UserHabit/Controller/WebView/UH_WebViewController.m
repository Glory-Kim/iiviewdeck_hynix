//
//  UH_WebViewController.m
//  UserHabit
//
//  Created by lotco on 2017. 12. 29..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import "UH_WebViewController.h"
#import "UH_SessionController.h"
#import "UH_ViewControllerManager.h"
#import "UH_FileController.h"
#import "UH_ActionFlow.h"
#import "UH_TapInteraction.h"

#import "UH_UITapGestureRecognizer.h"
#import "UH_ConnectionController.h"
#import "UH_ObjectInfo.h"
#import "UH_ManualCaptureController.h"
#import "UH_FileController.h"

#import "UH_WKWebViewObject.h"

NSString *const webViewObjectId = @"userhabitId";
NSString *const webViewObjectBeganXdown = @"userhabitXdown";
NSString *const webViewObjectBeganYdown = @"userhabitYdown";

NSString *const webViewFunctionUserhabitReadObject = @"userhabitReadObject();";

@implementation UH_WebViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        _webViewSet = [NSMutableSet new];
    }
    return self;
}

#pragma mark - public
- (void)captureWebviewObjectImage{
    
    [[self visibleWebObjectSet] enumerateObjectsUsingBlock:^(UH_WebViewObject *  _Nonnull obj, BOOL * _Nonnull stop) {
        UH_WKWebViewObject *inWebViewObject = (UH_WKWebViewObject *)obj;
        [inWebViewObject.webView evaluateJavaScript:webViewFunctionUserhabitReadObject completionHandler:nil];
//            [inWebViewObject.webView evaluateJavaScript:[NSString stringWithFormat:@"alert('%@');", inWebViewObject.name] completionHandler:nil];
    }];
}

- (void)cropObjectToWebviewImage:(UIImage *)image withWebObject:(__weak UH_WebViewObject *)webObject{
    webObject.objectCaptureActionBlock = ^(NSError *error) {
        UH_ScreenInfo *screenInfo = [UH_ViewControllerManager sharedInstance].screenHistoryList[NSStringFromClass([UH_VCManager.visibleViewController class])];
        if (!screenInfo.objectSet) {
            screenInfo.objectSet = [NSMutableSet new];
        }
        
        //todo:: 모델 만들기
        for (NSMutableDictionary * inObject in webObject.objectArray) {
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"objectId", inObject[UHWebObjectKeyId]];
            NSSet *compareSet = [screenInfo.objectSet filteredSetUsingPredicate:predicate];
            
            if ([inObject[UHWebObjectKeyWidth] intValue] > 0 &&
                [inObject[UHWebObjectKeyHeight] intValue] &&
                ![compareSet anyObject]) {
                
                RLog(@"오브젝트 추가");
                
                CGRect objectCropRect = CGRectMake([inObject[UHWebObjectKeyLeft] intValue],
                                                   [inObject[UHWebObjectKeyTop] intValue] ,
                                                   [inObject[UHWebObjectKeyWidth] intValue],
                                                   [inObject[UHWebObjectKeyHeight] intValue]);
                
                UIImage *objectImage = [self cropImage:image rect:objectCropRect];
                inObject[UHWebObjectKeyPath] = UH_SessionManager.userhabitObjectImagePath;
                inObject[UHWebObjectKeyFileName] = [UH_Util imageFileName];
                NSString *path = [NSString stringWithFormat:@"%@/%@.jpg", inObject[UHWebObjectKeyPath], inObject[UHWebObjectKeyFileName]];
                
                NSError *error;
                if ([UIImagePNGRepresentation(objectImage) writeToFile:path options:NSDataWritingWithoutOverwriting error:&error]) {
                    RLog(@"파일 생성 성공 %@ / %@", inObject[UHWebObjectKeyId], path);
                } else {
                    RLog(@"파일 생성 실패 %@", error);
                }
                
                [UIImagePNGRepresentation(objectImage) writeToFile:path atomically:YES];
                
                UH_ObjectInfo *objectInfo = [[UH_ObjectInfo alloc] initId:inObject[UHWebObjectKeyId]
                                                                   typeOf:[UH_Util removeSwiftTargetName:UH_VCManager.visibleViewController]
                                                                    frame:objectCropRect];
                objectInfo.path = inObject[UHWebObjectKeyPath];
                objectInfo.fileName = inObject[UHWebObjectKeyFileName];
                
                RLog(@"%@   /// %@", objectInfo.path, objectInfo.fileName);
                [screenInfo.objectSet addObject:objectInfo];
                
            }
#ifdef UHForDeveloper
            else {
                
                
                RLog(@"중복 추가 안함 %@ //// %@ ",inObject[UHWebObjectKeyId], ((UH_ObjectInfo *)[compareSet anyObject]).objectId);
            }
#endif
        }
        
        [UH_SessionManager.connectionController chkSendObjectInfo:NSStringFromClass([UH_VCManager.visibleViewController class])];
    };
    
    RLog(@"%@", webObject.objectArray);
    if (webObject.objectArray) {
        webObject.objectCaptureActionBlock(nil);
    }
}

- (NSSet *)visibleWebObjectSet {
    RLog(@"%@", _webViewSet);
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"visibleViewController", UH_VCManager.visibleViewController];
    return [_webViewSet filteredSetUsingPredicate:predicate];
}

#pragma mark - util

- (NSString *)makeWebViewName:(UIView *)webView{
    return [NSString stringWithFormat:@"%@-%@",
                NSStringFromClass([UH_VCManager.visibleViewController class]),
                NSStringFromCGRect([webView convertRect:webView.frame toView:UH_SessionManager.rootWindow])];
}

#pragma mark - web method
-(NSDictionary *)messageToDict:(NSString *)message{
    RLog_ViewCycle;
    NSMutableDictionary *dict = [NSMutableDictionary new];
    NSArray *parameter = [message componentsSeparatedByString:@"&"];
    
    for (NSString *inParameter in parameter) {
        NSArray *data = [inParameter componentsSeparatedByString:@"="];
        if ([data count] >= 2) {
            NSString *value = [inParameter substringFromIndex:[data[0] length] + 1];
            NSString *decoded = [value stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            dict[data[0]] = decoded;
        } else {
            dict[data[0]] = data[1];
        }
    }
    return dict;
}

- (UH_WebViewObject *)webViewObjectInquiryWebviewName:(NSString *)webViewName{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"name", webViewName];
    NSSet *compareWebViewSet = [_webViewSet filteredSetUsingPredicate:predicate];
    
    if ([compareWebViewSet count] == 1) {
        return [compareWebViewSet anyObject];
    } else {
        return  nil;
    }
}

-(void)clickNoObject:(NSDictionary *)webviewMessage{
    
    CGPoint webviewEndedPoint = CGPointMake([webviewMessage[@"xUp"]intValue], [webviewMessage[@"yUp"]intValue]);
    CGPoint deviceWindowPoint;
    
    UH_WebViewObject *webViewObject = [self webViewObjectInquiryWebviewName:webviewMessage[@"webViewName"]];
    deviceWindowPoint = [webViewObject.view convertPoint:webviewEndedPoint
                                                  toView:UH_SessionManager.rootWindow];
    
    CGPoint webviewBeganPoint = CGPointMake([webviewMessage[webViewObjectBeganXdown] intValue], [webviewMessage[webViewObjectBeganYdown] intValue]);
    
    UHSwipeDirection swipeDirection = [UH_UITapGestureRecognizer checkSwipeGesture:webviewBeganPoint endedPoint:webviewEndedPoint];
    
    if (swipeDirection == UHSwipeDirectionNil) {
        UH_TapInteraction *tap = [[UH_TapInteraction alloc] initPosition:deviceWindowPoint];
        UH_ActionFlow *actionFlow = [UH_SessionManager.currentSession genActionFlowTap:UHNowTimeStamp - (long long)(UHGestureProcessDelayTime * 1000) with:tap];
        [UH_SessionManager.currentSession insertActionFlow:actionFlow];
    }
}

-(void)clickObject:(NSDictionary *)webviewMessage{
    
    CGPoint webviewBeganPoint = CGPointMake([webviewMessage[webViewObjectBeganXdown] intValue], [webviewMessage[webViewObjectBeganYdown] intValue]);
    CGPoint webviewEndedPoint = CGPointMake([webviewMessage[@"xUp"] intValue], [webviewMessage[@"yUp"] intValue]);
    
    CGPoint deviceWindowPoint;
    
    UH_WebViewObject *webViewObject = [self webViewObjectInquiryWebviewName:webviewMessage[@"webViewName"]];
    
    deviceWindowPoint = [webViewObject.view convertPoint:webviewEndedPoint
                                                  toView:UH_SessionManager.rootWindow];
    
    UHSwipeDirection swipeDirection = [UH_UITapGestureRecognizer checkSwipeGesture:webviewBeganPoint endedPoint:webviewEndedPoint];
    
    if (swipeDirection == UHSwipeDirectionNil) {
        UH_TapInteraction *tap = [[UH_TapInteraction alloc] initPosition:deviceWindowPoint];
        tap.objectId = webviewMessage[webViewObjectId];
        tap.objectDescription = webviewMessage[webViewObjectId];
        UH_ActionFlow *actionFlow = [UH_SessionManager.currentSession genActionFlowTap:UHNowTimeStamp - (long long)(UHGestureProcessDelayTime * 1000) with:tap];
        [UH_SessionManager.currentSession insertActionFlow:actionFlow];
    }
}

- (UIImage *)cropImage:(UIImage *)image rect:(CGRect)rect {
    if (image.scale > 1.0f) {
        rect = CGRectMake(rect.origin.x * image.scale,
                          rect.origin.y * image.scale,
                          rect.size.width * image.scale,
                          rect.size.height * image.scale);
    }
    
    CGImageRef imageRef = CGImageCreateWithImageInRect(image.CGImage, rect);
    UIImage *result = [UIImage imageWithCGImage:imageRef scale:image.scale orientation:image.imageOrientation];
    CGImageRelease(imageRef);
    
    return result;
}

#pragma mark - WKWebView
- (void)settingWKWebView:(WKWebView *)webView{
    NSString *compareWebViewName = [self makeWebViewName:webView];
    UH_WebViewObject *compareWebViewObject = [self webViewObjectInquiryWebviewName:compareWebViewName];
    
    [[UH_ManualCaptureController sharedInstance] changeScreen];    
    
    if (!compareWebViewObject) {
        RLog(@"WKWebView 추가");
        UH_WKWebViewObject *webViewObject = [UH_WKWebViewObject new];
        [webViewObject setWebView:webView withVisibleViewController:UH_VCManager.visibleViewController];
        
        [[webView.configuration userContentController] removeScriptMessageHandlerForName:@"UserHabitMessageHandler"];
        [[webView.configuration userContentController] addScriptMessageHandler:self name:@"UserHabitMessageHandler"];

        
        [_webViewSet addObject:webViewObject];
        
    } else {
        RLog(@"중복 웹뷰 추가 안함!!");
    }
    RLog(@"%@", _webViewSet);
}

- (void)userContentController:(WKUserContentController *)userContentController
      didReceiveScriptMessage:(WKScriptMessage *)message{
    RLog(@"%@", message.body);
    UH_WKWebViewObject *wkWebViewObject = (UH_WKWebViewObject *)[self webViewObjectInquiryWebviewName:message.body[@"webViewName"]];
    
    if ([message.body isKindOfClass:[NSDictionary class]]) {
        if ([message.body[@"methodName"] isEqualToString:@"clickNoObject"]) {
            [self clickNoObject:message.body];
        } else if ([message.body[@"methodName"] isEqualToString:@"clickObject"]) {
            [self clickObject:message.body];
        } else if ([message.body[@"methodName"] isEqualToString:@"objectList"]) {
            
            
            if (!wkWebViewObject.objectArray) {
                wkWebViewObject.objectArray = [NSArray new];
            }
            
            wkWebViewObject.objectArray = message.body[@"objects"];
            [wkWebViewObject.objectArray enumerateObjectsUsingBlock:^(NSMutableDictionary *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                obj[@"id"] = [obj[@"id"] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            }];
            
            if (wkWebViewObject.objectCaptureActionBlock) {
                wkWebViewObject.objectCaptureActionBlock(nil);
            }

        } else if ([message.body[@"methodName"] isEqualToString:@"setScreen"]) {
            [UserHabit setScreen:wkWebViewObject.visibleViewController withName:message.body[@"name"]];
        } else if ([message.body[@"methodName"] isEqualToString:@"setContent"]) {
            [UserHabit setContentKey:message.body[@"key"] value:message.body[@"value"]];
        }
        
    }else {
        RLog(@"글자 %@", message.body);
    }
}
@end
