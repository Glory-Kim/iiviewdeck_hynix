//
//  UH_WebViewController.h
//  UserHabit
//
//  Created by lotco on 2017. 12. 29..
//  Copyright © 2017년 Andbut. All rights reserved.
//
//#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@class UH_WebViewObject;

@interface UH_WebViewController : NSObject<WKScriptMessageHandler>{

}
/*
 *  webview set
 */
@property NSMutableSet *webViewSet;

- (void)cropObjectToWebviewImage:(UIImage *)image withWebObject:(UH_WebViewObject *)webObject;
- (void)captureWebviewObjectImage;
- (NSSet *)visibleWebObjectSet;

#pragma mark - WKWebView
- (void)settingWKWebView:(WKWebView *)webView;
@end

