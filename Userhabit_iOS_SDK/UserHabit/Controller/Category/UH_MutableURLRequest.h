//
//  NSMutableURLRequest+UserHabit.h
//  UserHabit
//
//  Created by r on 2017. 6. 9..
//  Copyright © 2017년 Andbut. All rights reserved.
//
/*
 * 카테고리로 작업을 하면 sdk에서 참조 에러가 남
 * 카테고리 패턴으로 사용하기 위해 만든 하위 클래스
 */
#import <Foundation/Foundation.h>
#import "UH_ObjectInfo.h"

@interface UH_MutableURLRequest : NSMutableURLRequest

#pragma mark - make base64 request Only use VP
+ (NSMutableURLRequest *)requestBase64Url:(NSString *)urlString withObject:(id)object withData:(NSData *)data;
+ (NSMutableURLRequest *)sessionEndRequestBase64Url:(NSString *)urlString fileData:(NSData *)fileData object:(id)object;
+ (NSMutableURLRequest *)objectRequestBase64Url:(NSString *)urlString jsonData:(NSData *)jsonData imageData:(NSData *)imageData objectInformation:(UH_ObjectInfo *)objectInformation test:(id)test;

#pragma mark - make multipartform request
+ (NSMutableURLRequest *)requestMultipartFormUrl:(NSString *)urlString object:(id)object data:(NSData *)data fileName:(NSString *)fileName;
+ (NSMutableURLRequest *)sessionEndRequestMultipartFormUrl:(NSString *)urlString fileStream:(NSInputStream *)fileStream fileInfo:(NSDictionary *)fileInfo jsonData:(NSData *)jsonData;
+ (NSMutableURLRequest *)objectMultipartformUrl:(NSString *)urlString jsonData:(NSData *)jsonData imageData:(NSData *)imageData objectInformation:(UH_ObjectInfo *)objectInformation;
/*
 * 스크롤뷰 전송 메소드
 */
+ (NSMutableURLRequest *)objectMultipartformUrl:(NSString *)urlString jsonData:(NSData *)jsonData imageData:(NSData *)imageData fileName:(NSString *)fileName;

@end
