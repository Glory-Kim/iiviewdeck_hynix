//
//  NSMutableURLRequest+UserHabit.m
//  UserHabit
//
//  Created by r on 2017. 6. 9..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import "UH_MutableURLRequest.h"
#import "UHURLRequestSerialization.h"
#import "UH_SessionController.h"

@implementation UH_MutableURLRequest

#pragma mark - make base64 request Only use VP
+ (NSMutableURLRequest *)requestBase64Url:(NSString *)urlString withObject:(id)object withData:(NSData *)data{
    
    NSString *imageString = [NSString stringWithFormat:@"""%@""", [data base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn]];
    
    NSMutableDictionary *httpBodyDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"d":object}];
    httpBodyDictionary[@"dd"] = imageString;
    httpBodyDictionary[@"dt"] = @"image/jpeg";
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:httpBodyDictionary
                                                       options:0
                                                         error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    request.HTTPMethod = UHHttpMethodTypePOST;
    request.HTTPBody = jsonData;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    request.timeoutInterval = requestTimeoutInterval;
    
    return request;
}

+ (NSMutableURLRequest *)sessionEndRequestBase64Url:(NSString *)urlString fileData:(NSData *)fileData object:(id)object{
    NSString *fileString = [NSString stringWithFormat:@"""%@""", [fileData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn]];
    
    NSMutableDictionary *httpBodyDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"d":object}];
    httpBodyDictionary[@"dd"] = fileString;
    httpBodyDictionary[@"dt"] = @"JSON-gzip";
    
    NSData *bodyData = [NSJSONSerialization dataWithJSONObject:httpBodyDictionary
                                                       options:0
                                                         error:nil];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    request.HTTPMethod = UHHttpMethodTypePOST;
    request.HTTPBody = bodyData;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    request.timeoutInterval = requestTimeoutInterval;
    
    return request;
}

+ (NSMutableURLRequest *)objectRequestBase64Url:(NSString *)urlString jsonData:(NSData *)jsonData imageData:(NSData *)imageData objectInformation:(UH_ObjectInfo *)objectInformation test:(id)test{
//    NSData *testData = [NSJSONSerialization dataWithJSONObject:@{} options:0 error:nil];
    
    NSString *imageString = [NSString stringWithFormat:@"""%@""", [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn]];
//    NSString *jsonString = [NSString stringWithFormat:@"""%@""", [jsonData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn]];

    //    NSMutableDictionary *httpBodyDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"d":jsonData}];
    NSMutableDictionary *httpBodyDictionary = [NSMutableDictionary new];
//    httpBodyDictionary[@"dd2"] = @{@"asdasd":@"asdsad"};
    httpBodyDictionary[@"d"] = test;
//    NSMutableDictionary *httpBodyDictionary = [NSMutableDictionary dictionaryWithDictionary:@{@"d":@""}];
    httpBodyDictionary[@"dd"] = imageString;
    httpBodyDictionary[@"dt"] = @"image/jpeg";
    
    RLog(@"%@", httpBodyDictionary);

    NSData *bodyData = [NSJSONSerialization dataWithJSONObject:httpBodyDictionary
                                                       options:0
                                                         error:nil];

    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    request.HTTPMethod = UHHttpMethodTypePOST;
    request.HTTPBody = bodyData;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    request.timeoutInterval = requestTimeoutInterval;
    return request;
}

#pragma mark - make multipartform request
+ (NSMutableURLRequest *)requestMultipartFormUrl:(NSString *)urlString object:(id)object data:(NSData *)data fileName:(NSString *)fileName{
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object
                                                       options:0
                                                         error:nil];
    
    NSMutableURLRequest *request = [[UHHTTPRequestSerializer serializer] multipartFormRequestWithMethod:UHHttpMethodTypePOST
                                                                                              URLString:urlString
                                                                                             parameters:nil
                                                                              constructingBodyWithBlock:^(id<UHMultipartFormData> formData){
                                                                                  
                                                                                  [formData appendPartWithFileData:data
                                                                                                              name:@"f"
                                                                                                          fileName:fileName
                                                                                                          mimeType:@"image/jpeg"];
                                                                                  [formData appendPartWithFormData:jsonData
                                                                                                              name:@"d"];
                                                                              } error:nil];
    request.timeoutInterval = requestTimeoutInterval;
    
    return request;
}

+ (NSMutableURLRequest *)sessionEndRequestMultipartFormUrl:(NSString *)urlString fileStream:(NSInputStream *)fileStream fileInfo:(NSDictionary *)fileInfo jsonData:(NSData *)jsonData{
    RLog_ViewCycle;
    //하이닉스 전용
    NSString *fileName = [NSString stringWithFormat:@"%@-%@.gz", UH_SessionManager.apiKey, UH_SessionManager.venderIdentifier];
//    NSString *fileName = [NSString stringWithFormat:@"%@-%@.zip", UH_SessionManager.apiKey, UH_SessionManager.venderIdentifier];
    NSMutableURLRequest *request;
    request = [[UHHTTPRequestSerializer serializer] multipartFormRequestWithMethod:UHHttpMethodTypePOST
                                                                         URLString:urlString
                                                                        parameters:nil
                                                         constructingBodyWithBlock:^(id<UHMultipartFormData> formData){
                                                             [formData appendPartWithInputStream:fileStream
                                                                                            name:@"f"
                                                                                        fileName:fileName
                                                                                          length:[fileInfo fileSize]
                                                                                        mimeType:@"JSON-gzip"];
                                                             
                                                             [formData appendPartWithFormData:jsonData name:@"d"];
                                                         } error:nil];
    
    
    return request;
}

+ (NSMutableURLRequest *)objectMultipartformUrl:(NSString *)urlString jsonData:(NSData *)jsonData imageData:(NSData *)imageData objectInformation:(UH_ObjectInfo *)objectInformation{
    
    NSMutableURLRequest *request = [[UHHTTPRequestSerializer serializer] multipartFormRequestWithMethod:UHHttpMethodTypePOST
                                                                                              URLString:urlString
                                                                                             parameters:nil
                                                                              constructingBodyWithBlock:^(id<UHMultipartFormData> formData){
                                                                                  [formData appendPartWithFormData:jsonData
                                                                                                              name:@"d"];
                                                                                  [formData appendPartWithFileData:imageData
                                                                                                              name:@"f"
                                                                                                          fileName:[NSString stringWithFormat:@"%@.jpg", objectInformation.objectId]
                                                                                                          mimeType:@"image/jpeg"];
                                                                              }error:nil];
    return request;
}


+ (NSMutableURLRequest *)objectMultipartformUrl:(NSString *)urlString jsonData:(NSData *)jsonData imageData:(NSData *)imageData fileName:(NSString *)fileName{
    
    NSMutableURLRequest *request = [[UHHTTPRequestSerializer serializer] multipartFormRequestWithMethod:UHHttpMethodTypePOST
                                                                                              URLString:urlString
                                                                                             parameters:nil
                                                                              constructingBodyWithBlock:^(id<UHMultipartFormData> formData){
                                                                                  [formData appendPartWithFormData:jsonData
                                                                                                              name:@"d"];
                                                                                  [formData appendPartWithFileData:imageData
                                                                                                              name:@"f"
                                                                                                          fileName:[NSString stringWithFormat:@"%@.jpg", fileName]
                                                                                                          mimeType:@"image/jpeg"];
                                                                              }error:nil];
    return request;
}


@end
