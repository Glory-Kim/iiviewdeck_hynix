//
//  UH_NotiManager.m
//  UserHabit
//
//  Created by DoHyoungKim on 2015. 12. 6..
//  Copyright © 2015년 Andbut. All rights reserved.
//

#import "UH_NotiManager.h"
#import "UH_Util.h"
#import "UH_ViewControllerManager.h"
#import "UH_Session.h"
#import "UH_ActionFlow.h"
#import "UH_ActionController.h"


@interface UH_NotiManager()

@property (atomic, strong) NSTimer *delayTimer;

@end

@implementation UH_NotiManager

+ (instancetype)sharedInstance {
    static id singletonObject = nil;

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singletonObject = [UH_NotiManager new];
        [singletonObject initializeNoti];
    });

    return singletonObject;
}

- (void)initializeNoti {
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(AppDidEnterBackground:) name:UIApplicationWillResignActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(appWillForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(terminateApp:) name:UIApplicationWillTerminateNotification object:nil];
    
    //수동 스크린샷, 가로모드 추적
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserverForName:UIDeviceOrientationDidChangeNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        
        if (UH_SessionManager.isDevelopMode) {
            [[UH_ManualCaptureController sharedInstance] orientationChanged:note];
//            return;
        }
        
        
        UH_VCManager.orientation = [[UIDevice currentDevice] orientation];

        //todo::가로세로 화면 설정
//        [UH_VCManager addViewController:[UH_ViewControllerManager sharedInstance].visibleViewController
//                         withScreenName:UH_SessionManager.nowShowController
//                          withIsSubView:NO];
    }];
}

-(void)terminateApp:(NSNotification *)noti{
    RLog_ViewCycle;
    [UH_SessionManager sessionClose];
}


//앱이 background 모드로 돌아갈때 호출되는 함수
- (void)appWillResignActive:(NSNotification *)notif {
    RLog_ViewCycle;
    
    BOOL backgroundSupported = NO;
    if ([[UIDevice currentDevice] respondsToSelector:@selector(isMultitaskingSupported)]) {
        backgroundSupported = [UIDevice currentDevice].multitaskingSupported;
    }
    
    if (backgroundSupported) {
        float delayTime = UH_SessionManager.sessionDelayTime.floatValue;
        //        NSLog(@"Userhabit : in background mode session delay time = %f", delayTime);
        
        UIApplication *app = [UIApplication sharedApplication];
        self.bgTaskIdentifier = [app beginBackgroundTaskWithExpirationHandler:^{
            [app endBackgroundTask:self.bgTaskIdentifier];
            self.bgTaskIdentifier = UIBackgroundTaskInvalid;
        }];
        
//        UH_Session *currentSession = [UH_SessionController sharedInstance].currentSession;
//        UH_ActionFlow *actionFLow = [UH_SessionManager.currentSession genActionFlowWillResignActive:UHNowTimeStamp];
//        [[UH_SessionController sharedInstance].currentSession insertActionFlow:actionFLow];
        
        [UH_SessionManager.currentSession insertActionFlow:[UH_SessionManager.currentSession genActionFlowWillResignActive:UHNowTimeStamp]];
        
        _delayTimer = [NSTimer scheduledTimerWithTimeInterval:delayTime target:[UH_SessionController sharedInstance] selector:@selector(sessionClose) userInfo:nil repeats:NO];
    } else {
        NSLog(@"Userhabit : this OS is background work not supported");
    }

}

//app이 background 모드에서 돌아올때 호출되는 함수
- (void)appWillForeground:(NSNotification *)noti {
    RLog_ViewCycle;
    if (_delayTimer) {
        [_delayTimer invalidate];
        _delayTimer = nil;
    }
    if (!UH_SessionManager.onSession) {
        [UH_SessionManager sessionStart];
    }
    
    if ([UH_ViewControllerManager sharedInstance].visibleViewController &&
        [UH_SessionController sharedInstance].nowShowController){
        
        [UH_VCManager addViewController:[UH_ViewControllerManager sharedInstance].visibleViewController
                         withScreenName:[UH_SessionController sharedInstance].nowShowController
                          withIsSubView:[UH_ViewControllerManager sharedInstance].isThisSubView];
    }
}

//앱이 백그라운드가 아닌 OS에 점유를 뺏긴 상태로 진입할때 호출 되는 함수
-(void)AppDidEnterBackground:(NSNotification *)notif
{
    RLog_ViewCycle;
    UH_Session *currentSession = [UH_SessionController sharedInstance].currentSession;
    UH_ActionFlow *actionFLow = [currentSession genActionFlowInterBackground:UHNowTimeStamp];

    UH_ActionController *actionController = [UH_ActionController new];
    [actionController processMaximumScrollView:UH_VCManager.visibleViewController.view];
    
    [[UH_SessionController sharedInstance].currentSession insertActionFlow:actionFLow];
}

-(void)keyboardShow:(NSNotification *)notif
{
    UH_Session *currentSession = [UH_SessionController sharedInstance].currentSession;
    UH_ActionFlow *actionFLow = [currentSession genActionFlowKeyboardEnableStatus:UHNowTimeStamp];

    [[UH_SessionController sharedInstance].currentSession insertActionFlow:actionFLow];
}

-(void)keyboardHide:(NSNotification *)notif
{
    UH_Session *currentSession = [UH_SessionController sharedInstance].currentSession;
    UH_ActionFlow *actionFLow = [currentSession genActionFlowKeyboardDisableStatus:UHNowTimeStamp];

    [[UH_SessionController sharedInstance].currentSession insertActionFlow:actionFLow];
}

@end
