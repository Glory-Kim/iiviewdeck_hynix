//
//  UH_ViewControllerManager.m
//  UserHabit
//
//  Created by DoHyoungKim on 2016. 1. 7..
//  Copyright © 2016년 Andbut. All rights reserved.
//

#import "UH_ViewControllerManager.h"
#import "UH_ConnectionController.h"
#import "UH_SessionController.h"
#import "UH_Util.h"
#import "UH_Session.h"
#import "UH_ObjectController.h"
#import "UH_ScreenInfo.h"
#import "UH_ManualCaptureController.h"
#import "UH_ImageController.h"
#import "UH_ManualCaptureController.h"



@implementation UH_ViewControllerManager

+(instancetype)sharedInstance
{
    static UH_ViewControllerManager *singletonObject;

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singletonObject = [[UH_ViewControllerManager alloc]init];
        singletonObject.screenHistoryList = [NSMutableDictionary new];
        singletonObject.secretViewSet = [NSMutableSet new];
    });
    
    return singletonObject;
}
/*
 * 뷰 컨트롤러를 screen 단위로 추가한다
 */
-(void)addViewController:(UIViewController *)controller withScreenName:(NSString *)controllerName withIsSubView:(BOOL)isSubView{
    
    [UH_Util logDebugType:UHDebugTypeScreenChange
              withMessage:controllerName];
    if (controllerName.length == 0 ||
        !controllerName) {
        [UH_Util logDebugType:UHDebugTypeWarning
                  withMessage:[NSString stringWithFormat:@"Please. check controllerName [%@]", controllerName]];
        
        
        return;
    }
    
    @try {
        controllerName = [UH_Util removeSwiftTargetName:controllerName];
        _visibleViewController = controller;
        RLog(@"Change visible view controller %@", _visibleViewController);
        
        if (UH_SessionManager.isDebug &&
            UH_SessionManager.isDevelopMode) {
            if ([UH_ManualCaptureController sharedInstance]) {
                [[UH_ManualCaptureController sharedInstance] checkManualView];
            }
        }
        
        if (!UH_SessionManager.currentSession) {
            RLog(@"error:현재 세션이 존재 하지 않음");
            return ;
        }

        NSString *fragmentName;

        if (isSubView) {
            fragmentName = controllerName;
        }
        _isThisSubView = isSubView;
        UH_ScreenInfo *screenInfo = [[UH_ScreenInfo alloc] initInDefault:controllerName
                                                                fragment:fragmentName
                                                             orientation:UH_VCManager.orientation
                                                                    size:controller.view.frame.size];
        screenInfo.rootViewController = controller;
        UH_ActionFlow *actionFlow = [UH_SessionManager.currentSession genActionFlowScreenStart:UHNowTimeStamp
                                                                                          with:screenInfo];
        
        [UH_SessionManager.currentSession insertActionFlow:actionFlow];

        if (UH_SessionManager.isDevelopMode ) {
            
            if (!_screenHistoryList[controllerName]) {
                _screenHistoryList[controllerName] = screenInfo;
                if ([UH_ImageManager chkScreenShot:UH_SessionManager.nowShowController with:YES]) {
                    
                    RLog(@"이미지 촬영");
                    
                    if ([UH_SessionManager.currentSession chkTransportStatus:UHTransportMethodScreenshotInfo]) {
                        [UH_ImageManager renderScreenShot:screenInfo];
                    }
                    UH_ObjectController *manager = [UH_ObjectController new];
                    
                    [manager readAllObject:self.visibleViewController
                         completionHandler:^(NSException *exception) {
                             if (!exception) {
                                 [[UH_SessionManager connectionController] chkSendScreenInfo];
                                 [[UH_SessionManager connectionController] chkSendObjectInfo:UH_SessionManager.nowShowController];
                             }
                    }];
                } else {
                    RLog(@"스크린샷 수집 안함 %@", [controller class]);
                }
            }
        }
    } @catch (NSException *exception) {
        [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                   typeOf:UHInnerDataTypeTryCatch
                                               actionFlow:nil];
    }
}

#pragma mark - webview
- (void)createWebViewController{
    _webViewController = [UH_WebViewController new];
}
@end
