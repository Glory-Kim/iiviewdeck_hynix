//
//  UH_ViewControllerManager.h
//  UserHabit
//
//  Created by DoHyoungKim on 2016. 1. 7..
//  Copyright © 2016년 Andbut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UH_ScreenInfo.h"
#import "UH_WebViewController.h"

#define UH_VCManager [UH_ViewControllerManager sharedInstance]

@interface UH_ViewControllerManager : NSObject

@property (nonatomic, strong) NSMutableDictionary<NSString *, UH_ScreenInfo *> *screenHistoryList;
@property (nonatomic, strong) NSMutableSet *secretViewSet;
@property (nonatomic) BOOL secretMode;
@property (atomic, weak) UIViewController *touchInViewController;
@property (atomic, weak) UIViewController *visibleViewController;

//@property (nonnull, strong) BOOL visibleWebView;                //

@property (nonatomic, strong) NSArray<NSString *> *excludeArray;
@property (nonatomic, assign) BOOL isThisSubView;
@property (nonatomic, strong) UH_WebViewController *webViewController;
@property (nonatomic) UIDeviceOrientation orientation;

+(instancetype)sharedInstance;
-(void)addViewController:(UIViewController *)controller withScreenName:(NSString *)controllerName withIsSubView:(BOOL)isSubView;

#pragma mark - webview
- (void)createWebViewController;
@end
