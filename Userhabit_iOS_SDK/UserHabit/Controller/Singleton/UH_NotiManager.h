//
//  UH_NotiManager.h
//  UserHabit
//
//  Created by DoHyoungKim on 2015. 12. 6..
//  Copyright © 2015년 Andbut. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "UserHabitConstant.h"
#import "UH_ManualCaptureController.h"
#import "UH_SessionController.h"

@interface UH_NotiManager : NSObject

@property (nonatomic, assign) NSUInteger bgTaskIdentifier;

+(instancetype)sharedInstance;
-(void)initializeNoti;

@end
