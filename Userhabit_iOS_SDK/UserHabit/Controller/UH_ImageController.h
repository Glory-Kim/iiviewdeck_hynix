//
//  UH_ImageController.h
//  UserHabit
//
//  Created by lotco on 13/02/2019.
//  Copyright © 2019 Andbut. All rights reserved.
//

#import <Foundation/Foundation.h>

#define UH_ImageManager [UH_ImageController sharedInstance]

@class UH_ScreenInfo;
@class UH_WebViewObject;
@class UH_ObjectInfo;

@interface UH_ImageController : NSObject

@property (nonatomic, strong) NSMutableSet <UH_ScreenInfo *> *screenShotSet;
@property (nonatomic, strong) NSMutableDictionary *objectShotSet;

@property BOOL forceCapture;


+ (instancetype)sharedInstance;

- (void)renderTakeScreenShot:(UH_ScreenInfo *)screenInformation isForce:(BOOL)isForce;
- (void)renderScreenShot:(UH_ScreenInfo *)screenInfo;
- (BOOL)chkScreenShot:(NSString *)chkName with:(BOOL)isScreen;

#pragma mark - scroll view capture
- (void)scrollCaptureViewProcess:(RCompleteHandlerBlock)completeHandler;
- (void)webviewCaptureViewProcess:(UH_WebViewObject *)webObject;

#pragma mark - object
-(void)takeObjectScreenShot:(UH_ObjectInfo *)objectInformation completeHandler:(RActionBlock)completeHandler;
-(UIImage *)captureControllerImage;
@end
