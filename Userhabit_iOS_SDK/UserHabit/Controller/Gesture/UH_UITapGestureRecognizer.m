//
//  UH_UITapGestureRecognizer.m
//  UserHabit
//
//  Created by r on 2017. 5. 17..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import "UH_UITapGestureRecognizer.h"
#import "UH_SessionController.h"
#import "UH_ViewControllerManager.h"
#import "UH_Util.h"

@implementation UH_UITapGestureRecognizer

- (instancetype)init
{
    self = [super init];
    if (self) {
        touchSet = [NSMutableSet new];
    }
    return self;
}

- (void)gestureSettingInitialization{
    if (gestureTimer) {
        [gestureTimer invalidate];
        [gestureTimer fire];
        gestureTimer = nil;
    }
    
    [touchSet removeAllObjects];
    _beganPoint = CGPointMake(-1, -1);
    _endedPoint = CGPointMake(-1, -1);
    tapCount = 0;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    @try {
        RLog(@"터치 시작");
        UH_VCManager.touchInViewController = UH_VCManager.visibleViewController;
        
        UITouch *touch = [touches anyObject];
        CGPoint beganPoint = [touch locationInView:self.view];
        
        UIPanGestureRecognizer *panGesture = [UH_Util findPanGestureTo:touch.view];
        if (panGesture) {
            [panGesture addTarget:self action:@selector(scrollViewPanGestureAction:)];
        }
        
        if (gestureTimer) {
            [gestureTimer invalidate];
            [gestureTimer fire];
            gestureTimer = nil;
            if (![self comparePoint:beganPoint whitOtherPoint:_beganPoint]) {
                [self processGesture];
            }
        }
        
        _beganPoint = beganPoint;
        _endedPoint = CGPointZero;
        
        for (UITouch *inTouch in [touches allObjects]) {
            [touchSet addObject:inTouch];
        }
        
        RLog(@"%@", touch.view);
        
        
//        UIAlertController
    } @catch (NSException *exception) {
        [UH_Util logDebugType:UHDebugTypeCrash withMessage:@"Faile Read Gesture"];
    }
}

-(void)scrollViewPanGestureAction:(UIPanGestureRecognizer *)gesture{
    if (gesture.state == UIGestureRecognizerStateBegan) {
        if ([UH_Util findScrollView:gesture.view]) {
            _scrollViewBeforeOffset = ((UIScrollView *)gesture.view).contentOffset;
            _scrollView = (UIScrollView *)gesture.view;
        }
    } else if (gesture.state == UIGestureRecognizerStateEnded) {
        CGPoint endPoint = [gesture locationInView:gesture.view.window];
        if ([UH_Util findScrollView:gesture.view]) {
            
            //스크롤뷰의 도달율 계산할때 사용. contentOffset.y에 스크롤뷰의 높이를 더해서 사용한다
            _scrollViewAfterOffset = CGPointMake(((UIScrollView *)gesture.view).contentOffset.x, ((UIScrollView *)gesture.view).contentOffset.y + ((UIScrollView *)gesture.view).bounds.size.height);
            _scrollView = (UIScrollView *)gesture.view;
            _scrollViewEndedPoint = [gesture locationInView:_scrollView];
        }
        
        //뷰의 속성에 시크릿뷰가 있을경우 터치 좌표를 강제로 (-1,-1) 로 바꾼다
        if ([UH_Util checkSecretView:gesture.view]) {
            _endedPoint = CGPointMake(-1.f, -1.f);
        } else {
            _endedPoint = endPoint;
        }

        NSMutableDictionary *InformationDictionary = [NSMutableDictionary new];
        InformationDictionary[UHGestureInformationKeyUITouchArray] = @[];
        if (gesture.view) {
            InformationDictionary[UHGestureInformationKeyTouchObject] = gesture.view;
        }
        
        gestureTimer = [NSTimer scheduledTimerWithTimeInterval:UHGestureProcessDelayTime
                                                        target:self
                                                      selector:@selector(processGesture)
                                                      userInfo:InformationDictionary
                                                       repeats:NO];
        tapCount = (int)gesture.numberOfTouches;
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    RLog(@"터치 끝!");
    
    [super touchesEnded:touches withEvent:event];
    @try {
        if (!touches) {
            return;
        }
        UITouch *touch = [touches anyObject];
        
        if (!touch) {
            return;
        }
        
        if ([UH_Util checkSecretView:touch.view]) {
            _endedPoint = CGPointMake(-1.f, -1.f);
        } else {
            _endedPoint = [touch locationInView:self.view];

            UH_ScrollViewInfo *scrollViewInformation = [UH_Util findScrollView:UH_VCManager.visibleViewController.view];
            if (scrollViewInformation) {
                _scrollViewEndedPoint = [touch locationInView:scrollViewInformation.scrollView];
            }
        }
        
        NSMutableDictionary *InformationDictionary = [NSMutableDictionary new];
        if ([[touches allObjects]firstObject]) {
            InformationDictionary[UHGestureInformationKeyUITouchArray] = [touches allObjects];
        }
        if (touch.view) {
            InformationDictionary[UHGestureInformationKeyTouchObject] = touch.view;
            RLog(@"%@", touch.view);
        }
        gestureTimer = [NSTimer scheduledTimerWithTimeInterval:UHGestureProcessDelayTime
                                                        target:self
                                                      selector:@selector(processGesture)
                                                      userInfo:InformationDictionary
                                                       repeats:NO];
        tapCount = (int)touch.tapCount;
    } @catch (NSException *exception) {
        [UH_Util logDebugType:UHDebugTypeCrash withMessage:@"faile read gesture"];
    }
}

- (void)processGesture{
    NSArray *endedTouchArray = gestureTimer.userInfo[UHGestureInformationKeyUITouchArray];
    id touchObject;
    
    if(gestureTimer.userInfo[UHGestureInformationKeyTouchObject]){
        touchObject = gestureTimer.userInfo[UHGestureInformationKeyTouchObject];
    }
//    if ([touchObject isKindOfClass:[UIView class]]){
//        RLog(@"not view - %@", [touchObject class]);
//    }
    /*
     * [touchSet count] == 1                        터치한 손가락이 하나일 경우만
     * (tapCount == 2 && [touchSet count] == 2)     2번 누르고 손가락 좌표가 2개 일 경우 더블탭인지 확인한다
     * (tapCount == 1 && [touchSet count] == 2)     1번 누르고 손가락 좌표가 2개 일 경우 더블탭인지 확인한다
     * ----------------------------------------------------------------------------------------------------------------
     * (tapCount == 0 && [touchSet count] == 2)     두 손가락으로 핀치, 회전일 확율이 큼 제외
     */
    if ([touchSet count] == 1 ||
        (tapCount == 2 && [touchSet count] ==2) ||
        (tapCount == 1 && [touchSet count] ==2)) {
        if (tapCount == 0){
            
            UHSwipeDirection swipeDirection = [self checkSwipeGesture];
            
            if([self simularPoint]) {
                [_uhGestureDelegate longPressGesture:self withTouchObject:touchObject];
            } else if (swipeDirection != UHSwipeDirectionNil){
                [_uhGestureDelegate swipeGesture:self withDirection:swipeDirection];
            } else {
                /*
                 * 손가락 두개 이상을 터치 할때 1번을 놓기 전에 2번 손가락이 닿아 있으면 생기는 경우
                 */
//                RLog(@"failed read gesture %@ / %@", NSStringFromCGPoint(_beganPoint), NSStringFromCGPoint(_endedPoint));
            }
        } else if (tapCount == 1){
            [_uhGestureDelegate tapGesture:self withTouchObject:touchObject];
            RLog(@"%@", touchObject);
        } else if(tapCount == 2){
            [_uhGestureDelegate doubleTapGesture:self withTouchObject:touchObject];
            [touchSet removeAllObjects];
        } else {
            RLog(@"failed read gesture %@ / %@", NSStringFromCGPoint(_beganPoint), NSStringFromCGPoint(_endedPoint));
        }
    }else {
//        RLog(@"Not support multi touch %d", (int)[touchSet count]);
        [touchSet removeAllObjects];
    }
    
    for (UITouch *inTouch in endedTouchArray) {
        [touchSet removeObject:inTouch];
    }

    [self gestureSettingInitialization];
}
#pragma mark - gesture check util
- (BOOL)simularPoint{
    return [self comparePoint:_beganPoint whitOtherPoint:_endedPoint];
}
- (BOOL)comparePoint:(CGPoint)pointA whitOtherPoint:(CGPoint)pointB{
//    RLog(@"%@ / %@", NSStringFromCGPoint(pointA), NSStringFromCGPoint(pointB));
    
    if (fabs(pointA.x - pointB.x) < UHGestureLongPressAllowablePoint &&
        fabs(pointA.y - pointB.y) < UHGestureLongPressAllowablePoint) {
        return YES;
    } else {
        return NO;
    }
}
- (UHSwipeDirection)checkSwipeGesture{
    return [UH_UITapGestureRecognizer checkSwipeGesture:_beganPoint endedPoint:_endedPoint];
}

#pragma mark - public method

- (void)startProgressGesture{
    RLog_ViewCycle;
    [gestureTimer fire];
}

+ (UHSwipeDirection)checkSwipeGesture:(CGPoint)beganPoint endedPoint:(CGPoint)endedPoint{
    
    CGPoint comparePoint = CGPointMake(beganPoint.x - endedPoint.x,
                                       beganPoint.y - endedPoint.y);
    if (beganPoint.x < 0) {
        return UHSwipeDirectionNil;
    }else if (fabs(comparePoint.x) > UHGestureMinimunMovePoint){
        if(beganPoint.x > endedPoint.x){
            return UHSwipeDirectionLeft;
        } else {
            return UHSwipeDirectionRight;
        }
    }else if(fabs(comparePoint.y) > UHGestureMinimunMovePoint) {
        if(beganPoint.y > endedPoint.y){
            return UHSwipeDirectionUp;
        } else {
            return UHSwipeDirectionDown;
        }
    } else {
        return UHSwipeDirectionNil;
    }
}


@end
