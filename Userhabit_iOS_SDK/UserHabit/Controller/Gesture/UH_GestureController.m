//
//  UH_GestureController.m
//  UserHabit
//
//  Created by r on 2017. 5. 17..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import "UH_GestureController.h"

#import <UIKit/UIKit.h>


#import "UH_SessionController.h"
#import "UH_ViewControllerManager.h"
#import "UH_TapInteraction.h"
#import "UH_SwipeInteraction.h"
#import "UH_ScrollViewOffsetInfo.h"
#import "UH_ObjectController.h"
#import "UH_ActionFlow.h"
#import "UH_SessionInfo.h"
#import "UH_Session.h"

@implementation UH_GestureController

-(void)gestureCreate{
    if(!gesture){
        gesture = [UH_UITapGestureRecognizer new];
        gesture.uhGestureDelegate = self;
        [gesture setCancelsTouchesInView:NO];
    }
}

-(void)windowSetting:(UIWindow *)window{
    [self gestureCreate];
    [window addGestureRecognizer:gesture];
}

-(void)gestureSetting{
    [self gestureCreate];
    [UH_SessionManager.rootWindow addGestureRecognizer:gesture];        
}
#pragma mark - 탭 처리 
- (void)makeActionFlowType:(UHActionFlowType)actionFlowType withGesture:(UH_UITapGestureRecognizer *)gestureRecognizer withTouchView:(UIView *)touchView{
    UH_TapInteraction *tapInteraction = [[UH_TapInteraction alloc]initPosition:gestureRecognizer.endedPoint];
    
    if (touchView.gestureRecognizers.count != 0 ||
        [touchView isKindOfClass:[UIControl class]]) {
        // check an object
        UH_ObjectController *manager = [UH_ObjectController new];
        [manager catchObject:tapInteraction forView:touchView];
        [UH_Util logDebugType:UHDebugTypeObjectTouch
                  withMessage:[UH_Util removeSwiftTargetName:touchView]];
    }
    UH_ActionFlow *actionFlow;
    
    tapInteraction.scrollEndedPoint = gestureRecognizer.scrollViewEndedPoint;
    
    //todo: predicate로 고도화 하기
    //        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"scrollViewInformation.superViewController",NSStringFromClass([UH_VCManager.visibleViewController class])];
    
    UH_ScrollViewInfo *scrollViewInformation;
    
    for (UH_ScrollViewInfo * _Nonnull obj in UH_SessionManager.currentSession.scrollViewArray) {
        if (obj.superViewController == UH_VCManager.touchInViewController) {
            scrollViewInformation = obj;
        }
    }
    if (scrollViewInformation) {
        tapInteraction.scrollMapping = scrollViewInformation.mapping;
    } else {
        tapInteraction.scrollMapping = -1;
    }
    
    /*
     * 제스쳐의 롱탭, 스와이프, 더블탭을 구분하기 위해 처음 터치가 시작되고 UHGestureProcessDelayTime 만큼 딜레이가 되는데 해당 딜레이때문에 터치 전에 화면이동이 잡혀 순서가 꼬여보여서 수동으로 제스쳐의 시간을 UHGestureProcessDelayTime 만큼 빼준다
     */
    if (actionFlowType == UHActionFlowTypeTap) {
        actionFlow = [UH_SessionManager.currentSession genActionFlowTap:UHNowTimeStamp - (long long)(UHGestureProcessDelayTime * 1000)
                                                                   with:tapInteraction];
    } else if (actionFlowType == UHActionFlowTypeDoubleTap) {
        actionFlow = [UH_SessionManager.currentSession genActionFlowDoubleTap:UHNowTimeStamp - (long long)(UHGestureProcessDelayTime * 1000)
                                                                         with:tapInteraction];
    } else if (actionFlowType == UHActionFlowTypeLongPress) {
        actionFlow = [UH_SessionManager.currentSession genActionFlowLongPress:UHNowTimeStamp - (long long)(UHGestureProcessDelayTime * 1000)
                                                                         with:tapInteraction];
    }
    RLog(@"=> %@", [actionFlow build]);
    [UH_SessionManager.currentSession insertActionFlow:actionFlow];
}
    
#pragma mark - tap gesture delegate
- (void)tapGesture:(UH_UITapGestureRecognizer *)gestureRecognizer withTouchObject:(id _Nullable)touchObject{
//    RLog(@"터치 %ld / %@", UHNowTimeStamp, touchObject);
    [self makeActionFlowType:UHActionFlowTypeTap withGesture:gestureRecognizer withTouchView:touchObject];
}

- (void)doubleTapGesture:(UH_UITapGestureRecognizer *)gestureRecognizer withTouchObject:(id _Nullable)touchObject{
    RLog(@"더블 탭");
    [self makeActionFlowType:UHActionFlowTypeDoubleTap withGesture:gestureRecognizer withTouchView:touchObject];
}

- (void)longPressGesture:(UH_UITapGestureRecognizer *)gestureRecognizer withTouchObject:(id _Nullable)touchObject{
    RLog(@"long press");
    [self makeActionFlowType:UHActionFlowTypeLongPress withGesture:gestureRecognizer withTouchView:touchObject];
}

- (void)swipeGesture:(UH_UITapGestureRecognizer *)gestureRecognizer withDirection:(UHSwipeDirection)direction{
    RLog(@"스와이프");
    if (gestureRecognizer.scrollViewAfterOffset.y > 0) {
        UH_ScrollViewInfo *scrollViewInformation = [UH_Util findScrollView:gestureRecognizer.scrollView];
        
        /*
         * systemUpTime =  현재 시간 - 앱 시작 시간
         * lastScrollTime = 마지막으로 스크롤의 컨텐츠 오프셋을 기록한 시간
         * compareTime =
         */
        
        int systemUpTime = (int)(UHNowTimeStamp - UH_SessionManager.currentSession.sessionInfo.uptimeStart);
        int lastScrollTime = UH_SessionManager.currentSession.lastScrollViewOffsetActionFlow.uptime;
        int compareTime = systemUpTime - lastScrollTime;
        
        if (!UH_SessionManager.currentSession.lastScrollViewOffsetActionFlow ||
            compareTime > 500) {
            UH_ScrollViewOffsetInfo *scrollViewOffsetInfo = [UH_ScrollViewOffsetInfo new];
            scrollViewOffsetInfo.scrollMappingNumber = scrollViewInformation.mapping;
            scrollViewOffsetInfo.contentsOffset = gestureRecognizer.scrollViewAfterOffset;
            
            UH_ActionFlow *scrollActionFlow = [[UH_ActionFlow alloc] initAt:UHNowTimeStamp
                                                                       from:UH_SessionManager.currentSession.sessionInfo.uptimeStart + 5
                                                                     typeOf:UHActionFlowTypeScrollView
                                                                       with:scrollViewOffsetInfo];
            
            [UH_SessionManager.currentSession insertActionFlow:scrollActionFlow];
            UH_SessionManager.currentSession.lastScrollViewOffsetActionFlow = scrollActionFlow;
            
        } else {
            RLog(@"0.05f미만");
        }
        
        /*
         * 스크롤뷰의 최대값을 저장한다
         */
        if (scrollViewInformation.maximumScrollViewPosition.y < gestureRecognizer.scrollViewAfterOffset.y) {
            if (gestureRecognizer.scrollViewAfterOffset.y == 0) {
                scrollViewInformation.maximumScrollViewPosition = CGPointMake(gestureRecognizer.scrollViewAfterOffset.x, gestureRecognizer.scrollView.bounds.size.height);
            } else {
                scrollViewInformation.maximumScrollViewPosition = gestureRecognizer.scrollViewAfterOffset;
            }
        }
    }
    UH_SwipeInteraction *swipeInteraction = [[UH_SwipeInteraction alloc]initDirection:direction withGesture:gestureRecognizer];
    
    UH_ScrollViewInfo *scrollViewInformation;
    
    for (UH_ScrollViewInfo * _Nonnull obj in UH_SessionManager.currentSession.scrollViewArray) {
        if (obj.superViewController == UH_VCManager.touchInViewController) {
            scrollViewInformation = obj;
        }
    }
    
    swipeInteraction.scrollEndedPoint = gestureRecognizer.scrollViewEndedPoint;
    
    UH_ActionFlow *actionFlow = [UH_SessionManager.currentSession genActionFlowSwipe:UHNowTimeStamp
                                                                                with:swipeInteraction];
    
    if (scrollViewInformation) {
        swipeInteraction.scrollMapping = scrollViewInformation.mapping;
    } else {
        swipeInteraction.scrollMapping = -1;
    }
    
    [UH_SessionManager.currentSession insertActionFlow:actionFlow];
}

#pragma mark - getter
- (UH_UITapGestureRecognizer *)gesture{
    return gesture;
}
@end
