//
//  UH_GestureController.h
//  UserHabit
//
//  Created by r on 2017. 5. 17..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UH_UITapGestureRecognizer.h"

@interface UH_GestureController : NSObject<UH_GestureDelegate>{
    UH_UITapGestureRecognizer *gesture;
}

- (void)windowSetting:(UIWindow *)window;
- (void)gestureSetting;
- (UH_UITapGestureRecognizer *)gesture;
@end
