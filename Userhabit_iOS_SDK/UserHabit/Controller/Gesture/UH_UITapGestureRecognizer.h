//
//  UH_UITapGestureRecognizer.h
//  UserHabit
//
//  Created by r on 2017. 5. 17..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIGestureRecognizerSubclass.h>
#import "UserHabitConstant.h"

@protocol UH_GestureDelegate;

@interface UH_UITapGestureRecognizer : UITapGestureRecognizer{
    NSMutableSet<UITouch *> *touchSet;
    NSTimer *gestureTimer;
    int tapCount;
}
@property (readonly) CGPoint beganPoint;    //스와이프등 제스쳐에 사용 터치가 시작된 지점을 기록
@property (readonly) CGPoint endedPoint;    //일반적인 탭, 더블탭등 손가락이 떨어진 지점을 기록

#pragma mark - 스크롤 뷰
/*
 * 스크롤을 시작하기 전과 후의 offset을 기록한다 스크롤 도달율 계산에 사용
 * todo: scrollViewBeforeOffset 사용안되는것으로 추정 확인 후 삭제 
 */
@property (readonly) CGPoint scrollViewBeforeOffset;
@property (readonly) CGPoint scrollViewAfterOffset;

/*
 * 스크롤뷰를 기준으로 현재 탭한 상대 좌표
 */
@property (readonly) CGPoint scrollViewEndedPoint;

@property (weak) UIScrollView * _Nullable scrollView;

@property (nonatomic, weak, nullable) id <UH_GestureDelegate> uhGestureDelegate;

- (void)startProgressGesture;
+ (UHSwipeDirection)checkSwipeGesture:(CGPoint)beganPoint endedPoint:(CGPoint)endedPoint;
@end

#pragma mark - delegate
@protocol UH_GestureDelegate <NSObject>

-(void)tapGesture:(UH_UITapGestureRecognizer *_Nonnull)gestureRecognizer withTouchObject:(id _Nullable)touchObject;
-(void)doubleTapGesture:(UH_UITapGestureRecognizer *_Nonnull)gestureRecognizer withTouchObject:(id _Nullable)touchObject;
-(void)longPressGesture:(UH_UITapGestureRecognizer *_Nonnull)gestureRecognizer withTouchObject:(id _Nullable)touchObject;
-(void)swipeGesture:(UH_UITapGestureRecognizer *_Nonnull)gestureRecognizer withDirection:(UHSwipeDirection)direction;
@end
