//
//  UH_SessionController.m
//  UserHabit
//
//  Created by DoHyoungKim on 2015. 12. 11..
//  Copyright © 2015년 Andbut. All rights reserved.
//

#import "UH_SessionController.h"
#import "UH_Util.h"
#import "UH_NotiManager.h"
#import "UIViewController+Logger.h"
#import "UH_UncaughtExceptionHandler.h"
#import "UH_UserInfo.h"
#import "UH_ResendController.h"

NSString *const UHUserhabitPath = @"/UserHabit/";
NSString *const UHUserhabitScreenImagePath = @"/UserHabit/screens/";
NSString *const UHUserhabitObjectImagePath = @"/UserHabit/objects/";

NSString *const UHSessionCntFileName = @"sessionCnt.dat";
NSString *const UHLatestSuccessTimeFileName = @"latestSuccessTime.dat";
NSString *const UHDeviceIdFileName = @"deviceId.data";
NSString *const UHUserInfoFileName = @"userInfo.data";
static NSString *const UHReferrerFileName = @"referrerInfo.dat";

@interface UH_SessionController ()

@property (nonatomic, strong) UH_Session *currentSession;
@property (nonatomic, strong) UH_NotiManager *notiManager;
@property (nonatomic, assign) BOOL initialized;

@end

@implementation UH_SessionController

+(instancetype)sharedInstance
{
    static UH_SessionController *singletonObject = nil;

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singletonObject = [UH_SessionController new];
    });
    
    return singletonObject;
}

-(id)init{
    self = [super init];
    if (self) {
        
        [self dataPathSetting];
        [self checkPathVaildity];
        
        _sessionDelayTime = @10.0F;
        _isDeviceRandomId = NO;
        _isScreenshotCaptureMode = YES;
        _nowOrientation = UHScreenOrientationPortrait;
        _initialized = NO;
        _operationQueue = dispatch_queue_create(UHQueueDomain, DISPATCH_QUEUE_CONCURRENT);
        _connectionController = [UH_ConnectionController new];
        _isLoadingComplete = NO;
    }
    RLog(@"%@", _userhabitPath);
    return self;
}

#pragma mark - file path
-(void)dataPathSetting{
    _userhabitPath = [UH_Util applicationLibraryDirectoryWith:UHUserhabitPath];
    _userhabitScreenImagePath = [UH_Util applicationLibraryDirectoryWith:UHUserhabitScreenImagePath];
    _userhabitObjectImagePath = [UH_Util applicationLibraryDirectoryWith:UHUserhabitObjectImagePath];
}
/*
 * 데이터 및 파일 저장 경로가 /App/Document 에서 /App/Library로 변경되며 해당 경로를 마이그레이션 해준다
 * 데이터 카피 중 에러가 날 경우 -(void)previousFilePath 를 호출하여 예전 데이터 저장 경로를 사용한다
 */
-(void)checkPathVaildity{
    @try {
        NSString *previousFilePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/UserHabit/"];
        
        if ([[NSFileManager defaultManager] isReadableFileAtPath:previousFilePath]){
            NSError *error = nil;
            
            if ([[NSFileManager defaultManager]moveItemAtPath:previousFilePath toPath:_userhabitPath error:&error]){
                if (!error) {
                    [self previousFileRemove:previousFilePath];
                } else {
                    RLog(@"file copy error %@", error);
                    [self previousFilePath];
                    return;
                }
            } else {
                [self previousFileRemove:previousFilePath];
            }
        }
    } @catch (NSException *exception) {
        RLog(@"%@", exception);
        [self previousFilePath];
        [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                   typeOf:UHInnerDataTypeTryCatch
                                               actionFlow:nil];

    }
}

- (void)previousFileRemove:(NSString *)previousFilePath{
    NSError *error = nil;
    if ([[NSFileManager defaultManager]removeItemAtPath:previousFilePath error:&error]) {
        if (!error){
            [UH_Util logDebugType:UHDebugTypeUserHabit withMessage:@"Change file path"];
        } else {
            RLog(@"file delete fail");
            [self previousFilePath];
            return;
        }
    }
}

//1.0.13 -> 1.0.14 migration code
- (void)previousFilePath {
    RLog(@"Failed change file path. Use the old Path '/Documents/~'");
    _userhabitPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/UserHabit/"];
    _userhabitScreenImagePath = ([NSHomeDirectory() stringByAppendingPathComponent:@"Documents/UserHabit/screens/"]);
    _userhabitObjectImagePath = ([NSHomeDirectory() stringByAppendingPathComponent:@"Documents/UserHabit/objects/"]);
}

#pragma mark - get file path
- (NSString *)getSessionCntFilePath {
//    RLog_ViewCycle;
    return [_userhabitPath stringByAppendingPathComponent:UHSessionCntFileName];
}

- (NSString *)getReferrerFileNamePath {
//    RLog_ViewCycle;
    return [_userhabitPath stringByAppendingPathComponent:UHReferrerFileName];
}

- (NSString *)getLatestSuccessTimeFilePath {
//    RLog_ViewCycle;
    return [_userhabitPath stringByAppendingPathComponent:UHLatestSuccessTimeFileName];
}

- (NSString *)getDeviceIdFilePath {
//    RLog_ViewCycle;
    return [_userhabitPath stringByAppendingPathComponent:UHDeviceIdFileName];
}

- (NSString *)getUserInfoFilePath {
//    RLog_ViewCycle;
    return [_userhabitPath stringByAppendingPathComponent:UHUserInfoFileName];
}

- (void)removeUnusefulData {

    NSArray *contents = [NSFileManager.defaultManager contentsOfDirectoryAtPath:self.userhabitPath error:nil];

    for (NSString *content in contents) {
        BOOL remove = YES;

        if ([content hasSuffix:@"dat"]) {
            remove = NO;
        } else if ([content hasSuffix:@"data"]) {
            remove = NO;
        } else if ([content isEqualToString:@"screens"] || [content isEqualToString:@"objects"]) {
            remove = NO;
        } else if ([content hasPrefix:@"."]) {
            remove = NO;
        } else  {
            NSScanner *scanner = [NSScanner scannerWithString:content];

            if ([scanner scanInteger:NULL] && [scanner isAtEnd]) {
                // the filename is a number
                int num = [content intValue];

                if (num > 0) {
                    remove = NO;
                }
            }
        }

        if (remove) {
            NSString *removalItem = [self.userhabitPath stringByAppendingPathComponent:content];
            [NSFileManager.defaultManager removeItemAtPath:removalItem error:nil];
        }
    }
}

- (void)initialize {
    if (!self.initialized) {
        // Insert below, code to run once in an app process

        // initialize UH_Util
        [UH_Util sharedInstance];

        // initialize UIViewController_Logger
//        [UIViewController_Logger new]
        [UIViewController_Logger swizzle_lifeCycle];
        
        // initialize notiManager
        _notiManager = [UH_NotiManager sharedInstance];

        // install UncaughtExceptionHandler
        if (!UH_SessionManager.options ||
            [UH_SessionManager.options[UHOptionTrackingException] boolValue]) {
            InstallUncaughtExceptionHandler();
            RLog(@"InstallUncaughtExceptionHandler");
        } else {
            RLog(@"크래시 핸들러 미등록 %@", UH_SessionManager.options);
        }

        if (!_rootWindow) {
            for (NSUInteger i = 0; i < [UIApplication sharedApplication].windows.count; i++) {
                _rootWindow = [UIApplication sharedApplication].delegate.window;
            }
        }
        
        if (!_gestureController &&
            _isAutoCatch) {
            _gestureController = [UH_GestureController new];
            [_gestureController gestureSetting];
        }
        
        // Initialize directories
        BOOL flag = YES;
        if (![[NSFileManager defaultManager]fileExistsAtPath:self.userhabitPath isDirectory:&flag]) {
            [[NSFileManager defaultManager]createDirectoryAtPath:self.userhabitPath withIntermediateDirectories:YES attributes:nil error:nil];
        }

        if ([UH_SessionController sharedInstance].isDevelopMode) {
            if (![NSFileManager.defaultManager fileExistsAtPath:self.userhabitScreenImagePath isDirectory:&flag]) {
                [NSFileManager.defaultManager createDirectoryAtPath:self.userhabitObjectImagePath withIntermediateDirectories:YES attributes:nil error:nil];
                [NSFileManager.defaultManager createDirectoryAtPath:self.userhabitScreenImagePath withIntermediateDirectories:YES attributes:nil error:nil];
            }
        }
        
        // Initialize parameters
        self.venderIdentifier = [self genVenderId];
        self.initialized = true;
    }
}

-(void)sessionStart
{
    self.onSession = YES;
    // Session Start
    int sessionCount = [self loadSessionCount];
    
    _currentSession = [[UH_Session alloc] init];

    [_currentSession startSession:sessionCount
                       withApikey:self.apiKey
                     withDeviceId:self.venderIdentifier
                    networkStatus:[UH_Util networkStatus]
                latestSuccessTime:[self loadLatestSessionTime]];

    [UH_Util logDebugType:UHDebugTypeSession withMessage:[NSString stringWithFormat:@"Start (%d)",sessionCount]];
    // old session
    // CHECK!! if too slow, use operation queue
    [[UH_ResendController sharedInstance] startSendingOldSessions:sessionCount];
}
-(void)sessionCloseWithUploadData:(BOOL)isUpload completeHandler:(void(^)(void))completeHandler{
    RLog(@"session close :isUpload(%d)", isUpload);
    //TODO:스톱 세션 변경 사이드 이펙트 확인
    [UH_Util logDebugType:UHDebugTypeScreenChange withMessage:@"Quit"];
    [_currentSession stopSessionWithUploadData:isUpload completeHandler:completeHandler];
    _currentSession = nil;
    [UH_SessionController sharedInstance].onSession = NO;
}

-(void)sessionClose{
    RLog(@"auto close");
    [self sessionCloseWithUploadData:YES completeHandler:nil];
}

#pragma mark - load/save methods

-(int)loadSessionCount
{
    int sessionCount;

    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"UH_SessionCnt"] != nil) {
        sessionCount = [[[NSUserDefaults standardUserDefaults]objectForKey:@"UH_SessionCnt"] intValue];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UH_SessionCnt"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    NSString *uhSessionCntFilePath = [self getSessionCntFilePath];
    if ([[NSFileManager defaultManager]fileExistsAtPath:uhSessionCntFilePath]) {
        NSData *fileData = [NSData dataWithContentsOfFile:uhSessionCntFilePath];
        sessionCount = [[NSString alloc]initWithData:fileData encoding:NSUTF8StringEncoding].intValue;
        sessionCount++;
    }else{
        sessionCount = 1;
    }
    [NSFileManager.defaultManager createFileAtPath:uhSessionCntFilePath
                                          contents:[[NSString stringWithFormat:@"%d", sessionCount]
                                                    dataUsingEncoding:NSUTF8StringEncoding]
                                        attributes:nil];

    return sessionCount;
}

-(long long)loadLatestSessionTime
{
    NSString *latestSuccessTimeFilePath = [self getLatestSuccessTimeFilePath];

    if ([[NSFileManager defaultManager]fileExistsAtPath:latestSuccessTimeFilePath]) {
        NSData *fileData = [NSData dataWithContentsOfFile:latestSuccessTimeFilePath];
        return [[NSString alloc]initWithData:fileData encoding:NSUTF8StringEncoding].longLongValue;
    } else {
        return 0;
    }
}

-(void)saveLatestSessionTime:(long long)latestSuccessTime
{
    NSString *latestSuccessTimeFilePath = [self getLatestSuccessTimeFilePath];
    [[NSFileManager defaultManager]createFileAtPath:latestSuccessTimeFilePath
                                           contents:[[NSString stringWithFormat:@"%lld", latestSuccessTime] dataUsingEncoding:NSUTF8StringEncoding]
                                         attributes:nil];
}

- (void)saveUserInfo{
    dispatch_sync(UH_SessionManager.operationQueue, ^{
        NSDictionary *dict = [self->_userInformation build];
        if(![dict writeToFile:[self getUserInfoFilePath] atomically:YES]){
            RLog(@"fail save user info");
        }
    });
}

- (NSDictionary *)loadUserInfo
{
    return [NSDictionary dictionaryWithContentsOfFile:self.getUserInfoFilePath];
}

-(NSString*)genVenderId
{
    NSString* retId;

    if ([UH_SessionController sharedInstance].isDeviceRandomId) {
        NSString* path = [[UH_SessionController sharedInstance] getDeviceIdFilePath];

        if ([[NSFileManager defaultManager]fileExistsAtPath:path]) {
            NSData *fileData = [NSData dataWithContentsOfFile:path];
            retId = [[NSString alloc]initWithData:fileData encoding:NSUTF8StringEncoding];

        } else {
            NSString* newId = [[NSUUID UUID].UUIDString stringByReplacingOccurrencesOfString:@"-" withString:@""];
            retId = [NSString stringWithFormat:@"AA%@", newId];

            [[NSFileManager defaultManager]createFileAtPath:path contents:[retId dataUsingEncoding:NSUTF8StringEncoding] attributes:nil];
        }


    } else {
        retId = [UIDevice currentDevice].identifierForVendor.UUIDString;
        retId = [retId stringByReplacingOccurrencesOfString:@"-" withString:@""];
    }

    return retId;
}
@end
