//
//  SystemServices.m
//  SystemServicesDemo
//
//  Created by Shmoopi LLC on 9/15/12.
//  Copyright (c) 2012 Shmoopi LLC. All rights reserved.
//

#import "UH_SystemServices.h"

@interface UH_SystemServices () {
    // Private
    
    /* Hardware Information */
    
    // System Uptime (dd hh mm)
    NSString *systemsUptime;
    
    // Model of Device
    NSString *deviceModel;
    
    // Device Name
    NSString *deviceName;
    
    // System Name
    NSString *systemName;
    
    // System Version
    NSString *systemsVersion;
    
    // System Device Type (Not Formatted = iPhone1,0)
    NSString *systemDeviceTypeNotFormatted;
    
    // System Device Type (Formatted = iPhone 1)
    NSString *systemDeviceTypeFormatted;
    
    // Get the Screen Width (X)
    NSInteger screenWidth;
    
    // Get the Screen Height (Y)
    NSInteger screenHeight;
    
    // Get the Screen Brightness
    float screenBrightness;
    
    // Multitasking enabled?
    BOOL multitaskingEnabled;
    
    // Proximity sensor enabled?
    BOOL proximitySensorEnabled;
    
    // Debugger Attached?
    BOOL debuggerAttached;
    
    // Plugged In?
    BOOL pluggedIn;
    
    /* Jailbreak Check */
    
    // Jailbroken?
    int jailbroken;
    
    /* Processor Information */
    
    // Number of processors
    NSInteger numberProcessors;
    
    // Number of Active Processors
    NSInteger numberActiveProcessors;
    
    // Processor Speed in MHz
    NSInteger processorSpeed;
    
    // Processor Bus Speed in MHz
    NSInteger processorBusSpeed;
    
    /* Accessory Information */
    
    // Are any accessories attached?
    BOOL accessoriesAttached;
    
    // Are headphone attached?
    BOOL headphonesAttached;
    
    // Number of attached accessories
    NSInteger numberAttachedAccessories;
    
    // Name of attached accessory/accessories (seperated by , comma's)
    NSString *nameAttachedAccessories;
    
    /* Carrier Information */
    
    // Carrier Name
    NSString *carrierName;
    
    // Carrier Country
    NSString *carrierCountry;
    
    // Carrier Mobile Country Code
    NSString *carrierMobileCountryCode;
    
    // Carrier ISO Country Code
    NSString *carrierISOCountryCode;
    
    // Carrier Mobile Network Code
    NSString *carrierMobileNetworkCode;
    
    // Carrier Allows VOIP
    BOOL carrierAllowsVOIP;
    
    /* Battery Information */
    
    // Battery Level
    float batteryLevel;
    
    // Charging?
    BOOL charging;
    
    // Fully Charged?
    BOOL fullyCharged;
    
    /* Network Information */
    
    // Get Current IP Address
    NSString *currentIPAddress;
    
    // Get Current MAC Address
    NSString *currentMACAddress;
    
    // Get External IP Address
    NSString *externalIPAddress;
    
    // Get Cell IP Address
    NSString *cellIPAddress;
    
    // Get Cell MAC Address
    NSString *cellMACAddress;
    
    // Get Cell Netmask Address
    NSString *cellNetmaskAddress;
    
    // Get Cell Broadcast Address
    NSString *cellBroadcastAddress;
    
    // Get WiFi IP Address
    NSString *wiFiIPAddress;
    
    // Get WiFi MAC Address
    NSString *wiFiMACAddress;
    
    // Get WiFi Netmask Address
    NSString *wiFiNetmaskAddress;
    
    // Get WiFi Broadcast Address
    NSString *wiFiBroadcastAddress;
    
    // Get WiFi Router Address
    NSString *wiFiRouterAddress;
    
    // Connected to WiFi?
    BOOL connectedToWiFi;
    
    // Connected to Cellular Network?
    BOOL connectedToCellNetwork;
    
    /* Process Information */
    
    // Process ID
    int processID;
    
    // Process Name
    NSString *processName;
    
    // Process Status
    int processStatus;
    
    // Parent Process ID
    int parentPID;
    
    // List of process information including PID's, Names, PPID's, and Status'
    NSMutableArray *processesInformation;
    
    /* Disk Information */
    
    // Total Disk Space
    NSString *diskSpace;
    
    // Total Free Disk Space (Raw)
    NSString *freeDiskSpaceinRaw;
    
    // Total Free Disk Space (Percentage)
    NSString *freeDiskSpaceinPercent;
    
    // Total Used Disk Space (Raw)
    NSString *usedDiskSpaceinRaw;
    
    // Total Used Disk Space (Percentage)
    NSString *usedDiskSpaceinPercent;
    
    // Get the total disk space in long format
    long long longDiskSpace;
    
    // Get the total free disk space in long format
    long long longFreeDiskSpace;
    
    /* Memory Information */
    
    // Total Memory
    double totalMemory;
    
    // Free Memory (Raw)
    double freeMemoryinRaw;
    
    // Free Memory (Percent)
    double freeMemoryinPercent;
    
    // Used Memory (Raw)
    double usedMemoryinRaw;
    
    // Used Memory (Percent)
    double usedMemoryinPercent;
    
    // Active Memory (Raw)
    double activeMemoryinRaw;
    
    // Active Memory (Percent)
    double activeMemoryinPercent;
    
    // Inactive Memory (Raw)
    double inactiveMemoryinRaw;
    
    // Inactive Memory (Percent)
    double inactiveMemoryinPercent;
    
    // Wired Memory (Raw)
    double wiredMemoryinRaw;
    
    // Wired Memory (Percent)
    double wiredMemoryinPercent;
    
    // Purgable Memory (Raw)
    double purgableMemoryinRaw;
    
    // Purgable Memory (Percent)
    double purgableMemoryinPercent;
    
    /* Localization Information */
    
    // Country
    NSString *country;
    
    // Language
    NSString *language;
    
    // TimeZone
    NSString *timeZoneSS;
    
    // Currency Symbol
    NSString *currency;
    
    /* Application Information */
    
    // Application Version
    NSString *applicationVersion;
    
    // Clipboard Content
    NSString *clipboardContent;
    
    /* Universal Unique Identifiers */
    
    // Unique ID
    NSString *uniqueID;
    
    // Device Signature
    NSString *deviceSignature;
    
    // CFUUID
    NSString *cfuuid;
    
    // CPU Usage
    float cpuUsage;
}

@end

@implementation UH_SystemServices

@dynamic systemsUptime, deviceModel, deviceName, systemName, systemsVersion, systemDeviceTypeNotFormatted, systemDeviceTypeFormatted, screenWidth, screenHeight, screenBrightness, multitaskingEnabled, proximitySensorEnabled, debuggerAttached, pluggedIn, jailbroken, numberProcessors, numberActiveProcessors, processorSpeed, processorBusSpeed, batteryLevel, charging, fullyCharged, currentIPAddress, currentMACAddress, externalIPAddress, cellIPAddress, cellMACAddress, cellNetmaskAddress, cellBroadcastAddress, wiFiIPAddress, wiFiMACAddress, wiFiNetmaskAddress, wiFiBroadcastAddress, wiFiRouterAddress, connectedToWiFi, connectedToCellNetwork, processID, processName, processStatus, parentPID, processesInformation, diskSpace, freeDiskSpaceinRaw, freeDiskSpaceinPercent, usedDiskSpaceinRaw, usedDiskSpaceinPercent, longDiskSpace, longFreeDiskSpace, totalMemory, freeMemoryinRaw, freeMemoryinPercent, usedMemoryinRaw, usedMemoryinPercent, activeMemoryinRaw, activeMemoryinPercent, inactiveMemoryinRaw, inactiveMemoryinPercent, wiredMemoryinRaw, wiredMemoryinPercent, purgableMemoryinRaw, purgableMemoryinPercent, country, language, timeZoneSS, currency, applicationVersion, clipboardContent, uniqueID, deviceSignature, cfuuid, cpuUsage;

// Singleton
+ (instancetype)sharedServices {
    static UH_SystemServices *sharedSystemServices = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSystemServices = [[self alloc] init];
    });
    return sharedSystemServices;
}

// Init
- (instancetype)init {
    if (self = [super init]) {
        // Initialize variables
        //[self refreshValues];
    }
    
    return self;
}

// System Information

- (NSString *)systemsUptime {
    return [UH_HardwareInfo systemUptime];
}

- (NSString *)deviceModel {
    return [UH_HardwareInfo deviceModel];
}

- (NSString *)deviceName {
    return [UH_HardwareInfo deviceName];
}

- (NSString *)systemName {
    return [UH_HardwareInfo systemName];
}

- (NSString *)systemsVersion {
    return [UH_HardwareInfo systemVersion];
}

- (NSString *)systemDeviceTypeNotFormatted {
    return [UH_HardwareInfo systemDeviceTypeFormatted:NO];
}

- (NSString *)systemDeviceTypeFormatted {
    return [UH_HardwareInfo systemDeviceTypeFormatted:YES];
}

- (NSInteger)screenWidth {
    return [UH_HardwareInfo screenWidth];
}

- (NSInteger)screenHeight {
    return [UH_HardwareInfo screenHeight];
}

- (float)screenBrightness {
    return [UH_HardwareInfo screenBrightness];
}

- (BOOL)multitaskingEnabled {
    return [UH_HardwareInfo multitaskingEnabled];
}

- (BOOL)proximitySensorEnabled {
    return [UH_HardwareInfo proximitySensorEnabled];
}

- (BOOL)debuggerAttached {
    return [UH_HardwareInfo debuggerAttached];
}

- (BOOL)pluggedIn {
    return [UH_HardwareInfo pluggedIn];
}

- (NSInteger)numberProcessors {
    return [UH_ProcessorInfo numberProcessors];
}

- (NSInteger)numberActiveProcessors {
    return [UH_ProcessorInfo numberActiveProcessors];
}

- (NSInteger)processorSpeed {
    return [UH_ProcessorInfo processorSpeed];
}

- (NSInteger)processorBusSpeed {
    return [UH_ProcessorInfo processorBusSpeed];
}

- (float)batteryLevel {
    return [UH_BatteryInfo batteryLevel];
}

- (BOOL)charging {
    return [UH_BatteryInfo charging];
}

- (BOOL)fullyCharged {
    return [UH_BatteryInfo fullyCharged];
}

- (NSString *)currentIPAddress {
    return [UH_NetworkInfo currentIPAddress];
}

- (NSString *)currentMACAddress{
    return [UH_NetworkInfo currentMACAddress];
}

- (NSString *)externalIPAddress {
    return [UH_NetworkInfo externalIPAddress];
}

- (NSString *)cellIPAddress {
    return [UH_NetworkInfo cellIPAddress];
}

- (NSString *)cellMACAddress {
    return [UH_NetworkInfo cellMACAddress];
}

- (NSString *)cellNetmaskAddress {
    return [UH_NetworkInfo cellNetmaskAddress];
}

- (NSString *)cellBroadcastAddress {
    return [UH_NetworkInfo cellBroadcastAddress];
}

- (NSString *)wiFiIPAddress {
    return [UH_NetworkInfo wiFiIPAddress];
}

- (NSString *)wiFiMACAddress {
    return [UH_NetworkInfo wiFiMACAddress];
}

- (NSString *)wiFiNetmaskAddress {
    return [UH_NetworkInfo wiFiNetmaskAddress];
}

- (NSString *)wiFiBroadcastAddress {
    return [UH_NetworkInfo wiFiBroadcastAddress];
}

- (NSString *)wiFiRouterAddress {
    return [UH_NetworkInfo wiFiRouterAddress];
}

- (BOOL)connectedToWiFi {
    return [UH_NetworkInfo connectedToWiFi];
}

- (BOOL)connectedToCellNetwork {
    return [UH_NetworkInfo connectedToCellNetwork];
}

- (int)processID {
    return [UH_ProcessInfo processID];
}

- (NSString *)processName {
    return [UH_ProcessInfo processName];
}

- (int)processStatus {
    return [UH_ProcessInfo processStatus];
}

- (int)parentPID {
    return [UH_ProcessInfo parentPID];
}

- (NSMutableArray *)processesInformation {
    return [UH_ProcessInfo processesInformation];
}

- (NSString *)diskSpace {
    return [UH_DiskInfo diskSpace];
}

- (NSString *)freeDiskSpaceinRaw {
    return [UH_DiskInfo freeDiskSpace:NO];
}

- (NSString *)freeDiskSpaceinPercent {
    return [UH_DiskInfo freeDiskSpace:YES];
}

- (NSString *)usedDiskSpaceinRaw {
    return [UH_DiskInfo usedDiskSpace:NO];
}

- (NSString *)usedDiskSpaceinPercent {
    return [UH_DiskInfo usedDiskSpace:YES];
}

- (long long)longDiskSpace {
    return [UH_DiskInfo longDiskSpace];
}

- (long long)longFreeDiskSpace {
    return [UH_DiskInfo longFreeDiskSpace];
}

- (double)totalMemory {
    return [UH_MemoryInfo totalMemory];
}

- (double)freeMemoryinRaw {
    return [UH_MemoryInfo freeMemory:NO];
}

- (double)freeMemoryinPercent {
    return [UH_MemoryInfo freeMemory:YES];
}

- (double)usedMemoryinRaw {
    return [UH_MemoryInfo usedMemory:NO];
}

- (double)usedMemoryinPercent {
    return [UH_MemoryInfo usedMemory:YES];
}

- (double)activeMemoryinRaw {
    return [UH_MemoryInfo activeMemory:NO];
}

- (double)activeMemoryinPercent {
    return [UH_MemoryInfo activeMemory:YES];
}

- (double)inactiveMemoryinRaw {
    return [UH_MemoryInfo inactiveMemory:NO];
}

- (double)inactiveMemoryinPercent {
    return [UH_MemoryInfo inactiveMemory:YES];
}

- (double)wiredMemoryinRaw {
    return [UH_MemoryInfo wiredMemory:NO];
}

- (double)wiredMemoryinPercent {
    return [UH_MemoryInfo wiredMemory:YES];
}

- (double)purgableMemoryinRaw {
    return [UH_MemoryInfo purgableMemory:NO];
}

- (double)purgableMemoryinPercent {
    return [UH_MemoryInfo purgableMemory:YES];
}

- (NSString *)country {
    return [UH_LocalizationInfo country];
}

- (NSString *)language {
    return [UH_LocalizationInfo language];
}

- (NSString *)timeZoneSS {
    return [UH_LocalizationInfo timeZone];
}

- (NSString *)currency {
    return [UH_LocalizationInfo currency];
}

- (NSString *)applicationVersion {
    return [UH_ApplicationInfo applicationVersion];
}

- (NSString *)clipboardContent {
    return [UH_ApplicationInfo clipboardContent];
}

- (float)cpuUsage {
    return [UH_ApplicationInfo cpuUsage];
}

// Parent ID for a certain PID
- (int)parentPIDForProcess:(int)pid {
    // Get the Parent Process ID For a process
    int Number = [UH_ProcessInfo parentPIDForProcess:pid];
    // Validate it
    if (Number <= 0) {
        // Error, no value returned
        return -1;
    }
    // Successful
    return Number;
}

@end
