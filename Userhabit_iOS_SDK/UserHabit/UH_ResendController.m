//
// Created by Jinuk Baek on 05/02/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_ResendController.h"
#import "UserHabitConstant.h"
#import "UH_Session.h"
#import "UH_SessionController.h"
#import "UH_Util.h"
#import "UH_ConnectionController.h"

@interface UH_ResendController()

@property (nonatomic, assign) int processingSessionNumber;
@property (nonatomic, assign) int lastSessionNumber;

@end

@implementation UH_ResendController


+ (instancetype)sharedInstance {
    static id singletonObject = nil;

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singletonObject = [[[self class] alloc] init];
    });

    return singletonObject;
}


- (void)removeOldData:(int)currentSessionNumber {

    // unnecessary work
    if (currentSessionNumber <= (MAX_SAVE_SESSION_COUNT + 1)) {
        [self clearWork];
        return ;
    }

    NSString *userhabitPath = [[UH_SessionController sharedInstance] userhabitPath];
    NSArray<NSString *> *fileNames = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:userhabitPath
                                                                                         error:nil];

    if (fileNames) {
        for (NSString *fileName in fileNames) {
            NSInteger sessionCount = [fileName integerValue];

            if (sessionCount <= 0) {
                continue;
            }

            // now sending session
            if (self.processingSessionNumber > 0
                    && self.processingSessionNumber == sessionCount) {
                continue;
            }

            // if currentCount = 10
            // ex: MAX_SAVE_SESSION_COUNT = 5
            // remove Session Data lesser than 4
            int removalCount = currentSessionNumber - MAX_SAVE_SESSION_COUNT - 1;

            if (sessionCount < removalCount) {
                [UH_Session deleteSessionDataAt:sessionCount];
            }
        }
    }
}

- (void)startSendingOldSessions:(int)currentSessionNumber {
    // Now working!!
    if (self.processingSessionNumber > 0) {
        // if below is more than MAX_SAVE_SESSION_COUNT, then clear
        if (ABS(self.processingSessionNumber - currentSessionNumber - 1) > MAX_SAVE_SESSION_COUNT) {
            [self clearWork];
        } else {
            return ;
        }
    }

    [self removeOldData:currentSessionNumber];
    self.lastSessionNumber = currentSessionNumber;
    self.processingSessionNumber = currentSessionNumber - MAX_SAVE_SESSION_COUNT - 1;

    [self runOldSessionWork];
}


- (void)clearWork {
    self.lastSessionNumber = 0;
    self.processingSessionNumber = 0;
}


/*!
 * send an old session to server
 *
 * @discussion CAUTION : multiple concurrent jobs is not allowed
 */
- (void)runOldSessionWork {

    self.processingSessionNumber++;


    // until existing session
    for (; self.processingSessionNumber < self.lastSessionNumber; self.processingSessionNumber++) {

        UH_Session *session = [UH_Session loadSession:self.processingSessionNumber];

        if (session) {
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:[session getActionFlowFilePath]]) {
                [[UH_Util sharedInstance] compressDataFrom:[session getActionFlowFilePath]
                                                        to:[session getSessionDataFilePath]];
            }

            [[UH_SessionManager connectionController] sessionEndConnection:session completeHandler:nil];
            

            break;
        }
    }

    // completed works
    if (self.lastSessionNumber == self.processingSessionNumber) {
        [self clearWork];
    }
}

@end
