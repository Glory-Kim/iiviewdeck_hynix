
# 프로그램 설계

## SDK 생명 주기별 처리 작업

UH_SessionController에 생명주기별 호출함수 구현 함 

### SDK 초기화 

앱이 처음으로 켜지면 1회 실행

- `~/Library/UserHabit` 디렉토리 생성
- `~/Library/UserHabit/screens/`  디렉토리 생성
- `~/Library/UserHabit/objects/`  디렉토리 생성
- 개발자 모드 체크
- 기타 설정 모드 체크 
- UIViewController_Logger 등록 : 화면 orientation 검출 및 viewcontroller 등장 검출
- UH_NotiManager 등록 : 앱 백그라운드 이동, 키보드창 검출
- UH_Util 등록 : wifi, cellular 모드 검출
- ExceptionHandler 등록 : 앱 크래시시 발생하는 예외처리 핸들러 등록
- window에 touchObserver 설치 
- appInfo/deviceInfo 가져옴

### 사용 라이브러리
- AFNetworking Latest commit 33a403c on 14 Oct 2016

### 세션 시작

세션이 시작될 때 1회 실행

- 이전 세션이 ~/Documents/에 저장되어있나 확인 후 ~/Library/~ 로 이동
- 세션 전용 디렉토리 생성 
- 기존 세션 정리 작업
    - 현재 세션넘버가 오래된 것 제거
    - 이상한 파일들 제거 
- 세션 열기 메시지 전달

### 데이터 수집


### 세션 종료

세션이 종료될 때 1회 실행

- UH_NotiManager의 appWillResignActive에 의해 트리거 됨
- 트리거 발생 후 설정된 시간 경과 후 종료 프로세스 진행
- UH_SessionController로 메시지 전달
- UH_SessionController는 currentSession에 대해 작업 수행


### 앱 종료

앱이 종료됨을 일반적으로는 검출 불가능함

#배포시 주의 사항

## 개발 플래그 설정
Userhabit.pch의 UHForDeveloper 플래그를 반드시 1로 설정해두고 배포할것
