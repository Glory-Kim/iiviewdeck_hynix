//
//  UserHabitConstant.h
//  Userhabit
//
//  Created by DoHyoungKim on 2015. 5. 16..
//  Copyright (c) 2015년 Andbut. All rights reserved.
//


#ifndef Userhabit_UserHabitConstant_h
#define Userhabit_UserHabitConstant_h

#define UHNowTimeStamp (long long)([NSDate date].timeIntervalSince1970 * 1000000000L)  //1000000000L
/*!
 * @typedef UHActionFlowType
 * @brief Action flow type
 * @constant UHActionFlowTypeAppStart App Start
 * @constant UHActionFlowTypeAppEnd App End
 * @constant UHActionFlowTypeScreenStart Screen Start
 * @constant UHActionFlowTypeInterBackground The app enter background mode
 * @constant UHActionFlowTypeWillResignActive The app go to OS by context switching
 * @constant UHActionFlowTypeTap Tap action
 * @constant UHActionFlowTypeDoubleTap Double tap action
 * @constant UHActionFlowTypeLongPress Long press action
 * @constant UHActionFlowTypeSwipe Swipe action
 * @constant UHActionFlowTypeKeyboardEnableStatus shown the keyboard
 * @constant UHActionFlowTypeKeyboardDisableStatus hidden the keyboard
 * @constant UHActionFlowTypeCrashed App is crashed
 * @constant UHActionFlowTypeInnerData inner data
 */
typedef NS_ENUM(int, UHActionFlowType) {
    UHActionFlowTypeNil                     = 0x0000,
    // Screen Type
    UHActionFlowTypeAppStart                = 0x1000,              //4096
    UHActionFlowTypeAppEnd                  = 0x1001,
    UHActionFlowTypeScreenStart             = 0x1101,
    UHActionFlowTypeInterBackground         = 0x1200,      //App이 백그라운드 상태로 들어갔을때
    UHActionFlowTypeWillResignActive        = 0x1240,      //앱이 OS로 context switching 됐을때

    // Action Type
    UHActionFlowTypeTap                     = 0x2000,                   //8192
    UHActionFlowTypeDoubleTap               = 0x2001,
    UHActionFlowTypeLongPress               = 0x2002,
    UHActionFlowTypeSwipe                   = 0x2003,               //8195
    UHActionFlowTypeKeyboardEnableStatus    = 0x2009,               //8201
    UHActionFlowTypeKeyboardDisableStatus   = 0x200A,

    // Dynamic Screen Type
    UHActionFlowTypeAddListRow              = 0x3000,               //12288
    UHActionFlowTypeRemoveListRow,
    UHActionFlowTypeScrollView,                                     //12290
    UHActionFlowTypeMaxScroll,
    
    // Meta Info Type
    UHActionFlowTypeCrashed                 = 0x5003,
    UHActionFlowTypeContentTracking         = 0x5004,       //20484

    // inner data
    UHActionFlowTypeInnerData               = 0xF000,
};

/*!
 * @typedef UHNetworkStatus
 * @brief Network status type
 * @constant UHNetworkStatusWwan WWan
 * @constant UHNetworkStatusWifi Wifi
 * @constant UHNetworkStatusNone None
 */
typedef NS_ENUM(NSUInteger, UHNetworkStatus) {
    UHNetworkStatusWwan = 1,
    UHNetworkStatusWifi = 2,
    UHNetworkStatusNone = 3,
};


/*!
 * @typedef UHTransportMethod
 * @brief transport method from opened session response
 * @constant UHTransportMethodSessionClose closing session message is sent
 * @constant UHTransportMethodScreenshotInfo screenshots info message is sent
 */
typedef NS_ENUM(NSUInteger, UHTransportMethod) {
    UHTransportMethodSessionClose = 0,
    UHTransportMethodScreenshotInfo = 2,
};


/*!
 * @typedef UHSwipeDirection
 * @brief swipe direction by an user
 * @constant UHSwipeDirectionUp direction to up side
 * @constant UHSwipeDirectionDown direction to down side
 * @constant UHSwipeDirectionLeft direction to left
 * @constant UHSwipeDirectionRight direction to right
 */
typedef NS_ENUM(NSUInteger, UHSwipeDirection) {
    UHSwipeDirectionNil = -1,
    UHSwipeDirectionUp = 1,
    UHSwipeDirectionDown,
    UHSwipeDirectionLeft,
    UHSwipeDirectionRight
};


/*!
 * @typedef UHScreenOrientation
 * @brief orientation of a screen
 * @constant UHScreenOrientationPortrait portrait
 * @constant UHScreenOrientationLandscape landscape
 */
typedef NS_ENUM(NSUInteger, UHScreenOrientation) {
    UHScreenOrientationPortrait = 1,
    UHScreenOrientationLandscapeLeft = 2,
    UHScreenOrientationPortraitUpsideDown = 3,
    UHScreenOrientationLandscapeRight = 4
};

/*!
 * @typedef UHScreenType
 * @brief a type of the screen
 * @constant UHScreenTypeDefault default type
 * @constant UHScreenTypeDialog dialog type
 */
typedef NS_ENUM(NSUInteger, UHScreenType) {
    UHScreenTypeDefault = 0,
    UHScreenTypeDialog = 1
};


typedef NS_ENUM(NSUInteger, UHSessionStatusType) {
    UHSessionStatusTypeNormal = 1,
    UHSessionStatusTypeResendNormal = 2,
    UHSessionStatusTypeResendCellular = 3,
    UHSessionStatusTypeResendAbnormal = 4
};


/**
 * Version Code
 */
extern int UHVersionCode;

/**
 * Sdk Version
 */
extern NSString *const UHSdkVersion;

/**
 * Api Server Hostname
 */
extern NSString *const UHGatewayHost;

/**
 * Api Referrer Hostname
 */
extern NSString *const UHReferrerHost;

#define MAX_SAVE_SESSION_COUNT 5

/**
 * debug log for userhabit developer
 */
//#define USERHABIT_SDK_DEBUG

#endif
