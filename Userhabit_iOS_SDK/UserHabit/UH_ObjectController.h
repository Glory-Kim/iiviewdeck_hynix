//
//  UH_ObjectManager.h
//  UserHabit
//
//  Created by DoHyoungKim on 2015. 12. 3..
//  Copyright © 2015년 Andbut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UH_ObjectInfo.h"
#import <UIKit/UIKit.h>

@class UH_TapInteraction;

@interface UH_ObjectController : NSObject{
    NSMutableSet<UH_ObjectInfo *> *objectSet;
    UIViewController *showController;
}

- (void)readAllObject:(UIViewController *)thisController completionHandler:(RExceptionBlock)completionHandler;
- (void)readObject:(UIView *)readView;
- (UH_TapInteraction *)catchObject:(UH_TapInteraction *)tapInteraction forView:(UIView *)readView;

@end
