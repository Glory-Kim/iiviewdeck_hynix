//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_Model.h"
#import "UserHabitConstant.h"

@class UH_ScreenInfo;
@class UH_TapInteraction;
@class UH_SwipeInteraction;
@class UH_CrashInfo;
@class UH_ContentInfo;
@class UH_InnerData;

@interface UH_ActionFlow : UH_Model

@property (nonatomic, assign, readonly) UHActionFlowType actionFlowType;
@property (nonatomic, assign, readonly) int uptime;
@property (nonatomic, strong, readonly) UH_Model *message;       // UH_TapInteraction

- (instancetype)initAt:(long long)uptime from:(long long)startTime typeOf:(UHActionFlowType)type;
- (instancetype)initAt:(long long)uptime from:(long long)startTime typeOf:(UHActionFlowType)type with:(UH_Model *)message;


- (instancetype)genActionFlowInnerDataUptime:(long long)upTime withNow:(long long)now message:(NSString *)message type:(NSString *)type;
/*
 * actionFlow와 self가 같은 객체인지 비교한다
 * uptime과 flowType이 같으면 같은 actionFlow로 판단한다
 */
- (BOOL)isEqualToActionFlow:(UH_ActionFlow *)actionFlow;

- (void)setUptime:(int)uptime;
@end
