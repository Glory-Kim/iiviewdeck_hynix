//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_InnerData.h"

@implementation UH_InnerData
NSString *const UHInnerDataTypeLog = @"log";
NSString *const UHInnerDataTypeTryCatch = @"try-catch";
NSString *const UHInnerDataTypeTimeError = @"time error";

- (instancetype)initValue:(NSString *)value typeOf:(NSString *)type {
    self = [super init];

    if (self) {
        self.type = type;
        self.value = value;
    }

    return self;
}

- (NSDictionary *)build {
    NSDictionary *innerData = @{
            @"t": self.type,
            @"v": self.value
    };

    return innerData;
}

@end
