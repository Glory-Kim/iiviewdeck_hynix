//
// Created by Jinuk Baek on 24/01/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_SystemServices.h"
#import "UH_SessionController.h"
#import "UH_ConnectionController.h"
#import "UH_ViewControllerManager.h"
#import "UH_Util.h"
#import "UH_Session.h"
#import "UserHabitConstant.h"
#import "UH_AppInfo.h"
#import "UH_ActionFlow.h"

#import "UH_DeviceInfo.h"
#import "UH_SessionInfo.h"
#import "UH_CrashInfo.h"
#import "UH_SwipeInteraction.h"
#import "UH_TapInteraction.h"
#import "UH_ContentInfo.h"
#import "UH_ScreenInfo.h"
#import "UH_FileController.h"
#import "UH_ManualCaptureController.h"

#define INSERT_READY_QUEUE_SIZE 20
#define INSERT_READY_QUEUE_MAX_SIZE 20
#define INSERT_READY_QUEUE_WRITE_SIZE 10
#define ACTION_FLOWS_FLUSH_BUFFER_SIZE 4096

#if INSERT_READY_QUEUE_MAX_SIZE > INSERT_READY_QUEUE_SIZE
#error "INSERT_READY_QUEUE_SIZE have to be greater than INSERT_READY_QUEUE_MAX_SIZE"
#elif INSERT_READY_QUEUE_WRITE_SIZE > INSERT_READY_QUEUE_MAX_SIZE
#error "INSERT_READY_QUEUE_MAX_SIZE have to be greater than INSERT_READY_QUEUE_WRITE_SIZE"
#endif

static NSString *const UHActionFlowFileName = @"actionFlow.db";
static NSString *const UHSessionInfoFileName = @"sessionInfo.data";
static NSString *const UHSessionDataFileName = @"session.dat";

typedef NS_ENUM(NSUInteger, UHWriteStatus) {
    UHWriteStatusNone,
    UHWriteStatusTen,           //메모리에 액션이 10개 이상일경우 파일에 기록
    UHWriteStatusAll,
    UHWriteStatusScreenStart,   //화면 시작으로 인한 파일 작성
};


@interface UH_Session()

@property (nonatomic, strong) NSString *dbPath;

@property (atomic, strong) NSMutableArray<UH_ActionFlow *> *insertReadyQueue;
@property (atomic, strong) NSFileHandle *handler;

@property (nonatomic, strong) UH_AppInfo *appInfo;
@property (nonatomic, strong) UH_DeviceInfo *deviceInfo;
@property (nonatomic, strong) UH_SessionInfo *sessionInfo;

@property (nonatomic, assign) int sessionCount;
@property (nonatomic, assign) BOOL reloaded;

@end

@implementation UH_Session

-(void)startSession:(int)sessionCount
         withApikey:(NSString *)apiKey
       withDeviceId:(NSString *)deviceId
      networkStatus:(UHNetworkStatus)networkStatus
  latestSuccessTime:(long long)latestSuccessTime
{
    RLog_ViewCycle;
    // initialize variables
    long long uptime = UHNowTimeStamp;
    _scrollViewArray = [NSMutableArray new];
    _transportMethod = nil;
    self.sessionCount = sessionCount; 
    self.dbPath = [self getActionFlowFilePath];
    self.insertReadyQueue = [NSMutableArray arrayWithCapacity:INSERT_READY_QUEUE_SIZE];

    // initialize data
//todo: uptime, devicetime 확인하여 구분하기
    long long deviceTime = UHNowTimeStamp;

    self.appInfo = [[UH_AppInfo alloc] initWithData:apiKey];
    self.deviceInfo = [[UH_DeviceInfo alloc] initWithID:deviceId];
    self.sessionInfo = [[UH_SessionInfo alloc] initWithData:[NSUUID UUID].UUIDString
                                               sessionCount:sessionCount
                                              networkStatus:networkStatus
                                                     uptime:uptime
                                                 deviceTime:deviceTime
                                           latestSucessTime:latestSuccessTime];
    
    // create directories
    [NSFileManager.defaultManager createDirectoryAtPath:[self getSessionDirPath] withIntermediateDirectories:YES attributes:nil error:nil];
    [NSFileManager.defaultManager createFileAtPath:self.dbPath contents:nil attributes:nil];
    [NSFileManager.defaultManager createFileAtPath:[self getSessionInfoFilePath] contents:nil attributes:nil];

    _handler = [NSFileHandle fileHandleForWritingAtPath:self.dbPath];

    /*
     * todo:
     * [UHNetworkReachabilityManager sharedManager].reachable 네트워크 상태를 체크하기전에 로딩하여 해당 부분을 2초후에 실행하도록 변경 추후 callback나 notification등으로 개선할 것
     * CoreTelephony등을 사용해 직접 확인해볼것
     */
//    [NSTimer scheduledTimerWithTimeInterval:2.f target:self selector:@selector(startSessionProgress) userInfo:nil repeats:NO];
    [self startSessionProgress];

    // add app start to action flows
    UH_ActionFlow *actionFlow = [self genActionFlowAppStart:UHNowTimeStamp];
    [self insertActionFlow:actionFlow];
    [self saveSessionInfo];
}

-(void)startSessionProgress{
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"UH_ConstVersion"]) {
        NSString *versionStr = [NSString stringWithFormat:@"%@", [NSBundle mainBundle].infoDictionary[@"CFBundleVersion"]];
        
        if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"UH_ConstVersion"]isEqualToString:versionStr]) {
            UH_SessionManager.onSession = NO;
        } else {
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"UH_ConstVersion"];
            [UH_SessionManager.connectionController sessionStartConnection:self];
        }
    }else{
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"UH_ConstVersion"];
        [UH_SessionManager.connectionController sessionStartConnection:self];
    }
}

-(void)stopSessionWithUploadData:(BOOL)isUpload completeHandler:(void(^)(void))completeHandler{
    RLog_ViewCycle;
    UH_ActionFlow *actionFlow = [self genActionFlowAppEnd:UHNowTimeStamp];
    [self insertActionFlow:actionFlow];

    if (isUpload &&
        [self chkTransportStatus:UHTransportMethodSessionClose] &&
        [UHNetworkReachabilityManager sharedManager].reachable) {
        RLog_ViewCycle;
        
        [UH_FileController checkSessionFile:[self getActionFlowFilePath]
                            completeHandler:^(NSError *error) {
            if (error){
//                [self deleteSessionData];
                [self->_appInfo checkErrorSession];
            }
            
            
            [[UH_Util sharedInstance] compressDataFrom:[self getActionFlowFilePath]
                                                    to:[self getSessionDataFilePath]];
            
            [UH_SessionManager.connectionController sessionEndConnection:self completeHandler:completeHandler];
        }];
        
        
        RLog(@"%@", [self getActionFlowFilePath]);
        RLog(@"%@", [self getSessionDataFilePath]);

    } else {
        RLog_ViewCycle;
        if (completeHandler) {
            completeHandler();
        }
    }
}

-(void)saveSessionInfo{
    NSDictionary *sendDic = @{
            @"d" : [self.deviceInfo build],
            @"a" : [self.appInfo build],
            @"s" : [self.sessionInfo build],
            @"w" : @(self.sessionStatus)
    };

    [sendDic writeToFile:[self getSessionInfoFilePath] atomically:YES];
}


+ (instancetype)loadSession:(int)sessionCount {

    // fast return
    if (![UH_Session existSessionDataAt:sessionCount]) {
        return nil;
    }

    UH_Session *newSession = [[UH_Session alloc] init];
    newSession.sessionCount = sessionCount;
    BOOL exist = [newSession existSessionData];

    if (!exist) {
        [newSession deleteSessionData];
        newSession = nil;
    } else {
        NSDictionary *loadedDic = [NSDictionary dictionaryWithContentsOfFile:[newSession getSessionInfoFilePath]];

        newSession.deviceInfo = [[UH_DeviceInfo alloc] initDictionary:loadedDic[@"d"]];
        newSession.appInfo = [[UH_AppInfo alloc] initDictionary:loadedDic[@"a"]];
        newSession.sessionInfo = [[UH_SessionInfo alloc] initDictionary:loadedDic[@"s"]];
        newSession.sessionStatus = (UHSessionStatusType)[loadedDic[@"w"] intValue];
        newSession.reloaded = YES;
    }
    RLog(@"%@", newSession);

    return newSession;
}

#pragma mark - directory management helpers
- (NSString *)getSessionDirPath {
    NSString *sessionCntStr = [NSString stringWithFormat:@"%d", self.sessionCount];
    return [[UH_SessionController sharedInstance].userhabitPath stringByAppendingPathComponent:sessionCntStr];
}

- (NSString *)getActionFlowFilePath {
    return [[self getSessionDirPath] stringByAppendingPathComponent:UHActionFlowFileName];
}

- (NSString *)getSessionInfoFilePath {
    return [[self getSessionDirPath] stringByAppendingPathComponent:UHSessionInfoFileName];
}

- (NSString *)getSessionDataFilePath {
    return [[self getSessionDirPath] stringByAppendingPathComponent:UHSessionDataFileName];
}

- (BOOL)existActionFlowFile {
    BOOL exist = [NSFileManager.defaultManager fileExistsAtPath:[self getActionFlowFilePath]];

    if (exist) {
        NSDictionary *fileInfo = [NSFileManager.defaultManager attributesOfItemAtPath:[self getActionFlowFilePath]
                                                                                error:nil];
        exist = ([fileInfo fileSize] > 0L);
    }
    return exist;
}

- (BOOL)existSessionInfoFile {
    return [NSFileManager.defaultManager fileExistsAtPath:[self getSessionInfoFilePath]];
}

- (BOOL)existSessionDataFile {
    return [NSFileManager.defaultManager fileExistsAtPath:[self getSessionDataFilePath]];
}



#pragma mark - action flows

/*
 * 제스쳐를 입력받아 큐에 저장한다 
 * 큐는 최대 20개까지 있다
 */
-(void)insertActionFlow:(UH_ActionFlow *)actionFlow {
    
    if (!actionFlow) {
        return ;
    }
    
    /*
     * 이상 세션 (마지막 세션과 UHLastActionCompareValue 이상 차이가 날 경우)가 발 생할 경우 마지막 액션에서 1초를 더 한 후 세션을 종료한다
     */
    if([UH_Util validationTime:actionFlow.uptime]){
        if (actionFlow.actionFlowType == UHActionFlowTypeAppEnd) {
            [actionFlow setUptime:UH_SessionManager.latestAction.uptime + 1000];
        } else {
            if (!(UH_SessionManager.latestAction.actionFlowType == UHActionFlowTypeInnerData)) {
                [UH_SessionManager.currentSession insertInnerData:[NSString stringWithFormat:@"time error %d / %d", UH_SessionManager.latestAction.uptime, actionFlow.uptime]
                                                           typeOf:UHInnerDataTypeTimeError
                                                       actionFlow:^(UH_ActionFlow *innerData) {
                                                           [innerData setUptime:UH_SessionManager.latestAction.uptime + 1000];
                                                           UH_SessionManager.latestAction = innerData;
                                                       }];
            }
            [[UH_SessionController sharedInstance] sessionClose];
            return;
        }
    }
    
    /*
     * 세션의 uptime이 같을경우 +5해준다
     */
    if (UH_SessionManager.latestAction.uptime == actionFlow.uptime &&
        UH_SessionManager.latestAction.actionFlowType != actionFlow.actionFlowType) {
        RLog(@"중복 시간 발생");
        actionFlow.uptime = actionFlow.uptime + 5;
    }

    //따닥 실행
    if([UH_SessionManager.options[UHOptionScreenTime] floatValue] > 0 &&
       actionFlow.actionFlowType == UHActionFlowTypeScreenStart &&
       UH_SessionManager.latestScreenAction){
        
        
        //        UH_SessionManager.latestScreenAction
        RLog(@"따닥 확인! %@", UH_SessionManager.latestScreenAction);
        
        if ([UH_SessionManager.options[UHOptionScreenTime] floatValue] * 1000 > actionFlow.uptime - UH_SessionManager.latestScreenAction.uptime
            ) {
            RLog(@"따닥!!! %@", _insertReadyQueue);
            
            [UH_SessionManager.currentSession insertInnerData:[NSString stringWithFormat:@"따닥 %d / %@", UH_SessionManager.latestScreenAction.actionFlowType, [UH_SessionManager.latestScreenAction.message build]]
                                                       typeOf:UHInnerDataTypeTimeError
                                                   actionFlow:^(UH_ActionFlow *innerData) {
                                                       [innerData setUptime:UH_SessionManager.latestAction.uptime + 1000];
                                                       UH_SessionManager.latestAction = innerData;
                                                   }];
            
            [_insertReadyQueue removeObject:UH_SessionManager.latestScreenAction];

            RLog(@"따닥!!! 삭제 %@", _insertReadyQueue);
        } else {
            RLog(@"통과");
        }
        
        
        RLog(@"%@ / %d", UH_SessionManager.options[UHOptionScreenTime] , actionFlow.uptime - UH_SessionManager.latestScreenAction.uptime);
        
//        500
    }

    
    
    UH_SessionManager.latestAction = actionFlow;
    
    //최근 본 화면 저장
    if (actionFlow.actionFlowType == UHActionFlowTypeScreenStart) {
        UH_SessionManager.latestScreenAction = actionFlow;
        if (UH_SessionManager.isDebug &&
            UH_SessionManager.isDevelopMode) {
            if ([UH_ManualCaptureController sharedInstance]) {
                [[UH_ManualCaptureController sharedInstance] changeScreen];
            }
        }
    }



    @try {
        int i = 0;
        if([_insertReadyQueue firstObject]){
            for (UH_ActionFlow *inFlow in _insertReadyQueue) {
                i++;
                if ([inFlow isEqualToActionFlow:actionFlow]) {
                    
                    RLog(@"\n\n\n !!!! warning !!!! \n\n\n duplicate action flow \n %@ \n %@", inFlow, actionFlow);
                    RLog(@"%@", _insertReadyQueue);
                    return;
                } else {
                    if (i == [_insertReadyQueue count]) {
                        [_insertReadyQueue addObject:actionFlow];
                    }
//                    RLog(@"중복 없음 \n %@ \n %@", inFlow, actionFlow);
                }
            }
        } else {
            
            if (UH_SessionManager.isLoadingComplete ||
                actionFlow.actionFlowType == UHActionFlowTypeAppStart ||
                actionFlow.actionFlowType == UHActionFlowTypeScreenStart) {
                [_insertReadyQueue addObject:actionFlow];
            } else {
                
                
                //todo:한번 이쪽으로 들어오면 메시지가 계속 중복해서 붙으면서 늘어남
//                [UH_SessionManager.currentSession insertInnerData:[NSString stringWithFormat:@"view error %d / %@", actionFlow.actionFlowType, [actionFlow.message build]]
//                                                           typeOf:UHInnerDataTypeTimeError
//                                                       actionFlow:^(UH_ActionFlow *innerData) {
//                                                           [innerData setUptime:UH_SessionManager.latestAction.uptime + 1000];
//                                                           UH_SessionManager.latestAction = innerData;
//                                                       }];
            }
            
        }
//        RLog(@"action count %@ / %lu", actionFlow,  (unsigned long)[_insertReadyQueue count]);
        
        int writeStatus = UHWriteStatusNone;
        UHActionFlowType actionType = actionFlow.actionFlowType;
        
        
        
        
        switch(actionType) {
            case UHActionFlowTypeAppEnd:
            case UHActionFlowTypeWillResignActive:
            case UHActionFlowTypeInterBackground:
            case UHActionFlowTypeCrashed:
                writeStatus = UHWriteStatusAll;
                break;
                
            case UHActionFlowTypeScreenStart:
                writeStatus = UHWriteStatusScreenStart;
                break;
                
            default:
                if (_insertReadyQueue.count >= INSERT_READY_QUEUE_MAX_SIZE) {
                    writeStatus = UHWriteStatusTen;
                }
        }
        
        if (writeStatus != UHWriteStatusNone) {
            if (writeStatus == UHWriteStatusAll) {
                NSArray *actionFlows;
                
                actionFlows = [NSArray arrayWithArray:_insertReadyQueue];
                [_insertReadyQueue removeAllObjects];
                
                NSMutableString *resultString = [NSMutableString stringWithCapacity:ACTION_FLOWS_FLUSH_BUFFER_SIZE];

        
                //resultString = {"t":4353,"u":119,"m":{"p":{"w":375,"h":667},"r":1,"t":0,"a":"FirstViewController"}}
//                {"t":20483,
//                 "u":4205,
//                 "m":{"p":8,
//                     "b":-1,
//                     "fm":3230605312,
//                     "fd":146364608512,
//                     "c":false,
//                     "j":false,
//                     "s":"errorcode-",
//                     "td":250685575168,
//                     "tm":8589934592,
//                     "t":"NSRangeException",
//                     "m":"*** -[__NSArray0 objectAtIndex:]: index 100 beyond bounds for empty NSArray",
//                     "n":2}
//                }
                for (UH_ActionFlow *inActionFlow in actionFlows) {
                    [resultString appendString:[UH_Util makeJsonString:[inActionFlow build]]];
                    [resultString appendString:@"\n"];
                }
                [_handler seekToEndOfFile];
                [_handler writeData:[resultString dataUsingEncoding:NSUTF8StringEncoding]];
                RLog(@"file save");
            } else {
                UH_ActionFlow *firstAction = [_insertReadyQueue firstObject];
                NSMutableString *resultString = [NSMutableString new];
                
                [resultString appendString:[UH_Util makeJsonString:[firstAction build]]];
                [resultString appendString:@"\n"];
                
                [_handler seekToEndOfFile];
                [_handler writeData:[resultString dataUsingEncoding:NSUTF8StringEncoding]];
                [_handler synchronizeFile];
                
                [_insertReadyQueue removeObject:firstAction];
            }
        }
    } @catch (NSException *exception) {
        RLog(@"%@", exception.reason);
        [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                   typeOf:UHInnerDataTypeTryCatch
                                               actionFlow:nil];
    }
}

- (void)insertInnerData:(NSString *)message typeOf:(NSString *)type actionFlow:(void (^)(UH_ActionFlow *innerData)) innerDataBlock{
    UH_ActionFlow *actionFlow = [[UH_ActionFlow alloc] genActionFlowInnerDataUptime:_sessionInfo.uptimeStart withNow:UHNowTimeStamp message:message type:type];

    if (innerDataBlock) {
        innerDataBlock(actionFlow);
    } else {
        RLog_ViewCycle;
    }
    
    [self insertActionFlow:actionFlow];
}

#pragma mark - Screen Type

- (UH_ActionFlow *)genActionFlowAppStart:(long long)upTime {
    RLog_ViewCycle;
    return [[UH_ActionFlow alloc] initAt:upTime from:self.sessionInfo.uptimeStart typeOf:UHActionFlowTypeAppStart];
}

- (UH_ActionFlow *)genActionFlowAppEnd:(long long)upTime {
    RLog_ViewCycle;
    return [[UH_ActionFlow alloc] initAt:upTime from:self.sessionInfo.uptimeStart typeOf:UHActionFlowTypeAppEnd];
}

- (UH_ActionFlow *)genActionFlowScreenStart:(long long)upTime with:(UH_ScreenInfo *)screenInfo {
    RLog_ViewCycle;
    UH_SessionManager.isLoadingComplete = YES;
    return [[UH_ActionFlow alloc] initAt:upTime
                                    from:_sessionInfo.uptimeStart
                                  typeOf:UHActionFlowTypeScreenStart
                                    with:screenInfo];
}

- (UH_ActionFlow *)genActionFlowInterBackground:(long long)upTime {
    RLog_ViewCycle;
    
    return [[UH_ActionFlow alloc] initAt:upTime from:self.sessionInfo.uptimeStart typeOf:UHActionFlowTypeInterBackground];
}

- (UH_ActionFlow *)genActionFlowWillResignActive:(long long)upTime {
    RLog_ViewCycle;
    return [[UH_ActionFlow alloc] initAt:upTime from:self.sessionInfo.uptimeStart typeOf:UHActionFlowTypeWillResignActive];
}

#pragma mark - Action Type
//todo::액션플로우로 옴기기
- (UH_ActionFlow *)genActionFlowTap:(long long)upTime with:(UH_TapInteraction *)tapInteraction {
    return [[UH_ActionFlow alloc] initAt:upTime
                                    from:self.sessionInfo.uptimeStart
                                  typeOf:UHActionFlowTypeTap
                                    with:tapInteraction];
}

- (UH_ActionFlow *)genActionFlowDoubleTap:(long long)upTime with:(UH_TapInteraction *)tapInteraction {
    return [[UH_ActionFlow alloc] initAt:upTime
                                    from:self.sessionInfo.uptimeStart
                                  typeOf:UHActionFlowTypeDoubleTap
                                    with:tapInteraction];
}

- (UH_ActionFlow *)genActionFlowLongPress:(long long)upTime with:(UH_TapInteraction *)tapInteraction {
    return [[UH_ActionFlow alloc] initAt:upTime
                                    from:self.sessionInfo.uptimeStart
                                  typeOf:UHActionFlowTypeLongPress
                                    with:tapInteraction];
}

- (UH_ActionFlow *)genActionFlowSwipe:(long long)upTime with:(UH_SwipeInteraction *)swipeInteraction {
    return [[UH_ActionFlow alloc] initAt:upTime from:self.sessionInfo.uptimeStart typeOf:UHActionFlowTypeSwipe with:swipeInteraction];
}

- (UH_ActionFlow *)genActionFlowKeyboardEnableStatus:(long long)upTime {
    RLog_ViewCycle;
    return [[UH_ActionFlow alloc] initAt:upTime from:self.sessionInfo.uptimeStart typeOf:UHActionFlowTypeKeyboardEnableStatus];
}

- (UH_ActionFlow *)genActionFlowKeyboardDisableStatus:(long long)upTime {
    RLog_ViewCycle;
    return [[UH_ActionFlow alloc] initAt:upTime from:self.sessionInfo.uptimeStart typeOf:UHActionFlowTypeKeyboardDisableStatus];
}

#pragma mark - Meta Info Type

- (UH_ActionFlow *)genActionFlowCrash:(long long)upTime with:(UH_CrashInfo *)crashInfo {
    RLog_ViewCycle;
    return [[UH_ActionFlow alloc] initAt:upTime from:self.sessionInfo.uptimeStart typeOf:UHActionFlowTypeCrashed with:crashInfo];
}

- (UH_ActionFlow *)genActionFlowContentTracking:(long long)upTime with:(UH_ContentInfo *)contentInfo {
//    RLog_ViewCycle;
    return [[UH_ActionFlow alloc] initAt:upTime from:self.sessionInfo.uptimeStart typeOf:UHActionFlowTypeContentTracking with:contentInfo];
}



#pragma mark - session

-(BOOL)existSessionData
{
    RLog_ViewCycle;
    NSString *directory = [self getSessionDirPath];
    BOOL result = NO;
    BOOL isDirectory;

    BOOL exist = [NSFileManager.defaultManager fileExistsAtPath:directory isDirectory:&isDirectory];

    if (exist) {
        if (isDirectory) {
            BOOL sessionInfoFile = [self existSessionInfoFile];
            BOOL sessionDataFile = [self existSessionDataFile];
            BOOL actionFlowFile = [self existActionFlowFile];

            result = sessionInfoFile && (sessionDataFile || actionFlowFile);
        }
    }

    return result;
}

+(BOOL)existSessionDataAt:(int)sessionNumber {
//    RLog_ViewCycle;
    NSString *sessionCntStr = [NSString stringWithFormat:@"%d", sessionNumber];
    NSString *directory = [[UH_SessionController sharedInstance].userhabitPath stringByAppendingPathComponent:sessionCntStr];

    return [NSFileManager.defaultManager fileExistsAtPath:directory];
}

- (void)deleteSessionData {
//    RLog_ViewCycle;
    [NSFileManager.defaultManager removeItemAtPath:[self getSessionDirPath] error:nil];
}

+ (void)deleteSessionDataAt:(NSInteger)sessionCount {
    RLog_ViewCycle;
    NSString *sessionCntStr = [NSString stringWithFormat:@"%li", (long)sessionCount];
    NSString *removalDir = [[UH_SessionController sharedInstance].userhabitPath stringByAppendingPathComponent:sessionCntStr];

    [NSFileManager.defaultManager removeItemAtPath:removalDir error:nil];
}


#pragma mark - transport codes

-(BOOL)decideTransportStatusCode:(UHTransportMethod)method{
    NSString *transportMethod = self.transportMethod;

    if (transportMethod != nil) {
        unichar chk = [transportMethod characterAtIndex:method];

        if (chk == 'o') {
            return YES;
        } else if (chk == 'w' &&
                   [UH_Util networkStatus] == UHNetworkStatusWifi) {
            return YES;
        }

    }

    return NO;
}

-(BOOL)chkTransportStatus:(UHTransportMethod)method {
    // Session Closed works!
    if (method == UHTransportMethodSessionClose) {
        if (!_transportMethod) {
            return YES;
        } else {
            return [self decideTransportStatusCode:method];
        }

    } else if (method == UHTransportMethodScreenshotInfo) {
        if (!UH_SessionManager.isDevelopMode) {
            return NO;
        } else {
            if (!_transportMethod) {
                return UH_SessionManager.isScreenshotCaptureMode;
            } else {
                return [self decideTransportStatusCode:method];
            }
        }

    } else {
        return NO;
    }
}

- (void)setSessionTransportData:(NSDictionary *)data{
    RLog(@"%@", data);
    
    _transportMethod = data[@"transportMethod"][@"d"];
    _transportConst = UHTransportConstMake([data[@"transportConst"][@"t"] intValue], [data[@"transportConst"][@"v"] longValue]);
}

- (NSDictionary *)sessionStartInformationDictionary{
    NSMutableDictionary *result = [NSMutableDictionary new];
#if USERHABIT_U2
    [result addEntriesFromDictionary        : [_appInfo build] ];
    [result addEntriesFromDictionary        : [_deviceInfo build] ];
    [result addEntriesFromDictionary        : [_sessionInfo build] ];
#else
     result = @{
     UHProtocolKeyAppInformation            : [_appInfo build],
     UHProtocolKeyDeviceInformation         : [_deviceInfo build],
     UHProtocolKeySessionInformation        : [_sessionInfo build] };
#endif
    //NSLog(@"테스트로그%@",result);
    
    return [NSDictionary dictionaryWithDictionary:result];
}

- (NSDictionary *)sessionEndInformationDictionary{
    NSMutableDictionary *sessionEndInformationDictionary = [NSMutableDictionary new];
    sessionEndInformationDictionary[UHProtocolKeyAppInformation] = [_appInfo build];
    sessionEndInformationDictionary[UHProtocolKeyDeviceInformation] = [_deviceInfo build];
    sessionEndInformationDictionary[UHProtocolKeySessionInformation] = [_sessionInfo build];
    sessionEndInformationDictionary[@"w"] = @(UHSessionStatusTypeNormal);
    sessionEndInformationDictionary[@"n"] = @([UH_Util networkStatus]);     //네트워크 값 [s][n]은 미사용?
    
    if ([_scrollViewArray firstObject]) {
        NSMutableArray *tempArray = [NSMutableArray new];
        for (UH_ScrollViewInfo *inInfo in _scrollViewArray) {
            [tempArray addObject:inInfo.scrollViewName];
        }
        sessionEndInformationDictionary[@"m"] = tempArray;
    }    
    if (_transportMethod) {
        sessionEndInformationDictionary[@"r"] = _transportMethod;
    }
    /*
     * 유효성 검증 코드 현재 미 사용
     * q - 전송 시작할 Action  flow의 Array index
     * c - 전송하는 Action  flow의 갯수
     */
    sessionEndInformationDictionary[@"q"] = @(0);
    sessionEndInformationDictionary[@"c"] = @(0);
    
    NSDictionary *userInfo = [UH_SessionManager loadUserInfo];
    if (userInfo && userInfo.count > 0) {
        sessionEndInformationDictionary[@"u"] = userInfo;
    }
    
    if (_reloaded) {
        if (_sessionStatus == UHSessionStatusTypeNormal) {
            sessionEndInformationDictionary[@"w"] = @(UHSessionStatusTypeResendNormal);
        } else {
            if (_sessionInfo.networkStatus == UHNetworkStatusWwan) {
                sessionEndInformationDictionary[@"w"] = @(UHSessionStatusTypeResendCellular);
            } else {
                sessionEndInformationDictionary[@"w"] = @(UHSessionStatusTypeResendAbnormal);
            }
        }
    } else {
        sessionEndInformationDictionary[@"w"] = @(UHSessionStatusTypeNormal);
    };
    
    return sessionEndInformationDictionary;
}

- (NSDictionary *)sessionInformationDictionary{
    NSDictionary *returnDictionary = @{
                                                UHProtocolKeyAppInformation: [_appInfo build],
                                                UHProtocolKeyDeviceInformation: [_deviceInfo build]
    };
    
    return returnDictionary;
}

- (NSDictionary *)sessionInformationAddObject:(NSDictionary *)object{
    NSMutableDictionary *returnDictionary = [NSMutableDictionary dictionaryWithDictionary:[self sessionInformationDictionary]];
    [returnDictionary addEntriesFromDictionary:object];
    
    return returnDictionary;
}

@end
