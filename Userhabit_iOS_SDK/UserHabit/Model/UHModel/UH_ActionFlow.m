//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_ActionFlow.h"
#import "UH_ScreenInfo.h"
#import "UH_TapInteraction.h"
#import "UH_SwipeInteraction.h"
#import "UH_CrashInfo.h"
#import "UH_ContentInfo.h"
#import "UH_InnerData.h"

@interface UH_ActionFlow ()

@property (nonatomic, assign) UHActionFlowType actionFlowType;
@property (nonatomic, assign) int uptime;
@property (nonatomic, strong) UH_Model *message;

@end


@implementation UH_ActionFlow

- (instancetype)initAt:(long long)uptime from:(long long)startTime typeOf:(UHActionFlowType)type {
    self = [super init];

    if (self) {
        self.uptime = (int)(uptime - startTime);
        self.actionFlowType = type;
    }

    return self;
}


- (instancetype)initAt:(long long)uptime from:(long long)startTime typeOf:(UHActionFlowType)type with:(UH_Model *)message {
    self = [super init];

    if (self) {
        self.uptime = (int)(uptime - startTime);
        self.actionFlowType = type;
        self.message = message;
    }

    return self;
}


#pragma mark - util

- (void)setUptime:(int)uptime{
    _uptime = uptime;
}

- (BOOL)isEqualToActionFlow:(UH_ActionFlow *)actionFlow{
    if(_actionFlowType == actionFlow.actionFlowType &&
       _uptime == actionFlow.uptime){
        return YES;
    }
        
    return NO;
}

- (NSDictionary *)build {
    NSMutableDictionary *actionFlow = [NSMutableDictionary new];
    
#if USERHABIT_U2
    /**
     UHProtocolKeyActionFlowType = t
     UHProtocolKeyActionFlowUpTime = u
     */
    actionFlow[UHProtocolKeyActionFlowType] = @(_actionFlowType);
    actionFlow[UHProtocolKeyActionFlowUpTime] = @(_uptime);
    
    if (self.message) {
            
        [actionFlow addEntriesFromDictionary:[_message build]];
    }
    
#else
    if (self.message) {
        actionFlow = @{
                UHProtocolKeyActionFlowType: @(_actionFlowType),
                UHProtocolKeyActionFlowUpTime: @(_uptime),
                @"m": [_message build]
        };
    } else {
        actionFlow= @{
                UHProtocolKeyActionFlowType: @(_actionFlowType),
                UHProtocolKeyActionFlowUpTime: @(_uptime)
        };
    }
#endif
//    NSLog(@"액션추적 로그 !!\n%@",actionFlow);
    return [NSDictionary dictionaryWithDictionary:actionFlow];
}

- (NSString *)description{
    NSString *returnString;
    if (_message){
        returnString = [NSString stringWithFormat:@"actionFlow Type:%d / uptime : %d / message : %@", _actionFlowType, _uptime, _message];
    } else {
        returnString = [NSString stringWithFormat:@"actionFlow Type:%d / uptime : %d", _actionFlowType, _uptime];
        
    }
    
    return returnString;
}

#pragma mark - Inner Data Type
- (instancetype)genActionFlowInnerDataUptime:(long long)upTime withNow:(long long)now message:(NSString *)message type:(NSString *)type{
//    RLog_ViewCycle;
    UH_InnerData *innerData = [[UH_InnerData alloc] initValue:message typeOf:type];
    return [[UH_ActionFlow alloc] initAt:now from:upTime typeOf:UHActionFlowTypeInnerData with:innerData];
}

@end
