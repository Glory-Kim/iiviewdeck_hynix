//
//  UH_U2Model.m
//  UserHabit
//
//  Created by 김영광 on 2020/08/11.
//  Copyright © 2020 ;. All rights reserved.
//

#import "UH_U2Model.h"

@implementation UH_U2Model : NSObject
-(id)init{
    self = [super init];
    return self;
}

- (NSDictionary*)convertU2Model:(NSArray<NSDictionary *> *)dic_list withFrag:(NSArray<NSString *>*)frags{
    NSMutableDictionary *result_dic= [NSMutableDictionary new];
    
    //비동기
//    [dic_list enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//
//    }];
    
    for(int i=0; i <dic_list.count; ++i){
        NSDictionary * dic = [dic_list objectAtIndex:i];
        for(id keyStr in dic){
            result_dic[[NSString stringWithFormat:@"%@%@",frags[i],keyStr]] = [dic valueForKey:keyStr];
        }
    }
    return [NSDictionary dictionaryWithDictionary:result_dic];
}
@end
