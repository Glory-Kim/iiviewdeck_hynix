//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_DeviceInfo.h"
#import "UH_SystemServices.h"
#import <UIKit/UIKit.h>

@interface UH_DeviceInfo ()

@property (nonatomic, assign) int deviceResolutionWidth;
@property (nonatomic, assign) int deviceResolutionHeight;

@end

@implementation UH_DeviceInfo

- (instancetype)initWithID:(NSString *)identifier {
    self = [super init];

    if (self) {
        self.brandName = @"apple";
        self.deviceBoard = @"apple";
        self.deviceModel = [[UH_SystemServices sharedServices] systemDeviceTypeNotFormatted];
        self.deviceName = [[UH_SystemServices sharedServices] systemDeviceTypeNotFormatted];
        self.deviceId = identifier;
        self.displayDpi = 0;
        RLog(@"%@", NSStringFromCGSize([[UIScreen mainScreen] bounds].size));
        _deviceResolutionSize = [[UIScreen mainScreen] bounds].size;
        
        RLog_ViewCycle;
    }

    return self;
}

- (instancetype)initDictionary:(NSDictionary *)dictionary {
    self = [super init];

    if (self) {
        self.brandName = dictionary[@"c"];
        self.deviceBoard = dictionary[@"b"];
        self.deviceId = dictionary[@"i"];
        self.deviceModel = dictionary[@"m"];
        self.deviceName = dictionary[@"n"];
        _deviceResolutionSize = CGSizeMake([dictionary[@"s"][@"w"] intValue], [dictionary[@"s"][@"h"] intValue]);
        self.displayDpi = [dictionary[@"d"] intValue];
        RLog_ViewCycle;
    }

    return self;
}


- (void)setDeviceResolutionSize:(CGSize)size {
    _deviceResolutionSize = size;
}

- (NSDictionary *)build {
    NSDictionary *result = @{
            UHProtocolKeyDeviceCompany      : self.brandName,
            UHProtocolKeyDeviceBoard        : self.deviceBoard,
            UHProtocolKeyDeviceID           : self.deviceId,
            UHProtocolKeyDeviceModel        : self.deviceModel,
            UHProtocolKeyDeviceName         : self.deviceName,
        #if USERHABIT_U2
            UHProtocolKeyDeviceScreen       : [NSString stringWithFormat:@"%@x%@",
                                              @(_deviceResolutionSize.width),
                                              @(_deviceResolutionSize.height)],
            UHProtocolKeyDeviceWidth        : @(_deviceResolutionSize.width),
            UHProtocolKeyDeviceHeight       : @(_deviceResolutionSize.height),
               
        #else
        UHProtocolKeyDeviceScreen           : @{
            UHProtocolKeyDeviceWidth        : @(_deviceResolutionSize.width),
            UHProtocolKeyDeviceHeight       : @(_deviceResolutionSize.height)
        },
        #endif
            UHProtocolKeyDeviceDPI          : @(self.displayDpi),
            UHProtocolKeyDeviceOS           : @2  // this field OS platform is always 2
    };
    return result;
}

@end
