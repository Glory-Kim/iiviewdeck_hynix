//
//  UH_U2Model.h
//  UserHabit
//
//  Created by 김영광 on 2020/08/10.
//  Copyright © 2020 Andbut. All rights reserved.
//

//#import <UserHabit/UserHabit.h>
#import <Foundation/Foundation.h>
@interface UH_U2Model:NSObject

@property (nonatomic,assign) NSDictionary *u2Model;
-(id)init;
- (NSDictionary*)convertU2Model:(NSArray<NSDictionary *> *)dic_list withFrag:(NSArray<NSString *>*)frag;
@end

