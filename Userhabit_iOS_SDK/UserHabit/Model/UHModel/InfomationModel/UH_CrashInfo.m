//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_CrashInfo.h"


@implementation UH_CrashInfo

- (NSDictionary *)build {
    NSDictionary *crashInfo = @{
            UHProtocolKeyCrashInfoBattery: @(self.battery),
            UHProtocolKeyCrashInfoCharging: @(self.charging),
            UHProtocolKeyCrashInfoMessage: self.crashMessage,
            UHProtocolKeyCrashInfoExceptionType: self.exceptionType,
            UHProtocolKeyCrashInfoFreeDisk: @(self.freeDisk),
            UHProtocolKeyCrashInfoFreeMemory: @(self.freeMemory),
            UHProtocolKeyCrashInfoJailBreaking: @(self.jailBreaking),
            UHProtocolKeyCrashInfoNetworkStatus: @(self.networkStatus),
            UHProtocolKeyCrashInfoProcesses: @(self.processes),
            UHProtocolKeyCrashInfoStackTrace: self.stackTrace,
            UHProtocolKeyCrashInfoTotalDisk: @(self.totalDisk),
            UHProtocolKeyCrashInfoTotalMemory: @(self.totalMemory)
    };
    
    return crashInfo;
}

@end
