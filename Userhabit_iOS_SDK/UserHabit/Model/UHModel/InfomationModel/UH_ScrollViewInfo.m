//
//  UH_ScrollViewInfo.m
//  UserHabit
//
//  Created by lotco on 2017. 7. 12..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import "UH_ScrollViewInfo.h"

@implementation UH_ScrollViewInfo

-(NSDictionary *)build{
    NSDictionary *dictionary = @{
                                 @"c":_scrollViewName,
                                 @"i":@(_mapping),
                                 };
    RLog(@"%@", dictionary);
    return dictionary;
}

-(NSString *)description{
    return [NSString stringWithFormat:@"UH_ScrollViewInfo { %@[%d] (%@) }", _scrollViewName, _mapping, NSStringFromCGPoint(_maximumScrollViewPosition)];
}
@end
