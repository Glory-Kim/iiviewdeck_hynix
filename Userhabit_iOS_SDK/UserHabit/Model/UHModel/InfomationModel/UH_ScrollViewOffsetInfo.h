//
//  UH_ScrollViewOffsetInfo.h
//  UserHabit
//
//  Created by lotco on 2017. 7. 19..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import "UH_Model.h"
//{"m":{"m":3,"p":[0,7903]},"t":12290,"u":6555}
@interface UH_ScrollViewOffsetInfo : UH_Model
@property int scrollMappingNumber;
@property CGPoint contentsOffset;
@end
