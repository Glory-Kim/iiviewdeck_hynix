//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_Model.h"

@interface UH_AppInfo : UH_Model

@property (nonatomic, strong) NSString *apiKey;
@property (nonatomic, assign) int appVersionCode;
@property (nonatomic, strong) NSString *appVersionName;
@property (nonatomic, assign) int sdkVersionCode;
@property (nonatomic, strong) NSString *sdkVersionName;

-(instancetype)initWithData:(NSString *)apiKey;
-(instancetype)initDictionary:(NSDictionary *)dictionary;
-(void)checkErrorSession;
@end
