//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import <UserHabit/UserHabit.h>
#import "UH_Model.h"


@interface UH_UserInfo : UH_Model

@property (nonatomic, strong) NSString *customUserId;
@property (nonatomic, assign) int customUserType;
@property (nonatomic, assign) UHUserInfoGender gender;
@property (nonatomic, assign) int age;

@end
