//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_Model.h"
#import "UH_ScrollViewInfo.h"
#import "UserHabitConstant.h"

FOUNDATION_EXPORT NSString * const UHScreenInformationScreenName;
FOUNDATION_EXPORT NSString * const UHScreenInformationFragmentName;
FOUNDATION_EXPORT NSString * const UHScreenInformationOrientation;
FOUNDATION_EXPORT NSString * const UHScreenInformationScreenType;
FOUNDATION_EXPORT NSString * const UHScreenInformationPoint;
FOUNDATION_EXPORT NSString * const UHScreenInformationSize;
FOUNDATION_EXPORT NSString * const UHScreenInformationScrollInformation;
FOUNDATION_EXPORT NSString * const UHScreenInformationObjectInformation;

@class UH_ObjectInfo;

@interface UH_ScreenInfo : UH_Model

@property (nonatomic, strong) NSString *screenName;
@property (nonatomic, strong) NSString *fragment;
@property (nonatomic, assign) UIDeviceOrientation orientation;
@property (nonatomic, assign) UHScreenType screenType;
@property (nonatomic, assign) CGRect frame;
@property (nonatomic, strong) NSMutableSet<UH_ObjectInfo *> *objectSet;
@property (nonatomic, weak) UIViewController *rootViewController;
@property (nonatomic, weak) UH_ScrollViewInfo *scrollViewInformation;
@property NSString *path;
@property NSString *fileName;

@property BOOL isTakeScreenshot;
@property BOOL isCompleteUploadScreenshot;
@property BOOL isUploadLoading;
@property int uploadTryCount;           //3이 넘을 경우 파일 삭제

- (instancetype)initWithDictionary:(NSDictionary *)data;

-(instancetype)initInDefault:(NSString *)screenName
                    fragment:(NSString *)fragment
                 orientation:(UIDeviceOrientation)orientation
                        size:(CGSize)size;

@end
