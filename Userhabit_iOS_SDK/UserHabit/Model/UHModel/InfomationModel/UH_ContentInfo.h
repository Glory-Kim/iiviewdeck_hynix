//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_Model.h"

@interface UH_ContentInfo : UH_Model

@property (nonatomic, strong) NSString *action;
@property (nonatomic, strong) NSString *contentName;

- (instancetype)initAction:(NSString *)action conentNameOf:(NSString *)contentName;

@end
