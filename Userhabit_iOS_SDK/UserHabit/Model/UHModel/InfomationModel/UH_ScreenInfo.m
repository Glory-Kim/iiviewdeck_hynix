//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_ScreenInfo.h"
#import "UH_ObjectInfo.h"
#import "UH_SessionController.h"

NSString * const UHScreenInformationScreenName           = @"a";
NSString * const UHScreenInformationFragmentName         = @"f";
NSString * const UHScreenInformationOrientation          = @"r";
NSString * const UHScreenInformationScreenType           = @"t";
NSString * const UHScreenInformationPoint                = @"b";
NSString * const UHScreenInformationSize                 = @"p";
NSString * const UHScreenInformationObjectInformation     = @"o";
NSString * const UHScreenInformationScrollInformation     = @"s";

@implementation UH_ScreenInfo

- (instancetype)init
{
    self = [super init];
    if (self) {
        _uploadTryCount = 0;
    }
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)data{
    self = [super init];
    
    if (self) {
        [data enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            if ([key isEqualToString:UHScreenInformationScreenName]) {
                self->_screenName = obj;
            } else if ([key isEqualToString:UHScreenInformationFragmentName]){
                self->_fragment = obj;
            } else if ([key isEqualToString:UHScreenInformationOrientation]){
                self->_orientation = [obj intValue];
            } else if ([key isEqualToString:UHScreenInformationScreenType]){
                self->_screenType = [obj intValue];
            } else if ([key isEqualToString:UHScreenInformationPoint] || [key isEqualToString:UHScreenInformationSize]){
                RLog(@"warning!!! - point, size 형식 확인!! %@", data);
            } else {
                RLog(@"warning!!! - unknown data 형식 확인!! %@", data);
            }
        }];
    }
    return self;

}
- (instancetype)initInDefault:(NSString *)screenName fragment:(NSString *)fragment orientation:(UIDeviceOrientation)orientation size:(CGSize)size{
    self = [super init];

    @try {
        if (self) {
            _screenName = screenName;
            if (!fragment ||
                fragment.length == 0) {
                _fragment = @"";
            } else {
                _fragment = fragment;
            }
            _orientation = orientation;
            _frame = CGRectMake(-100, -100, size.width, size.height);
            _screenType = UHScreenTypeDefault;
        }
    } @catch (NSException *exception) {
        [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                   typeOf:UHInnerDataTypeTryCatch
                                               actionFlow:nil];
    }

    return self;
}

- (NSDictionary *)build {

    NSMutableDictionary *screenInfo = [NSMutableDictionary dictionaryWithCapacity:20];

    screenInfo[UHScreenInformationScreenName] = self.screenName;

    if (self.fragment && self.fragment.length > 0) {
        screenInfo[UHScreenInformationFragmentName] = self.fragment;
    }

    //1 똑바로
    //2 뒤집
    //3 오른쪽 홈버튼
    //4 왼쪽 홈버튼

    if (_orientation == UIDeviceOrientationPortrait ||
        _orientation == UIDeviceOrientationPortraitUpsideDown) {
        screenInfo[UHScreenInformationOrientation] = @1;
    } else if (_orientation == UIDeviceOrientationLandscapeLeft ||
               _orientation == UIDeviceOrientationLandscapeRight){
        screenInfo[UHScreenInformationOrientation] = @2;
    } else {
        screenInfo[UHScreenInformationOrientation] = @1;
    }
    
    
    
    screenInfo[UHScreenInformationScreenType] = @(self.screenType);
#if USERHABIT_U2
    screenInfo[UHScreenInformationSize] = @{ @"w": @(_frame.size.width), @"h": @(_frame.size.height) };

    if (_frame.origin.x >= 0 &&
        _frame.origin.y >= 0) {
        screenInfo[UHScreenInformationPoint] = @{ @"w": @(_frame.origin.x), @"h": @(_frame.origin.y) };
    }

    if (_objectSet &&
        [_objectSet anyObject]) {
        NSMutableArray<NSDictionary *> *objectJSONArray = [NSMutableArray new];

        for (UH_ObjectInfo *objectInfo in [_objectSet allObjects]) {
            [objectJSONArray addObject:[objectInfo build]];
        }

        screenInfo[UHScreenInformationObjectInformation] = objectJSONArray;
    }
#else
    screenInfo[UHScreenInformationSize] = @{ @"w": @(_frame.size.width), @"h": @(_frame.size.height) };

    if (_frame.origin.x >= 0 &&
        _frame.origin.y >= 0) {
        screenInfo[UHScreenInformationPoint] = @{ @"w": @(_frame.origin.x), @"h": @(_frame.origin.y) };
    }

    if (_objectSet &&
        [_objectSet anyObject]) {
        NSMutableArray<NSDictionary *> *objectJSONArray = [NSMutableArray new];

        for (UH_ObjectInfo *objectInfo in [_objectSet allObjects]) {
            [objectJSONArray addObject:[objectInfo build]];
        }

        screenInfo[UHScreenInformationObjectInformation] = objectJSONArray;
    }
#endif
    return [NSDictionary dictionaryWithDictionary:screenInfo];
}

@end
