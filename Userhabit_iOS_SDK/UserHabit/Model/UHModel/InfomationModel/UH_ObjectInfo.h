//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_Model.h"

@interface UH_ObjectInfo : UH_Model


@property (weak) UIView *objectView;
@property(nonatomic, strong) NSString *objectId;
@property(nonatomic, strong) NSString *objectType;
@property(nonatomic, assign, readonly) CGRect objectFrame;
@property NSString *path;
@property NSString *fileName;


- (instancetype)initId:(NSString *)objectId typeOf:(NSString *)type frame:(CGRect)frame;

@end
