//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_Model.h"

@interface UH_DeviceInfo : UH_Model

@property (nonatomic, strong) NSString *brandName;
@property (nonatomic, strong) NSString *deviceBoard;
@property (nonatomic, strong) NSString *deviceId;
@property (nonatomic, strong) NSString *deviceModel;
@property (nonatomic, strong) NSString *deviceName;
@property (nonatomic, assign) int displayDpi;
@property (nonatomic, assign, readonly) CGSize deviceResolutionSize;

#pragma mark - initializer
-(instancetype)initWithID:(NSString *)identifier;
-(instancetype)initDictionary:(NSDictionary *)dictionary;

#pragma mark - custom setters

-(void)setDeviceResolutionSize:(CGSize)size;

@end
