//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_ObjectInfo.h"

@implementation UH_ObjectInfo

- (instancetype)initId:(NSString *)objectId
                typeOf:(NSString *)type
                 frame:(CGRect)frame{
    self = [super init];

    if (self) {
        _objectId = objectId;
        _objectType = type;
//        _objectFrame = frame;
    }

    return self;
}

- (NSDictionary *)build {

    if (!_objectId) {
        _objectId = @"null object id";
    }
    NSMutableDictionary *objectInfo = [NSMutableDictionary new];
    
    objectInfo[UHProtocolKeyObjectID] = self.objectId;
    objectInfo[UHProtocolKeyObjectType] = self.objectType;
    
#if USERHABIT_U2
     objectInfo[UHProtocolKeyObjectFrameX] = @(_objectFrame.origin.x);
     objectInfo[UHProtocolKeyObjectFrameY] = @(_objectFrame.origin.y);
     objectInfo[UHProtocolKeyObjectFrameSizeWidth] = @(_objectFrame.size.width);
     objectInfo[UHProtocolKeyObjectFrameSizeHeith] = @(_objectFrame.size.height);
#else
     objectInfo[@"r"] = @{
            UHProtocolKeyObjectFrameX: @(_objectFrame.origin.x),
            UHProtocolKeyObjectFrameY: @(_objectFrame.origin.y),
            UHProtocolKeyObjectFrameSizeWidth: @(_objectFrame.size.width),
            UHProtocolKeyObjectFrameSizeHeith: @(_objectFrame.size.height)
     };
#endif
    return [NSDictionary dictionaryWithDictionary:objectInfo];
}

@end
