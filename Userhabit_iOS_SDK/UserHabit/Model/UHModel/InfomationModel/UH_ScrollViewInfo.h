//
//  UH_ScrollViewInfo.h
//  UserHabit
//
//  Created by lotco on 2017. 7. 12..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import "UH_Model.h"
#import "UH_ScrollImageInformation.h"


/*
 * 세션
 */
@interface UH_ScrollViewInfo : UH_Model

@property (weak) UIViewController *superViewController;
@property (nonatomic, strong) NSString *scrollViewName;
@property (nonatomic, weak) UIScrollView *scrollView;
@property int mapping;
@property CGPoint maximumScrollViewPosition;
@property NSMutableArray <UH_ScrollImageInformation *> *images;
@end
