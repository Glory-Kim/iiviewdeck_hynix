//
//  UH_ScrollImageInformation.h
//  UserHabit
//
//  Created by lotco on 2017. 10. 24..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UH_ScrollImageInformation : NSObject

@property NSString *filePath;
@property int index;
@property CGSize size;

- (instancetype)initWithIndex:(int)index path:(NSString *)path size:(CGSize)size;

@end
