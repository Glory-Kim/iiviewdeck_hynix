//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_Model.h"
#import "UserHabitConstant.h"


@interface UH_SessionInfo : UH_Model

@property (nonatomic, strong) NSString *sessionId;
@property (nonatomic, assign) long long sessionStartTime;
@property (nonatomic, strong) NSString *osVersion;
@property (nonatomic, strong) NSString *locale;
@property (nonatomic, strong) NSString *timezoneOffset;
@property (nonatomic, strong) NSString *timezoneName;
@property (nonatomic, assign) long long uptimeStart;
@property (nonatomic, assign) long long deviceTime;
@property (nonatomic, assign) int sessionCount;
@property (nonatomic, assign) UHNetworkStatus networkStatus;
@property (nonatomic, assign) long long referrerTimestamp;
@property (nonatomic, assign) long long latestSuccessTimestamp;


-(instancetype)initWithData:(NSString *)sessionId
               sessionCount:(int)sessionCount
              networkStatus:(UHNetworkStatus)networkStatus
                     uptime:(long long)uptime
                 deviceTime:(long long)deviceTime
           latestSucessTime:(long long)latestSuccessTime;
-(instancetype)initDictionary:(NSDictionary *)dictionary;
@end
