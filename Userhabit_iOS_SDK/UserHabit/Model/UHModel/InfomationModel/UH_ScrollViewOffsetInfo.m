//
//  UH_ScrollViewOffsetInfo.m
//  UserHabit
//
//  Created by lotco on 2017. 7. 19..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import "UH_ScrollViewOffsetInfo.h"

@implementation UH_ScrollViewOffsetInfo
-(NSDictionary *)build{
    
    NSDictionary *dictionary = @{@"m":@(_scrollMappingNumber),
                                 @"p":@[@(_contentsOffset.x), @(_contentsOffset.y)]};
    RLog(@"%@", dictionary);
    return dictionary;
}

@end
