//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_SessionInfo.h"
#import "UH_SystemServices.h"
#import "UH_Util.h"
#import "UHNetworkReachabilityManager.h"

@implementation UH_SessionInfo

- (instancetype)initWithData:(NSString *)sessionId
                sessionCount:(int)sessionCount
               networkStatus:(UHNetworkStatus)networkStatus
                      uptime:(long long)uptime
                  deviceTime:(long long)deviceTime
            latestSucessTime:(long long)latestSuccessTime {
    self = [super init];

    if (self) {
        self.sessionId = sessionId;
        self.sessionCount = sessionCount;
        self.networkStatus = networkStatus;
        self.uptimeStart = uptime;
        self.sessionStartTime = deviceTime;
        self.deviceTime = deviceTime;
        self.latestSuccessTimestamp = latestSuccessTime;

        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        dateFormat.dateFormat = @"yyyyMMddHHmmss";
        NSDate *nowDate = [[NSDate alloc]init];
        NSDateFormatter *timeZoneFormat = [[NSDateFormatter alloc]init];
        timeZoneFormat.dateFormat = @"z";       
        NSString *timezoneString = [timeZoneFormat stringFromDate:nowDate];
        NSString *timeZoneOffset = [timezoneString substringFromIndex:3];
        
        if (timeZoneOffset.length == 0) {
            if ([timezoneString isEqualToString:@"EDT"]) {
                timeZoneOffset = @"-4";
            } else if ([timezoneString isEqualToString:@"EST"] ||
                       [timezoneString isEqualToString:@"CDT"]) {
                timeZoneOffset = @"-5";
            } else if ([timezoneString isEqualToString:@"PDT"]) {
                timeZoneOffset = @"-7";
                } else if ([timezoneString isEqualToString:@"PST"]) {
                timeZoneOffset = @"-8";
            }
        }
        
        self.osVersion = [UIDevice currentDevice].systemVersion;
        self.locale = [[UH_SystemServices sharedServices] language];
        self.timezoneOffset = timeZoneOffset;
        self.timezoneName = [[UH_SystemServices sharedServices] timeZoneSS];
    }

    return self;
}

- (instancetype)initDictionary:(NSDictionary *)dictionary {
    self = [super init];

    if (self) {
        self.sessionId = dictionary[@"i"];
        self.sessionStartTime = [dictionary[@"t"] longLongValue];
        self.osVersion = dictionary[@"v"];
        self.locale = dictionary[@"l"];
        self.timezoneOffset = dictionary[@"o"];
        self.timezoneName = dictionary[@"z"];
        self.uptimeStart = [dictionary[@"u"] longLongValue];
        self.deviceTime = [dictionary[@"d"] longLongValue];
        self.sessionCount = [dictionary[@"c"] intValue];
        self.networkStatus = (UHNetworkStatus)[dictionary[@"n"] intValue];
        self.referrerTimestamp = [dictionary[@"rt"] longLongValue];
        self.latestSuccessTimestamp = [dictionary[@"lt"] longLongValue];
    }

    return self;
}

- (NSDictionary *)build {
//    NSDictionary *sessionInfo;
//
//    sessionInfo = @{
//            @"i": self.sessionId,
//            @"t": @(self.sessionStartTime),
//            @"v": self.osVersion,
//            @"l": self.locale,
//            @"o": self.timezoneOffset,
//            @"z": self.timezoneName,
//            @"u": @(self.uptimeStart),
//            @"d": @(self.deviceTime),
//            @"c": @(self.sessionCount),
//            @"n": @(_networkStatus),
//            @"lt": @(self.latestSuccessTimestamp)
//    };

    return @{
        UHProtocolKeySessionID              : self.sessionId,
        UHProtocolKeySessionStartTime       : @(self.sessionStartTime),
        UHProtocolKeySessionVersion         : self.osVersion,
        UHProtocolKeySessionLocale          : self.locale,
        UHProtocolKeySessionTimeZone        : self.timezoneName,
        UHProtocolKeySessionCount           : @(self.sessionCount),
        UHProtocolKeySessionNetworkStatus   : @(_networkStatus),

#if USERHABIT_U2
        UHProtocolKeySessionLatestSuccessTimestamp  : @(self.latestSuccessTimestamp)
#else
        UHProtocolKeySessionDeviceTime      : @(self.deviceTime),
        UHProtocolKeySessionTimeOffset      : self.timezoneOffset,
        UHProtocolKeySessionUpTimeStart     : @(self.uptimeStart),
        UHProtocolKeySessionLatestSuccessTimestamp  : @(self.latestSuccessTimestamp)

#endif
        
    };
}

@end
