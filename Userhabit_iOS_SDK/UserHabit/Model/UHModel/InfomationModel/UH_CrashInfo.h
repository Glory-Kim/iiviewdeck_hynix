//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_Model.h"
#import "UserHabitConstant.h"

@interface UH_CrashInfo : UH_Model

@property (nonatomic, assign) float battery;
@property (nonatomic, assign) BOOL charging;
@property (nonatomic, strong) NSString *crashMessage;
@property (nonatomic, strong) NSString *exceptionType;
@property (nonatomic, assign) long long freeDisk;
@property (nonatomic, assign) long long freeMemory;
@property (nonatomic, assign) BOOL jailBreaking;
@property (nonatomic, assign) UHNetworkStatus networkStatus;
@property (nonatomic, assign) int processes;
@property (nonatomic, strong) NSString *stackTrace;
@property (nonatomic, assign) long long totalDisk;
@property (nonatomic, assign) long long totalMemory;

@end
