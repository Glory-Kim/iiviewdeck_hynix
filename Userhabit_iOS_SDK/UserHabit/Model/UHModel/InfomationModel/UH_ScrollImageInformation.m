//
//  UH_ScrollImageInformation.m
//  UserHabit
//
//  Created by lotco on 2017. 10. 24..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import "UH_ScrollImageInformation.h"

@implementation UH_ScrollImageInformation
- (instancetype)initWithIndex:(int)index path:(NSString *)path size:(CGSize)size
{
    self = [super init];
    if (self) {
        _index = index;
        _filePath = path;
        _size = size;
    }
    return self;
}
@end
