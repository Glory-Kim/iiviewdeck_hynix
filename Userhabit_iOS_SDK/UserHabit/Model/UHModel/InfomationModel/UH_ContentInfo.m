//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_ContentInfo.h"


@implementation UH_ContentInfo

- (instancetype)initAction:(NSString *)action conentNameOf:(NSString *)contentName {
    self = [super init];

    if (self) {
        self.action = action;
        self.contentName = contentName;
    }

    return self;
}

- (NSDictionary *)build {
    NSDictionary *contentInfo = @{
            UHProtocolKeyContentAction: self.action,
            UHProtocolKeyContentName: self.contentName
    };

    return contentInfo;
}

@end
