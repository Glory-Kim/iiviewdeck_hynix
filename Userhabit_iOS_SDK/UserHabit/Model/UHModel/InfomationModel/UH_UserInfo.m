//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_UserInfo.h"


@implementation UH_UserInfo


- (instancetype)init {
    self = [super init];
    if (self) {
        _customUserType = -1;
        _gender = UHUserInfoGenderUnuse;
        _age = -1;
    }

    return self;
}


- (NSDictionary *)build {
    NSMutableDictionary *mutableUserInfo = [NSMutableDictionary dictionaryWithCapacity:4];

    if (self.customUserId) {
        mutableUserInfo[@"i"] = self.customUserId;
    }

    if (self.customUserType >= 0) {
        mutableUserInfo[@"t"] = @(self.customUserType);
    }

    if (self.gender >= UHUserInfoGenderMale && self.gender <= UHUserInfoGenderUncategorized) {
        mutableUserInfo[@"g"] = @(self.gender);
    }

    if (self.age >= 0) {
        mutableUserInfo[@"a"] = @(self.age);
    }

    NSDictionary *userInfo = [NSDictionary dictionaryWithDictionary:mutableUserInfo];

    return userInfo;
}

@end