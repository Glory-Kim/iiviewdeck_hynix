//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_AppInfo.h"
#import "UserHabitConstant.h"


@implementation UH_AppInfo

- (instancetype)initWithData:(NSString *)apiKey  {
    self = [super init];

    if (self) {
        NSDictionary *bundleDictionary = [NSBundle mainBundle].infoDictionary;
        NSArray *bundleVersionArray = [bundleDictionary[@"CFBundleVersion"] componentsSeparatedByString:@"."];

        self.apiKey = apiKey;
        self.appVersionCode = [bundleVersionArray[0] intValue];
        self.appVersionName = bundleDictionary[@"CFBundleShortVersionString"];
        self.sdkVersionCode = UHVersionCode;
        self.sdkVersionName = UHSdkVersion;
    }

    return self;
}

- (instancetype)initDictionary:(NSDictionary *)dictionary {
    self = [super init];

    if (self) {
        self.apiKey = dictionary[@"a"];
        self.appVersionCode = [dictionary[@"c"] intValue];
        self.appVersionName = dictionary[@"n"];
        self.sdkVersionCode = [dictionary[@"s"] intValue];
        self.sdkVersionName = dictionary[@"v"];
    }

    return self;
}


- (NSDictionary *)build {

    NSDictionary *appInfo = @{
            UHProtocolKeyAppID: self.apiKey,
            UHProtocolKeyAppCode: @(self.appVersionCode),
            UHProtocolKeyAppName: self.appVersionName,
            UHProtocolKeyAppSDK: @(self.sdkVersionCode),
            UHProtocolKeyAppSDKVersion: self.sdkVersionName
    };
    return appInfo;
}

-(void)checkErrorSession{
    _sdkVersionName = [_sdkVersionName stringByAppendingString:@"_e"];
}
@end
