//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
// 탭에 관련된 정보 모델 탭, 더블탭, 롱탭을 생성한다.
//

#import "UH_GestureModel.h"

@interface UH_TapInteraction : UH_GestureModel

- (instancetype)initPosition:(CGPoint)point;

@end
