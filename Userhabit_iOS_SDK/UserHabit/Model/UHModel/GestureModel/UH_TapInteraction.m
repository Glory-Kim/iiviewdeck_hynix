//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_TapInteraction.h"

@implementation UH_TapInteraction

- (instancetype)initPosition:(CGPoint)point {
    self = [super init];

    if (self) {
        self.gesturePoint = point;
    }

    return self;
}

- (NSDictionary *) build {
    NSMutableDictionary *tapInteraction = [NSMutableDictionary dictionaryWithCapacity:20];

      // another fields
      if (self.objectId) {
          tapInteraction[UHProtocolKeyGestureObjectID] = self.objectId;
      }
    
      if (self.objectDescription) {
          tapInteraction[UHProtocolKeyGestureDesctiption] = self.objectDescription;
      }
#if USERHABIT_U2
    tapInteraction[UHProtocolKeyGesturePointX] = @(self.gesturePoint.x);
    tapInteraction[UHProtocolKeyGesturePointY] = @(self.gesturePoint.y);

    if(self.scrollMapping >= 0) {
        tapInteraction[UHProtocolKeyGestureScrollX] = @(self.scrollEndedPoint.x);
        tapInteraction[UHProtocolKeyGestureScrollY] = @(self.scrollEndedPoint.y);
        tapInteraction[UHProtocolKeyGestureScrollMapping] = @(self.scrollMapping);
    }
#else
    // required fields
    tapInteraction[UHProtocolKeyGesturePoint] = @[@(self.gesturePoint.x), @(self.gesturePoint.y)];
    
    // scroll info fields
    if(self.scrollMapping >= 0) {
        tapInteraction[UHProtocolKeyGestureScrollPoint] = @[@(self.scrollEndedPoint.x), @(self.scrollEndedPoint.y)];
        tapInteraction[UHProtocolKeyGestureScrollMapping] = @(self.scrollMapping);
    }
#endif
    return tapInteraction;
}

-(NSString *)description {
    return [NSString stringWithFormat:@"%@ - %@", self.objectId, NSStringFromCGPoint(self.gesturePoint)];
}
@end
