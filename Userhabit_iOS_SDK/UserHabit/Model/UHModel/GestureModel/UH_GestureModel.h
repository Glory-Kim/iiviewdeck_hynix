//
//  UH_GestureModel.h
//  UserHabit
//
//  Created by r on 2017. 4. 21..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import "UH_Model.h"

@interface UH_GestureModel : UH_Model

@property (nonatomic, assign) CGPoint gesturePoint;
@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSString *objectDescription;

@property (nonatomic, assign) int scrollMapping;
@property (nonatomic, assign) CGPoint scrollEndedPoint;

@end
