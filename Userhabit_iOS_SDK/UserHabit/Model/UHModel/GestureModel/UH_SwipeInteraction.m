//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_SwipeInteraction.h"

@implementation UH_SwipeInteraction
-(instancetype)initDirection:(UHSwipeDirection)direction withGesture:(UH_UITapGestureRecognizer *)gestureRecognizer{
    self = [super init];
    
    if (self) {
        _direction = direction;
        self.gesturePoint = gestureRecognizer.beganPoint;
        _endPoint = gestureRecognizer.endedPoint;
        if (gestureRecognizer.scrollViewBeforeOffset.y > 0) {
            _scrollBeforePosition = gestureRecognizer.scrollViewBeforeOffset;
            _scrollAfterPosition = gestureRecognizer.scrollViewAfterOffset;
        }
    }
    
    return self;
}
-(instancetype)initDirection:(UHSwipeDirection)direction beginPoint:(CGPoint)beginPoint withEndPoint:(CGPoint)endPoint{
    self = [super init];

    if (self) {
        _direction = direction;
        self.gesturePoint = beginPoint;
        _endPoint = endPoint;
    }

    return self;
}

- (NSDictionary *)build {
    NSMutableDictionary *swipeInteraction = [NSMutableDictionary dictionaryWithCapacity:20];

    // required fields
    swipeInteraction[UHProtocolKeySwipeDirection] = @(_direction);
    

    // optional fields
    if (self.objectId) {
        swipeInteraction[UHProtocolKeyGestureObjectID] = self.objectId;
    }

    if (self.objectDescription) {
        swipeInteraction[UHProtocolKeyGestureDesctiption] = self.objectDescription;
    }

#if USERHABIT_U2
    
    swipeInteraction[UHProtocolKeySwipePointX] = @(self.gesturePoint.x);
    swipeInteraction[UHProtocolKeySwipePointY] = @(self.gesturePoint.y);
    swipeInteraction[UHProtocolKeySwipeEndPointX] = @(_endPoint.x);
    swipeInteraction[UHProtocolKeySwipeEndPointY] = @(_endPoint.y);
    
    if (_scrollBeforePosition.y > 0) {
        swipeInteraction[UHProtocolKeySwipeBeforePositionX] = @(_scrollBeforePosition.x);
        swipeInteraction[UHProtocolKeySwipeBeforePositionY] = @(_scrollBeforePosition.y);
    }
    
    // scroll info fields
    if (self.scrollMapping >= 0) {
        swipeInteraction[UHProtocolKeySwipeEndPointX] = @(self.scrollEndedPoint.x);
        swipeInteraction[UHProtocolKeySwipeEndPointY] = @(self.scrollEndedPoint.y);
        swipeInteraction[UHProtocolKeyGestureScrollMapping] = @(self.scrollMapping);
    }
    
#else
    
    swipeInteraction[UHProtocolKeySwipePoint] = @[@(self.gesturePoint.x), @(self.gesturePoint.y)];
    swipeInteraction[UHProtocolKeySwipeEndPoint] = @[@(_endPoint.x), @(_endPoint.y)];
    
    if (_scrollBeforePosition.y > 0) {
        swipeInteraction[UHProtocolKeySwipeBeforePosition] = @[@(_scrollBeforePosition.x), @(_scrollBeforePosition.y)];
    }
    
    // scroll info fields
       if (self.scrollMapping >= 0) {
           swipeInteraction[UHProtocolKeySwipeEndPoint] = @[@(self.scrollEndedPoint.x), @(self.scrollEndedPoint.y)];
           swipeInteraction[UHProtocolKeyGestureScrollMapping] = @(self.scrollMapping);
       }
#endif
    
    return swipeInteraction;
}

@end
