//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_GestureModel.h"
#import "UserHabitConstant.h"
#import "UH_UITapGestureRecognizer.h"

@interface UH_SwipeInteraction : UH_GestureModel
@property (nonatomic, assign, readonly) UHSwipeDirection direction;
@property (nonatomic, assign) CGPoint endPoint;

@property (nonatomic, assign) CGPoint scrollBeforePosition;     //scroll view contents offset of touch began
@property (nonatomic, assign) CGPoint scrollAfterPosition;      //scroll view contents offset of touch ended

-(instancetype)initDirection:(UHSwipeDirection)direction withGesture:(UH_UITapGestureRecognizer *)gestureRecognizer;

/*
 * 테스트 할때 사용
 */
-(instancetype)initDirection:(UHSwipeDirection)direction beginPoint:(CGPoint)beginPoint withEndPoint:(CGPoint)endPoint;
@end
