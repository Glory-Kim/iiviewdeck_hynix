//
//  UH_ProtocolKey.h
//  UserHabit
//
//  Created by r on 2017. 4. 21..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#ifndef UH_ProtocolKey_h
#define UH_ProtocolKey_h

#define UHProtocolKeyAppInformation               @"a"
#define UHProtocolKeyDeviceInformation            @"d"
#define UHProtocolKeySessionInformation           @"s"

#define UHProtocolKeyScreenInformation            @"s"
#define UHProtocolKeyObjectInformation            @"o"
#define UHProtocolKeyCurrentTimeStamp             @"t"


#define UHProtocolKeyActionFlowType               @"t"
#define UHProtocolKeyActionFlowUpTime             @"u"
#pragma mark - U2 key
#if USERHABIT_U2
    #define UHProtocolKeyAppID                  @"ai"
    #define UHProtocolKeyAppCode                @"ac"
    #define UHProtocolKeyAppName                @"an"
    #define UHProtocolKeyAppSDK                 @"as"
    #define UHProtocolKeyAppSDKVersion          @"av"

    #define UHProtocolKeyDeviceCompany          @"dc"
    #define UHProtocolKeyDeviceBoard            @"db"
    #define UHProtocolKeyDeviceID               @"di"
    #define UHProtocolKeyDeviceModel            @"dm"
    #define UHProtocolKeyDeviceName             @"dn"
    #define UHProtocolKeyDeviceScreen           @"ds"
    #define UHProtocolKeyDeviceWidth            @"dw"
    #define UHProtocolKeyDeviceHeight           @"dh"
    #define UHProtocolKeyDeviceDPI              @"dd"
    #define UHProtocolKeyDeviceOS               @"do"
    
    #define UHProtocolKeySessionID              @"si"
    #define UHProtocolKeySessionStartTime       @"st"
    #define UHProtocolKeySessionVersion         @"sv"
    #define UHProtocolKeySessionLocale          @"sl"
    #define UHProtocolKeySessionTimeZone        @"sz"
    #define UHProtocolKeySessionCount           @"sc"
    #define UHProtocolKeySessionNetworkStatus   @"sn"
    #define UHProtocolKeySessionLatestSuccessTimestamp  @"slt"
    #define UHProtocolKeySessionDeviceTime      @"1"
    #define UHProtocolKeySessionTimeOffset      @"2"
    #define UHProtocolKeySessionUpTimeStart     @"3"

    #define UHProtocolKeyGestureObjectID        @"go"
    #define UHProtocolKeyGesturePointX          @"gx"
    #define UHProtocolKeyGesturePointY          @"gy"
    #define UHProtocolKeyGestureDesctiption     @"gd"
    #define UHProtocolKeyGestureScrollX         @"gsx"
    #define UHProtocolKeyGestureScrollY         @"gsy"
    #define UHProtocolKeyGestureScrollMapping   @"m"

    #define UHProtocolKeySwipeDirection         @"gv"
    #define UHProtocolKeySwipePointX            @"gx"
    #define UHProtocolKeySwipePointY            @"gy"
    #define UHProtocolKeySwipeEndPointX         @"gex"
    #define UHProtocolKeySwipeEndPointY         @"gey"
    #define UHProtocolKeySwipeBeforePositionX   @"gmx"
    #define UHProtocolKeySwipeBeforePositionY   @"gmy"

    #define UHProtocolKeyContentAction          @"dk"
    #define UHProtocolKeyContentName            @"dv"

    #define UHProtocolKeyCrashInfoBattery       @"cb"
    #define UHProtocolKeyCrashInfoCharging      @"cc"
    #define UHProtocolKeyCrashInfoMessage       @"cm"
    #define UHProtocolKeyCrashInfoExceptionType @"ct"
    #define UHProtocolKeyCrashInfoFreeDisk      @"cfd"
    #define UHProtocolKeyCrashInfoFreeMemory    @"cfm"
    #define UHProtocolKeyCrashInfoJailBreaking  @"cj"
    #define UHProtocolKeyCrashInfoNetworkStatus @"cn"
    #define UHProtocolKeyCrashInfoProcesses     @"cp"
    #define UHProtocolKeyCrashInfoStackTrace    @"cs"
    #define UHProtocolKeyCrashInfoTotalDisk     @"ctd"
    #define UHProtocolKeyCrashInfoTotalMemory   @"ctm"

    #define UHProtocolKeyObjectID               @"i"
    #define UHProtocolKeyObjectType             @"t"
    #define UHProtocolKeyObjectFrameX           @"rl"
    #define UHProtocolKeyObjectFrameY           @"rt"
    #define UHProtocolKeyObjectFrameSizeWidth   @"rw"
    #define UHProtocolKeyObjectFrameSizeHeith   @"rh"

#pragma mark - U1 key
#else
    //a
//    #define UHProtocolKeyAppID                  @"i"
    #define UHProtocolKeyAppID                  @"a"
    #define UHProtocolKeyAppCode                @"c"
    #define UHProtocolKeyAppName                @"n"
    #define UHProtocolKeyAppSDK                 @"s"
    #define UHProtocolKeyAppSDKVersion          @"v"
    //d
    #define UHProtocolKeyDeviceCompany          @"c"
    #define UHProtocolKeyDeviceBoard            @"b"
    #define UHProtocolKeyDeviceID               @"i"
    #define UHProtocolKeyDeviceModel            @"m"
    #define UHProtocolKeyDeviceName             @"n"
    #define UHProtocolKeyDeviceScreen           @"s"
    #define UHProtocolKeyDeviceWidth            @"w"
    #define UHProtocolKeyDeviceHeight           @"h"
    #define UHProtocolKeyDeviceDPI              @"d"
    #define UHProtocolKeyDeviceOS               @"o"
    //s
    #define UHProtocolKeySessionID              @"i"
    #define UHProtocolKeySessionStartTime       @"t"
    #define UHProtocolKeySessionVersion         @"v"
    #define UHProtocolKeySessionLocale          @"l"
    #define UHProtocolKeySessionTimeZone        @"z"
    #define UHProtocolKeySessionCount           @"c"
    #define UHProtocolKeySessionNetworkStatus   @"n"
    #define UHProtocolKeySessionLatestSuccessTimestamp  @"lt"
    #define UHProtocolKeySessionDeviceTime      @"d"
    #define UHProtocolKeySessionTimeOffset      @"o"
    #define UHProtocolKeySessionUpTimeStart     @"u"


    //gesture
    #define UHProtocolKeyGestureObjectID        @"o"
    #define UHProtocolKeyGesturePoint           @"p"
    #define UHProtocolKeyGestureDesctiption     @"d"
    #define UHProtocolKeyGestureScrollPoint     @"s"
    #define UHProtocolKeyGestureScrollMapping   @"m"
    //swipe
    #define UHProtocolKeySwipeDirection         @"i"
    #define UHProtocolKeySwipePoint             @"p"
    #define UHProtocolKeySwipeEndPoint          @"t"
    #define UHProtocolKeySwipeBeforePosition    @"b"
    //content
    #define UHProtocolKeyContentAction          @"a"
    #define UHProtocolKeyContentName            @"o"
    //crash
    #define UHProtocolKeyCrashInfoBattery       @"b"
    #define UHProtocolKeyCrashInfoCharging      @"c"
    #define UHProtocolKeyCrashInfoMessage       @"m"
    #define UHProtocolKeyCrashInfoExceptionType @"t"
    #define UHProtocolKeyCrashInfoFreeDisk      @"fd"
    #define UHProtocolKeyCrashInfoFreeMemory    @"fm"
    #define UHProtocolKeyCrashInfoJailBreaking  @"j"
    #define UHProtocolKeyCrashInfoNetworkStatus @"n"
    #define UHProtocolKeyCrashInfoProcesses     @"p"
    #define UHProtocolKeyCrashInfoStackTrace    @"s"
    #define UHProtocolKeyCrashInfoTotalDisk     @"td"
    #define UHProtocolKeyCrashInfoTotalMemory   @"tm"
    //object
    #define UHProtocolKeyObjectID               @"i"
    #define UHProtocolKeyObjectType             @"t"
    #define UHProtocolKeyObjectFrameX           @"l"
    #define UHProtocolKeyObjectFrameY           @"t"
    #define UHProtocolKeyObjectFrameSizeWidth   @"w"
    #define UHProtocolKeyObjectFrameSizeHeith   @"h"

#endif
#endif /* UH_ProtocolKey_h */
