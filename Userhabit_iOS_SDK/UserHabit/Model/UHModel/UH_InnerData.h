//
// Created by Jinuk Baek on 09/03/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import "UH_Model.h"

@interface UH_InnerData : UH_Model
extern NSString *const UHInnerDataTypeLog;
extern NSString *const UHInnerDataTypeTryCatch;
extern NSString *const UHInnerDataTypeTimeError;

@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *value;

- (instancetype)initValue:(NSString *)value typeOf:(NSString *)type;

@end
