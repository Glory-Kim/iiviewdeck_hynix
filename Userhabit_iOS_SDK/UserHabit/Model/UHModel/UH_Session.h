//
// Created by Jinuk Baek on 24/01/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UH_InnerData.h"
#import "UH_ScrollViewInfo.h"

typedef NS_ENUM(int, UHTransportConstType) {
    UHTransportConstTypeNone,
    UHTransportConstTypeCountProhibition,
    UHTransportConstTypeTimeProhibition,
    UHTransportConstTypeVersionProhibition,
};


/*
 * type
 * 0 - 아무일도 안함
 * 1 - value[count] 만큼의 세션 데이터 전송 금지
 * 2 - value[ms] 만큼 데이터 전송 금지
 * 3 - 다음 앱 버젼 업 까지 데이터 전송 금지
 * value
 * type에 따라 [ms/count]의 값으로 사용
 */
struct UHTransportConst {
    UHTransportConstType type;
    long value;
};
typedef struct UHTransportConst UHTransportConst;
CG_INLINE UHTransportConst
UHTransportConstMake(int type, long value)
{
    UHTransportConst transportConst; transportConst.type = type; transportConst.value = value; return transportConst;
}
@class UH_ActionFlow;
@class UH_AppInfo;
@class UH_DeviceInfo;
@class UH_SessionInfo;
@class UH_InnerData;
@class UH_CrashInfo;
@class UH_SwipeInteraction;
@class UH_TapInteraction;
@class UH_ContentInfo;
@class UH_ScreenInfo;
@class UH_ScrollViewOffsetInfo;

@interface UH_Session : NSObject

/*!
 * App Info Data
 */
@property (nonatomic, readonly) UH_AppInfo *appInfo;

/*!
 * Device Info Data
 */
@property (nonatomic, readonly) UH_DeviceInfo *deviceInfo;

/*!
 * Session Info Data
 */
@property (nonatomic, readonly) UH_SessionInfo *sessionInfo;
@property (nonatomic, assign) UHSessionStatusType sessionStatus;
@property UHTransportConst transportConst;
@property (nonatomic, strong) NSString *transportMethod;

@property (nonatomic, readonly) BOOL reloaded;
@property (nonatomic, readonly) int sessionCount;
@property (nonatomic, strong) NSMutableArray <UH_ScrollViewInfo *> *scrollViewArray;

/*
 * 마지막 스크롤 뷰 갱신한 시간을 비교하여 0.5초 이상 차이가 나면 데이터를 갱신한다
 */
@property (weak) UH_ActionFlow *lastScrollViewOffsetActionFlow;

-(NSString *)getSessionDataFilePath;
-(NSString *)getActionFlowFilePath;
-(NSString *)getSessionInfoFilePath;
-(NSString *)getSessionDirPath;

-(BOOL)existActionFlowFile;
-(BOOL)existSessionInfoFile;
-(BOOL)existSessionDataFile;

+(instancetype)loadSession:(int)sessionCount;

-(void)startSession:(int)sessionCount
         withApikey:(NSString *)apiKey
       withDeviceId:(NSString *)deviceId
      networkStatus:(UHNetworkStatus)networkStatus
  latestSuccessTime:(long long)latestSuccessTime;
-(void)stopSessionWithUploadData:(BOOL)isUpload completeHandler:(void(^)(void))completeHandler;

-(void)insertActionFlow:(UH_ActionFlow *)actionFlow;
-(void)insertInnerData:(NSString *)message typeOf:(NSString *)type actionFlow:(void (^)(UH_ActionFlow *innerData))innerDataBlock;

-(void)saveSessionInfo;

-(BOOL)existSessionData;
+(BOOL)existSessionDataAt:(int)sessionNumber;
-(void)deleteSessionData;
+(void)deleteSessionDataAt:(NSInteger)sessionCount;


- (UH_ActionFlow *)genActionFlowAppStart:(long long)upTime;
- (UH_ActionFlow *)genActionFlowAppEnd:(long long)upTime;
- (UH_ActionFlow *)genActionFlowScreenStart:(long long)upTime with:(UH_ScreenInfo *)screenInfo;
- (UH_ActionFlow *)genActionFlowInterBackground:(long long)upTime;
- (UH_ActionFlow *)genActionFlowWillResignActive:(long long)upTime;


- (UH_ActionFlow *)genActionFlowTap:(long long)upTime with:(UH_TapInteraction *)tapInteraction;
- (UH_ActionFlow *)genActionFlowDoubleTap:(long long)upTime with:(UH_TapInteraction *)tapInteraction;
- (UH_ActionFlow *)genActionFlowLongPress:(long long)upTime with:(UH_TapInteraction *)tapInteraction;
- (UH_ActionFlow *)genActionFlowSwipe:(long long)upTime with:(UH_SwipeInteraction *)swipeInteraction;
- (UH_ActionFlow *)genActionFlowKeyboardEnableStatus:(long long)upTime;
- (UH_ActionFlow *)genActionFlowKeyboardDisableStatus:(long long)upTime;

- (UH_ActionFlow *)genActionFlowCrash:(long long)upTime with:(UH_CrashInfo *)crashInfo;
- (UH_ActionFlow *)genActionFlowContentTracking:(long long)upTime with:(UH_ContentInfo *)contentInfo;

-(BOOL)chkTransportStatus:(UHTransportMethod)method;

/*
 * 세션을 오픈하고 데이터의 통신전략을 입력한다
 */
- (void)setSessionTransportData:(NSDictionary *)data;
/*
 * - sessionStartConnectionReal: 에 사용되는 네트워크 통신에 필요한 모델을 만든다
 */
- (NSDictionary *)sessionStartInformationDictionary;
- (NSDictionary *)sessionStartInformationDictionary_new;
- (NSDictionary *)sessionStartInformationDictionary_new2;
- (NSDictionary *)sessionEndInformationDictionary;
- (NSDictionary *)sessionInformationDictionary;
- (NSDictionary *)sessionInformationAddObject:(NSDictionary *)object;

- (NSArray *)insertReadyQueue;
@end
