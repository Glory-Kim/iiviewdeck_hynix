//
//  UH_Model.h
//  UserHabit
//
//  Created by r on 2017. 4. 21..
//  Copyright © 2017년 Andbut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UH_ProtocolKey.h"

@interface UH_Model : NSObject
-(NSDictionary *)build;
@end
