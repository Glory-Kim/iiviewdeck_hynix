//
//  UH_WKWebViewObject.h
//  UserHabit
//
//  Created by lotco on 2018. 5. 25..
//  Copyright © 2018년 Andbut. All rights reserved.
//

#import "UH_WebViewObject.h"
#import <WebKit/WebKit.h>


@interface UH_WKWebViewObject : UH_WebViewObject

@property (weak) WKWebView *webView;

@end
