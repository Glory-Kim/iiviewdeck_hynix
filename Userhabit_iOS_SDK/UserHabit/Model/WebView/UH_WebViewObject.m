//
//  UH_WebViewObject.m
//  UserHabit
//
//  Created by lotco on 2018. 6. 21..
//  Copyright © 2018년 Andbut. All rights reserved.
//

#import "UH_WebViewObject.h"
#import "UH_SessionController.h"

NSString const *UHWebObjectKeyId = @"id";
NSString const *UHWebObjectKeyLeft = @"left";
NSString const *UHWebObjectKeyTop = @"top";
NSString const *UHWebObjectKeyWidth = @"width";
NSString const *UHWebObjectKeyHeight = @"height";
NSString const *UHWebObjectKeyPath = @"path";
NSString const *UHWebObjectKeyFileName = @"fileName";

@implementation UH_WebViewObject

- (void)setWebView:(id)webView withVisibleViewController:(UIViewController *)viewController{
    _view = webView;
    _visibleViewController = viewController;
    
    _name = [NSString stringWithFormat:@"%@-%@",NSStringFromClass([_visibleViewController class]), NSStringFromCGRect([_view convertRect:_view.frame toView:UH_SessionManager.rootWindow])];
}

- (NSString *)description{
    return [NSString stringWithFormat:@"WebObjectName[%@]",_name];
}
@end
