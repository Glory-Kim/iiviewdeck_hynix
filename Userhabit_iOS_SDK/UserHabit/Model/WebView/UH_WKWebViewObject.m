//
//  UH_WKWebViewObject.m
//  UserHabit
//
//  Created by lotco on 2018. 5. 25..
//  Copyright © 2018년 Andbut. All rights reserved.
//

#import "UH_WKWebViewObject.h"
#import "UH_SessionController.h"
#import "UH_FileController.h"
#import "UH_ManualCaptureController.h"

@implementation UH_WKWebViewObject

-(void)setWebView:(WKWebView *)webView withVisibleViewController:(UIViewController *)viewController{
    RLog(@"wkwebview 설정 추가 %@", webView);
    [super setWebView:webView withVisibleViewController:viewController];
    
    webView.configuration.preferences.javaScriptEnabled = YES;
    webView.configuration.preferences.javaScriptCanOpenWindowsAutomatically = YES;
    _webView = webView;
    
    NSString *path = [UH_SessionManager.userhabitPath stringByAppendingString:UHUserhabitJSFilename];
    NSString *content = [NSString stringWithContentsOfFile:path
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
    
    [webView evaluateJavaScript:content completionHandler:^(id _Nullable data, NSError * _Nullable error) {
        RLog(@"자바 스크립 인젝션 완료");
        [webView evaluateJavaScript:[NSString stringWithFormat:@"userhabitSetPlatform(22); userhabitSetWebViewName('%@');", self.name]  completionHandler:^(id _Nullable data, NSError * _Nullable error) {
            RLog(@"웹뷰 셋팅 완료 ");
        }];
    }];
    
    if (UH_SessionManager.isDebug &&
        UH_SessionManager.isDevelopMode) {
        if ([UH_ManualCaptureController sharedInstance]) {
            [[UH_ManualCaptureController sharedInstance] changeScreen];
        }
    }
}

@end
