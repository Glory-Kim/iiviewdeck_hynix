//
//  UH_WebViewObject.h
//  UserHabit
//
//  Created by lotco on 2018. 6. 21..
//  Copyright © 2018년 Andbut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

extern NSString *UHWebObjectKeyId;
extern NSString *UHWebObjectKeyLeft;
extern NSString *UHWebObjectKeyTop;
extern NSString *UHWebObjectKeyWidth;
extern NSString *UHWebObjectKeyHeight;
extern NSString *UHWebObjectKeyPath;
extern NSString *UHWebObjectKeyFileName;

@interface UH_WebViewObject : NSObject

@property NSString *name;
@property (weak) UIView *view;
@property (weak) UIViewController *visibleViewController;
@property __block NSArray *objectArray;
@property int imageCaptureCount;
@property RActionBlock objectCaptureActionBlock;

-(void)setWebView:(id)webView withVisibleViewController:(UIViewController *)viewController;
@end
