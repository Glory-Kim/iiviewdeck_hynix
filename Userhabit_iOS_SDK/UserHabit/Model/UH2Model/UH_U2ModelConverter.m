//
//  UH_U2ModelConverter.m
//  UserHabit
//
//  Created by 김영광 on 2020/08/11.
//  Copyright © 2020 Andbut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UH_U2ModelConverter.h"

@implementation UH_U2ModelConverter : NSObject
-(id)init{
    self = [super init];
    return self;
}

//- (void)convertU2Model:(NSArray<NSDictionary *> *)dic_list withFrag:(NSArray<NSString *>*)frags withCompleteHandler:(RActionBlock)completeHandler{
//    __block NSMutableDictionary *result_dic= [NSMutableDictionary new];
//
//    //비동기
//    [dic_list enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        dic_list[idx];
//
//
//        if (idx> dic_list.count) {
//            if (completeHandler) {
//                completeHandler(nil);
//            }
//        }
//    }];
//
//    for(int i=0; i <dic_list.count; ++i){
//        NSDictionary * dic = [dic_list objectAtIndex:i];
//        for(id keyStr in dic){
//            result_dic[ [NSString stringWithFormat:@"%@%@",frags[i],keyStr]] = [dic valueForKey:keyStr];
//        }
//    }
//}



- (NSDictionary*)convertU2Model:(NSArray<NSDictionary *> *)dic_list
                       withFrag:(NSArray<NSString *>*)frags{
   
    NSMutableDictionary *result_dic= [NSMutableDictionary new];
    
//    비동기
//    [dic_list enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        dic_list[idx];
//    }];
    
    for(int i=0; i <dic_list.count; ++i){
        NSDictionary * dic = [dic_list objectAtIndex:i];
        for(id keyStr in dic){
            result_dic[ [NSString stringWithFormat:@"%@%@",frags[i],keyStr]] = [dic valueForKey:keyStr];
        }
    }
    return [NSDictionary dictionaryWithDictionary:result_dic];
}
@end
