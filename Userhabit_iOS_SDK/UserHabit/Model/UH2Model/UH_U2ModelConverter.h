//
//  UH_U2ModelConverter.h
//  UserHabit
//
//  Created by 김영광 on 2020/08/11.
//  Copyright © 2020 Andbut. All rights reserved.
//

@interface UH_U2ModelConverter:NSObject

@property (nonatomic,assign) NSDictionary *u2Model;
-(id)init;
- (void)convertU2Model:(NSArray<NSDictionary *> *)dic_list withFrag:(NSArray<NSString *>*)frags withCompleteHandler:(RActionBlock)completeHandler;
- (NSDictionary*)convertU2Model:(NSArray<NSDictionary *> *)dic_list withFrag:(NSArray<NSString *>*)frag;
@end
