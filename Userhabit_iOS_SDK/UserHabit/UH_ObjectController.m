//
//  UH_ObjectManager.m
//  UserHabit
//
//  Created by DoHyoungKim on 2015. 12. 3..
//  Copyright © 2015년 Andbut. All rights reserved.
//

#import "UH_ObjectController.h"
#import "UH_Util.h"
#import <objc/runtime.h>
#import "UH_ConnectionController.h"
#import "UH_ViewControllerManager.h"
#import "UH_SessionController.h"
#import "UH_Session.h"
#import "UH_SessionInfo.h"
#import "UH_TapInteraction.h"
#import "UH_AppInfo.h"
#import "UH_DeviceInfo.h"
#import "UH_ScreenInfo.h"
#import "UH_ObjectInfo.h"
#import "UH_ImageController.h"
#import <CommonCrypto/CommonDigest.h>


@implementation UH_ObjectController

- (instancetype)init
{
    self = [super init];
    if (self) {
        objectSet = [NSMutableSet new];
    }
    return self;
}

/*
 * thisController 을 읽어와서 안의 뷰들의 구조를 읽어 UH_VCManager.screenHistoryList[UH_SessionManager.nowShowController]).objectInfos 에 저장한다
 */
-(void)readAllObject:(UIViewController *)thisController completionHandler:(RExceptionBlock)completionHandler{
    RLog(@"%@", thisController);
    @try {
        showController = thisController;
        for (UIView *inView in thisController.view.subviews) {
            [self readObject:inView];
        }
        
        if (UH_VCManager.screenHistoryList[UH_SessionManager.nowShowController]) {
            if((UH_VCManager.screenHistoryList[UH_SessionManager.nowShowController]).objectSet){
                (UH_VCManager.screenHistoryList[UH_SessionManager.nowShowController]).objectSet = [NSMutableSet new];
            }
        }
        
        completionHandler(nil);
    } @catch (NSException *exception) {
        [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                   typeOf:UHInnerDataTypeTryCatch
                                               actionFlow:nil];
        completionHandler(exception);
    }
}

-(void)readObject:(UIView *)readView{
    RLog(@"%@ / %@", readView, readView.accessibilityIdentifier);
    if ([readView isKindOfClass:[UIControl class]] ||
        readView.gestureRecognizers.count > 0) {
        CGRect convertRect = [readView convertRect:readView.bounds toView:[UIApplication sharedApplication].keyWindow];
        
        NSString *objectId;
        
        if (readView.accessibilityIdentifier) {
            objectId = [UH_Util checkScreenName:readView.accessibilityIdentifier];
        } else {
            objectId = [NSString stringWithFormat:@"%@", [self makeObjectId:readView]];
        }
        RLog(@"오브젝트 생성");
        UH_ObjectInfo *objectInfo = [[UH_ObjectInfo alloc] initId:objectId
                                                           typeOf:[UH_Util removeSwiftTargetName:readView]
                                                            frame:convertRect];
        objectInfo.objectView = readView;
        objectInfo.fileName = [UH_Util imageFileName];
        objectInfo.path = UH_SessionManager.userhabitObjectImagePath;
        
        [objectSet addObject:objectInfo];
        
        if(UH_ImageManager.forceCapture){
            RLog(@"오브젝트 촬영 요청");
            
            [self reqObjectScreenshot:objectInfo];
        }
    }
    
    if (showController) {
        if (![readView isKindOfClass:[UIControl class]] && readView.subviews.count != 0) {
            for (NSUInteger i = 0; i < readView.subviews.count; i++) {
                [self readObject:(readView.subviews)[i]];
            }
        }
    }
}

- (UH_TapInteraction *)catchObject:(UH_TapInteraction *)tapInteraction forView:(UIView *)readView{
    RLog(@"%@", readView);
    if (!showController) {
        showController = [UH_Util detectViewControllerClass:readView];
    }
    
    NSString *objectId;
    
    if (readView.accessibilityIdentifier) {
        objectId = [UH_Util checkScreenName:readView.accessibilityIdentifier];
    } else {
        if([NSStringFromClass([readView class]) containsString:@"UITableViewCell"]){
            UIView *findTempView = readView;
            
            //sk증권에서 해당 부분 while로 돌렸는데 window못찾아서 루프 걸렸음
            for (int i = 0; i <= 5; i ++) {
                if([findTempView isKindOfClass:[UITableViewCell class]]){
                    break;
                } else if([findTempView isKindOfClass:[UIWindow class]]){
                    findTempView = nil;
                    break;
                } else {
                    findTempView = readView.superview;
                }
            }
            
            if(findTempView){
                objectId = findTempView.accessibilityIdentifier;
            } else {
                objectId = [NSString stringWithFormat:@"%@", [self makeObjectId:readView]];
            }
            
        } else {
            objectId = [NSString stringWithFormat:@"%@", [self makeObjectId:readView]];
        }
    }
    
    if (![UH_Util checkSecretView:readView]) {
        tapInteraction.objectId = objectId;
        tapInteraction.objectDescription = objectId;
    }
    CGRect convertRect = [readView convertRect:readView.bounds toView:[UIApplication sharedApplication].keyWindow];
    RLog(@"오브젝트 생성????");
    UH_ObjectInfo *objectInfo = [[UH_ObjectInfo alloc] initId:objectId
                                                       typeOf:[UH_Util removeSwiftTargetName:readView]
                                                        frame:convertRect];
    objectInfo.fileName = [UH_Util imageFileName];
    objectInfo.objectView = readView;
    objectInfo.path = UH_SessionManager.userhabitObjectImagePath;
    //오브젝트 스크린샷 촬영
    RLog(@"오브젝트 촬영 요청");
    
    [self reqObjectScreenshot:objectInfo];

    return tapInteraction;
}

-(void)reqObjectScreenshot:(UH_ObjectInfo *)objectInformation{
    
    if (([UH_ImageManager chkScreenShot:objectInformation.objectId with:NO] &&
        UH_SessionManager.currentSession &&
        [UH_SessionManager.currentSession chkTransportStatus:UHTransportMethodScreenshotInfo]) ||
        UH_ImageManager.forceCapture) {
        dispatch_async(dispatch_get_main_queue(), ^{
            @try {
                [UH_ImageManager takeObjectScreenShot:objectInformation completeHandler:^(NSError *error) {
                    //스크린샷 파일을 쓰기 완료 한 후 서버로 전송한다.
                    
                    if ([UH_ViewControllerManager sharedInstance].screenHistoryList[[UH_SessionController sharedInstance].nowShowController] &&
                        [UHNetworkReachabilityManager sharedManager].reachable) {
                        [UH_SessionManager.connectionController objectMessageSend:objectInformation
                                                                       screenName:[UH_SessionController sharedInstance].nowShowController];
                    }
                }];
            } @catch (NSException *exception) {
                [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                           typeOf:UHInnerDataTypeTryCatch
                                                       actionFlow:nil];
            }
        });
    }
}

-(NSString *)makeObjectId:(UIView *)sender{
    
    NSString *returnObjectId;
    NSString *senderString = [NSString new];
    
    if ([sender isKindOfClass:[UIControl class]]) {
        UIControl *btn = (UIControl *)sender;
        
        for (id target in btn.allTargets) {
            NSArray *actions = [btn actionsForTarget:target forControlEvent:UIControlEventTouchUpInside];
            
            for (NSString *action in actions) {
                senderString = [senderString stringByAppendingString:action];
            }
        }

        if (!senderString ||
            senderString.length == 0) {
            senderString = NSStringFromClass([sender class]);
        }
//        RLog(@"%@  / %@", senderString, sender.accessibilityIdentifier);
    }else{
        senderString = NSStringFromClass([sender class]);
    }
    
    //TODO::오브젝트 아이디 이름 규칙
    returnObjectId = [NSString stringWithFormat:@"UserhabitID::%@::%@(%d)",[UH_SessionController sharedInstance].nowShowController, senderString, (int)[sender.superview.subviews indexOfObject:sender]];
    RLog(@"%@ / %@", sender, returnObjectId);
    return returnObjectId;
}

//todo::이미지 파일 이름 변경

- (void)renderScrollViewToImage:(UIScrollView *)_scrollView withName:(NSString *)objectName
{
    RLog(@"%@",objectName);
    dispatch_async(dispatch_get_main_queue(), ^{
        // UIScrollView의 기존 frame을 저장 (이 예제에선 UITableView가 UIScrollView의 역할)
        CGRect originTableViewFrame = _scrollView.frame;
        
        // capture할 영역을 지정. UIScrollView의 컨텐츠 사이즈
        CGSize captureSize = CGSizeMake(_scrollView.contentSize.width, _scrollView.contentSize.height);
        
        // bitmap graphic context 생성
        UIGraphicsBeginImageContextWithOptions(captureSize, YES, 0.0);
        
        // UIScrollView의 frame을 content 영역으로 변경
        _scrollView.frame = CGRectMake(0, 0, captureSize.width, captureSize.height);
        
        // UIScrollView frame영역을 bitmap image context에 그림
        [_scrollView.layer renderInContext:UIGraphicsGetCurrentContext()];
        
        //  bitmap graphic context로부터 이미지 획득
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        
        // UIScrollView의 frame을 원래대로 변경 (변경하지 않으면 스크롤이 안됨)
        _scrollView.frame = originTableViewFrame;
        
        // bitmap image context 종료
        UIGraphicsEndImageContext();


        NSString *userhabitPath = [UH_SessionController sharedInstance].userhabitPath;
        NSData *imageData = [NSData dataWithData:UIImageJPEGRepresentation(image, 0.1f)];
        //todo::이미지 파일 이름 변경
        [imageData writeToFile:[NSString stringWithFormat:@"%@/%@.jpg", userhabitPath, objectName] atomically:YES];
        
    });
}

-(int)chkObjectDepth:(UIView *)sender
{
    RLog(@"%@", sender);
    int cnt = 0;
    for (UIView* next = sender.superview; next; next = next.superview) {
        cnt++;
        UIResponder* nextResponder = next.nextResponder;
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            break;
        }
    }
    
    return cnt;
}

@end
