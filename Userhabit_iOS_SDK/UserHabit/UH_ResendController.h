//
// Created by Jinuk Baek on 05/02/2017.
// Copyright (c) 2017 Andbut. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UH_ResendController : NSObject


+(instancetype)sharedInstance;


- (void)removeOldData:(int)currentSessionNumber;
- (void)startSendingOldSessions:(int)currentSessionNumber;
- (void)clearWork;
- (void)runOldSessionWork;

@end