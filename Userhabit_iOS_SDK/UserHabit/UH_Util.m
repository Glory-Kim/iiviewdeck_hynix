//
//  UH_Util.m
//  UserHabit
//
//  Created by DoHyoungKim on 2015. 12. 3..
//  Copyright © 2015년 Andbut. All rights reserved.
//

#import "UH_Util.h"
#import "UH_SessionController.h"
#import "UH_ViewControllerManager.h"
#import "zlib.h"
#import "UH_Session.h"
#import "UHNetworkReachabilityManager.h"
#import "UH_ActionFlow.h"

static NSString *const UHDebugTypeString[] = {
    @"USERHABIT",
    @"SCREEN_CHANGE",
    @"OBJECT_TOUCH",
    @"CRASH",
    @"WARNING",
    @"NETWORK",
    @"SESSION",
    @"SCREEN_CAPTURE_IMG",
    @"SCREEN_CAPTURE",
};

@implementation UH_Util

+(instancetype)sharedInstance
{
    static UH_Util *singletonObject;

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singletonObject = [[UH_Util alloc]init];
    });
    return singletonObject;
}

+ (void)logDebugType:(UHDebugType)type withMessage:(NSString *)message {
    if (UH_SessionManager.isDebug) {
        NSLog(@"%@ => %@", UHDebugTypeString[type], message);
    }
}

+ (UHNetworkStatus)networkStatus{
    if ([UHNetworkReachabilityManager sharedManager].networkReachabilityStatus == UHNetworkReachabilityStatusReachableViaWiFi) {

        return UHNetworkStatusWifi;
        
    } else if([UHNetworkReachabilityManager sharedManager].networkReachabilityStatus == UHNetworkReachabilityStatusReachableViaWWAN) {
        
        return UHNetworkStatusWwan;
    
    } else {
    
        return UHNetworkStatusNone;
    
    }
}

-(NSString *)detectViewController:(id)sendView {
    for (UIView* next = [sendView superview]; next; next = next.superview) {
        UIResponder* nextResponder = next.nextResponder;
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return [UH_Util removeSwiftTargetName:nextResponder];
        }
    }
    return nil;
}

+(UIViewController *)detectViewControllerClass:(id)sendView {
    if ([sendView isKindOfClass:[UIViewController class]]) {
        return sendView;
    } else {
        for (UIView* next = [sendView superview]; next; next = next.superview) {
            UIResponder* nextResponder = next.nextResponder;
            if ([nextResponder isKindOfClass:[UIViewController class]]) {
                return (UIViewController *)nextResponder;
            }
        }
    }
    return nil;
}

+ (UIPanGestureRecognizer * _Nullable)findPanGestureTo:(UIView *)view{
    @try {
        BOOL checkComplete = YES;
        
        do {
            //superview가 UIWindowd일경우 최상위로 판단 루프를 종료한다.
            if ([view isKindOfClass:[UIWindow class]]) {
                checkComplete = NO;
                return nil;
/*
 * TODO: 사용자가 panGesture를 추가한경우 제스쳐 추적 불가
 * 제스쳐를 추가/제거해가면서 테스트해보기
 */
#if USERHABIT_ALPHA
            } else if ([view.gestureRecognizers firstObject]){
                RLog_ViewCycle;
                for (UIGestureRecognizer * inGesture in view.gestureRecognizers) {
                    RLog_ViewCycle;
                    if ([inGesture isKindOfClass:[UIPanGestureRecognizer class]]) {
                        checkComplete = NO;
                        RLog(@"pan gesture!!!");
                        return (UIPanGestureRecognizer *)inGesture;
                    }else {
                        if (inGesture == [view.gestureRecognizers lastObject]) {
                            view = view.superview;
                        }
                    }
                }
#else
            } else if ([view isKindOfClass:[UIScrollView class]] &&
                      ![NSStringFromClass([view class]) isEqualToString:@"UITableViewWrapperView"]) {
                checkComplete = NO;
                return ((UIScrollView *)view).panGestureRecognizer;
            /*
             * open source 특화 처리된 소스 iCarousel을 사용 할 경우에 대해
             */
            } else if ([NSStringFromClass([view.superview class]) isEqualToString:@"iCarousel"]){
                for (UIGestureRecognizer * inGesture in view.gestureRecognizers) {
                    if ([inGesture isKindOfClass:[UIPanGestureRecognizer class]]) {
                        checkComplete = YES;
                        return (UIPanGestureRecognizer *)inGesture;
                    }
                }
#endif
            } else {
                //확인하려는 뷰의 상위뷰가 스크롤뷰인지 바꾸어서 다시 검사한다
                if (view.superview) {
                    view = view.superview;
                } else {
                    return nil;
                }
            }
        } while (checkComplete);
    } @catch (NSException *exception) {
        [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                   typeOf:UHInnerDataTypeTryCatch
                                               actionFlow:nil];
        return nil;
    }
    return nil;
    
    /*
     * ios 10 
     * UIWindow - UITabBarController - UINavigationController - UIViewController - UITableViewController
     * iOS 10 tableViewCell 구조
     * touch.view                                       - UITableViewCellContentView
     * touch.view.superview                             - UITableViewCell
     * touch.view.superview.superview                   - UITableViewWrapperView
     * touch.view.superview.superview.superview         - UITableView

    if ([view.superview.superview.superview isKindOfClass:[UITableView class]]) {
        returnScrollView = (UIScrollView *)view.superview.superview.superview;
    }
    return returnScrollView;
      */
}

+ (NSString *)makeJsonString:(NSDictionary *)dic
{
    if (dic) {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:nil];
        return [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    } else {
        RLog(@"-------------------error:null dicttionary-------------------");
        return nil;
    }
}

+ (BOOL)isCatchViewController:(NSString *)controllerName{
    @try {
        BOOL isCatch = NO;
        
        if (![controllerName hasPrefix:@"UI"] ||
            [controllerName isEqualToString:@"UIViewController"]) {
            isCatch = YES;
        }
        
        if (!UH_SessionManager.isAutoCatch ||
            [UH_SessionManager.nowShowController isEqualToString:controllerName] ||
            [controllerName isEqualToString:@"SFBrowserRemoteViewController"] ||
            [controllerName hasPrefix:@"UH_"]) {
            
            isCatch = NO;
        }else{
            //excludeArray 에 추가되있는 경우 자동 추적을 하지 않는다
            for (NSString *className in UH_VCManager.excludeArray) {
                if ([controllerName.lowercaseString isEqualToString:className.lowercaseString]) {
                    isCatch = NO;
                    break;
                }
            }
        }
//        RLog(@"controllerName = %@", controllerName);
//        RLog(@"isCatch = %d", isCatch);

        return isCatch;
    } @catch (NSException *exception) {
        [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                   typeOf:UHInnerDataTypeTryCatch
                                               actionFlow:nil];

        return NO;
    }
}


/*
 * 리퍼러 추적 관련 코드
 */
-(NSMutableDictionary*)parseQueryString:(NSString *)query
{
    NSMutableDictionary *itemsDictionary = [NSMutableDictionary dictionary];
    NSArray *items = [query componentsSeparatedByString:@"&"];
    
    for (NSString *item in items) {
        NSArray *pair = [item componentsSeparatedByString:@"="];
        
        if (pair.count == 1) {
            itemsDictionary[pair.firstObject] = @"";
        } else if (pair.count == 2) {
            itemsDictionary[pair.firstObject] = pair.lastObject;
        }
    }
    
    return itemsDictionary;
}

- (void)compressDataFrom:(NSString *)from to:(NSString*)to{
    
    //압축해야 할 파일이 용량이 일정 용량을 넘으면 분기 처리를 해야 하는 코드 추가 필요
    NSData *uncompressedData = [NSFileManager.defaultManager contentsAtPath:from];

    if (uncompressedData.length == 0) {
        RLog(@"actionflow.db 파일 없음");
        return;
    }

    z_stream strm;

    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.total_out = 0;
    strm.next_in=(Bytef *)uncompressedData.bytes;
    strm.avail_in = (unsigned int)uncompressedData.length;

    if (deflateInit2(&strm, Z_DEFAULT_COMPRESSION, Z_DEFLATED, (15+16), 8, Z_DEFAULT_STRATEGY) != Z_OK) {
        return ;
    }

    NSMutableData *compressed = [NSMutableData dataWithLength:16384];

    do{
        if (strm.total_out >= compressed.length) {
            [compressed increaseLengthBy:16384];
        }

        strm.next_out = compressed.mutableBytes + strm.total_out;
        strm.avail_out = (unsigned int)(compressed.length - strm.total_out);
        deflate(&strm, Z_FINISH);
    }while (strm.avail_out == 0);

    deflateEnd(&strm);

    compressed.length = strm.total_out;
    [[NSData dataWithData:compressed] writeToFile:to atomically:YES];

    //    debug::액션 파일 삭제
    //              기본값 --활성
    [NSFileManager.defaultManager removeItemAtPath:from error:nil];
}

+(NSString * _Nullable)checkObjectName:(NSString * _Nonnull)string{
    NSString *returnString = nil;
//    UserhabitID//FirstViewController//sideMenuButtonAction/(11)
    string = [string stringByReplacingOccurrencesOfString:@":" withString:@"\\:"];
    returnString = string;

    return returnString;
}
+(NSString * _Nullable)checkScreenName:(NSString * _Nonnull)string{
    NSString *returnString = nil;
    
    if (string.length >= UHMAXStringLength) {
        [UH_Util logDebugType:UHDebugTypeUserHabit withMessage:[NSString stringWithFormat:@"Warning! Please Check String [%@]", string]];
        returnString = [string substringToIndex:UHMAXStringLength];
    }
    string = [string stringByReplacingOccurrencesOfString:@"UserhabitID::" withString:@"_"];
//    string = [string stringByReplacingOccurrencesOfString:@":" withString:@"_"];
//    string = [string stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    returnString = string;
    return returnString;
}

+ (NSString *)applicationLibraryDirectoryWith:(NSString *)path {
    if (!path) {
        return [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
    } else {
        if (![[path substringFromIndex:[path length]] isEqualToString:@"/"]) {
            [path stringByAppendingPathComponent:@"/"];
        }
    }
    
    NSString *pathString =[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
    pathString = [pathString stringByAppendingPathComponent:path];
    
    return pathString;
}

+ (BOOL)checkSecretView:(UIView *)checkView{
    @try {
        if(UH_VCManager.secretMode){
            return YES;
        }
        
        BOOL checkComplete = YES;
        
        do {
            //superview가 UIWindowd일경우 최상위로 판단 루프를 종료한다.
            if ([checkView isKindOfClass:[UIWindow class]]) {
                checkComplete = NO;
                return NO;
            //상위뷰가 씨크릿뷰로 분류되어있으면 해당 액션은 시크릿으로 처리한다
            }else if ([[UH_VCManager secretViewSet] containsObject:checkView]) {
                checkComplete = NO;
                return YES;
            } else {
                //확인하려는 뷰의 상위뷰가 시크릿뷰인지 바꾸어서 다시 검사한다
                if (checkView.superview) {
                    checkView = checkView.superview;
                } else {
                    return NO;
                }
            }
        } while (checkComplete);
    } @catch (NSException *exception) {
        [UH_SessionManager.currentSession insertInnerData:exception.reason
                                                   typeOf:UHInnerDataTypeTryCatch
                                               actionFlow:nil];
        [UH_Util logDebugType:UHDebugTypeWarning withMessage:exception.reason];
        return NO;
    }
    return NO;
}

+ (NSString *)removeSwiftTargetName:(id _Nonnull)object{
    NSString *className;
    
    if ([object isKindOfClass:[UIView class]] ||
        [object isKindOfClass:[UIViewController class]]) {
        className = NSStringFromClass([object class]);
    } else if([object isKindOfClass:[NSString class]]){
        className = object;
        if (className.length == 0) {
            className = @"No Name Class";
        }
    } else {
        RLog(@"object 확인 %@", object);
        className = object;
    }
    
    NSArray *classNameArray = [className componentsSeparatedByString:@"."];
    if ([classNameArray count] >= 2 &&
        [classNameArray[0] isEqualToString:[[NSBundle mainBundle] infoDictionary][@"CFBundleExecutable"]]) {
        className = classNameArray[1];
    }
//    RLog(@"%@", className);
    return className;
}

+(UH_ScrollViewInfo *)findScrollView:(UIView *)view {
    for (UH_ScrollViewInfo *inScrollViewInfo in UH_SessionManager.currentSession.scrollViewArray) {
        if ([inScrollViewInfo.scrollView isEqual:view] ||
            [inScrollViewInfo.superViewController.view isEqual:view]) {
            return inScrollViewInfo;
            break;
        }
    }
    return nil;
}

+ (BOOL) validationTime:(int)compareTime{
    BOOL compareReturnValue = YES;
    
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    formatter.dateFormat = @"H";
    
    int timeHour = [[formatter stringFromDate:[[NSDate alloc]initWithTimeIntervalSince1970:compareTime]] intValue];
    
    formatter.dateFormat = @"M";
    int timeMonth = [[formatter stringFromDate:[[NSDate alloc]initWithTimeIntervalSince1970:compareTime]] intValue];
    
    formatter.dateFormat = @"D";
    int timeDay = [[formatter stringFromDate:[[NSDate alloc]initWithTimeIntervalSince1970:compareTime]] intValue];
    
    formatter.dateFormat = @"Y";
    int timeYear = [[formatter stringFromDate:[[NSDate alloc]initWithTimeIntervalSince1970:compareTime]] intValue];
    
    
    //todo::시간 관리 세션 보관위해 yes 처리
    if (!(UH_SessionManager.latestAction.uptime + UHLastActionCompareValue < compareTime)){
//        compareReturnValue = YES;
        compareReturnValue = NO;
    } else if (timeYear != 1970){
        compareReturnValue = NO;
    } else if (timeMonth != 1){
        compareReturnValue = NO;
    } else if (timeDay != 1){
        compareReturnValue = NO;
    } else if (timeHour >= 5){
        compareReturnValue = NO;
    }
        
    RLog(@"%@ / %d", [NSDate dateWithTimeIntervalSince1970:compareTime], compareReturnValue);
    return compareReturnValue;
}

#pragma mark - network util
+ (BOOL)responseCheck:(NSDictionary * _Nonnull)object{
    return [object[@"status"] isEqualToString:@"OK"];
}
#pragma mark - image

+(UIImage *)imageWithView:(UIView *)view{
    UIGraphicsBeginImageContext(view.bounds.size);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

+ (UIImage *)addImage:(UIImage *)image1 toImage:(UIImage *)image2 {
    CGSize imageSize = CGSizeMake(image1.size.width, image1.size.height + image2.size.height);
    UIGraphicsBeginImageContext(imageSize);
    [image1 drawInRect:CGRectMake(0, 0, image1.size.width, image1.size.height)];
    [image2 drawInRect:CGRectMake(0, image1.size.height, image2.size.width, image2.size.height)];
    UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resultingImage;
}

+ (NSString *)imageFileName {
    return [NSString stringWithFormat:@"%u", arc4random()];
}

@end
