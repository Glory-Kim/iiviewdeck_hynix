//
//  ThirdViewController.swift
//  UserhabitTestAppSwift
//
//  Created by Jinuk Baek on 10/12/2016.
//  Copyright © 2016 Jinuk Baek. All rights reserved.
//

import UIKit
import UserHabit

class ThirdViewController:UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    @IBOutlet weak var mainTableView: UITableView!
    var arrayList = [String]()
    
    override func viewWillAppear(_ animated: Bool) {
        UserHabit.addScrollView("scrollView", scrollView: mainTableView, rootViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        for i in 1...200 {
            arrayList.append(String.init(format: "%d >>> %d", i, arc4random_uniform(400)))
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickAddBtn(_ sender: Any) {
        
        for i in 1...200 {
            arrayList.append(String.init(format: "%d >>> %d", i, arc4random_uniform(400)))
        }
        
        mainTableView.reloadData()
        
        
        let alert = UIAlertController(title: "UserHabitAlert", message: "This is an alert.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .`default`, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        
        self.present(alert, animated: true, completion: nil)
        UserHabit.setScreen(self, withName: "UserHabitAlert")
    }
    
    @IBAction func clickClearBtn(_ sender: Any) {
        arrayList.removeAll()
        
        mainTableView.reloadData()
    }
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "Cell";
        
        var cell = tableView.dequeueReusableCell(withIdentifier: identifier)
        
        if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: identifier)
        }
        
        cell?.textLabel?.text = arrayList[indexPath.row]
        
        return cell!
    }
}

