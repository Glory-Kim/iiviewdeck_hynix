//
//  AppDelegate.swift
//  UserhabitTestAppSwift
//
//  Created by Jinuk Baek on 8/17/16.
//  Copyright © 2016 Jinuk Baek. All rights reserved.
//

import UIKit
import UserHabit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UserHabit.setDebug(true)
        UserHabit.sessionStart("dev_0d6b399846dd85c49483b7af3f583e76d4fbafcc")
        
        UserHabit.setUserhabitOption([UHOptionScreenshotTapticFeedback : true]);
        
//        UserHabit.enableDeviceRandomID();
//        UserHabit.setSessionDelayTime(2)
        
        
//        UserHabit.sharedInstance().setUrlScheme("uhswift")
//        let excludeArray:[String] = ["CustomNavigationController",
//                                     "TabBarController",
//                                     "UserhabitViewController"]
//        UserHabit.excludeClasses(excludeArray)
        
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
//        if (UserHabit.sharedInstance().chainDeeplink(url)) {
//            return true;
//        }
//        
        return true;
    }

}

