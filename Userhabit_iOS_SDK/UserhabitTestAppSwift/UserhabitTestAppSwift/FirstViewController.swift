//
//  FirstViewController.swift
//  UserhabitTestAppSwift
//
//  Created by Jinuk Baek on 8/17/16.
//  Copyright © 2016 Jinuk Baek. All rights reserved.
//

import UIKit
import UserHabit

class FirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserHabit.sharedInstance().addSecretView(self);
        
        
        let urlString = "string"

        
//        let url = URL.init(string: urlString)
//        let request = URLRequest.init(url: url)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func occurCrash1(_ sender: AnyObject) {
        let a = ["123","234","345","456"]
        
        NSLog("%@", a[10])
    }
    
    
    
    @IBAction func AppSchemeButtonAction(_ sender: Any) {
        
        
        UserHabit.sharedInstance().sessionClose(withUploadData: true) { 
            if let url = URL(string: "uhobjectivec://") {
                UIApplication.shared.openURL(url)
            }
            print("force exit")
            exit(0);
        }
        
        
        UserHabit.sharedInstance().sessionClose(withUploadData: false) {
            if let url = URL(string: "uhobjectivec://") {
                UIApplication.shared.openURL(url)
            }
            exit(0);
        }
        
        
    }

}

