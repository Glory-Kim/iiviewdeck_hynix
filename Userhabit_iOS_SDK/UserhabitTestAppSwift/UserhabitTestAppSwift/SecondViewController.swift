//
//  SecondViewController.swift
//  UserhabitTestAppSwift
//
//  Created by Jinuk Baek on 8/17/16.
//  Copyright © 2016 Jinuk Baek. All rights reserved.
//

import UIKit
import UserHabit
class SecondViewController: UIViewController {

    override func viewWillAppear(_ animated: Bool) {
        UserHabit.takeScreenShot(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
//        UserHabit.setScreen(self, withName:"야호:여름이다/안녕");
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func occurCrash2(_ sender: AnyObject) {
//        let b = 0;
//        let a = 4/b;
//        NSLog("%d helphelp", a);
        
        fatalError();
    }

}

