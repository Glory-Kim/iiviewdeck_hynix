# 프로젝트 설명
- UserHabit : 프레임워크 프로젝트
- UserhabitTestAppObjectiveC : Objective C 용 테스트 앱
- UserhabitTestAppSwift : Swift 용 테스트 앱

# 버젼업 방법

1. `UserHabit.m` 파일의 UHVersionCode 값 1 증가

1. `UserHabit.m` 파일의 UHSdkVersion 값 변경

1. `info.plist` 파일의 CFBundleShortVersionString 버젼 -d업 

1. `UserHabit.h` 파일의 버젼 변경

1. 빌드 후 홈페이지(S3)에 업로드 

1. `Userhabit.podspec` 파일의 버젼 업데이트 

1. `pod trunk push` 실행 

1. 홈페이지에 공지사항 업로드

# 릴리즈 전 처리

- 꼭 테스트 케이스를 돌려서 정상적인지 확인할 것!

# 이용중인 라이브러리

## iOS System Services 

- URL : https://github.com/Shmoopi/iOS-System-Services
- License : custom license(소스공개필요없음)
- Path : UH_SystemServices
- 목적 : 시스템 상태 관련 메소드(예: 디스크 용량, 메모리 사이즈, 루팅 여부 등)

## NSData_Base64

- URL : http://kyle.snowmintcs.com/cocoa_programming.php 
- License : BSD
- Path : NSDataAdditions
- 목적 : Base64 사용

## AFNetworking

- URL : https://github.com/AFNetworking/AFNetworking
- License : MIT
- Path : UH_AFNetworking
- 목적 : Multipart-form request 전송을 위함

# 클래스 설명

- UH_ConnectionController: TP서버와의 통신 관리
- UH_NotiManager: iOS Notification 관리(백그라운드 이동, 키보드 상태 이벤트)
- UH_ObjectController: Object 추적 관련 관리
- UH_SFSafariViewController: 레퍼럴 추적시 필요한 SFSafariViewController를 상속한 클래스
- UH_ResendController: 재전송 처리 컨트롤러
- UH_Session: 하나의 세션을 의미
- UH_SessionController: 세션을 관리하는 클래스
- UH_UncaughtExceptionHandler: 예외나 에러가 발생했을 때 처리
- UH_Util: 네트워크 상태 및 기타 기능들 모음 클래스
- UH_ViewControllerManager: 화면 천이 시나 스크린샷 취득 시 사용하는 클래스
- UIViewController+Logger: ViewController를 Swizzle하여 화면 변경을 자동으로 감지하는 클래스
- UserHabit: 고객에게 노출되는 대표 클래스

# 세션 흐름
`UserHabit/Documents/SessionFlow.md` 참고
