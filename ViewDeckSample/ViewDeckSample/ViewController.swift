//
//  HynixVIewController.swift
//  DemoApp(iOS)
//
//  Created by 김영광 on 2021/04/21.
//  Copyright © 2021 userhabit. All rights reserved.
//

import Foundation

import UIKit
class ViewController : UIViewController,UITabBarControllerDelegate,UINavigationControllerDelegate,IIViewDeckControllerDelegate{
    
    var viewdeckVC = IIViewDeckLogger(center: nil)
    static var tabBarController1 : UITabBarController?
    static var naviController : UINavigationController?
    static var vcArr2 : [UIViewController]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ViewController viewDidLoad  \(self.description)") //<ViewDeckSample.ViewController: 0x7fd9c7d08b20>
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("ViewController viewWillAppear  \(self.description)")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("ViewController viewWillDisappear  \(self.description)")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("ViewController viewDidDisappear  \(self.description)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("ViewController viewDidAppear  \(self.description)")
        ViewController.tabBarController1 = UITabBarController()
        
        let vc1 = FirstViewController().setBtnTitle(title: "First")
                                       .setIndex(num: 1)
                                       .setBackgroundColor(color: .systemPink)
        let vc2 = FirstViewController().setBtnTitle(title: "Second")
                                       .setIndex(num: 2)
                                       .setBackgroundColor(color: .systemBlue)
        let vc3 = FirstViewController().setBtnTitle(title: "Third")
                                       .setIndex(num: 3)
                                       .setBackgroundColor(color: .systemGray)
        let vc4 = FirstViewController().setBtnTitle(title: "Forth")
                                       .setIndex(num: 4)
                                       .setBackgroundColor(color: .systemOrange)
        vc1.tabDelegate = self
        vc2.tabDelegate = self
        vc3.tabDelegate = self
        vc4.tabDelegate = self
//
        let vcArr = [ vc1,
                      vc2,
                      vc3,
                      vc4]

        let icon1 = UITabBarItem(title: "title", image: UIImage(systemName: "star"), tag: 0)
        let icon2 = UITabBarItem(title: "title", image: UIImage(systemName: "star"), tag: 1)
        let icon3 = UITabBarItem(title: "title", image: UIImage(systemName: "star"), tag: 2)
        let icon4 = UITabBarItem(title: "title", image: UIImage(systemName: "star"), tag: 3)
        
        vcArr[0].tabBarItem = icon1
        vcArr[1].tabBarItem = icon2
        vcArr[2].tabBarItem = icon3
        vcArr[3].tabBarItem = icon4
        
        ViewController.naviController = UINavigationController()
//        naviVC.viewControllers
        
        
        
///
        ViewController.tabBarController1?.viewControllers = vcArr
        ViewController.naviController?.delegate = self
//
//        let vc5 = FirstViewController().setBtnTitle(title: "Right").setIndex(num: 5).setBackgroundColor(color: .systemPink)
        
//        vc5.btnDelegate = self
//
//        let tabBarController2 = UITabBarController()
//
        
        let vc6 = FirstViewController().setBtnTitle(title: "= First =").setIndex(num: 1).setBackgroundColor(color: .systemRed)
        let vc7 = FirstViewController().setBtnTitle(title: "= Second =").setIndex(num: 2).setBackgroundColor(color: .systemTeal)
        let vc8 = FirstViewController().setBtnTitle(title: "= Third =").setIndex(num: 3).setBackgroundColor(color: .systemGray2)
        let vc9 = FirstViewController().setBtnTitle(title: "= Forth =").setIndex(num: 4).setBackgroundColor(color: .systemPurple)

        vc6.naviDelegate = self
        vc7.naviDelegate = self
        vc8.naviDelegate = self
        vc9.naviDelegate = self
        
        ViewController.vcArr2 = [vc6,
                      vc7,
                      vc8,
                      vc9]
        
        ViewController.naviController?.delegate = self
        ViewController.tabBarController1?.delegate = self
        
        if ViewController.tabBarController1 != nil {
            ViewController .naviController?.addChild(ViewController.tabBarController1!)
        }

        viewdeckVC?.centerController = ViewController.naviController
//        viewdeckVC?.tabBarController = ViewController.tabBarController1
        viewdeckVC?.delegate = self
        
        if let w = UIApplication.shared.windows.filter{$0.isKeyWindow}.first {
            w.rootViewController = viewdeckVC //버그발생 (네비게이션 pushVC 시 애니메이션 효과 사라짐 )
//            w.rootViewController = ViewController.naviController //정상 작동
        }
        
    }
    
//    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        return someAnimator()
//    }
    
    func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return someAnimator()
    }
    
}

class FirstViewController : UIViewController {
    var tabDelegate : UITabBarControllerDelegate?
    var btnTitle : String?
    var naviDelegate : UINavigationControllerDelegate?
    var index : Int?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setBtnTitle(title : String) -> Self {
        self.btnTitle = title
        return self
    }
    func setIndex(num : Int) -> Self {
        self.index = num
        return self
    }
    func setBackgroundColor(color : UIColor ) ->Self {
        self.view.backgroundColor = color
        return self
    }
    override func viewWillAppear(_ animated: Bool) {
        print("FirstVC \(index)  => viewWillAppear")
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("FirstVC \(index) => viewDidAppear")
        
        print("FirstVC PARENTVC =>  \(String(describing: self.searchParentVC()))")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("FirstVC \(index) => viewWillDisappear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
        print("FirstVC \(index) => viewDidDisappear")
        
    }
    
    override func transition(from fromViewController: UIViewController, to toViewController: UIViewController, duration: TimeInterval, options: UIView.AnimationOptions = [], animations: (() -> Void)?, completion: ((Bool) -> Void)? = nil) {
        print("FirstVC  transition")
    }
    
    override func endAppearanceTransition() {
        super.endAppearanceTransition()
        print("FirstVC  endAppearanceTransition")
    }
    
    override func beginAppearanceTransition(_ isAppearing: Bool, animated: Bool) {
        super.beginAppearanceTransition(isAppearing, animated: animated)
//        super.viewDidAppear(animated)
        print("FirstVC  beginAppearanceTransition")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let w = UIApplication.shared.windows.filter{$0.isKeyWindow}.first {
            let view = UIView(frame: w.frame)
            view.backgroundColor = .white
            self.view = view
        }
        
        
        var button = UIButton(frame: CGRect(x: self.view.center.x,
                                            y: self.view.center.y,
                                            width: 100, height: 100))
        button.setTitle(btnTitle, for: .normal)
        button.titleLabel?.textColor = .black
        
        print(" =%> \(button.titleLabel?.text)")
        button.backgroundColor = .green
        button.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.act(sender:))))
        
        self.view.addSubview(button)
        self.tabDelegate = self.tabBarController?.delegate
//        self.naviDelegate = self
    }
    
    @objc func act(sender:Any){
        guard let naviVC = ViewController.naviController else { return }
        ViewController.vcArr2?.forEach({ it in
            if it is FirstViewController {
                if (it as! FirstViewController).index == self.index {
                    if naviVC.viewControllers.contains(it) {
                        naviVC.popViewController(animated: true)
                    }else {
                        naviVC.pushViewController(it, animated: true)
                    }
                   
                    
                }
            }
            
        })
//        naviVC.pushViewController(, animated: <#T##Bool#>)
        
    }
}



class someAnimator :NSObject,UIViewControllerAnimatedTransitioning {
    
    var presenting : Bool = false
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 1.0
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
//        var duration :Double = 1.0
        
        let containerView = transitionContext.containerView
        let toView = transitionContext.view(forKey: .to)!
        let recipeView = presenting ? toView : transitionContext.view(forKey: .from)!
        
        containerView.addSubview(toView)
        toView.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        UIView.animate(
            withDuration: 1.0,
              animations: {
                toView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
              },
              completion: { _ in
                transitionContext.completeTransition(true)
              }
         )
    }
}

extension UIViewController {
    func searchParentVC()-> UIViewController{
        var searchingVC = self
        var isSearching = true
        repeat {
            if let vc = searchingVC.parent {
                searchingVC = vc
            } else {
                isSearching = false
               return searchingVC
            }
        }while(isSearching)
        return searchingVC
    }
}
