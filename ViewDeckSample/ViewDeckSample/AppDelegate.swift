//
//  AppDelegate.swift
//  ViewDeckSample
//
//  Created by 김영광 on 2021/04/26.
//

import UIKit
//import UserHabit
@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
		//UserHabit.sessionStart("abc")
        //UIViewController.swizzleMethod() /// - 스위프트 버전은 잘 된다고.?
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

extension UIViewController {

    @objc class func swizzleMethod() {
        
       
        guard
            let originViewDidApper = class_getInstanceMethod(self,#selector(viewDidAppear(_:))),
//            let originViewDidApper = class_getInstanceMethod(self,#selector(viewDidAppear(_:))),
            let swizzleViewDidApper = class_getInstanceMethod(self, #selector(swizzle_viewDidAppear(_:)))
        else { return }
        method_exchangeImplementations(originViewDidApper, swizzleViewDidApper)
        
        guard
            let originalViewDidDisappear = class_getInstanceMethod(self,#selector(viewDidDisappear(_:))),
            let swizzledViewDidDisappear = class_getInstanceMethod(self,#selector(swizzle_viewDidDisappear(_:)))
        else { return }
        method_exchangeImplementations(originalViewDidDisappear, swizzledViewDidDisappear)
       
        guard
            let originalwillAnimateRotation = class_getInstanceMethod(self,#selector(willAnimateRotation(to:duration:))),
            let swizzledWillAnimateRotation = class_getInstanceMethod(self,#selector(swizzle_willAnimateRotation(to:duration:)))
        else { return }
        method_exchangeImplementations(originalwillAnimateRotation, swizzledWillAnimateRotation)
       
     }
        
    //MARK: - swizzle method
     @objc func swizzle_viewDidDisappear(_ animated: Bool) {
        print("swizzle_viewDidDisappear")
        ///self.swizzle_viewDidDisappear(animated)  // viewWillAppear -> origin
     }

     @objc func swizzle_viewDidAppear(_ animated: Bool) {
        print("swizzle_viewDidAppear")
       ///self.swizzle_viewDidAppear(animated)
     }

    @objc func swizzle_willAnimateRotation(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval){
        
        
        self.willAnimateRotation(to: toInterfaceOrientation, duration: duration)
    }

        func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
            return self.preferredInterfaceOrientationForPresentation
        }
}
