//
//  UIViewController+Tracking.m
//  ViewDeckSample
//
//  Created by 김영광 on 2021/05/20.
//

#import <Foundation/Foundation.h>
#import "UIViewController+Tracking.h"
#import <UIKit/UIKit.h>
#import <objc/runtime.h>

@implementation UIViewController(Tracking)

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
//
//        [self swizzleOriginalSelector:@selector(performSegueWithIdentifier:sender:)
//                     swizzledSelector:@selector(xxx_performSegueWithIdentifier:sender:)];
        
        [self swizzleOriginalSelector:@selector(viewDidAppear:)
                     swizzledSelector:@selector(xxx_viewWillAppear:)];
    });
}

+ (void)swizzleOriginalSelector:(SEL)originalSelector swizzledSelector:(SEL)swizzledSelector {
    
    Class class = [self class];
    
    Method originalMethod = class_getInstanceMethod(class, originalSelector);
    Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
    
    BOOL didAddMethod =
    class_addMethod(class,
                    originalSelector,
                    method_getImplementation(swizzledMethod),
                    method_getTypeEncoding(swizzledMethod));
    
    if (didAddMethod) {
        class_replaceMethod(class,
                            swizzledSelector,
                            method_getImplementation(originalMethod),
                            method_getTypeEncoding(originalMethod));
    } else {
        method_exchangeImplementations(originalMethod, swizzledMethod);
    }
}

#pragma mark - Method Swizzling

- (void)xxx_performSegueWithIdentifier:(NSString *)identifier
                                sender:(id)sender {
    
    NSLog(@"%@: segue with identifier: %@", NSStringFromClass([self class]), identifier);
    
    [self xxx_performSegueWithIdentifier:identifier sender:sender];
}

- (void)xxx_viewWillAppear:(BOOL)animated {
    
//    NSLog(@"class: %@", NSStringFromClass([self class]));
    NSLog(@"swizzle viewdidAppear");
    [self xxx_viewWillAppear:animated];
}

@end
