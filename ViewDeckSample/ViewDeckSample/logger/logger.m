////
////  logger.m
////  ViewDeckSample
////
////  Created by 김영광 on 2021/05/18.
////
//
//#import <Foundation/Foundation.h>
//#import "logger.h"
//#import <objc/runtime.h>
//#import <UIKit/UIKit.h>
//
////
////  UIViewController+Logger.m
////  UserHabit
////
////  Created by DoHyoungKim on 2015. 12. 11..
////  Copyright © 2015년 Andbut. All rights reserved.
////
//
//
//
//@implementation Logger
////origin 버그발생
//+(void)load {
//    static dispatch_once_t UH_OnceToken;
//    dispatch_once(&UH_OnceToken, ^{
//
//        //swizzled view did appear
//        Method didOriginal, didSwizzled;
//        // willAppear < -> viewDidAppear
//
//        // A -> swizzle 제외. 셋스크린 노가다
//        // B -> swizzle 메소드 시 점 변경
//        // screenshot 수집 안될 가능성 있음 / 스크린샷 수집 공수
////        didOriginal = class_getInstanceMethod(self, @selector(viewWillAppear:));
//        // C -> viewdeck (라이브러리 수정 포인트 찾아서 해결하고 SDK 수정 (난이도 상 )
//
//        // D -> viewdeck 의존성을 건다 ( 문제점 viewdeck만 수동수집 )
//
//		Class super1 = [self superclass];
//		Class class2 = [self class];
//
//		SEL fn1 = @selector(viewDidAppear:);
//		SEL fn2 = @selector(swizzled_viewDidAppear:);
//
//        didOriginal = class_getInstanceMethod(super1, fn1);
//        didSwizzled = class_getInstanceMethod(class2, fn2);
//
//        BOOL didAddMethod = class_addMethod([self class], @selector(viewDidAppear:), method_getImplementation(didSwizzled), method_getTypeEncoding(didSwizzled));
////
//        if (didAddMethod) {
//            class_replaceMethod([self class], @selector(swizzled_viewDidAppear:), method_getImplementation(didOriginal), method_getTypeEncoding(didOriginal));
//        } else {
//            method_exchangeImplementations(didOriginal, didSwizzled);
//        }
//
//		NSLog(@"%@", self);
//		//NSLog(@"%@", fn1);
//		//NSLog(@"%@", fn2);
////        method_exchangeImplementations(didOriginal, didSwizzled); /// -------------- 버그 케이스
//		///NSLog(@"오브젝트-C 버전 스위즐을 실행");
//
//        //swizzled view did disappear
//        Method didDisappearOriginal, didDisappearSwizzled;
//
//        didDisappearOriginal = class_getInstanceMethod(self, @selector(viewDidDisappear:));
//        didDisappearSwizzled = class_getInstanceMethod(self, @selector(swizzled_viewDidDisappear:));
//
//        method_exchangeImplementations(didDisappearOriginal, didDisappearSwizzled);
//
//    });
//}
//
//+(void)swizzle_lifeCycle{
//    static dispatch_once_t UH_OnceToken;
//        dispatch_once(&UH_OnceToken, ^{
//
//            //swizzled view did appear
//            Method didOriginal, didSwizzled;
//            // willAppear < -> viewDidAppear
//
//            // 현재까지 알아낸것들
//            // 1. swift 의 swizzle 과 objc 의 swizzle 은 서로 다르게 동작한다.
//            // 2. swift 의 swizzle 은 viewdeck 과 충돌을 일으키지 않는다 ( viewdidappear 가 제대로 호출됨 )
//            // 3. objc 의 swizzle 은 viewdeck 과 충돌을 일으킨다.( viewdidappear 가 호출안됨 )
//            // 4. +(void)load 함수는 무조건 최초에 호출된다.
//            // 5. viewdeck도 swizzle을 사용한다.
//            // 6. @Link : https://developpaper.com/objective-c-method-swizzling/
//
//            didOriginal = class_getInstanceMethod(self, @selector(viewDidAppear:));
//            didSwizzled = class_getInstanceMethod(self, @selector(swizzled_viewDidAppear:));
//
//            BOOL isMethodExists = !class_addMethod([self class], @selector(viewDidAppear:), method_getImplementation(didSwizzled), method_getTypeEncoding(didSwizzled));
//
//            if (isMethodExists) {
//                method_exchangeImplementations(didOriginal, didSwizzled);
//            } else {
//                class_replaceMethod([self class], @selector(swizzled_viewDidAppear:), method_getImplementation(didOriginal), method_getTypeEncoding(didOriginal));
//            }
//
////            method_exchangeImplementations(didOriginal, didSwizzled);
//
//
//            //swizzled view did disappear
//            Method didDisappearOriginal, didDisappearSwizzled;
//
//            didDisappearOriginal = class_getInstanceMethod(self, @selector(viewDidDisappear:));
//            didDisappearSwizzled = class_getInstanceMethod(self, @selector(swizzled_viewDidDisappear:));
//
//            method_exchangeImplementations(didDisappearOriginal, didDisappearSwizzled);
//
//        });
//}
//
//
//#pragma mark - swizzle method
//-(void)swizzled_viewDidDisappear:(BOOL)animated{
//	NSLog(@"%@", self);
//    NSLog(@"swizzled_viewDidDisappear");
//	//[self viewDidAppear:animated];
//	//[self swizzled_viewDidDisappear:animated];
//}
//
//-(void)swizzled_viewDidAppear:(BOOL)animated {
//
//    NSLog(@"OBJC swizzled_viewDidAppear  ::  %@",self);
////    [super viewDidAppear:animated]; 무한루프
//}
//
//@end
//
//@interface UIViewController()
//
//
//@end
