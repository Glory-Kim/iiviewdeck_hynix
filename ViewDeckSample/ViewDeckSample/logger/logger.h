

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface Logger : UIViewController

	+(void)swizzle_lifeCycle;
	-(void)swizzled_viewDidDisappear:(BOOL)animated;
	-(void)swizzled_viewDidAppear:(BOOL)animated;

@end
