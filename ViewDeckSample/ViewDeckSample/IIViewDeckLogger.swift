//
//  IIViewDeckLogger.swift
//  ViewDeckSample
//
//  Created by 김영광 on 2021/05/17.
//

import Foundation

class IIViewDeckLogger : IIViewDeckController,IIViewDeckControllerDelegate {
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("IIViewDeckLogger  viewDidDisappear ")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("IIViewDeckLogger  viewDidAppear ")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("IIViewDeckLogger  viewWillDisappear ")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("IIViewDeckLogger viewWillAppear ")
    }
    
    override func addChild(_ childController: UIViewController) {
        super.addChild(childController)
        print("IIViewDeckLogger addChild ")
    }
    
}
